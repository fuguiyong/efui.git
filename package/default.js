// components
import EfTable from "./components/table/main.js";
import EfIcon from "./components/icon/main.js";
import EfLoading from "./components/loading/main.js";
import EfCheckbox from "./components/checkbox/main.js";
import EfCheckboxGroup from "./components/checkboxGroup/main.js";
import EfTriangle from "./components/triangle/main.js";
import EfButton from "./components/button/main.js";
import EfModal from "./components/modal/main.js";
import EfMenu from "./components/menu/main.js";
import EfStep from "./components/step/main.js";
import EfInput from "./components/input/main.js";
import EfPopper from "./components/popper/main.js";
import EfPopover from "./components/popover/main.js";
import EfTooltip from "./components/tooltip/main.js";
import EfDropdown from "./components/dropdown/main.js";
import EfSelect from "./components/select/main.js";
import EfDatePicker from "./components/date-picker/main.js";
import EfTimePicker from './components/time-picker/main.js';
import EfRadio from './components/radio/main.js';
import EfCascader from './components/cascader/main.js';
import EfTree from "./components/tree/main.js";
import EfCollapseTransition from "./components/collapse-transition/main.js";
import EfForm from "./components/form";

// plugins
import EfMessage from "./components/message/main.js";
import EfAlert from "./components/alert/main.js";
import EfMark from "./components/mark/main.js";
import Util from "./components/util/index.js";

const componentsArray = [
	// components
	EfTable,
	EfIcon,
	EfLoading,
	EfCheckbox,
	EfCheckboxGroup,
	EfTriangle,
	EfButton,
	EfModal,
	EfMenu,
	EfStep,
	EfInput,
	EfPopper,
	EfPopover,
	EfTooltip,
	EfDropdown,
	EfSelect,
	EfDatePicker,
	EfTimePicker,
	EfRadio,
	EfCascader,
	EfTree,
	EfCollapseTransition,
	EfForm,
	// plugins
	Util,
	EfMessage,
	EfAlert,
	EfMark,
];

export default {
	install(app, option) {
		componentsArray.forEach(component => {
			app.use(component)
		})
	}
}
