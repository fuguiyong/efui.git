import Tree from "./tree.vue";

Tree.install = (app, options) => {
	app.component(Tree.name, Tree)
}

export default Tree;

export const EfTree = Tree;
