export default {
	data: {
		type: Array,
		default: _ => []
	},
	props: {
		type: Object,
		default: _ => ({
			leaf: 'leaf',
			disabled: 'disabled',
			children: 'children',
			value: "value",
			label: 'label',
		})
	},
	lazy: Boolean,
	lazyLoad: _ => {}
}
