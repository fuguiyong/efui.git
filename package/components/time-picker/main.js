import TimePicker from "./time-picker.vue";
import TimeRangePikcer from "./time-range-picker.vue";

TimePicker.install = (app, options) => {
	app.component(TimePicker.name, TimePicker)
}

TimeRangePikcer.install = (app, options) => {
	app.component(TimeRangePikcer.name, TimeRangePikcer)
}

export default {
	install(app, options) {
		app.use(TimePicker);
		app.use(TimeRangePikcer);
	}
};

export const EfTimePicker = TimePicker;
export const EfTimeRangePikcer = TimeRangePikcer;
