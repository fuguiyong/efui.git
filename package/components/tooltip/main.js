import Tooltip from "./tooltip.vue";

Tooltip.install = (app, options) => {
	app.component(Tooltip.name, Tooltip)
}

export default Tooltip;

export const EfTooltip = Tooltip;
