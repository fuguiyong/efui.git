import {
	props as popperProps
} from "../popper/config.js";

export const props = {
	...popperProps,
	content: {
		type: String,
		default: "this is tooltips,this is tooltips,this is tooltips"
	},
	width: {
		type: [Number, String],
		default: undefined
	},
	theme: {
		type: String,
		default: "dark"
	}
}
