import {
	props as popperProps
} from "../popper/config.js";

export const props = {
	...popperProps,
	maxHeight: {
		type: [Number, String],
		default: 350
	},
	itemHeight: {
		type: [Number, String],
		default: 50
	},
	placement: {
		type: String,
		default: 'bottom',
		validator: val => [
			'',
			'top',
			'top-start',
			'top-end',
			'bottom',
			'bottom-start',
			'bottom-end',
			'left',
			'left-start',
			'left-end',
			'right',
			'right-start',
			'right-end'
		].includes(val)
	},
	transitionName: {
		type: String,
		default: "zoom-in",
		validator: val => ['', 'fade-in-linear', 'zoom-in', ].includes(val)
	},
}
