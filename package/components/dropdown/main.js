import Dropdown from "./dropdown.vue";
import DropdownMenu from "./dropdown-menu.vue";
import DropdownItem from "./dropdown-item.vue";

Dropdown.install = (app, options) => {
	app.component(Dropdown.name, Dropdown)
}

DropdownMenu.install = (app, options) => {
	app.component(DropdownMenu.name, DropdownMenu)
}

DropdownItem.install = (app, options) => {
	app.component(DropdownItem.name, DropdownItem)
}

export default {
	install(app, options) {
		app.use(Dropdown);
		app.use(DropdownMenu);
		app.use(DropdownItem);
	}
};

export const EfDropdown = Dropdown;
export const EfDropdownMenu = DropdownMenu;
export const EfDropdownItem = DropdownItem;
