import Select from "./select.vue";
import Option from "./option.vue";
import OptionGroup from "./option-group.vue";

Select.install = (app, options) => {
	app.component(Select.name, Select)
}

Option.install = (app, options) => {
	app.component(Option.name, Option)
}

OptionGroup.install = (app, options) => {
	app.component(OptionGroup.name, OptionGroup)
}



export default {
	install(app, options) {
		app.use(Select);
		app.use(Option);
		app.use(OptionGroup);
	}
};

export const EfSelect = Select;
export const EfOption = Option;
export const EfOptionGroup = OptionGroup;

