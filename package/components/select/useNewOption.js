import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	provide,
	inject
} from 'vue';


export default function({
	props,
	selectedList,
	tagInputValue,
	hasOption,
	toggleSelectedData,
	addOption
}) {
	/**
	 * data
	 */
	const newOptionsMap = ref(new Map()); // 所有选项数据

	/**
	 * computed
	 */
	const newOptionVisible = computed(() => props.multiple && props.allowCreate && tagInputValue.value && (!hasOption(
		tagInputValue.value) || newOptionsMap.value.has(tagInputValue.value)));


	const newOptionItemStyle = computed(() => {
		let res = {};
		res.height = parseInt(props.itemHeight) + 'px';
		return res;
	});
	const newOptionIsSelected = computed(_ => selectedList.value.includes(tagInputValue.value));


	/**
	 * event
	 */
	const onNewOptionClick = e => {
		if (props.multiple) {
			newOptionsMap.value.set(tagInputValue.value, tagInputValue.value);
			addOption({
				value: tagInputValue.value,
				label: tagInputValue.value
			});
			toggleSelectedData(tagInputValue.value);
		}

	}


	return {
		newOptionItemStyle,
		newOptionVisible,
		newOptionIsSelected,
		onNewOptionClick,
		newOptionsMap
	};

}
