import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	provide,
	inject
} from 'vue';

export default function({
	popperVisible,
	props,
	selectedList,
	optionsMap,
	getOptionLabel,
	tagRef,
	efPopper,
	emit,
	clearSelectedData
}) {

	/**
	 * data
	 */
	// === data start ===
	const inputStyle = shallowRef({
		height: parseInt(props.height) + 'px'
	});
	const ifFocus = ref(false);
	const inputRef = ref(null);
	const inputValue = ref("");
	const inputPlaceholder = ref(props.placeholder);
	const filterValue = ref(""); // option 过滤使用
	// === data end ===


	/**
	 * computed
	 */
	const isEmpty = computed(_ => selectedList.value.length < 1);
	const inputSuffixIconRotate = computed(() => {
		return popperVisible.value ? -180 : 0;
	});
	const inputClearable = computed(() => {
		return !isEmpty.value && props.clearable;
	})
	const inputDisabled = computed(() => {
		return props.disabled;
	})



	/**
	 * watch
	 */

	watch(selectedList, (newVal, oldVal) => {
		if (!props.multiple) { // 单选
			if (newVal.length > 0) {
				inputValue.value = getOptionLabel(newVal[0]);
			} else {}
		} else { // 多选
			computedInputStyle();
			if (newVal.length > 0) {
				props.clearable && (inputValue.value = " "); // inputValue.value不为空才可以打开清除icon
			} else {

			}
		}
	}, {
		deep: true
	})

	watch(popperVisible, (newVal) => {
		if (!newVal) { // 关闭时恢复label显示
			if (props.filterable && !isEmpty.value) {
				if (!props.multiple) { // 单选
					inputValue.value = getOptionLabel(selectedList.value[0]);
					inputRef.value.blur(); // 防止已经聚焦，再次点击可以编辑
				} else { // 多选

				}
			}
		}
	})

	// 监听选项变化
	watch(optionsMap, newVal => {
		// options 异步加载时，回显Input Value
		if (isEmpty.value) {
			return;
		};
		if (!props.multiple && getOptionLabel(selectedList.value[0])) { // 单选
			inputValue.value = getOptionLabel(selectedList.value[0]); // 初始化input value
		} else {
			// TODO 多选回显

		}
	}, {
		deep: true
	})

	/**
	 * functions
	 */
	// === functions start ===

	const clearFocus = () => {
		ifFocus.value = false;
	}

	const openInputFocus = () => {
		ifFocus.value = true;
	}

	// 数据变化时计算input 高度
	const computedInputStyle = (isClear = false) => {
		nextTick(_ => {
			let inputTmpStyle = {};
			let tagStyle = window.getComputedStyle(tagRef.value);
			let height = parseInt(tagStyle.height) + 10;
			if (height < parseInt(props.singleRowHeight)) {
				height = parseInt(props.singleRowHeight);
			}
			inputTmpStyle.height = height + 'px';
			inputStyle.value = inputTmpStyle;
			// 重新计算popper弹出层位置，computeContentStyle函数已经用nexttick包装了
			efPopper.value.computeContentStyle();
		});
	}
	// === functions end ===

	/**
	 * events
	 */
	// == evnet  start ===
	const onInputChange = (val) => {}
	const onInputInput = (val) => {
		filterValue.value = val;
	}
	const onInputFocus = (e) => {
		if (!props.filterable) {
			e.currentTarget.blur();
		} else {
			if (!isEmpty.value) {
				if (!props.multiple) { // 单选
					inputPlaceholder.value = inputValue.value;
					inputValue.value = "";
					filterValue.value = ""; // option 过滤使用，清空filterValue展示所有option
				} else { // 多选

				}
			}
		}
	}
	const onInputBlur = (e) => {}
	const onClear = () => {
		clearSelectedData();
		if (!props.multiple) {} else { // 多选需要重新计算位置
			computedInputStyle(true);
		}
	}

	const onInputClick = e => {}

	// == evnet  end ===


	/**
	 * lifecycle
	 */
	// === lifecycle start ===
	onMounted(_ => {
		if (!props.multiple) { // 单选
			inputValue.value = getOptionLabel(selectedList.value[0]); // 初始化input value
		} else { // 多选
			// 清空单选数据
			inputPlaceholder.value = "";
			inputValue.value = "";
			props.clearable && !isEmpty.value && (inputValue.value = " "); // inputValue.value不为空才可以打开清除icon
			!isEmpty.value && computedInputStyle();
			if (isEmpty.value) {
				inputValue.value = "";
				inputPlaceholder.value = props.placeholder;
			}
		}
		document.body.addEventListener('click', clearFocus);

	})

	onUnmounted(() => {
		document.body.removeEventListener('click', clearFocus);
	})
	// === lifecycle end ===


	return {
		inputValue,
		inputSuffixIconRotate,
		inputClearable,
		inputDisabled,
		inputPlaceholder,
		onInputChange,
		onInputInput,
		onInputFocus,
		onInputBlur,
		inputRef,
		filterValue,
		ifFocus,
		inputStyle,
		openInputFocus,
		onClear,
		onInputClick
	}

}
