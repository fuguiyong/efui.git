import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	provide,
	inject
} from 'vue';

export default function({
	props,
	inputRef,
	selectedList
}) {

	const tagWrapperStyle = shallowRef({});

	const isEmpty = computed(_ => selectedList.value.length < 1);

	const computeTagWrapperStyle = () => {
		let tagStyle = {};
		let computedInputStyle = window.getComputedStyle(inputRef.value.$el.querySelector('.ef-input-inner'));
		let inputWidth = parseInt(computedInputStyle.width);
		let paddingRight = parseInt(computedInputStyle.paddingRight);
		let paddingLeft = parseInt(computedInputStyle.paddingLeft);
		// 特殊处理选择一个下拉项时，无法选中清除图标
		if (selectedList.value.length == 1) paddingRight += 20;
		tagStyle.maxWidth = (inputWidth - paddingRight - paddingLeft) + 'px';
		tagStyle.paddingLeft = paddingLeft + 'px';
		tagWrapperStyle.value = tagStyle;
	}

	watch(selectedList, (newVal, oldVal) => {
		if (!props.multiple) { // 单选
		} else { // 多选
			if (!isEmpty.value) {
				computeTagWrapperStyle();
			}
		}
	}, {
		deep: true
	})

	onMounted(_ => {
		computeTagWrapperStyle();
	})

	return {
		tagWrapperStyle,
	}

}
