import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	provide,
	inject
} from 'vue';

export default function({
	props,
	onInputInput,
	popperVisible,
	selectedList,
	filterValue
}) {

	/**
	 * data
	 */
	const tagInputValue = ref("");
	const tagInputPlaceholder = ref(props.placeholder);
	const tagInputRef = ref(null);

	/**
	 * computed
	 */
	const isEmpty = computed(_ => selectedList.value.length < 1);

	/**
	 * watch
	 */
	watch(selectedList, (newVal, oldVal) => {
		if (!props.multiple) { // 单选

		} else { // 多选
			if (props.filterable) {
				tagInputPlaceholder.value = "";
				//filterValue.value = ""; // 清空过滤data，展示全部数据
				focusTagInput();
				if (newVal.length < 1) {
					tagInputPlaceholder.value = props.placeholder;
				}
			}
		}
	}, {
		deep: true
	})

	watch(popperVisible, (newVal) => {
		if (!newVal) { // 关闭时恢复label显示
			if (props.filterable && props.multiple) {
				if (!isEmpty.value) {
					clearData();
				} else {
					resetData();
				}
			}
		} else {
			filterValue.value = ""; // 清空过滤data，展示全部数据
		}
	})

	/**
	 * functions
	 */
	const clearData = _ => {
		tagInputValue.value = "";
		tagInputPlaceholder.value = "";
	}

	const resetData = _ => {
		tagInputValue.value = "";
		tagInputPlaceholder.value = props.placeholder;
	}

	const focusTagInput = _ => {
		tagInputRef.value.focus();
	}

	/**
	 *  evnet
	 */
	const onTagInputInput = e => {
		let val = e.target.value;
		tagInputValue.value = val;
		onInputInput(val);
	}

	const onTagInputClick = e => {

		// console.log({
		// 	e
		// })

		// 选择弹窗打开，点击input取消冒泡事件，防止弹窗关闭
		if (popperVisible.value) {
			e.stopPropagation();
		}
	}

	/**
	 * lifecycle
	 */
	onMounted(_ => {
		if (!isEmpty.value) {
			clearData();
		}
	})

	return {
		onTagInputInput,
		onTagInputClick,
		tagInputValue,
		tagInputPlaceholder,
		tagInputRef
	}

}
