import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	provide,
	inject
} from 'vue';

import {
	getChildToParentTopDistance
} from "#/util/util.js";

import { useForm } from '#/hooks/use-form';

export default function({
	props,
	emit
}) {

	/**
	 * data
	 */
	const optionsMap = ref(new Map()); // 所有选项数据
	const efPopper = ref(null);
	const popperVisible = ref(false);
	const tagRef = ref(null);
	const contentRef = ref(null);
	const disabledKeys = ref([]);
	const { formContext, formItemContext } = useForm();

	// props传入了modelValue则进行双向绑定，否则ref
	let selectedList = undefined; // 已经选择的数据
	if (props.modelValue === undefined) {
		selectedList = ref([]);
	} else {
		selectedList = computed({
			get() {
				return props.multiple ? props.modelValue : (props.modelValue ? [props.modelValue] : []);
			},
			set(newVal, oldVal) {
				emit('update:modelValue', props.multiple ? newVal : newVal[0]);
				formItemContext?.validate('change');
			}
		});
	}


	/**
	 * computed
	 */

	const triggerStyle = computed(() => {
		let style = {};
		props.width && (style.width = parseInt(props.width) + 'px');
		return style;
	});
	const contentStyle = computed(() => {
		let style = {};
		props.maxHeight && (style.maxHeight = parseInt(props.maxHeight) + 'px');
		props.width && (style.width = parseInt(props.width) + 'px');
		return style;
	});

	/**
	 * watch
	 */
	watch(selectedList, (newVal) => {
		emit('change', props.multiple ? newVal : newVal[0]);
	}, {
		deep: true
	})

	watch(popperVisible, (newVal, oldVal) => {
		if (newVal) {
			// 设置选中选项位置
			setSelectedOptionPosition();
		}
	})


	/**
	 * functions
	 */

	// 设置选中选项位置
	const setSelectedOptionPosition = () => {
		if (selectedList.value.length < 1) return;
		nextTick(_ => {
			let optionEl = contentRef.value.querySelector(`li.selected`);
			let contentScrollTop = getChildToParentTopDistance(contentRef.value, optionEl) - contentRef
				.value.offsetHeight;

			contentRef.value.scrollTop = contentScrollTop > 0 ? contentScrollTop : 0;
		})
	}

	const addDisabledKey = (value) => {
		disabledKeys.value.push(value);
	}

	const isDisabledKey = value => disabledKeys.value.includes(value);

	const clearSelectedData = _ => {
		selectedList.value = [];
		emit('clear');
	}

	const setSelectedData = value => {
		selectedList.value = [value];
	}

	const addSelectedData = value => {
		if (!props.multiple) {
			clearSelectedData();
		}
		if (!selectedList.value.includes(value)) {
			selectedList.value.push(value);
		}
	}

	const removeSelectedData = value => {
		let index = selectedList.value.indexOf(value);
		if (index > -1) {
			selectedList.value.splice(index, 1);
		}
	}

	const toggleSelectedData = value => {
		let index = selectedList.value.indexOf(value);
		if (index > -1) {
			selectedList.value.splice(index, 1);
		} else {
			selectedList.value.push(value);
		}
	}

	const addOption = ({
		value,
		label
	}) => {
		optionsMap.value.set(value, label);
	}

	const getOptionLabel = value => {
		return optionsMap.value.get(value);
	}

	const removeOption = value => {
		optionsMap.value.has(value) && (optionsMap.value.delete(value));
	}

	const clearOptions = _ => {
		optionsMap.value.clear();
	}

	const hasOption = val => optionsMap.value.has(val);

	return {
		optionsMap,
		selectedList,
		addOption,
		removeOption,
		clearOptions,
		getOptionLabel,
		clearSelectedData,
		setSelectedData,
		toggleSelectedData,
		removeSelectedData,
		hasOption,
		efPopper,
		popperVisible,
		tagRef,
		triggerStyle,
		contentStyle,
		addDisabledKey,
		isDisabledKey,
		contentRef
	}
}
