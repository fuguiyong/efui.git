import {
	props as popperProps
} from "../popper/config.js";

export const props = {
	...popperProps,
	trigger: {
		type: String,
		default: 'click',
		validator: val => ['', 'click', 'hover', 'manual'].includes(val)
	},
	placement: {
		type: String,
		default: 'bottom',
		validator: val => [
			'',
			'top',
			'top-start',
			'top-end',
			'bottom',
			'bottom-start',
			'bottom-end',
			'left',
			'left-start',
			'left-end',
			'right',
			'right-start',
			'right-end'
		].includes(val)
	},
	transitionName: {
		type: String,
		default: "zoom-in",
		validator: val => ['', 'fade-in-linear', 'zoom-in', ].includes(val)
	},
	width: {
		type: [Number, String],
		default: 240
	},
	maxHeight: {
		type: [Number, String],
		default: 300
	},
	itemHeight: {
		type: [Number, String],
		default: 34
	},
	modelValue: {
		type: [String, Number, Boolean, Object],
		default: undefined
	},
	multiple: Boolean,
	disabled: Boolean,
	filterable: Boolean,
	clearable: Boolean,
	filterMethod: {
		type: Function,
		default: (inputVal, label) => {
			return label.indexOf(inputVal) > -1;
		}
	},
	// 主要用于多选清除是计算input高度
	singleRowHeight: {
		type: [Number, String],
		default: 45
	},
	allowCreate: Boolean, // 多选时，允许创建新标签
	placeholder: {
		type: String,
		default: "请选择"
	},
	height: {
		type: [Number, String],
		default: undefined
	},
	size: {
		type: String,
		default: ''
	}
}
