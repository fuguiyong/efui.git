import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	provide,
	inject
} from 'vue';

import {
	getChildToParentTopDistance
} from "../../util/util.js";

/**
 * 监听上下键选择option
 */

export default function({
	props,
	tagInputValue,
	newOptionsMap,
	optionsMap,
	setSelectedData,
	toggleSelectedData,
	popperVisible,
	isDisabledKey,
	selectedList,
	filterValue,
	newOptionVisible,
	addOption,
	contentRef
}) {

	/**
	 * data
	 */
	const optionsKeys = ref([]);
	const hoverIndex = ref(-1);

	/**
	 * computed
	 */
	const hoverKey = computed(_ => optionsKeys.value[hoverIndex.value]);

	/**
	 * watch
	 */
	watch(filterValue, (newVal, oldVal) => {
		genOptionsKeys();
		hoverIndex.value = 0;
	})

	watch(optionsMap, (newVal, oldVal) => {
		genOptionsKeys();
	}, {
		deep: true
	})

	watch(popperVisible, (newVal, oldVal) => {
		if (!newVal) {
			hoverIndex.value = -1;
		} else {
			// 初始化选中下标
			setHoverIndex();
		}
	})

	/**
	 * event
	 */
	const onKeydown = (e) => {

		const keyCode = e.keyCode;
		if (keyCode == 38) { // 上
			hoverIndex.value = hoverIndex.value - 1 < 0 ? optionsKeys.value.length - 1 : hoverIndex.value - 1;
			setContentScrollTop();
		} else if (keyCode == 40) { // 下
			hoverIndex.value = hoverIndex.value + 1 > optionsKeys.value.length - 1 ? 0 : hoverIndex.value + 1;
			setContentScrollTop();
		} else if (keyCode == 13) { // enter
			if (isDisabledKey(hoverKey.value)) {
				return;
			}
			if (!props.multiple && (hoverIndex.value == -1 || hoverKey.value == selectedList.value[0])) {
				return;
			}
			if (!props.multiple) {
				// 单选
				setSelectedData(hoverKey.value);
				popperVisible.value = false; // 立即关闭弹窗
			} else {
				// 多选
				if (newOptionVisible.value) { // 创建新选项
					newOptionsMap.value.set(tagInputValue.value, tagInputValue.value);
					addOption({
						value: tagInputValue.value,
						label: tagInputValue.value
					});
				}
				toggleSelectedData(hoverKey.value);
			}

		}
	}

	/**
	 * functions
	 */

	// 设置scrollTop
	const setContentScrollTop = _ => {
		nextTick(_ => {
			let contentScrollTopOld = contentRef.value.scrollTop;
			let optionEl = contentRef.value.querySelector(`li.hover`);
			let optionToParentTopDistance = getChildToParentTopDistance(contentRef.value, optionEl);
			let contentScrollTop = optionToParentTopDistance - contentRef.value.offsetHeight;
			contentRef.value.scrollTop = contentScrollTop > 0 ? contentScrollTop : 0;
			// if (contentScrollTopOld > optionToParentTopDistance || contentScrollTop <
			// 	optionToParentTopDistance) {
			// 	contentRef.value.scrollTop = contentScrollTop > 0 ? contentScrollTop : 0;
			// }

			// if (contentScrollTopOld < contentScrollTop || contentScrollTop <= 0) {
			// 	contentRef.value.scrollTop = contentScrollTop > 0 ? contentScrollTop : 0;
			// }
		})
	}

	// 初始化选中下标
	const setHoverIndex = _ => {
		if (selectedList.value.length < 1) return;
		let index = optionsKeys.value.indexOf(selectedList.value[0]);
		if (index > -1) {
			hoverIndex.value = index;
		}
	}

	const genOptionsKeys = _ => {
		optionsKeys.value = [...optionsMap.value.keys()].filter(key => {
			if (![...newOptionsMap.value.keys()].includes(key)) {
				if (props.filterable) {
					return props.filterMethod(filterValue.value, optionsMap.value.get(key));
				} else {
					return true;
				}
			} else {
				return false;
			}

		});

		// 排除新创建的选项
		if (props.multiple && props.filterable && props.allowCreate && tagInputValue.value && newOptionVisible
			.value) { // 多选 && 可以创建新选项
			optionsKeys.value.unshift(tagInputValue.value);
		}

		// console.log({
		// 	optionsKeys: optionsKeys.value
		// })

	}

	/**
	 * lifecycle
	 */
	onMounted(_ => {
		genOptionsKeys();
		// window.addEventListener('keydown', onKeydown);
	})

	onUnmounted(_ => {
		// window.removeEventListener('keydown', onKeydown);
	})

	/**
	 * execute
	 */

	return {
		onKeydown,
		hoverKey
	}
}
