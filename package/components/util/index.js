import * as Util from "./default.js";


const Util1 = {
	...Util,
	install(app, opts) {
		app.config.globalProperties.$efUtil = Util;
	}
};

// Util.install = (app, opts) => {
// 	app.config.globalProperties.$efUtil = Util;
// }

export const EfUtil = Util1;

export default Util1;
