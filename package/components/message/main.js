// import Vue from 'vue';
import {
	createVNode,
	render,
	isVNode
} from 'vue';

import MessageTempalte from './message.vue';

let instances = [];
let seed = 1;
let zIndex = 1001;
const interval = 16;

const Message = function(options = {}) {

	if (typeof window === 'undefined') { // 服务端渲染
		return;
	}

	// 1-配置props , onClose , onDestroy
	// 2-创建vnode
	// 3-render
	// 4-挂载到body

	if (typeof options === 'string') {
		options = {
			message: options
		};
	}

	// message id
	let id = 'ef_message_' + seed++;

	// verticalOffset
	let verticalOffset = options.offset || interval;
	instances.forEach(item => {
		verticalOffset += item.el.offsetHeight + interval;
	});

	const props = {
		...options,
		id,
		verticalOffset,
		zIndex: ++zIndex,
		onClose: () => {
			Message.close(id, options.onClose);
		},
		onDestroy: () => {
			render(null, container); // 销毁组件
		}
	};


	const container = document.createElement("div");

	const isFunction = (val) => typeof val === 'function';

	// const vm = createVNode(MessageTempalte, props, isVNode(options.message) ? {
	// 	default: () => options.message
	// } : null);

	const messageContent = props.message;
	const vm = createVNode(
		MessageTempalte,
		props,
		isFunction(messageContent) ? {
			default: messageContent
		} :
		isVNode(messageContent) ? {
			default: () => messageContent
		} :
		null
	);

	render(vm, container);

	// console.log("vm", vm);

	document.body.appendChild(vm.el);

	// vm.component.data.visible = true;

	instances.push(vm);

	return vm;
};

['success', 'warn', 'error'].forEach(type => {
	Message[type] = options => {
		if (typeof options === 'string') {
			options = {
				message: options
			};
		}
		options.type = type;
		return Message(options);
	};
});

Message.close = function(id, userOnClose) {

	let len = instances.length;
	let index = -1;
	let removedHeight;

	// find && delete
	for (let i = 0; i < len; i++) {
		if (id === instances[i].props.id) {
			removedHeight = instances[i].el.offsetHeight;
			index = i;
			if (typeof userOnClose === 'function') {
				userOnClose(instances[i]);
			}
			instances.splice(i, 1);
			break;
		}
	}
	if (len <= 1 || index === -1 || index > instances.length - 1) return;


	// move other message
	for (let i = index; i < len - 1; i++) {
		const pos =
			parseInt(instances[i].el.style['top'], 10) - removedHeight - interval;

		instances[i].component.props.verticalOffset = pos;

	}
};

Message.closeAll = function() {
	for (let i = instances.length - 1; i >= 0; i--) {
		instances[i].component.ctx.close();
	}
};


Message.install = (app, opts) => {
	app.config.globalProperties.$message = Message;
}

export const EfMessage = Message;

export default Message;
