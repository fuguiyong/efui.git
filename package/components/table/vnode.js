import {
	h
} from 'vue';

export default {
	props: {
		vnodes: {
			type: Object,
			default: null
		}
	},
	setup(props, {
		attrs,
		slots,
		emit
	}) {
		// console.log("props", props.vnodes);
		return () => h(props.vnodes[0], {}, null);
	}
}
