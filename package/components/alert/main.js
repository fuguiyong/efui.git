// import Vue from 'vue';

import {
	createVNode,
	render,
	isVNode
} from 'vue';


import alertVue from './alert.vue';
import {
	EfMark
} from "../mark/main.js";

const Alert = function(ops = {}) {

	/**
	 * 1. 创建vnode
	 * 2. 创建临时dom，render向里面渲染
	 * 3. 挂载临时dom
	 * 4. 销毁时务必清除组件
	 */

	// 用户自定义销毁回调函数
	const userDestroy = ops.onDestroy || function() {};

	const props = {
		...ops,
		onDestroy: () => {
			userDestroy();
			render(null, container); // 销毁组件
		}
	};

	const container = document.createElement("div");

	const vm = createVNode(alertVue, props, null);

	// console.log("vm", vm);

	render(vm, container);

	if (ops.showMark != false) {
		vm.component.data.markInstance = EfMark();
	}

	vm.component.data.visible = true;

	document.body.appendChild(vm.el);

	return vm.component;


	// let instance = new AlertVueConstructor({
	// 	data: option
	// });
	// if (option.showMark) {
	// 	instance.markInstance = EfMark();
	// }
	// instance.$mount();
	// document.body.appendChild(instance.$el);
	// instance.visible = true;
	// return instance;

}


Alert.install = (app, opts) => {
	app.config.globalProperties.$alert = Alert;
}

export const EfAlert = Alert;

export default Alert;
