import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	toRef
} from 'vue';


export default function({
	props,
	emit
}) {

	// props传入了visible则进行双向绑定，否则ref
	let contentVisible = undefined;
	if (props.visible === undefined) {
		contentVisible = ref(false);
	} else {
		contentVisible = computed({
			get() {
				return props.visible;
			},
			set(newVal, oldVal) {
				emit('update:visible', newVal);
			}
		});
	}

	watch(contentVisible, (newVal, oldVal) => {
		emit("visibleChange", newVal);
	});

	let contentHideTimer = null; // 延时关闭
	const delayHideTimes = toRef(props, 'delayHideTimes'); // 延时关闭times

	const openDelayhideContent = () => {
		contentHideTimer = setTimeout(() => {
			contentVisible.value = false;
		}, delayHideTimes.value);
	};

	const clearContentHideTimer = () => {
		contentHideTimer && clearTimeout(contentHideTimer);
	};

	const immediateHideContent = () => {
		contentVisible.value = false;
	};

	const showContent = () => {
		clearContentHideTimer();
		contentVisible.value = true;
	};

	const toggleContent = () => {
		contentVisible.value = !contentVisible.value;
	}

	const bodyClickEvent = (e) => {
		// console.log('body click');
		if (!['manual', 'hover'].includes(props.trigger)) {
			immediateHideContent();
		}
	}

	onMounted(() => {
		document.body.addEventListener('click', bodyClickEvent);
	})

	onUnmounted(() => {
		clearContentHideTimer();
		document.body.removeEventListener('click', bodyClickEvent);
	})

	return {
		contentVisible,
		openDelayhideContent,
		clearContentHideTimer,
		immediateHideContent,
		showContent,
		toggleContent
	}
}
