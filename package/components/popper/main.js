import Popper from "./popper.vue";

Popper.install = (app, options) => {
	app.component(Popper.name, Popper)
}

export default Popper;

export const EfPopper = Popper;
