export const props = {
	trigger: {
		type: String,
		default: 'hover',
		validator: val => ['', 'click', 'hover', 'manual'].includes(val)
	},
	placement: {
		type: String,
		default: 'top',
		validator: val => [
			'',
			'top',
			'top-start',
			'top-end',
			'bottom',
			'bottom-start',
			'bottom-end',
			'left',
			'left-start',
			'left-end',
			'right',
			'right-start',
			'right-end'
		].includes(val)
	},
	delayHideTimes: {
		type: Number,
		default: 200
	},
	transitionName: {
		type: String,
		default: "fade-in-linear",
		validator: val => ['', 'fade-in-linear', 'zoom-in', ].includes(val)
	},
	theme: {
		type: String,
		default: "light",
		validator: val => ['', 'dark', 'light', ].includes(val)
	},
	visible: {
		type: Boolean,
		default: undefined
	},
	showArrow: {
		type: Boolean,
		default: true
	},
	closeAllOnClick: {
		type: Boolean,
		default: true
	},
	interval: {
		type: Number,
		default: 15
	},
	disabled:Boolean
}
