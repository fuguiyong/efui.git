import {
	onMounted,
	onUnmounted,
	ref,
	reactive,
	computed,
	nextTick,
	shallowRef,
	watchEffect,
	watch,
	toRef
} from 'vue';
import {
	getZIndex
} from '../../util/zIndex.js';
import {
	getDocumentScrollTop,
	getDocumentScrollLeft,
	isElementInViewport,
	getViewportRect
} from '../../util/util.js';

// 计算内容区域位置
export default function({
	props,
	contentVisible,
	emit
}) {
	const interval = toRef(props, 'interval'); // trigger content interval
	const triggerRef = ref(null);
	const contentRef = ref(null);
	const arrowRef = ref(null);
	const contentStyle = shallowRef({});
	const allowStyle = shallowRef({});
	const zIndex = getZIndex();
	// const placement = toRef(props, 'placement');
	const placement = ref(props.placement);
	let placementBackup = props.placement;
	let nextPlacementIndex = {
		value: 0
	}; // 当前placement内容不在可视区域时，下一个切换的方向,Object 为了引用

	/**
	 * functions
	 */
	const resetPlacement = _ => {
		placementBackup = props.placement;
		placement.value = props.placement;
		nextPlacementIndex = {
			value: 0
		};
	}
	const onResize = _ => {
		computeContentStyle();
	}
	const onScroll = _ => {
		computeContentStyle();
	}
	const computeContentStyle = () => {
		if (!contentVisible.value) {
			return;
		}
		computeContentPosition({
			triggerRef,
			contentRef,
			arrowRef,
			allowStyle,
			contentStyle,
			placement,
			interval,
			zIndex,
			placementBackup,
			nextPlacementIndex,
			resetPlacement
		});
	};

	// onMounted(()=>{})
	computeContentStyle(); // 初始化位置
	watch(contentVisible, (newVal, oldVal) => {
		newVal && computeContentStyle();
	});

	watch(_ => props.placement, (newVal, oldVal) => {
		// 重置获取方向
		resetPlacement();
	});

	watch(placement, (newVal, oldVal) => {
		computeContentStyle();
	});

	onMounted(() => {
		window.addEventListener('resize', onResize);
		window.addEventListener('scroll', onScroll);
	});

	onUnmounted(() => {
		window.removeEventListener('resize', onResize);
		window.removeEventListener('scroll', onScroll);
	});

	return {
		interval,
		triggerRef,
		contentRef,
		arrowRef,
		contentStyle,
		allowStyle,
		computeContentStyle,
		placement
	}
}

function computeContentPosition({
	triggerRef,
	contentRef,
	arrowRef,
	allowStyle,
	contentStyle,
	placement,
	interval,
	zIndex,
	placementBackup,
	nextPlacementIndex,
	resetPlacement
}) {
	nextTick(() => {
		let x, y, allowX, allowY;
		let contentRes = {};
		let allowRes = {};
		let inViewport = true; // 是否在视图中

		// console.log({
		// 	triggerRefValue: triggerRef.value
		// })

		const contentRect = contentRef.value.getBoundingClientRect();
		const triggerRect = triggerRef.value.$el.getBoundingClientRect(); // trigger位置
		const triggerComputedStyle = window.getComputedStyle(triggerRef.value.$el); // trigger style
		const contentComputedStyle = window.getComputedStyle(contentRef.value); // content style
		const arrowComputedStyle = window.getComputedStyle(arrowRef.value); // arrow style

		const viewportRect = getViewportRect(); // 可视区域rect


		// console.log({
		// 	triggerRect,
		// 	contentRect,
		// 	triggerHeight: triggerComputedStyle.height,
		// 	contentHeight: contentComputedStyle.height,
		// 	triggerWidth: triggerComputedStyle.width,
		// 	contentWidth: contentComputedStyle.width
		// });
		// 计算是否在可视区域
		const getInViewport = (placement) => {
			let tempPlacement = placement.split('-')[0];
			let inViewport = true;
			if (tempPlacement == 'top') {
				inViewport = (parseInt(contentComputedStyle.height) + interval.value) <= triggerRect.top;
			} else if (tempPlacement == 'bottom') {
				inViewport = (parseInt(contentComputedStyle.height) + interval.value) <= (viewportRect
					.height -
					triggerRect.bottom);
			} else if (tempPlacement == 'left') {
				inViewport = (parseInt(contentComputedStyle.width) + interval.value) <= triggerRect.left;
			} else if (tempPlacement == 'right') {
				inViewport = (parseInt(contentComputedStyle.width) + interval.value) <= (viewportRect
					.width -
					triggerRect.right);
			}
			return inViewport;
		}

		// 如果props.placement在可视区优先显示
		if (getInViewport(placementBackup) && placementBackup != placement.value) {
			resetPlacement();
			return;
		}

		// 当前方向是否在可视区域
		inViewport = getInViewport(placement.value);

		// console.log({
		// 	placement: placement.value,
		// 	tempPlacement,
		// 	inViewport
		// })

		// 计算位置
		if (placement.value == 'top') {
			// content position
			x =
				triggerRect.left +
				getDocumentScrollLeft() +
				parseInt(triggerComputedStyle.width) / 2 -
				parseInt(contentComputedStyle.width) / 2;
			y =
				triggerRect.top +
				getDocumentScrollTop() -
				interval.value -
				parseInt(contentComputedStyle.height);
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowX = parseInt(contentComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
			allowRes = {
				bottom: 0,
				left: 0,
				transform: `translate(${allowX}px, 50%) rotate(45deg)`
			};
		} else if (placement.value == "top-start") {
			// content position
			x =
				triggerRect.left +
				getDocumentScrollLeft();
			y =
				triggerRect.top +
				getDocumentScrollTop() -
				interval.value -
				parseInt(contentComputedStyle.height);
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
			allowRes = {
				bottom: 0,
				left: 0,
				transform: `translate(${allowX}px, 50%) rotate(45deg)`
			};
		} else if (placement.value == "top-end") {
			// content position
			x =
				triggerRect.right +
				getDocumentScrollLeft() -
				parseInt(contentComputedStyle.width);
			y =
				triggerRect.top +
				getDocumentScrollTop() -
				interval.value -
				parseInt(contentComputedStyle.height);
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
			allowRes = {
				bottom: 0,
				right: 0,
				transform: `translate(-${allowX}px, 50%) rotate(45deg)`
			};
		} else if (placement.value == 'bottom') {
			// content position
			x =
				triggerRect.left +
				getDocumentScrollLeft() +
				parseInt(triggerComputedStyle.width) / 2 -
				parseInt(contentComputedStyle.width) / 2;
			y =
				triggerRect.top +
				getDocumentScrollTop() +
				parseInt(triggerComputedStyle.height) +
				interval.value;
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowX = parseInt(contentComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
			allowRes = {
				top: 0,
				left: 0,
				transform: `translate(${allowX}px, -50%) rotate(45deg)`
			};
		} else if (placement.value == 'bottom-start') {
			// content position
			x =
				triggerRect.left +
				getDocumentScrollLeft()
			y =
				triggerRect.top +
				getDocumentScrollTop() +
				parseInt(triggerComputedStyle.height) +
				interval.value;
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
			allowRes = {
				top: 0,
				left: 0,
				transform: `translate(${allowX}px, -50%) rotate(45deg)`
			};

		} else if (placement.value == 'bottom-end') {

			// content position
			x =
				triggerRect.right +
				getDocumentScrollLeft() -
				parseInt(contentComputedStyle.width);
			y =
				triggerRect.top +
				getDocumentScrollTop() +
				parseInt(triggerComputedStyle.height) +
				interval.value;
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
			allowRes = {
				top: 0,
				right: 0,
				transform: `translate(-${allowX}px, -50%) rotate(45deg)`
			};

		} else if (placement.value == 'left') {

			// content position
			x =
				triggerRect.left + getDocumentScrollLeft() - interval.value - parseInt(contentComputedStyle
					.width);

			// 内容到达可视区域底部时
			if (triggerRect.top <= viewportRect.height && (triggerRect.top + parseInt(
						contentComputedStyle.height) / 2 + parseInt(triggerComputedStyle.height) / 2 >=
					viewportRect.height)) {
				y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
				allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) +
					parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			} else if ((triggerRect.top + parseInt(triggerComputedStyle.height)) >= 0 && (triggerRect.top +
					parseInt(
						triggerComputedStyle.height) / 2 <= parseInt(
						contentComputedStyle
						.height) / 2)) {
				// 内容到达可视区域顶部时
				y = getDocumentScrollTop();
				allowY = triggerRect.top + parseInt(triggerComputedStyle.height) / 2 - parseInt(
					arrowComputedStyle.height) / 2;
			} else { // 内容在可视区域内
				y =
					triggerRect.top +
					getDocumentScrollTop() +
					parseInt(triggerComputedStyle.height) / 2 -
					parseInt(contentComputedStyle.height) / 2;
				allowY = parseInt(contentComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			}

			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowRes = {
				top: 0,
				right: 0,
				transform: `translate(50%, ${allowY}px) rotate(45deg)`
			};

		} else if (placement.value == 'left-start') {

			// content position
			x =
				triggerRect.left + getDocumentScrollLeft() - interval.value - parseInt(contentComputedStyle
					.width);

			// 内容到达可视区域底部时
			if (triggerRect.top <= viewportRect.height && (triggerRect.top + parseInt(
					contentComputedStyle.height) >= viewportRect.height)) {
				y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
				allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) +
					parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			} else { // 内容在可视区域内
				y =
					triggerRect.top +
					getDocumentScrollTop();
				allowY = parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) /
					2;
			}
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowRes = {
				top: 0,
				right: 0,
				transform: `translate(50%, ${allowY}px) rotate(45deg)`
			};

		} else if (placement.value == 'left-end') {

			// content position
			x =
				triggerRect.left + getDocumentScrollLeft() - interval.value - parseInt(contentComputedStyle
					.width);

			// 内容到达可视区域顶部时
			if ((triggerRect.top + parseInt(triggerComputedStyle.height)) >= 0 && (triggerRect.top +
					parseInt(
						triggerComputedStyle.height) <= parseInt(
						contentComputedStyle
						.height))) {
				y = getDocumentScrollTop();
				allowY = 0 - (parseInt(contentComputedStyle.height) - triggerRect.top - parseInt(
					triggerComputedStyle
					.height) / 2 - parseInt(arrowComputedStyle.height) / 2);
			} else { // 内容在可视区域内
				y =
					triggerRect.top +
					getDocumentScrollTop() + parseInt(triggerComputedStyle.height) - parseInt(
						contentComputedStyle
						.height);
				allowY = 0 - parseInt(triggerComputedStyle.height) / 2 + parseInt(arrowComputedStyle
						.height) /
					2;
			}

			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowRes = {
				bottom: 0,
				right: 0,
				transform: `translate(50%, ${allowY}px) rotate(45deg)`
			};

		} else if (placement.value == 'right') {

			// content position
			x =
				triggerRect.right + getDocumentScrollLeft() + interval.value;


			// 内容到达可视区域底部时
			if (triggerRect.top <= viewportRect.height && (triggerRect.top + parseInt(
						contentComputedStyle.height) / 2 + parseInt(triggerComputedStyle.height) / 2 >=
					viewportRect.height)) {
				y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
				allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) +
					parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			} else if ((triggerRect.top + parseInt(triggerComputedStyle.height)) >= 0 && (triggerRect.top +
					parseInt(
						triggerComputedStyle.height) / 2 <= parseInt(
						contentComputedStyle
						.height) / 2)) {
				// 内容到达可视区域顶部时
				y = getDocumentScrollTop();
				allowY = triggerRect.top + parseInt(triggerComputedStyle.height) / 2 - parseInt(
					arrowComputedStyle.height) / 2;
			} else { // 内容在可视区域内
				y =
					triggerRect.top +
					getDocumentScrollTop() +
					parseInt(triggerComputedStyle.height) / 2 -
					parseInt(contentComputedStyle.height) / 2;
				allowY = parseInt(contentComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			}

			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowRes = {
				top: 0,
				left: 0,
				transform: `translate(-50%, ${allowY}px) rotate(45deg)`
			};

		} else if (placement.value == 'right-start') {

			// content position
			x =
				triggerRect.right + getDocumentScrollLeft() + interval.value;

			// 内容到达可视区域底部时
			if (triggerRect.top <= viewportRect.height && (triggerRect.top + parseInt(
					contentComputedStyle.height) >= viewportRect.height)) {
				y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
				allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) +
					parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			} else { // 内容在可视区域内
				y = triggerRect.top + getDocumentScrollTop();
				allowY = parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			}

			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowRes = {
				top: 0,
				left: 0,
				transform: `translate(-50%, ${allowY}px) rotate(45deg)`
			};

		} else if (placement.value == 'right-end') {

			// content position
			x =
				triggerRect.right + getDocumentScrollLeft() + interval.value;

			// 内容到达可视区域顶部时
			if ((triggerRect.top + parseInt(triggerComputedStyle.height)) >= 0 && (triggerRect.top +
					parseInt(
						triggerComputedStyle.height) <= parseInt(
						contentComputedStyle
						.height))) {
				y = getDocumentScrollTop();
				allowY = (parseInt(contentComputedStyle.height) - triggerRect.top - parseInt(
					triggerComputedStyle
					.height) / 2 - parseInt(arrowComputedStyle.height) / 2);
			} else { // 内容在可视区域内
				y =
					triggerRect.top +
					getDocumentScrollTop() +
					parseInt(triggerComputedStyle.height) -
					parseInt(contentComputedStyle.height);
				allowY = parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
			}
			contentRes = {
				left: `${x}px`,
				top: `${y}px`,
				zIndex: zIndex
			};
			// allow position
			allowRes = {
				bottom: 0,
				left: 0,
				transform: `translate(-50%, -${allowY}px) rotate(45deg)`
			};
		}


		// 判断是否在可视区域，不在则渲染在下一个方向
		if (isElementInViewport(triggerRef.value.$el)) {
			// 响应式数据赋值

			if (inViewport || nextPlacementIndex.value > 3) {
				// nextPlacementIndex.value >3 说明四个方向都不在视图区域，直接保留用户设计方向
				allowStyle.value = allowRes;
				contentStyle.value = contentRes;
			} else {
				placement.value = getNextPlacemnet(placementBackup, nextPlacementIndex);
			}
		} else {
			allowStyle.value = allowRes;
			contentStyle.value = contentRes;
		}


	});
};


function getNextPlacemnet(placementBackup, nextPlacementIndex) {
	let placementMap = {
		top: ['bottom', 'right', 'left'],
		bottom: ['top', 'right', 'left'],
		left: ['right', 'bottom', 'top'],
		right: ['left', 'bottom', 'top'],
	};
	let tempArr = placementMap[placementBackup.split('-')[0]];
	if (tempArr && nextPlacementIndex.value < tempArr.length) {
		return tempArr[nextPlacementIndex.value++];
	}
	return placementBackup;
}
