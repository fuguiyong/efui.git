import {
	ref,
	computed,
	reactive,
	watch
} from 'vue';

export default function({
	props
}) {

	/**
	 * data
	 */
	// 视图面板列表
	const panelList = ref([]);
	// 展示的列表
	const viewItemList = ref([]);
	// 选中的values
	const selectedValueList = ref([]);
	// 选中的label
	const selectedLabelList = ref([]);

	/**
	 * watch
	 */
	watch(_ => props.options, newVal => {
		if (!props.props.lazy) {
			panelList.value = [newVal];
		}
	}, {
		immediate: true
	});

	const clear = _ => {
		viewItemList.value = [];
		selectedLabelList.value = [];
		selectedValueList.value = [];
	}

	watch(_ => props.modelValue, newVal => {
		selectedValueList.value = newVal;
		// 清空事件
		if (!newVal || newVal.length < 1) {
			clear();
		}
	}, {
		immediate: true
	});

	watch(_ => props.props.lazy, newVal => {
		if (newVal) {
			let node = {
				level: 0
			};
			const resolve = nodes => {
				nodes.length > 0 && (panelList.value = [nodes]);
			}
			props.props.lazyLoad(node, resolve);
		}
	}, {
		immediate: true
	})

	/**
	 * functions
	 */



	const addNextPanel = (currentPanelIndex, item) => {
		let tmpArr = panelList.value.slice(0, currentPanelIndex + 1);
		// 保存选择的Item
		viewItemList.value[currentPanelIndex] = item;
		// 重新渲染级联面板
		if (!props.props.lazy && Array.isArray(item.children) && item.children.length > 0) {
			panelList.value = [...tmpArr, item.children];
		} else if (props.props.lazy && !item[props.props.leaf]) {

			item.lazyLoading = true;
			let node = {
				level: currentPanelIndex + 1,
				...item
			};
			const resolve = nodes => {
				nodes.length > 0 && (panelList.value = [...tmpArr, nodes]);
				item.lazyLoading = false;
			}
			props.props.lazyLoad(node, resolve);

			// new Promise((res, rej) => {
			// 	item.lazyLoading = true;
			// 	let node = {
			// 		level: currentPanelIndex + 1,
			// 		...item
			// 	};
			// 	props.props.lazyLoad(node, res);
			// }).then(nodes => {
			// 	panelList.value = [...tmpArr, nodes];
			// 	item.lazyLoading = false;
			// })

		} else {
			// 没有children，选择完毕 || lazy 叶子节点
			panelList.value = panelList.value.slice(0, currentPanelIndex + 1);
			viewItemList.value = viewItemList.value.slice(0, currentPanelIndex + 1);
			selectedValueList.value = viewItemList.value.map(item => item[props.props.value]);
			selectedLabelList.value = viewItemList.value.map(item => item[props.props.label]);
		}

	}

	return {
		addNextPanel,
		panelList,
		viewItemList,
		selectedLabelList,
		selectedValueList
	}
}
