export default {
	modelValue: {
		type: Array,
		default: _ => []
	},
	options: {
		type: Array,
		default: _ => []
	},
	props: {
		type: Object,
		default: _ => ({
			expandTrigger: 'click', // click hover
			leaf: 'leaf',
			disabled: 'disabled',
			children: 'children',
			value: "value",
			label: 'label',
			lazy: false,
			lazyLoad: _ => {}
		})
	},
	disabled:Boolean
}
