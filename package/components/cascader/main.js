import CascaderPanel from "./cascaderPanel.vue";
import CascaderIndex from "./cascaderIndex.vue";

CascaderPanel.install = (app, options) => {
	app.component(CascaderPanel.name, CascaderPanel)
}

CascaderIndex.install = (app, options) => {
	app.component(CascaderIndex.name, CascaderIndex)
}

export default {
	install(app) {
		app.use(CascaderPanel);
		app.use(CascaderIndex);
	}
};

export const EfCascaderPanel = CascaderPanel;
export const EfCascaderIndex = CascaderIndex;
