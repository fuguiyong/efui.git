import Popover from "./popover.vue";

Popover.install = (app, options) => {
	app.component(Popover.name, Popover)
}

export default Popover;

export const EfPopover = Popover;
