import {
	props as popperProps
} from "../popper/config.js";

export const props = {
	...popperProps,
	title: {
		type: String,
		default: "Title",
	},
	content: {
		type: String,
		default: "this is content,this is content,this is content"
	},
	width: {
		type: [Number, String],
		default: undefined
	}
}
