import Radio from "./radio.vue";
import RadioGroup from "./radio-group.vue";

Radio.install = (app, options) => {
	app.component(Radio.name, Radio)
}

RadioGroup.install = (app, options) => {
	app.component(RadioGroup.name, RadioGroup)
}

export default {
	install(app) {
		app.use(Radio);
		app.use(RadioGroup);
	}
};

export const EfRadio = Radio;
export const EfRadioGroup = RadioGroup;
