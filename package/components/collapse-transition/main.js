import CollapseTransition from './collapse-transition.vue'

CollapseTransition.install = (app, options) => {
	app.component(CollapseTransition.name, CollapseTransition)
}

export default CollapseTransition;

export const EfCollapseTransition = CollapseTransition;
