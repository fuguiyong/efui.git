// import Vue from 'vue';
import {
	createVNode,
	render,
	isVNode
} from 'vue';

import markVue from './mark.vue'; // vnode


// const MarkVueConstructor = Vue.extend(markVue);
// const MarkVueConstructor = Vue.createApp(markVue);

const Mark = function(ops = {}) {

	/**
	 * 1. 创建vnode
	 * 2. 创建临时dom，render向里面渲染
	 * 3. 挂载临时dom
	 * 4. 销毁时务必清楚临时dom
	 */

	// 用户自定义销毁回调函数
	const userDestroy = ops.onDestroy || function() {};

	// 用户配置
	const options = {
		data: {
			// ...ops,
			visible: false
		}
	};

	// 临时容器
	const container = document.createElement("div");
	// container.className = "mark-container";

	// 创建vnode
	const vm = createVNode(
		markVue,
		null,
		null
	);

	// 渲染vnode
	render(vm, container);

	vm.component.data.onDestroy = () => {
		userDestroy();
		render(null, container); // 销毁组件
	}

	vm.component.data.visible = true;

	document.body.appendChild(vm.el);

	// console.log("vm", vm);


	return vm.component;

}

Mark.install = (app, opts) => {
	app.config.globalProperties.$mark = Mark;
}

export const EfMark = Mark;

export default Mark;
