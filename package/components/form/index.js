import Form from "./src/form.vue";
import FormItem from "./src/form-item.vue";

Form.install = (app, options) => {
	app.component(Form.name, Form)
}

FormItem.install = (app, options) => {
	app.component(FormItem.name, FormItem)
}

export default {
	install(app, options) {
		app.use(Form);
		app.use(FormItem);
	}
};

export const EfForm = Form;
export const EfFormItem = FormItem;
