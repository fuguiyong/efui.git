export const formItemProps = {
	label: String,
	itemWidth: {
		type: [String, Number],
		default: '',
	},
	labelWidth: {
		type: [String, Number],
		default: '',
	},
	prop: [String, Array],
	required: {
		type: Boolean,
		default: undefined
	},
	error: String,
	for: String,
	inlineMessage: {
		type: [String, Boolean],
		default: '',
	},
	showMessage: {
		type: Boolean,
		default: true,
	},
	size: {
		type: String,
		values: ['', 'small', 'middle', 'large'],
	},
	rules: Object,
}
