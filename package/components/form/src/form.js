export const formProps = {
	model: Object,
	rules: Object,
	itemWidth: {
		type: [String, Number],
		default: '',
	},
	labelPosition: String,
	labelWidth: {
		type: [String, Number],
		default: '',
	},
	labelSuffix: {
		type: String,
		default: '',
	},
	inline: Boolean,
	inlineMessage: Boolean,
	statusIcon: Boolean,
	showMessage: {
		type: Boolean,
		default: true,
	},
	size: {
		type: String,
		values: ['', 'small', 'middle', 'large'],
	},
	disabled: Boolean,
	validateOnRuleChange: {
		type: Boolean,
		default: true,
	},
	hideRequiredAsterisk: {
		type: Boolean,
		default: false,
	},
	scrollToError: {
		type: Boolean,
		default: false,
	},
}
