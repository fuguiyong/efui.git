import dayjs from 'dayjs';

// 获取年
export const getYearByYear = year => {
	const columns = 4;
	let yearData = [
		[]
	];

	// YYY0 2000 2010 2020 
	let startYear = Math.floor(year / 10) * 10;
	const startYearDayjs = dayjs(startYear.toString(), 'YYYY');

	for (let i = 0; i < 10; i++) {
		if (yearData[yearData.length - 1].length >= columns) {
			yearData.push([]);
		}
		let lastYearArr = yearData[yearData.length - 1];
		let currentDayjs = startYearDayjs.add(i, 'year');
		let yearObj = {
			year: currentDayjs.format('YYYY'),
			value: currentDayjs.format('YY')
		};
		lastYearArr.push(yearObj);
	}

	return yearData;
}

// 获取月份 year => YYYY
export const getMonthByYear = year => {
	const columns = 4;
	const rows = 3;

	let monthData = [
		[]
	];

	const startMonth = year + '01';
	const startMonthDayjs = dayjs(startMonth, 'YYYYMM');

	for (let i = 0; i < columns * rows; i++) {
		if (monthData[monthData.length - 1].length >= columns) {
			monthData.push([]);
		}
		let lastMonthArr = monthData[monthData.length - 1];
		let currentDayjs = startMonthDayjs.add(i, 'month');
		let monthObj = {
			month: currentDayjs.format('YYYYMM'),
			value: currentDayjs.format('M')
		};
		lastMonthArr.push(monthObj);
	}

	return monthData;
}

// 获取日历数据 ym => YYYYMM
export const getCalendarByMonth = ym => {
	// 7 x 6 矩阵

	const commonFormat = 'YYYYMMDD';
	const columns = 7;
	const rows = 6;

	let calendarData = [
		[]
	];

	// 本月天数
	const daysInMonth = dayjs(ym, 'YYYYMM').daysInMonth();
	// 本月开始日期
	const startDateOfMonth = ym + '01';
	// 本月结束日期
	const endDateOfMonth = '' + ym + daysInMonth;
	const startDateOfMonthDayjs = dayjs(startDateOfMonth, commonFormat);
	const endDateOfMonthDayjs = dayjs(endDateOfMonth, commonFormat);
	// 本月开始是星期几
	const startWeekdayOfMonth = startDateOfMonthDayjs.day();
	// 本月结束是星期几
	const endWeekdayOfMonth = endDateOfMonthDayjs.day();
	const startDateDayjs = startDateOfMonthDayjs.subtract(
		startWeekdayOfMonth == 7 ? 0 : startWeekdayOfMonth,
		'day'
	);

	for (let i = 0; i < columns * rows; i++) {
		if (calendarData[calendarData.length - 1].length >= columns) {
			calendarData.push([]);
		}
		let lastWeekArr = calendarData[calendarData.length - 1];
		let currentDayjs = startDateDayjs.add(i, 'day');
		let dateObj = {
			date: currentDayjs.format(commonFormat),
			value: currentDayjs.format('D'),
			isCurrentMonth: currentDayjs.isSameOrAfter(startDateOfMonthDayjs) &&
				currentDayjs.isSameOrBefore(endDateOfMonthDayjs)
		};
		lastWeekArr.push(dateObj);
	}
	
	// console.log({
	// 	calendarData,
	// 	ym
	// })

	return calendarData;
};


// // 获取日历数据 ym => YYYYMM 7 x n
export const getCalendarDataV2 = ym => {
	const commonFormat = 'YYYYMMDD';
	let calendarData = [
		[]
	];

	// 本月天数
	const daysInMonth = dayjs(ym, 'YYYYMM').daysInMonth();
	// 本月开始日期
	const startDateOfMonth = ym + '01';
	// 本月结束日期
	const endDateOfMonth = '' + ym + daysInMonth;
	const startDateOfMonthDayjs = dayjs(startDateOfMonth, commonFormat);
	const endDateOfMonthDayjs = dayjs(endDateOfMonth, commonFormat);
	// 本月开始是星期几
	const startWeekdayOfMonth = startDateOfMonthDayjs.day();
	// 本月结束是星期几
	const endWeekdayOfMonth = endDateOfMonthDayjs.day();
	const startDateDayjs = startDateOfMonthDayjs.subtract(
		startWeekdayOfMonth == 7 ? 0 : startWeekdayOfMonth,
		'day'
	);
	const endDateDayjs = endDateOfMonthDayjs.add(endWeekdayOfMonth == 7 ? 6 : 6 - endWeekdayOfMonth, 'day');

	let startDatePointer = startDateDayjs;

	while (startDatePointer.isSameOrAfter(startDateDayjs) && startDatePointer.isSameOrBefore(endDateDayjs)) {
		if (calendarData[calendarData.length - 1].length >= 7) {
			calendarData.push([]);
		}
		let lastWeekArr = calendarData[calendarData.length - 1];
		let dateObj = {
			date: startDatePointer.format(commonFormat),
			value: startDatePointer.format('D'),
			isCurrentMonth: startDatePointer.isSameOrAfter(startDateOfMonthDayjs) &&
				startDatePointer.isSameOrBefore(endDateOfMonthDayjs)
		};
		lastWeekArr.push(dateObj);
		startDatePointer = startDatePointer.add(1, 'day');
	}

	return calendarData;
};
