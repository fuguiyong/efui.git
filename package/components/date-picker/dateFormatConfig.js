export const defaulDateFormat = {
	year: "YYYY",
	month: "YYYY-MM",
	date: "YYYY-MM-DD",
	datetime: "YYYY-MM-DD HH:mm:ss",
	daterange: "YYYY-MM-DD",
	monthrange: "YYYY-MM",
	datetimerange: "YYYY-MM-DD HH:mm:ss",
	time: 'HH:mm:ss',
	timerange: 'HH:mm:ss'
}

// 组件开发统一format
export const devDateFormat = {
	year: "YYYY",
	month: "YYYYMM",
	date: "YYYYMMDD",
	datetime: "YYYYMMDD HH:mm:ss",
	daterange: "YYYYMMDD",
	monthrange: "YYYYMM",
	datetimerange: "YYYYMMDD HH:mm:ss",
	time: 'HH:mm:ss',
	timerange: 'HH:mm:ss'
}
