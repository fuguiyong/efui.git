import dayjs from 'dayjs';
import isSameOrBefore from 'dayjs/plugin/isSameOrBefore';
import isSameOrAfter from 'dayjs/plugin/isSameOrAfter';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isBetween from 'dayjs/plugin/isBetween';
import 'dayjs/locale/zh-cn';
dayjs.extend(isSameOrBefore);
dayjs.extend(isSameOrAfter);
dayjs.extend(customParseFormat);
dayjs.extend(isBetween);

import BasicDateTable from "./basic-date-table.vue";
BasicDateTable.install = (app, options) => {
	app.component(BasicDateTable.name, BasicDateTable);
}

import BasicMonthTable from "./basic-month-table.vue";
BasicMonthTable.install = (app, options) => {
	app.component(BasicMonthTable.name, BasicMonthTable);
}

import BasicYearTable from "./basic-year-table.vue";
BasicYearTable.install = (app, options) => {
	app.component(BasicYearTable.name, BasicYearTable);
}

import PanelDatePick from "./panel-date-pick.vue";
PanelDatePick.install = (app, options) => {
	app.component(PanelDatePick.name, PanelDatePick);
}

import DatePicker from "./date-common-pick.vue";
DatePicker.install = (app, options) => {
	app.component(DatePicker.name, DatePicker);
}

import PanelDateRangePick from "./panel-date-range-pick.vue";
PanelDateRangePick.install = (app, options) => {
	app.component(PanelDateRangePick.name, PanelDateRangePick);
}

import PanelMonthRangePick from "./panel-month-range-pick.vue";
PanelMonthRangePick.install = (app, options) => {
	app.component(PanelMonthRangePick.name, PanelMonthRangePick);
}

export const EfBasicDateTable = BasicDateTable;
export const EfBasicMonthTable = BasicMonthTable;
export const EfBasicYearTable = BasicYearTable;
export const EfPanelDatePick = PanelDatePick;
export const EfDatePicker = DatePicker;
export const EfPanelDateRangePick = PanelDateRangePick;
export const EfPanelMonthRangePick = PanelMonthRangePick;


export default {
	install(app, options) {
		app.use(DatePicker);
	}
};
