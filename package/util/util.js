import {
	get,
	set
} from 'lodash-unified'

export {
	castArray
}
from 'lodash-unified'


export const addUnit = (val, unit = 'px') => {
	if (/^[0-9]+$/.test(val)) {
		return val + unit;
	}
	return val;
}

export const getObjValByPath = (obj, path, defaultValue) => {
	return {
		get value() {
			return get(obj, path, defaultValue)
		},
		set value(val) {
			set(obj, path, val)
		},
	}
}


/**
 * 通过scrollTop 滚动函数
 */
export const scrollToScrollTop = (el, currentScrollTop, targetScrollTop, step = 3, interval = 10) => {
	var executor = _ => {
		// 滚动完成
		if (currentScrollTop >= targetScrollTop) {
			el.scrollTop = targetScrollTop;
			clearInterval(timer);
		} else {
			currentScrollTop += step;
			el.scrollTop = currentScrollTop;
		}
	}
	executor();
	var timer = setInterval(executor, interval);
	return timer;
}


/**
 * 获取dom高度（用于无法直接获取dom高度）
 */
export const getDomHeight = (el) => {
	const HIDDEN_STYLE = `
	  height:0 !important;
	  visibility:hidden !important;
	  overflow:hidden !important;
	  position:absolute !important;
	  z-index:-1000 !important;
	  top:0 !important;
	  right:0 !important;
	`;
	let hiddenDiv = document.createElement('div');
	document.body.appendChild(hiddenDiv);
	hiddenDiv.setAttribute('style', `${HIDDEN_STYLE}`);
	const cloneEl = el.cloneNode(true);
	cloneEl.style.margin = 0;
	hiddenDiv.appendChild(cloneEl);
	let height = hiddenDiv.scrollHeight;
	return height;
}

/**
 * 获取子级到父级元素top距离
 */
export const getChildToParentTopDistance = (parent, child) => {
	let total = 0;
	while (child && child != parent) {
		total += child.offsetTop;
		child = child.parentNode;
	}
	return total;
}


/**
 * 防抖函数
 * @param func 事件触发的操作
 * @param delay 多少毫秒内连续触发事件，不会执行
 * @returns {Function}
 */
export const debounce = (func, delay) => {
	let timer = null;
	return function() {
		timer && clearTimeout(timer);
		// setTimeOut 务必使用箭头函数 确保 this ，arguments 作用域是父级函数
		timer = setTimeout(_ => {
			func(arguments);
		}, delay);
	}
}

/**
 * 节流函数
 * @param func 事件触发的操作
 * @param mustRunDelay 间隔多少毫秒需要触发一次事件
 */
export const throttle = (func, wait, mustRun) => {
	let timeout,
		startTime = new Date();
	return function() {
		let curTime = new Date();
		timeout && clearTimeout(timeout);
		// 如果达到了规定的触发时间间隔，触发 handler
		if (curTime - startTime >= mustRun) {
			func(arguments);
			startTime = curTime;
			// 没达到触发间隔，重新设定定时器
		} else {
			// setTimeOut 务必使用箭头函数 确保 this ，arguments 作用域是父级函数
			timeout = setTimeout(_ => {
				func(arguments);
			}, wait);
		}
	};
};


// 获取可视区域宽高
export const getViewportRect = () => ({
	width: (window.innerWidth || document.documentElement.clientWidth),
	height: (window.innerHeight || document.documentElement.clientHeight)
})

//获取元素是否在可视区域
export const isElementInViewport = (el) => {
	var rect = el.getBoundingClientRect();
	// console.log({
	// 	rect,
	// 	innerHeight:(window.innerHeight || document.documentElement.clientHeight),
	// 	innnerWidth:(window.innerWidth || document.documentElement.clientWidth)
	// })
	return (
		rect.top >= 0 &&
		rect.left >= 0 &&
		rect.bottom <=
		(window.innerHeight || document.documentElement.clientHeight) &&
		rect.right <=
		(window.innerWidth || document.documentElement.clientWidth)
	);
}

export const getDocumentScrollLeft = () => document.documentElement.scrollLeft || window.pageXOffset || document.body
	.scrollLeft;

export const getDocumentScrollTop = () => document.documentElement.scrollTop || window.pageYOffset || document.body
	.scrollTop;

export const isObject = (val) => Object.prototype.toString.call(val) === '[object Object]';

export const isArray = (val) => Object.prototype.toString.call(val) === '[object Array]';

export const isValidComponentSize = (val) => ['', 'large', 'medium', 'small', 'mini', 'middle'].includes(val)

export function entries(obj) {
	return Object
		.keys(obj)
		.map(key => [key, obj[key]])
}

export function isPromise(obj) {
	return !!obj //有实际含义的变量才执行方法，变量null，undefined和''空串都为false
		&&
		(typeof obj === 'object' || typeof obj === 'function') // 初始promise 或 promise.then返回的
		&&
		typeof obj.then === 'function';
}


export function setStyle(obj = {}, el) {
	for (let item in obj) {
		el.style[item] = obj[item]
	}
}

export function getNoneStyle(el) {
	// 获取隐藏元素
	let node = el;

	setStyle({
		display: 'block',
		visibility: 'hidden',
		position: 'absolute'
	}, node);

	const {
		clientHeight,
		clientWidth
	} = el;

	setStyle({
		display: '',
		visibility: '',
		position: ''
	}, node);

	return {
		width: clientWidth,
		height: clientHeight
	}

}

export function getNearEle(ele, type) {
	type = (type == 1) ? 'previousSibling' : 'nextSibling';
	var nearEle = ele[type];
	while (nearEle) {
		if (nearEle.nodeType == 1) { // 节点类型是元素时返回该元素，排除文本节点（空格）、注释
			return nearEle;
		} else {
			nearEle = nearEle[type];
		}
	}
	return null;
}
