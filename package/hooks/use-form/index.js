import {
	inject
} from 'vue'

export const useForm = () => {
	const formContext = inject('efForm', undefined)
	const formItemContext = inject('efFormItem', undefined)
	return {
		formContext,
		formItemContext,
	}
}
