var fs = require('fs');
var path = require('path');


console.log("genCssFile start...");

const basepath = path.resolve(__dirname, '../../theme/');
const themeSrcPath = path.resolve(basepath, 'src');

const isFile = (filePath) => {
	try {
		return fs.statSync(filePath).isFile();
	} catch (err) {
		return false;
	}
}
// copyIt(from.js, to.js);
const copyIt = (from, to) => {
	fs.writeFileSync(to, fs.readFileSync(from));
	//fs.createReadStream(src).pipe(fs.createWriteStream(dst));大文件复制
}


let isSCSS = true;
let indexContent = "";
let exclude = ['index.scss','transition.scss'];

const dirList = fs.readdirSync(themeSrcPath);

console.log({
	dirList
})

dirList.forEach(dir => {
	let dirPath = path.resolve(themeSrcPath, dir);
	if (isFile(dirPath)) {
		if (!exclude.includes(dir)) { // 排除不需要打包css的组件
			indexContent += '@import "./' + dir + '";\n';
		}
	}
})


fs.writeFileSync(path.resolve(themeSrcPath, isSCSS ? 'index.scss' : 'index.css'), indexContent);

console.log("genCssFile success");
