import vue from '@vitejs/plugin-vue'
import DefineOptions from 'unplugin-vue-define-options/vite'
import {
	defineConfig
} from 'vite'
import {
	resolve
} from 'path'

// https://vitejs.dev/config/
export default defineConfig({
	mode: "production",
	resolve: {
		alias: {
			'#': resolve(__dirname, '../package'),
			'&': resolve(__dirname, '../theme')
		}
	},
	plugins: [vue(), DefineOptions()],
	build: {
		outDir: resolve(__dirname, '../lib'),
		lib: {
			entry: resolve(__dirname, '../package/index.js'),
			name: 'EFUI',
			fileName: (format) => `EFUI.${format}.js`,
		},
		rollupOptions: {
			// make sure to externalize deps that shouldn't be bundled
			// into your library
			external: ['vue'],
			output: {
				// Provide global variables to use in the UMD build
				// for externalized deps
				globals: {
					vue: 'Vue',
				},
			},
		},
	},
})
