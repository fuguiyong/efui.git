import { openBlock, createElementBlock, normalizeStyle, createElementVNode, normalizeClass, resolveComponent, createBlock, Fragment, renderList, renderSlot, h, withModifiers, toDisplayString, createVNode, createCommentVNode, withDirectives, vShow, withCtx, Transition, render, createTextVNode, toRef, ref, shallowRef, watch, onMounted, onUnmounted, nextTick, computed, mergeProps, Teleport, inject, getCurrentInstance, reactive, watchEffect, vModelDynamic, vModelText, provide, unref, isRef, vModelRadio, useSlots, useAttrs as useAttrs$1, defineComponent, toHandlers, toRefs, onBeforeUnmount, isVNode } from "vue";
var _export_sfc = (sfc, props2) => {
  const target = sfc.__vccOpts || sfc;
  for (const [key, val] of props2) {
    target[key] = val;
  }
  return target;
};
const _sfc_main$G = {
  name: "ef-icon",
  props: {
    type: {
      type: String,
      default: "icon-loading"
    },
    rotate: {
      type: [String, Number],
      default: 0
    },
    size: {
      type: String,
      default: "small"
    },
    color: {
      type: String,
      default: ""
    },
    fontSize: {
      type: [String, Number],
      default: ""
    },
    disabled: {
      type: Boolean,
      default: false
    }
  },
  computed: {
    styleObj() {
      let style = {};
      let sizeMap = {
        mini: "14px",
        small: "20px",
        middle: "30px"
      };
      style.fontSize = sizeMap[this.size];
      if (this.color) {
        style.color = this.color;
      }
      if (this.fontSize) {
        style.fontSize = parseInt(this.fontSize) + "px";
      }
      return style;
    }
  },
  data() {
    return {};
  },
  methods: {},
  mounted() {
  },
  created() {
  }
};
function _sfc_render$w(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("span", {
    class: "ef-icon-wrapper",
    style: normalizeStyle({ fontSize: $options.styleObj.fontSize, transform: `rotate(${$props.rotate}deg)` })
  }, [
    createElementVNode("i", {
      class: normalizeClass([[
        $props.type,
        { "ef-icon-spin": Object.prototype.hasOwnProperty.call(_ctx.$attrs, "spin"), disabled: $props.disabled }
      ], "ef-iconfont ef-icon"]),
      style: normalizeStyle($options.styleObj)
    }, null, 6)
  ], 4);
}
var EfIcon = /* @__PURE__ */ _export_sfc(_sfc_main$G, [["render", _sfc_render$w]]);
const _sfc_main$F = {
  name: "ef-loading",
  components: {
    EfIcon
  },
  props: {
    size: {
      type: String,
      default: "middle"
    }
  },
  data() {
    return {};
  },
  methods: {
    test() {
    }
  },
  mounted() {
  },
  created() {
  }
};
function _sfc_render$v(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return openBlock(), createBlock(_component_ef_icon, {
    type: "icon-loading",
    size: $props.size,
    spin: ""
  }, null, 8, ["size"]);
}
var component$5 = /* @__PURE__ */ _export_sfc(_sfc_main$F, [["render", _sfc_render$v]]);
const _sfc_main$E = {
  name: "ef-tabel-colgroup",
  props: {
    columns: {
      type: Array,
      default: () => []
    }
  },
  data() {
    return {};
  },
  methods: {
    style(col) {
      let res = {};
      res.minWidth = "100px";
      if (col.width) {
        res.width = col.width + "px";
        res.minWidth = col.width + "px";
      }
      return res;
    },
    test() {
    }
  },
  mounted() {
  },
  created() {
  }
};
function _sfc_render$u(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("colgroup", null, [
    (openBlock(true), createElementBlock(Fragment, null, renderList($props.columns, (col, i) => {
      return openBlock(), createElementBlock("col", {
        key: col.key || i,
        style: normalizeStyle($options.style(col))
      }, null, 4);
    }), 128))
  ]);
}
var EfColgroup = /* @__PURE__ */ _export_sfc(_sfc_main$E, [["render", _sfc_render$u]]);
const _sfc_main$D = {
  name: "ef-checkbox",
  emits: ["change", "update:modelValue"],
  props: {
    halfCheck: {
      type: Boolean,
      default: false
    },
    modelValue: {
      type: Boolean,
      default: false
    },
    disabled: {
      type: Boolean,
      default: false
    },
    dataValue: null,
    halfClick: {
      type: Boolean,
      default: false
    }
  },
  watch: {
    modelValue(newVal) {
      if (!this.disabled) {
        this.checked = newVal;
        this.$nextTick((_) => {
          this.$refs["checkbox-input"].checked = newVal;
        });
      }
    }
  },
  data() {
    return {
      checked: this.modelValue
    };
  },
  methods: {
    onchange(e) {
      let checked = e.target.checked;
      if (!this.disabled) {
        if (this.halfClick === true && this.dataValue == "half") {
          this.$emit("update:modelValue", true);
          this.$emit("change", true, this.dataValue);
          this.$nextTick((_) => {
            this.$refs["checkbox-input"].checked = true;
          });
        } else {
          this.checked = checked;
          this.$emit("update:modelValue", checked);
          this.$emit("change", checked, this.dataValue);
        }
      }
    },
    test() {
    },
    init() {
      this.$refs["checkbox-input"].checked = this.modelValue;
    }
  },
  mounted() {
    this.init();
  },
  created() {
  }
};
function _sfc_render$t(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("label", {
    class: normalizeClass(["ef-checkbox-label-container", { "disabled-label-container": $props.disabled }])
  }, [
    createElementVNode("span", {
      class: normalizeClass(["ef-checkbox-container", { "disabled-inner": $props.disabled }])
    }, [
      createElementVNode("input", {
        ref: "checkbox-input",
        type: "checkbox",
        name: "",
        id: "",
        value: "",
        onChange: _cache[0] || (_cache[0] = ($event) => $options.onchange($event))
      }, null, 544),
      createElementVNode("span", {
        class: normalizeClass(["ef-checkbox-inner", { "is-checked": $data.checked, "disabled": $props.disabled, "half-checked": $props.halfCheck }])
      }, null, 2)
    ], 2),
    createElementVNode("span", {
      class: normalizeClass(["margin-left-smm", { "disabled-span": $props.disabled }])
    }, [
      renderSlot(_ctx.$slots, "default")
    ], 2)
  ], 2);
}
var checkbox = /* @__PURE__ */ _export_sfc(_sfc_main$D, [["render", _sfc_render$t]]);
var VNodes$1 = {
  props: {
    vnodes: {
      type: Object,
      default: null
    }
  },
  setup(props2, {
    attrs,
    slots,
    emit
  }) {
    return () => h(props2.vnodes[0], {}, null);
  }
};
const _sfc_main$C = {
  name: "ef-table-tbody",
  components: {
    EfCheckbox: checkbox,
    "v-nodes": VNodes$1
  },
  props: {
    columns: {
      type: Array,
      default: () => []
    }
  },
  data() {
    return {};
  },
  methods: {
    test() {
    }
  },
  mounted() {
  },
  created() {
  }
};
const _hoisted_1$v = { class: "ef-table-tbody" };
const _hoisted_2$q = ["row"];
const _hoisted_3$j = ["onDblclick", "onClick"];
const _hoisted_4$f = { key: 1 };
function _sfc_render$s(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_checkbox = resolveComponent("ef-checkbox");
  const _component_v_nodes = resolveComponent("v-nodes");
  return openBlock(), createElementBlock("tbody", _hoisted_1$v, [
    (openBlock(true), createElementBlock(Fragment, null, renderList(_ctx.$parent.dataSource, (row, i) => {
      return openBlock(), createElementBlock("tr", {
        onMouseleave: _cache[1] || (_cache[1] = ($event) => _ctx.$parent.onMouseleaveTr($event)),
        onMouseenter: _cache[2] || (_cache[2] = ($event) => _ctx.$parent.onMouseenterTr($event)),
        row: "row-index-" + (i + 1),
        style: {},
        key: (typeof _ctx.$parent.rowKey == "function" ? _ctx.$parent.rowKey(row) : row[_ctx.$parent.rowKey]) || row.key || row.id || i,
        class: normalizeClass([_ctx.$parent.trClassName(row, i + 1), _ctx.$parent.stripe && (i + 1) % 2 == 0 ? "ef-tbale-stripe" : ""])
      }, [
        (openBlock(true), createElementBlock(Fragment, null, renderList($props.columns, (col, j) => {
          return openBlock(), createElementBlock("td", {
            onDblclick: ($event) => _ctx.$parent.onTdDbclick(row),
            onClick: ($event) => _ctx.$parent.onTdClick(row),
            class: normalizeClass({
              "td-selected": _ctx.$parent.selectedConfig && _ctx.$parent.selectedConfig.rowHighLighted && _ctx.$parent.selectedValue.includes(row[_ctx.$parent.selectedConfig.bindDataIndex]),
              "table-td-bordered": _ctx.$parent.bordered
            }),
            key: col.key,
            style: normalizeStyle(_ctx.$parent.tdStyleMethod(col, j, $props.columns))
          }, [
            col.key == "ef-table-checkbox" ? (openBlock(), createBlock(_component_ef_checkbox, {
              key: 0,
              onClick: _cache[0] || (_cache[0] = withModifiers(() => {
              }, ["stop"])),
              onChange: _ctx.$parent.onCheckboxChange,
              modelValue: _ctx.$parent.selectedValue.includes(row[_ctx.$parent.selectedConfig.bindDataIndex]),
              dataValue: _ctx.$parent.selectedConfig && row[_ctx.$parent.selectedConfig.bindDataIndex]
            }, null, 8, ["onChange", "modelValue", "dataValue"])) : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
              col.customRowRender ? (openBlock(), createBlock(_component_v_nodes, {
                key: 0,
                vnodes: _ctx.$parent.$slots[col.customRowRender]({ row, index: i + 1, text: row[col["dataIndex"]] })
              }, null, 8, ["vnodes"])) : (openBlock(), createElementBlock("span", _hoisted_4$f, toDisplayString(row[col["dataIndex"]]), 1))
            ], 64))
          ], 46, _hoisted_3$j);
        }), 128))
      ], 42, _hoisted_2$q);
    }), 128))
  ]);
}
var EfTbody = /* @__PURE__ */ _export_sfc(_sfc_main$C, [["render", _sfc_render$s]]);
const _sfc_main$B = {
  name: "ef-triangle",
  props: {
    width: {
      type: Number,
      default: 8
    },
    color: {
      type: String,
      default: ""
    },
    rotate: {
      type: Number,
      default: 0
    },
    classList: {
      type: Array,
      default: () => []
    }
  },
  computed: {
    style() {
      let res = {};
      this.width && (res.borderWidth = this.width + "px");
      this.color && (res.borderColor = this.color);
      res.borderStyle = "solid";
      res.transform = `rotate(${this.rotate}deg)`;
      return res;
    }
  },
  data() {
    return {};
  },
  methods: {
    test() {
    }
  },
  mounted() {
  },
  created() {
  }
};
function _sfc_render$r(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("span", {
    class: normalizeClass(["ef-triangle", $props.classList]),
    style: normalizeStyle($options.style)
  }, null, 6);
}
var component$4 = /* @__PURE__ */ _export_sfc(_sfc_main$B, [["render", _sfc_render$r]]);
const _sfc_main$A = {
  name: "ef-table-thead",
  props: {
    headerList: {
      type: Array,
      default: () => []
    }
  },
  components: {
    EfCheckbox: checkbox,
    EfTriangle: component$4,
    "v-nodes": VNodes$1
  },
  data() {
    return {};
  },
  methods: {
    test() {
    }
  },
  mounted() {
  },
  created() {
  }
};
const _hoisted_1$u = ["onClick", "colspan", "rowspan"];
const _hoisted_2$p = {
  key: 1,
  class: "th-wrapper"
};
const _hoisted_3$i = { key: 1 };
const _hoisted_4$e = {
  key: 2,
  class: "ef-sort-wrapper"
};
function _sfc_render$q(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_checkbox = resolveComponent("ef-checkbox");
  const _component_v_nodes = resolveComponent("v-nodes");
  const _component_ef_triangle = resolveComponent("ef-triangle");
  return openBlock(), createElementBlock("thead", {
    style: normalizeStyle({ height: _ctx.$parent.theadTrHeight }),
    class: "ef-table-thead"
  }, [
    (openBlock(true), createElementBlock(Fragment, null, renderList($props.headerList, (columns, index) => {
      return openBlock(), createElementBlock("tr", {
        key: index,
        class: normalizeClass(_ctx.$parent.headRowClassName)
      }, [
        (openBlock(true), createElementBlock(Fragment, null, renderList(columns, (col, i) => {
          return openBlock(), createElementBlock("th", {
            onClick: ($event) => _ctx.$parent.onSortChange(col),
            key: col.key || i,
            colspan: col.colspan || 1,
            rowspan: col.rowspan || 1,
            style: normalizeStyle(_ctx.$parent.thStyleMethod(col, i, columns)),
            class: normalizeClass({ "sort": col.hasOwnProperty("sort"), "table-td-bordered": _ctx.$parent.bordered })
          }, [
            col.key == "ef-table-checkbox" && _ctx.$parent.selectedConfig.showSelectAll ? (openBlock(), createBlock(_component_ef_checkbox, {
              key: 0,
              halfClick: true,
              onClick: _cache[0] || (_cache[0] = withModifiers(() => {
              }, ["stop"])),
              onChange: _ctx.$parent.onCheckboxAllChange,
              modelValue: _ctx.$parent.selectedValue.length > 0,
              halfCheck: _ctx.$parent.selectedType == "half",
              dataValue: _ctx.$parent.selectedType
            }, null, 8, ["onChange", "modelValue", "halfCheck", "dataValue"])) : col.key != "ef-table-checkbox" ? (openBlock(), createElementBlock("div", _hoisted_2$p, [
              col.customTitleRender ? (openBlock(), createBlock(_component_v_nodes, {
                key: 0,
                vnodes: _ctx.$parent.$slots[col.customTitleRender]({ col, title: col.title })
              }, null, 8, ["vnodes"])) : (openBlock(), createElementBlock("span", _hoisted_3$i, toDisplayString(col.title), 1)),
              col.hasOwnProperty("sort") ? (openBlock(), createElementBlock("span", _hoisted_4$e, [
                createVNode(_component_ef_triangle, {
                  classList: [col.sort == "asc" ? "ef-color-sort-active" : "ef-color-sort-unactive"],
                  width: 5
                }, null, 8, ["classList"]),
                createVNode(_component_ef_triangle, {
                  classList: [col.sort == "desc" ? "ef-color-sort-active" : "ef-color-sort-unactive"],
                  width: 5,
                  rotate: 180,
                  style: { "position": "relative", "top": "5px" }
                }, null, 8, ["classList"])
              ])) : createCommentVNode("", true)
            ])) : createCommentVNode("", true)
          ], 14, _hoisted_1$u);
        }), 128))
      ], 2);
    }), 128))
  ], 4);
}
var EfThead = /* @__PURE__ */ _export_sfc(_sfc_main$A, [["render", _sfc_render$q]]);
function configRowspanColumns(columnsConfig = []) {
  const findMaxLevel2 = (col, level = 0) => {
    if (col.hasOwnProperty("children")) {
      let maxLevel2 = 0;
      col.children.forEach((c) => {
        let nextLevel = level + 1;
        let cLevel = findMaxLevel2(c, nextLevel);
        if (cLevel > maxLevel2) {
          maxLevel2 = cLevel;
        }
      });
      return maxLevel2;
    } else {
      return level;
    }
  };
  const findMaxLevel1 = (columns) => {
    let maxLevel2 = 0;
    columns.forEach((col) => {
      let level = findMaxLevel2(col);
      if (level > maxLevel2) {
        maxLevel2 = level;
      }
    });
    return maxLevel2;
  };
  let maxLevel = findMaxLevel1(columnsConfig);
  let fixedLeftColumns = [];
  let fixedRightColumns = [];
  let notFixedColumns = [];
  let fixedLeftHeaders = [];
  let fixedRightHeaders = [];
  let notFixedHeaders = [];
  const getMaxLevelChildrenCount = (children) => {
    let count = 0;
    children.forEach((c) => {
      if (c.hasOwnProperty("children")) {
        count += getMaxLevelChildrenCount(c.children);
      } else {
        count += 1;
      }
    });
    return count;
  };
  const handleColumn = (col, maxLevel2, level = 0, fixed = false) => {
    if (level == 0 && col.fixed) {
      fixed = col.fixed;
    }
    if (!Array.isArray(notFixedHeaders[level])) {
      notFixedHeaders[level] = [];
      fixedRightHeaders[level] = [];
      fixedLeftHeaders[level] = [];
    }
    if (col.hasOwnProperty("children")) {
      col.rowspan = 1;
      col.colspan = getMaxLevelChildrenCount(col.children);
      let headerObj = {
        title: col.title,
        rowspan: col.rowspan,
        colspan: col.colspan
      };
      if (fixed == "left") {
        fixedLeftHeaders[level].push(headerObj);
      } else if (fixed == "right") {
        fixedRightHeaders[level].push(headerObj);
      } else {
        notFixedHeaders[level].push(headerObj);
      }
      col.children.forEach((c) => {
        handleColumn(c, maxLevel2, level + 1, fixed);
      });
    } else {
      col.rowspan = maxLevel2 - level + 1;
      col.colspan = 1;
      if (fixed == "left") {
        fixedLeftColumns.push(col);
      } else if (fixed == "right") {
        fixedRightColumns.push(col);
      } else {
        notFixedColumns.push(col);
      }
      if (fixed == "left") {
        fixedLeftHeaders[level].push(col);
      } else if (fixed == "right") {
        fixedRightHeaders[level].push(col);
      } else {
        notFixedHeaders[level].push(col);
      }
    }
  };
  const handleColumnMain = (columns) => {
    columns.forEach((col) => {
      handleColumn(col, maxLevel);
    });
  };
  handleColumnMain(columnsConfig);
  const mergeColumns = () => {
    return [...fixedLeftColumns, ...notFixedColumns, ...fixedRightColumns];
  };
  const mergeHeaders = () => {
    return notFixedHeaders.map((item, i) => [...fixedLeftHeaders[i], ...item, ...fixedRightHeaders[i]]);
  };
  return {
    fixedLeftColumns,
    fixedRightColumns,
    orderColumns: mergeColumns(),
    fixedLeftHeaders,
    fixedRightHeaders,
    orderHeaders: mergeHeaders(),
    maxLevel
  };
}
const _sfc_main$z = {
  name: "ef-table",
  emits: ["checkboxChange", "rowClick", "rowDbclick", "sortChange"],
  components: {
    EfLoading: component$5,
    EfColgroup,
    EfTbody,
    EfThead
  },
  props: {
    loadingText: {
      type: String,
      default: "\u62FC\u547D\u52A0\u8F7D\u4E2D"
    },
    stripe: {
      type: Boolean,
      default: false
    },
    selectedConfig: {
      type: Object,
      default: null
    },
    fixedHeader: {
      type: Boolean,
      default: false
    },
    height: {
      type: String,
      default: ""
    },
    width: {
      type: String,
      default: ""
    },
    loading: {
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: ""
    },
    bordered: {
      type: Boolean,
      default: false
    },
    tableLayout: {
      type: String,
      default: "auto"
    },
    headRowClassName: {
      type: String,
      default: ""
    },
    rowClassName: {
      type: [String, Function],
      default: ""
    },
    showHeader: {
      type: Boolean,
      default: true
    },
    columns: {
      type: Array,
      default: () => []
    },
    dataSource: {
      type: Array,
      default: () => []
    },
    rowKey: {
      type: [String, Function],
      default: ""
    },
    openRowspan: {
      type: Boolean,
      default: false
    },
    rowspanConfig: {
      type: Array,
      default: () => []
    }
  },
  computed: {
    selectedType() {
      if (this.selectedValue.length == this.dataSource.length) {
        return "all";
      } else if (this.selectedValue.length == 0) {
        return "none";
      } else if (this.selectedValue.length < this.dataSource.length) {
        return "half";
      }
    },
    tableContainerStyle() {
      let style = {
        width: "100%",
        height: "auto"
      };
      if (this.height) {
        style.height = /^[0-9]+$/.test(this.height) ? this.height + "px" : this.height;
      }
      if (this.width) {
        style.width = /^[0-9]+$/.test(this.width) ? this.width + "px" : this.width;
      }
      return style;
    },
    tableStyle() {
      let style = {};
      if (this.tableLayout) {
        style.tableLayout = this.tableLayout;
      }
      if (this.tableLayoutLock) {
        style.tableLayout = this.tableLayoutLock;
      }
      return style;
    },
    tdStyle() {
      let style = {};
      if (this.size == "small") {
        style.padding = "8px";
      } else if (this.size == "middle") {
        style.padding = "12px 8px";
      }
      return style;
    },
    thStyle() {
    }
  },
  watch: {
    "selectedConfig.selectedValue"(newVal, oldVal) {
      this.selectedValue = this.selectedConfig.selectedValue;
    },
    selectedValue() {
      this.selectedConfig.selectedValue = this.selectedValue;
    },
    dataSource(newVal, oldVal) {
      this.ifMounted = false;
      this.setupTableAttr();
      setTimeout(() => {
        this.setSelectedAllValue();
      }, 0);
    },
    columns(newVal, oldVal) {
      this.handleColumn(newVal);
      this.ifMounted = false;
      this.setupTableAttr();
    }
  },
  data() {
    return {
      tableLayoutLock: false,
      fixedLeftColumns: [],
      fixedRightColumns: [],
      notFixedColumns: [],
      orderColumns: [],
      fixedLeftHeaders: [],
      fixedRightHeaders: [],
      orderHeaders: [],
      ifScrollMaxLeft: true,
      ifScrollMaxRight: false,
      mouseEnterArea: "",
      tbodyTrHeight: "auto",
      theadTrHeight: "auto",
      selectedValue: this.selectedConfig && this.selectedConfig.selectedValue || [],
      selectedAllValue: [],
      ifMounted: false
    };
  },
  methods: {
    onSortChange(col) {
      if (!col.hasOwnProperty("sort"))
        return;
      let sort = col.sort;
      let sortArry = ["asc", "desc", "unset"];
      let index = sortArry.indexOf(sort);
      let nextIndex = index + 1 > sortArry.length - 1 ? 0 : index + 1;
      this.cancelAllSortColumns();
      col.sort = sortArry[nextIndex];
      this.$emit("sortChange", col.sort, col);
    },
    cancelAllSortColumns() {
      if (this.fixedLeftHeaders[0] && Array.isArray(this.fixedLeftHeaders[0])) {
        this.fixedLeftHeaders[0].forEach((item) => {
          if (item.hasOwnProperty("sort")) {
            item.sort = "unset";
          }
        });
      }
      if (this.fixedRightHeaders[0] && Array.isArray(this.fixedRightHeaders[0])) {
        this.fixedRightHeaders[0].forEach((item) => {
          if (item.hasOwnProperty("sort")) {
            item.sort = "unset";
          }
        });
      }
      if (this.orderHeaders[0] && Array.isArray(this.orderHeaders[0])) {
        this.orderHeaders[0].forEach((item) => {
          if (item.hasOwnProperty("sort")) {
            item.sort = "unset";
          }
        });
      }
    },
    onCheckboxAllChange(checked, value) {
      if (checked) {
        this.selectedValue = JSON.parse(JSON.stringify(this.selectedAllValue));
        this.$emit("checkboxChange", this.selectedValue, checked, value);
      } else {
        this.selectedValue = [];
        this.$emit("checkboxChange", this.selectedValue, checked, value);
      }
    },
    onTdDbclick(row) {
      this.$emit("rowDbclick", row);
    },
    onTdClick(row) {
      if (this.selectedConfig && this.selectedConfig.rowClick) {
        let val = row[this.selectedConfig.bindDataIndex];
        if (this.selectedConfig.radio === true) {
          this.selectedValue = [val];
          this.$emit("checkboxChange", this.selectedValue, true, val);
        } else {
          let index = this.selectedValue.indexOf(val);
          if (index == -1) {
            this.onCheckboxChange(true, val);
          } else {
            this.onCheckboxChange(false, val);
          }
        }
      }
      this.$emit("rowClick", row);
    },
    onCheckboxChange(checked, value) {
      if (checked) {
        this.selectedValue.push(value);
      } else {
        let index = this.selectedValue.indexOf(value);
        this.selectedValue.splice(index, 1);
      }
      this.$emit("checkboxChange", this.selectedValue, checked, value);
    },
    onMouseenterTr(e) {
      let attrRow = e.currentTarget.getAttribute("row");
      if (this.fixedLeftColumns.length > 0 && this.$refs["ef-table-left-container"]) {
        this.$refs["ef-table-left-container"].querySelector(` tbody tr[row=${attrRow}]`).classList.add("tr-hover");
      }
      if (this.fixedRightColumns.length > 0 && this.$refs["ef-table-right-container"]) {
        this.$refs["ef-table-right-container"].querySelector(` tbody tr[row=${attrRow}]`).classList.add("tr-hover");
      }
      this.$refs["ef-table-container"] && this.$refs["ef-table-container"].querySelector(` tbody tr[row=${attrRow}]`).classList.add("tr-hover");
    },
    onMouseleaveTr(e) {
      let attrRow = e.currentTarget.getAttribute("row");
      if (this.fixedLeftColumns.length > 0 && this.$refs["ef-table-left-container"]) {
        this.$refs["ef-table-left-container"].querySelector(` tbody tr[row=${attrRow}]`).classList.remove("tr-hover");
      }
      if (this.fixedRightColumns.length > 0 && this.$refs["ef-table-right-container"]) {
        this.$refs["ef-table-right-container"].querySelector(` tbody tr[row=${attrRow}]`).classList.remove("tr-hover");
      }
      this.$refs["ef-table-container"] && this.$refs["ef-table-container"].querySelector(` tbody tr[row=${attrRow}]`).classList.remove("tr-hover");
    },
    onRightScroll(e) {
      if (this.mouseEnterArea != "right")
        return;
      let { scrollLeft, scrollTop } = e.target;
      this.$refs["ef-table-body"].scrollTop = scrollTop;
      if (this.fixedLeftColumns.length > 0) {
        this.$refs["ef-table-left-body"].scrollTop = scrollTop;
      }
    },
    onLeftScroll(e) {
      if (this.mouseEnterArea != "left")
        return;
      let { scrollLeft, scrollTop } = e.target;
      this.$refs["ef-table-body"].scrollTop = scrollTop;
      if (this.fixedRightColumns.length > 0) {
        this.$refs["ef-table-right-body"].scrollTop = scrollTop;
      }
    },
    onBodyScroll(e) {
      if (this.mouseEnterArea != "body")
        return;
      let { scrollLeft, scrollTop } = e.target;
      if (scrollLeft > 0) {
        this.ifScrollMaxLeft = false;
      } else {
        this.ifScrollMaxLeft = true;
      }
      if (this.$refs["ef-table-body"].clientWidth + scrollLeft == this.$refs["ef-table-body"].scrollWidth) {
        this.ifScrollMaxRight = true;
      } else {
        this.ifScrollMaxRight = false;
      }
      if (this.fixedHeader && this.showHeader) {
        this.$refs["ef-table-body-header"].scrollLeft = scrollLeft;
      }
      if (this.fixedLeftColumns.length > 0 && this.dataSource.length > 0) {
        this.$refs["ef-table-left-body"].scrollTop = scrollTop;
      }
      if (this.fixedRightColumns.length > 0 && this.dataSource.length > 0) {
        this.$refs["ef-table-right-body"].scrollTop = scrollTop;
      }
    },
    test() {
    },
    handleColumn() {
      let columns = JSON.parse(JSON.stringify(this.columns));
      if (this.selectedConfig && !this.selectedConfig.radio) {
        this.checkSelectedConfig();
        let selectedItem = {
          key: "ef-table-checkbox",
          title: "",
          width: this.selectedConfig.width || "50",
          fixed: "left"
        };
        columns.unshift(selectedItem);
      }
      if (this.selectedConfig && this.selectedConfig.radio === true) {
        this.selectedConfig.rowClick = true;
        this.selectedConfig.showSelectAll = false;
        this.selectedConfig.rowHighLighted = true;
      }
      let {
        fixedLeftColumns,
        fixedRightColumns,
        orderColumns,
        fixedLeftHeaders,
        fixedRightHeaders,
        orderHeaders,
        maxLevel
      } = configRowspanColumns(columns);
      this.fixedLeftColumns = fixedLeftColumns;
      this.fixedRightColumns = fixedRightColumns;
      this.orderColumns = orderColumns;
      this.fixedLeftHeaders = fixedLeftHeaders;
      this.fixedRightHeaders = fixedRightHeaders;
      this.orderHeaders = orderHeaders;
      this.fixedLeftColumns.forEach((col) => {
        if (!col.width) {
          col.width = "120";
        }
      });
      this.fixedRightColumns.forEach((col) => {
        if (!col.width) {
          col.width = "120";
        }
      });
      if (this.fixedLeftColumns.length > 0 || this.fixedRightColumns.length > 0) {
        this.tableLayoutLock = "fixed";
      }
    },
    checkProps() {
      if (Object.prototype.toString.call(this.dataSource) != "[object Array]") {
        throw new Error("props dataSource must be Array");
      }
      if (Object.prototype.toString.call(this.columns) != "[object Array]") {
        throw new Error("props columns must be Array");
      }
      if (Object.prototype.toString.call(this.rowspanConfig) != "[object Array]") {
        throw new Error("props rowspanConfig must be Array");
      }
      if (this.openRowspan === true && Object.prototype.toString.call(this.rowspanConfig[0]) != "[object Array]") {
        throw new Error("props rowspanConfig must be multidimensional arrays");
      }
      if (this.fixedHeader && !this.showHeader) {
        throw new Error("props fixedHeader=true,showHeader must be true");
      }
    },
    setupTableAttr() {
      this.$nextTick((_) => {
        this.setTbodyTrHeight();
        this.setTheadTrHeight();
        let headerHeight = 0;
        if (this.showHeader) {
          let elThead = this.$refs["ef-table-container"].querySelector(" thead");
          headerHeight = elThead.offsetHeight;
        }
        let scrollXWidth = this.$refs["ef-table-body"].offsetHeight - this.$refs["ef-table-body"].clientHeight;
        let scrollYWidth = this.$refs["ef-table-body"].offsetWidth - this.$refs["ef-table-body"].clientWidth;
        if (this.fixedHeader && this.showHeader) {
          this.$refs["ef-table-body"].style.height = `calc(100% - ${headerHeight}px)`;
          this.$refs["ef-table-body-header"].style.width = `calc(100% - ${scrollYWidth}px)`;
          this.$refs["ef-table-body-loading"].style.height = `calc(100% - ${headerHeight}px)`;
          this.$refs["ef-table-body-nodata"].style.height = `calc(100% - ${headerHeight}px)`;
        }
        if (this.fixedLeftColumns.length > 0 && this.dataSource.length > 0) {
          this.$refs["ef-table-left-body"].style.marginRight = `-${scrollYWidth}px`;
          this.$refs["ef-table-left-container"].style.height = `calc(100% - ${scrollXWidth}px)`;
          if (this.fixedHeader && this.showHeader) {
            this.$refs["ef-table-left-body"].style.height = `calc(100% - ${headerHeight}px)`;
          }
        }
        if (this.fixedRightColumns.length > 0 && this.dataSource.length > 0) {
          this.$refs["ef-table-right-container"].style.height = `calc(100% - ${scrollXWidth}px)`;
          if (this.fixedHeader && this.showHeader) {
            this.$refs["ef-table-right-body"].style.height = `calc(100% - ${headerHeight}px)`;
          }
          if (scrollYWidth > 0) {
            this.$refs["ef-table-right-body"].style.overflowY = "scroll";
          }
        }
        this.ifMounted = true;
      });
    },
    setTbodyTrHeight() {
      if (this.dataSource.length < 1) {
        return;
      }
      if (this.fixedLeftColumns.length == 0 && this.fixedRightColumns.length == 0) {
        return;
      }
      if (!this.$refs["ef-table-container"].querySelector(" tbody tr")) {
        return;
      }
      let elTbodyTr = this.$refs["ef-table-container"].querySelectorAll(" tbody tr");
      let elTbodyLeftTrS = this.$refs["ef-table-left-container"] && this.$refs["ef-table-left-container"].querySelectorAll("tbody tr");
      let elTbodyRightTrS = this.$refs["ef-table-right-container"] && this.$refs["ef-table-right-container"].querySelectorAll("tbody tr");
      for (let i = 0; i < elTbodyTr.length; i++) {
        let tr = elTbodyTr[i];
        let trMaxHeight = tr.offsetHeight;
        let trLeft = elTbodyLeftTrS && elTbodyLeftTrS[i];
        let trRight = elTbodyRightTrS && elTbodyRightTrS[i];
        trLeft && trLeft.offsetHeight > trMaxHeight && (trMaxHeight = trLeft.offsetHeight);
        trRight && trRight.offsetHeight > trMaxHeight && (trMaxHeight = trRight.offsetHeight);
        if (trLeft && trMaxHeight > trLeft.offsetHeight) {
          trLeft.style.height = trMaxHeight + "px";
        }
        if (trRight && trMaxHeight > trRight.offsetHeight) {
          trRight.style.height = trMaxHeight + "px";
        }
        if (trMaxHeight > tr.offsetHeight) {
          tr.style.height = trMaxHeight + "px";
        }
      }
    },
    setTheadTrHeight() {
      if (!this.showHeader) {
        return 0;
      }
      let maxNum;
      let elTbodyTr = this.$refs["ef-table-container"].querySelector("thead");
      maxNum = elTbodyTr.offsetHeight;
      if (this.fixedLeftColumns.length > 0 && this.dataSource.length > 0) {
        let elLeftTbodyTr = this.$refs["ef-table-left-container"].querySelector("thead");
        if (elLeftTbodyTr && elLeftTbodyTr.offsetHeight > maxNum) {
          maxNum = elLeftTbodyTr.offsetHeight;
        }
      }
      if (this.fixedRightColumns.length > 0 && this.dataSource.length > 0) {
        let elRightTbodyTr = this.$refs["ef-table-right-container"].querySelector("thead");
        if (elRightTbodyTr && elRightTbodyTr.offsetHeight > maxNum) {
          maxNum = elRightTbodyTr.offsetHeight;
        }
      }
      this.theadTrHeight = maxNum + "px";
    },
    setProps() {
    },
    checkSelectedConfig() {
      if (!this.selectedConfig.hasOwnProperty("bindDataIndex")) {
        throw new Error("bindDataIndex \u5C5E\u6027\u5FC5\u987B");
      }
      if (this.selectedConfig.hasOwnProperty("selectedValue")) {
        if (Object.prototype.toString.call(this.selectedConfig.selectedValue) != "[object Array]") {
          throw new Error("selectedValue\u5C5E\u6027\u7C7B\u578B\u5FC5\u987B\u662F\u6570\u7EC4");
        }
      } else {
        this.selectedConfig.selectedValue = [];
      }
    },
    setSelectedAllValue() {
      let res = [];
      if (this.selectedConfig) {
        this.dataSource.forEach((item) => {
          res.push(item[this.selectedConfig.bindDataIndex]);
        });
      }
      this.selectedAllValue = res;
    },
    trClassName(row, index) {
      if (Object.prototype.toString.call(this.rowClassName) == "[object Function]") {
        return this.rowClassName(row, index);
      } else if (Object.prototype.toString.call(this.rowClassName) == "[object String]") {
        return this.rowClassName;
      } else {
        throw new Error("props rowClassName type error");
      }
    },
    tdStyleMethod(col, i, columns) {
      let obj2 = {
        ...this.tdStyle
      };
      if (col.key == "ef-table-checkbox") {
        obj2.textAlign = "center";
      }
      return obj2;
    },
    thStyleMethod(col, i, columns) {
      let obj2 = {
        ...this.tdStyle
      };
      if (col.key == "ef-table-checkbox") {
        obj2.textAlign = "center";
      }
      return obj2;
    }
  },
  mounted() {
    setTimeout(() => {
      this.setSelectedAllValue();
    }, 0);
    this.setupTableAttr();
    this.resizeFunc = () => {
      this.setupTableAttr();
    };
    window.addEventListener("resize", this.resizeFunc);
  },
  created() {
    this.checkProps();
    this.handleColumn();
  },
  unmounted() {
    window.removeEventListener("resize", this.resizeFunc);
  }
};
const _hoisted_1$t = {
  key: 0,
  class: "ef-table-body-header",
  ref: "ef-table-left-header"
};
const _hoisted_2$o = {
  key: 0,
  class: "ef-table-body-header",
  ref: "ef-table-right-header"
};
const _hoisted_3$h = {
  class: "ef-table-body",
  ref: "ef-table-container"
};
const _hoisted_4$d = {
  key: 0,
  class: "ef-table-body-header overflow-hidden",
  ref: "ef-table-body-header"
};
const _hoisted_5$8 = {
  class: "ef-spin-container flex-center",
  ref: "ef-table-body-loading"
};
const _hoisted_6$7 = {
  key: 1,
  class: "flex-column-center"
};
const _hoisted_7$7 = { class: "marin-top loading-text" };
const _hoisted_8$6 = {
  class: "ef-spin-container flex-center",
  ref: "ef-table-body-nodata"
};
const _hoisted_9$3 = { key: 1 };
function _sfc_render$p(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_colgroup = resolveComponent("ef-colgroup");
  const _component_ef_thead = resolveComponent("ef-thead");
  const _component_ef_tbody = resolveComponent("ef-tbody");
  const _component_ef_loading = resolveComponent("ef-loading");
  return openBlock(), createElementBlock("div", {
    class: normalizeClass(["ef-table-conainer", { "ef-table-no-data": $props.dataSource.length < 1, "ef-table-bordered": $props.bordered }]),
    style: normalizeStyle($options.tableContainerStyle)
  }, [
    $props.dataSource.length > 0 && $data.fixedLeftColumns.length > 0 ? (openBlock(), createElementBlock("div", {
      key: 0,
      class: normalizeClass(["ef-table-left", { "scroll-box-shadow-left": !$data.ifScrollMaxLeft, "ef-spin-blur": $props.loading }]),
      ref: "ef-table-left-container"
    }, [
      $props.fixedHeader && $props.showHeader ? (openBlock(), createElementBlock("div", _hoisted_1$t, [
        createElementVNode("table", {
          style: normalizeStyle($options.tableStyle)
        }, [
          createVNode(_component_ef_colgroup, { columns: $data.fixedLeftColumns }, null, 8, ["columns"]),
          createVNode(_component_ef_thead, { headerList: $data.fixedLeftHeaders }, null, 8, ["headerList"])
        ], 4)
      ], 512)) : createCommentVNode("", true),
      createElementVNode("div", {
        onScroll: _cache[0] || (_cache[0] = ($event) => $options.onLeftScroll($event)),
        onMouseenter: _cache[1] || (_cache[1] = ($event) => $data.mouseEnterArea = "left"),
        class: "ef-table-body-box overflow-auto-y",
        ref: "ef-table-left-body"
      }, [
        createElementVNode("table", {
          style: normalizeStyle($options.tableStyle)
        }, [
          createVNode(_component_ef_colgroup, { columns: $data.fixedLeftColumns }, null, 8, ["columns"]),
          $props.showHeader && !$props.fixedHeader ? (openBlock(), createBlock(_component_ef_thead, {
            key: 0,
            headerList: $data.fixedLeftHeaders
          }, null, 8, ["headerList"])) : createCommentVNode("", true),
          createVNode(_component_ef_tbody, { columns: $data.fixedLeftColumns }, null, 8, ["columns"])
        ], 4)
      ], 544)
    ], 2)) : createCommentVNode("", true),
    $props.dataSource.length > 0 && $data.fixedRightColumns.length > 0 ? (openBlock(), createElementBlock("div", {
      key: 1,
      class: normalizeClass(["ef-table-right", { "scroll-box-shadow-right": !$data.ifScrollMaxRight, "ef-spin-blur": $props.loading }]),
      ref: "ef-table-right-container"
    }, [
      $props.fixedHeader && $props.showHeader ? (openBlock(), createElementBlock("div", _hoisted_2$o, [
        createElementVNode("table", {
          style: normalizeStyle($options.tableStyle)
        }, [
          createVNode(_component_ef_colgroup, { columns: $data.fixedRightColumns }, null, 8, ["columns"]),
          createVNode(_component_ef_thead, { headerList: $data.fixedRightHeaders }, null, 8, ["headerList"])
        ], 4)
      ], 512)) : createCommentVNode("", true),
      createElementVNode("div", {
        onScroll: _cache[2] || (_cache[2] = ($event) => $options.onRightScroll($event)),
        onMouseenter: _cache[3] || (_cache[3] = ($event) => $data.mouseEnterArea = "right"),
        class: "ef-table-body-box overflow-auto-y",
        ref: "ef-table-right-body"
      }, [
        createElementVNode("table", {
          style: normalizeStyle($options.tableStyle)
        }, [
          createVNode(_component_ef_colgroup, { columns: $data.fixedRightColumns }, null, 8, ["columns"]),
          $props.showHeader && !$props.fixedHeader ? (openBlock(), createBlock(_component_ef_thead, {
            key: 0,
            headerList: $data.fixedRightHeaders
          }, null, 8, ["headerList"])) : createCommentVNode("", true),
          createVNode(_component_ef_tbody, { columns: $data.fixedRightColumns }, null, 8, ["columns"])
        ], 4)
      ], 544)
    ], 2)) : createCommentVNode("", true),
    createElementVNode("div", _hoisted_3$h, [
      createElementVNode("div", {
        class: normalizeClass(["ef-table-body-header-outer", { "ef-spin-blur": $props.loading }])
      }, [
        $props.fixedHeader && $props.showHeader ? (openBlock(), createElementBlock("div", _hoisted_4$d, [
          createElementVNode("table", {
            style: normalizeStyle($options.tableStyle)
          }, [
            createVNode(_component_ef_colgroup, { columns: $data.orderColumns }, null, 8, ["columns"]),
            createVNode(_component_ef_thead, { headerList: $data.orderHeaders }, null, 8, ["headerList"])
          ], 4)
        ], 512)) : createCommentVNode("", true)
      ], 2),
      createElementVNode("div", {
        class: normalizeClass([{ "ef-spin-blur": $props.loading }, "ef-table-body-box full-height-width overflow-auto-overlay pos-relative"]),
        onMouseenter: _cache[4] || (_cache[4] = ($event) => $data.mouseEnterArea = "body"),
        onScroll: _cache[5] || (_cache[5] = ($event) => $options.onBodyScroll($event)),
        ref: "ef-table-body"
      }, [
        createElementVNode("table", {
          style: normalizeStyle($options.tableStyle)
        }, [
          createVNode(_component_ef_colgroup, { columns: $data.orderColumns }, null, 8, ["columns"]),
          $props.showHeader && !$props.fixedHeader ? (openBlock(), createBlock(_component_ef_thead, {
            key: 0,
            headerList: $data.orderHeaders
          }, null, 8, ["headerList"])) : createCommentVNode("", true),
          createVNode(_component_ef_tbody, { columns: $data.orderColumns }, null, 8, ["columns"])
        ], 4)
      ], 34),
      withDirectives(createElementVNode("div", _hoisted_5$8, [
        _ctx.$slots.loading ? renderSlot(_ctx.$slots, "loading", { key: 0 }) : (openBlock(), createElementBlock("div", _hoisted_6$7, [
          createVNode(_component_ef_loading),
          createElementVNode("span", _hoisted_7$7, toDisplayString($props.loadingText), 1)
        ]))
      ], 512), [
        [vShow, $props.loading]
      ]),
      withDirectives(createElementVNode("div", _hoisted_8$6, [
        _ctx.$slots.nodata ? renderSlot(_ctx.$slots, "nodata", { key: 0 }) : (openBlock(), createElementBlock("span", _hoisted_9$3, "\u6682\u65E0\u6570\u636E"))
      ], 512), [
        [vShow, $data.ifMounted && !$props.loading && $props.dataSource.length < 1]
      ])
    ], 512)
  ], 6);
}
var table = /* @__PURE__ */ _export_sfc(_sfc_main$z, [["render", _sfc_render$p]]);
const tableObj = {
  install(app, options) {
    app.component(table.name, table);
  }
};
const obj$8 = {
  install(app, options) {
    app.component(EfIcon.name, EfIcon);
  }
};
const obj$7 = {
  install(app, options) {
    app.component(component$5.name, component$5);
  }
};
const obj$6 = {
  install(app, options) {
    app.component(checkbox.name, checkbox);
  }
};
const _sfc_main$y = {
  name: "ef-checkbox-group",
  emits: ["change", "update:modelValue"],
  components: {
    checkbox
  },
  inject: {
    efFormItem: {
      from: "efFormItem",
      default: void 0
    }
  },
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    options: {
      type: Array,
      default: (_) => []
    },
    modelValue: {
      type: Array,
      default: (_) => []
    }
  },
  watch: {
    modelValue(newVal) {
      this.dataValue = newVal;
    }
  },
  data() {
    return {
      dataValue: this.modelValue
    };
  },
  methods: {
    onChange(checked, value) {
      var _a;
      this.handleDataValue(checked, value);
      this.$emit("update:modelValue", this.dataValue);
      this.$emit("change", this.dataValue, checked, value);
      (_a = this == null ? void 0 : this.efFormItem) == null ? void 0 : _a.validate("change");
    },
    handleDataValue(checked, value) {
      if (checked) {
        this.dataValue.push(value);
      } else {
        let index = this.dataValue.indexOf(value);
        this.dataValue.splice(index, 1);
      }
    }
  },
  mounted() {
  },
  created() {
  }
};
const _hoisted_1$s = { class: "ef-checkbox-group-wrapper" };
const _hoisted_2$n = { key: 1 };
function _sfc_render$o(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_checkbox = resolveComponent("checkbox");
  return openBlock(), createElementBlock("div", _hoisted_1$s, [
    (openBlock(true), createElementBlock(Fragment, null, renderList($props.options, (item) => {
      return openBlock(), createElementBlock("div", {
        key: item.value,
        class: "ef-checkbox-wrapper"
      }, [
        createVNode(_component_checkbox, {
          onChange: $options.onChange,
          disabled: $props.disabled,
          dataValue: item.value,
          modelValue: $data.dataValue.includes(item.value)
        }, {
          default: withCtx(() => [
            _ctx.$slots.default ? renderSlot(_ctx.$slots, "default", {
              key: 0,
              option: item
            }) : (openBlock(), createElementBlock("span", _hoisted_2$n, toDisplayString(item.label), 1))
          ]),
          _: 2
        }, 1032, ["onChange", "disabled", "dataValue", "modelValue"])
      ]);
    }), 128))
  ]);
}
var component$3 = /* @__PURE__ */ _export_sfc(_sfc_main$y, [["render", _sfc_render$o]]);
const obj$5 = {
  install(app, options) {
    app.component(component$3.name, component$3);
  }
};
const obj$4 = {
  install(app, options) {
    app.component(component$4.name, component$4);
  }
};
const _sfc_main$x = {
  name: "ef-button",
  components: {
    EfIcon
  },
  emits: ["click"],
  props: {
    disabled: {
      type: Boolean,
      default: false
    },
    iconFontSize: {
      type: [Number, String],
      default: ""
    },
    type: {
      type: String,
      default: "default"
    },
    icon: {
      type: String,
      default: ""
    },
    shape: {
      type: String,
      default: ""
    },
    loading: {
      type: Boolean,
      default: false
    },
    size: {
      type: String,
      default: ""
    }
  },
  computed: {
    fontSize() {
      const fontSizeMap = {
        small: "14",
        middle: "14"
      };
      if (this.iconFontSize) {
        return parseInt(this.iconFontSize);
      }
      if (fontSizeMap[this.size]) {
        return fontSizeMap[this.size];
      }
      return "14";
    }
  },
  data() {
    return {};
  },
  methods: {
    onMousedown(e) {
      !this.disabled && e.currentTarget.classList.add("ef-button-click");
    },
    onMouseup(e) {
      !this.disabled && e.currentTarget.classList.remove("ef-button-click");
    },
    onClick(e) {
      if (this.disabled)
        return;
      this.$emit("click", e);
    }
  },
  mounted() {
  },
  created() {
  }
};
function _sfc_render$n(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return openBlock(), createElementBlock("button", {
    onMousedown: _cache[0] || (_cache[0] = ($event) => $options.onMousedown($event)),
    onMouseup: _cache[1] || (_cache[1] = ($event) => $options.onMouseup($event)),
    onClick: _cache[2] || (_cache[2] = (...args) => $options.onClick && $options.onClick(...args)),
    class: normalizeClass(["ef-button", {
      "ef-button-danger": $props.type == "danger",
      "ef-button-primary": $props.type == "primary",
      "ef-button-default": !$props.type || $props.type == "default",
      "ef-button-shape-circle": $props.shape == "circle",
      "ef-button-shape-round": $props.shape == "round",
      "ef-button-disabled": $props.disabled,
      "ef-button-loading": $props.loading,
      "ef-button-size-medium": $props.size == "middle",
      "ef-button-size-small": $props.size == "small"
    }])
  }, [
    $props.loading ? (openBlock(), createBlock(_component_ef_icon, {
      key: 0,
      fontSize: $options.fontSize,
      type: "icon-loading",
      spin: ""
    }, null, 8, ["fontSize"])) : $props.icon ? (openBlock(), createBlock(_component_ef_icon, {
      key: 1,
      fontSize: $options.fontSize,
      type: $props.icon
    }, null, 8, ["fontSize", "type"])) : createCommentVNode("", true),
    createElementVNode("div", {
      style: normalizeStyle([{ "text-align": "center" }, { marginLeft: $props.shape == "circle" ? "0px" : "5px" }])
    }, [
      renderSlot(_ctx.$slots, "default")
    ], 4)
  ], 34);
}
var EfButton = /* @__PURE__ */ _export_sfc(_sfc_main$x, [["render", _sfc_render$n]]);
const obj$3 = {
  install(app, options) {
    app.component(EfButton.name, EfButton);
  }
};
const _sfc_main$w = {
  name: "ef-mark",
  data() {
    return {
      visible: false,
      onDestroy: () => {
      }
    };
  },
  methods: {
    afterLeave() {
      this.onDestroy();
    },
    test() {
    }
  },
  mounted() {
  },
  created() {
  },
  unmounted() {
  }
};
const _hoisted_1$r = {
  key: 0,
  class: "ef-mark"
};
function _sfc_render$m(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createBlock(Transition, {
    name: "markbox-fade",
    onAfterLeave: $options.afterLeave
  }, {
    default: withCtx(() => [
      $data.visible ? (openBlock(), createElementBlock("div", _hoisted_1$r)) : createCommentVNode("", true)
    ]),
    _: 1
  }, 8, ["onAfterLeave"]);
}
var markVue = /* @__PURE__ */ _export_sfc(_sfc_main$w, [["render", _sfc_render$m]]);
const Mark = function(ops = {}) {
  const userDestroy = ops.onDestroy || function() {
  };
  const container = document.createElement("div");
  const vm = createVNode(markVue, null, null);
  render(vm, container);
  vm.component.data.onDestroy = () => {
    userDestroy();
    render(null, container);
  };
  vm.component.data.visible = true;
  document.body.appendChild(vm.el);
  return vm.component;
};
Mark.install = (app, opts) => {
  app.config.globalProperties.$mark = Mark;
};
const EfMark = Mark;
var freeGlobal = typeof global == "object" && global && global.Object === Object && global;
var freeGlobal$1 = freeGlobal;
var freeSelf = typeof self == "object" && self && self.Object === Object && self;
var root = freeGlobal$1 || freeSelf || Function("return this")();
var root$1 = root;
var Symbol$1 = root$1.Symbol;
var Symbol$2 = Symbol$1;
var objectProto$5 = Object.prototype;
var hasOwnProperty$4 = objectProto$5.hasOwnProperty;
var nativeObjectToString$1 = objectProto$5.toString;
var symToStringTag$1 = Symbol$2 ? Symbol$2.toStringTag : void 0;
function getRawTag(value) {
  var isOwn = hasOwnProperty$4.call(value, symToStringTag$1), tag = value[symToStringTag$1];
  try {
    value[symToStringTag$1] = void 0;
    var unmasked = true;
  } catch (e) {
  }
  var result = nativeObjectToString$1.call(value);
  if (unmasked) {
    if (isOwn) {
      value[symToStringTag$1] = tag;
    } else {
      delete value[symToStringTag$1];
    }
  }
  return result;
}
var objectProto$4 = Object.prototype;
var nativeObjectToString = objectProto$4.toString;
function objectToString(value) {
  return nativeObjectToString.call(value);
}
var nullTag = "[object Null]", undefinedTag = "[object Undefined]";
var symToStringTag = Symbol$2 ? Symbol$2.toStringTag : void 0;
function baseGetTag(value) {
  if (value == null) {
    return value === void 0 ? undefinedTag : nullTag;
  }
  return symToStringTag && symToStringTag in Object(value) ? getRawTag(value) : objectToString(value);
}
function isObjectLike(value) {
  return value != null && typeof value == "object";
}
var symbolTag = "[object Symbol]";
function isSymbol(value) {
  return typeof value == "symbol" || isObjectLike(value) && baseGetTag(value) == symbolTag;
}
function arrayMap(array4, iteratee) {
  var index = -1, length = array4 == null ? 0 : array4.length, result = Array(length);
  while (++index < length) {
    result[index] = iteratee(array4[index], index, array4);
  }
  return result;
}
var isArray = Array.isArray;
var isArray$1 = isArray;
var INFINITY$1 = 1 / 0;
var symbolProto = Symbol$2 ? Symbol$2.prototype : void 0, symbolToString = symbolProto ? symbolProto.toString : void 0;
function baseToString(value) {
  if (typeof value == "string") {
    return value;
  }
  if (isArray$1(value)) {
    return arrayMap(value, baseToString) + "";
  }
  if (isSymbol(value)) {
    return symbolToString ? symbolToString.call(value) : "";
  }
  var result = value + "";
  return result == "0" && 1 / value == -INFINITY$1 ? "-0" : result;
}
function isObject$1(value) {
  var type4 = typeof value;
  return value != null && (type4 == "object" || type4 == "function");
}
var asyncTag = "[object AsyncFunction]", funcTag = "[object Function]", genTag = "[object GeneratorFunction]", proxyTag = "[object Proxy]";
function isFunction(value) {
  if (!isObject$1(value)) {
    return false;
  }
  var tag = baseGetTag(value);
  return tag == funcTag || tag == genTag || tag == asyncTag || tag == proxyTag;
}
var coreJsData = root$1["__core-js_shared__"];
var coreJsData$1 = coreJsData;
var maskSrcKey = function() {
  var uid = /[^.]+$/.exec(coreJsData$1 && coreJsData$1.keys && coreJsData$1.keys.IE_PROTO || "");
  return uid ? "Symbol(src)_1." + uid : "";
}();
function isMasked(func) {
  return !!maskSrcKey && maskSrcKey in func;
}
var funcProto$1 = Function.prototype;
var funcToString$1 = funcProto$1.toString;
function toSource(func) {
  if (func != null) {
    try {
      return funcToString$1.call(func);
    } catch (e) {
    }
    try {
      return func + "";
    } catch (e) {
    }
  }
  return "";
}
var reRegExpChar = /[\\^$.*+?()[\]{}|]/g;
var reIsHostCtor = /^\[object .+?Constructor\]$/;
var funcProto = Function.prototype, objectProto$3 = Object.prototype;
var funcToString = funcProto.toString;
var hasOwnProperty$3 = objectProto$3.hasOwnProperty;
var reIsNative = RegExp("^" + funcToString.call(hasOwnProperty$3).replace(reRegExpChar, "\\$&").replace(/hasOwnProperty|(function).*?(?=\\\()| for .+?(?=\\\])/g, "$1.*?") + "$");
function baseIsNative(value) {
  if (!isObject$1(value) || isMasked(value)) {
    return false;
  }
  var pattern4 = isFunction(value) ? reIsNative : reIsHostCtor;
  return pattern4.test(toSource(value));
}
function getValue$1(object4, key) {
  return object4 == null ? void 0 : object4[key];
}
function getNative(object4, key) {
  var value = getValue$1(object4, key);
  return baseIsNative(value) ? value : void 0;
}
var defineProperty = function() {
  try {
    var func = getNative(Object, "defineProperty");
    func({}, "", {});
    return func;
  } catch (e) {
  }
}();
var defineProperty$1 = defineProperty;
var MAX_SAFE_INTEGER = 9007199254740991;
var reIsUint = /^(?:0|[1-9]\d*)$/;
function isIndex(value, length) {
  var type4 = typeof value;
  length = length == null ? MAX_SAFE_INTEGER : length;
  return !!length && (type4 == "number" || type4 != "symbol" && reIsUint.test(value)) && (value > -1 && value % 1 == 0 && value < length);
}
function baseAssignValue(object4, key, value) {
  if (key == "__proto__" && defineProperty$1) {
    defineProperty$1(object4, key, {
      "configurable": true,
      "enumerable": true,
      "value": value,
      "writable": true
    });
  } else {
    object4[key] = value;
  }
}
function eq(value, other) {
  return value === other || value !== value && other !== other;
}
var objectProto$2 = Object.prototype;
var hasOwnProperty$2 = objectProto$2.hasOwnProperty;
function assignValue(object4, key, value) {
  var objValue = object4[key];
  if (!(hasOwnProperty$2.call(object4, key) && eq(objValue, value)) || value === void 0 && !(key in object4)) {
    baseAssignValue(object4, key, value);
  }
}
var reIsDeepProp = /\.|\[(?:[^[\]]*|(["'])(?:(?!\1)[^\\]|\\.)*?\1)\]/, reIsPlainProp = /^\w*$/;
function isKey(value, object4) {
  if (isArray$1(value)) {
    return false;
  }
  var type4 = typeof value;
  if (type4 == "number" || type4 == "symbol" || type4 == "boolean" || value == null || isSymbol(value)) {
    return true;
  }
  return reIsPlainProp.test(value) || !reIsDeepProp.test(value) || object4 != null && value in Object(object4);
}
var nativeCreate = getNative(Object, "create");
var nativeCreate$1 = nativeCreate;
function hashClear() {
  this.__data__ = nativeCreate$1 ? nativeCreate$1(null) : {};
  this.size = 0;
}
function hashDelete(key) {
  var result = this.has(key) && delete this.__data__[key];
  this.size -= result ? 1 : 0;
  return result;
}
var HASH_UNDEFINED$1 = "__lodash_hash_undefined__";
var objectProto$1 = Object.prototype;
var hasOwnProperty$1 = objectProto$1.hasOwnProperty;
function hashGet(key) {
  var data = this.__data__;
  if (nativeCreate$1) {
    var result = data[key];
    return result === HASH_UNDEFINED$1 ? void 0 : result;
  }
  return hasOwnProperty$1.call(data, key) ? data[key] : void 0;
}
var objectProto = Object.prototype;
var hasOwnProperty = objectProto.hasOwnProperty;
function hashHas(key) {
  var data = this.__data__;
  return nativeCreate$1 ? data[key] !== void 0 : hasOwnProperty.call(data, key);
}
var HASH_UNDEFINED = "__lodash_hash_undefined__";
function hashSet(key, value) {
  var data = this.__data__;
  this.size += this.has(key) ? 0 : 1;
  data[key] = nativeCreate$1 && value === void 0 ? HASH_UNDEFINED : value;
  return this;
}
function Hash(entries2) {
  var index = -1, length = entries2 == null ? 0 : entries2.length;
  this.clear();
  while (++index < length) {
    var entry = entries2[index];
    this.set(entry[0], entry[1]);
  }
}
Hash.prototype.clear = hashClear;
Hash.prototype["delete"] = hashDelete;
Hash.prototype.get = hashGet;
Hash.prototype.has = hashHas;
Hash.prototype.set = hashSet;
function listCacheClear() {
  this.__data__ = [];
  this.size = 0;
}
function assocIndexOf(array4, key) {
  var length = array4.length;
  while (length--) {
    if (eq(array4[length][0], key)) {
      return length;
    }
  }
  return -1;
}
var arrayProto = Array.prototype;
var splice = arrayProto.splice;
function listCacheDelete(key) {
  var data = this.__data__, index = assocIndexOf(data, key);
  if (index < 0) {
    return false;
  }
  var lastIndex = data.length - 1;
  if (index == lastIndex) {
    data.pop();
  } else {
    splice.call(data, index, 1);
  }
  --this.size;
  return true;
}
function listCacheGet(key) {
  var data = this.__data__, index = assocIndexOf(data, key);
  return index < 0 ? void 0 : data[index][1];
}
function listCacheHas(key) {
  return assocIndexOf(this.__data__, key) > -1;
}
function listCacheSet(key, value) {
  var data = this.__data__, index = assocIndexOf(data, key);
  if (index < 0) {
    ++this.size;
    data.push([key, value]);
  } else {
    data[index][1] = value;
  }
  return this;
}
function ListCache(entries2) {
  var index = -1, length = entries2 == null ? 0 : entries2.length;
  this.clear();
  while (++index < length) {
    var entry = entries2[index];
    this.set(entry[0], entry[1]);
  }
}
ListCache.prototype.clear = listCacheClear;
ListCache.prototype["delete"] = listCacheDelete;
ListCache.prototype.get = listCacheGet;
ListCache.prototype.has = listCacheHas;
ListCache.prototype.set = listCacheSet;
var Map$1 = getNative(root$1, "Map");
var Map$2 = Map$1;
function mapCacheClear() {
  this.size = 0;
  this.__data__ = {
    "hash": new Hash(),
    "map": new (Map$2 || ListCache)(),
    "string": new Hash()
  };
}
function isKeyable(value) {
  var type4 = typeof value;
  return type4 == "string" || type4 == "number" || type4 == "symbol" || type4 == "boolean" ? value !== "__proto__" : value === null;
}
function getMapData(map, key) {
  var data = map.__data__;
  return isKeyable(key) ? data[typeof key == "string" ? "string" : "hash"] : data.map;
}
function mapCacheDelete(key) {
  var result = getMapData(this, key)["delete"](key);
  this.size -= result ? 1 : 0;
  return result;
}
function mapCacheGet(key) {
  return getMapData(this, key).get(key);
}
function mapCacheHas(key) {
  return getMapData(this, key).has(key);
}
function mapCacheSet(key, value) {
  var data = getMapData(this, key), size = data.size;
  data.set(key, value);
  this.size += data.size == size ? 0 : 1;
  return this;
}
function MapCache(entries2) {
  var index = -1, length = entries2 == null ? 0 : entries2.length;
  this.clear();
  while (++index < length) {
    var entry = entries2[index];
    this.set(entry[0], entry[1]);
  }
}
MapCache.prototype.clear = mapCacheClear;
MapCache.prototype["delete"] = mapCacheDelete;
MapCache.prototype.get = mapCacheGet;
MapCache.prototype.has = mapCacheHas;
MapCache.prototype.set = mapCacheSet;
var FUNC_ERROR_TEXT = "Expected a function";
function memoize(func, resolver) {
  if (typeof func != "function" || resolver != null && typeof resolver != "function") {
    throw new TypeError(FUNC_ERROR_TEXT);
  }
  var memoized = function() {
    var args = arguments, key = resolver ? resolver.apply(this, args) : args[0], cache = memoized.cache;
    if (cache.has(key)) {
      return cache.get(key);
    }
    var result = func.apply(this, args);
    memoized.cache = cache.set(key, result) || cache;
    return result;
  };
  memoized.cache = new (memoize.Cache || MapCache)();
  return memoized;
}
memoize.Cache = MapCache;
var MAX_MEMOIZE_SIZE = 500;
function memoizeCapped(func) {
  var result = memoize(func, function(key) {
    if (cache.size === MAX_MEMOIZE_SIZE) {
      cache.clear();
    }
    return key;
  });
  var cache = result.cache;
  return result;
}
var rePropName = /[^.[\]]+|\[(?:(-?\d+(?:\.\d+)?)|(["'])((?:(?!\2)[^\\]|\\.)*?)\2)\]|(?=(?:\.|\[\])(?:\.|\[\]|$))/g;
var reEscapeChar = /\\(\\)?/g;
var stringToPath = memoizeCapped(function(string3) {
  var result = [];
  if (string3.charCodeAt(0) === 46) {
    result.push("");
  }
  string3.replace(rePropName, function(match, number4, quote, subString) {
    result.push(quote ? subString.replace(reEscapeChar, "$1") : number4 || match);
  });
  return result;
});
var stringToPath$1 = stringToPath;
function toString(value) {
  return value == null ? "" : baseToString(value);
}
function castPath(value, object4) {
  if (isArray$1(value)) {
    return value;
  }
  return isKey(value, object4) ? [value] : stringToPath$1(toString(value));
}
var INFINITY = 1 / 0;
function toKey(value) {
  if (typeof value == "string" || isSymbol(value)) {
    return value;
  }
  var result = value + "";
  return result == "0" && 1 / value == -INFINITY ? "-0" : result;
}
function baseGet(object4, path) {
  path = castPath(path, object4);
  var index = 0, length = path.length;
  while (object4 != null && index < length) {
    object4 = object4[toKey(path[index++])];
  }
  return index && index == length ? object4 : void 0;
}
function get(object4, path, defaultValue) {
  var result = object4 == null ? void 0 : baseGet(object4, path);
  return result === void 0 ? defaultValue : result;
}
function castArray() {
  if (!arguments.length) {
    return [];
  }
  var value = arguments[0];
  return isArray$1(value) ? value : [value];
}
function baseSet(object4, path, value, customizer) {
  if (!isObject$1(object4)) {
    return object4;
  }
  path = castPath(path, object4);
  var index = -1, length = path.length, lastIndex = length - 1, nested = object4;
  while (nested != null && ++index < length) {
    var key = toKey(path[index]), newValue = value;
    if (key === "__proto__" || key === "constructor" || key === "prototype") {
      return object4;
    }
    if (index != lastIndex) {
      var objValue = nested[key];
      newValue = customizer ? customizer(objValue, key, nested) : void 0;
      if (newValue === void 0) {
        newValue = isObject$1(objValue) ? objValue : isIndex(path[index + 1]) ? [] : {};
      }
    }
    assignValue(nested, key, newValue);
    nested = nested[key];
  }
  return object4;
}
function set(object4, path, value) {
  return object4 == null ? object4 : baseSet(object4, path, value);
}
const addUnit = (val, unit = "px") => {
  if (/^[0-9]+$/.test(val)) {
    return val + unit;
  }
  return val;
};
const getObjValByPath = (obj2, path, defaultValue) => {
  return {
    get value() {
      return get(obj2, path, defaultValue);
    },
    set value(val) {
      set(obj2, path, val);
    }
  };
};
const getChildToParentTopDistance = (parent, child) => {
  let total = 0;
  while (child && child != parent) {
    total += child.offsetTop;
    child = child.parentNode;
  }
  return total;
};
const throttle = (func, wait, mustRun) => {
  let timeout, startTime = new Date();
  return function() {
    let curTime = new Date();
    timeout && clearTimeout(timeout);
    if (curTime - startTime >= mustRun) {
      func(arguments);
      startTime = curTime;
    } else {
      timeout = setTimeout((_) => {
        func(arguments);
      }, wait);
    }
  };
};
const getViewportRect = () => ({
  width: window.innerWidth || document.documentElement.clientWidth,
  height: window.innerHeight || document.documentElement.clientHeight
});
const isElementInViewport = (el) => {
  var rect = el.getBoundingClientRect();
  return rect.top >= 0 && rect.left >= 0 && rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && rect.right <= (window.innerWidth || document.documentElement.clientWidth);
};
const getDocumentScrollLeft = () => document.documentElement.scrollLeft || window.pageXOffset || document.body.scrollLeft;
const getDocumentScrollTop = () => document.documentElement.scrollTop || window.pageYOffset || document.body.scrollTop;
const isObject = (val) => Object.prototype.toString.call(val) === "[object Object]";
const isValidComponentSize = (val) => ["", "large", "medium", "small", "mini", "middle"].includes(val);
function entries(obj2) {
  return Object.keys(obj2).map((key) => [key, obj2[key]]);
}
function isPromise(obj2) {
  return !!obj2 && (typeof obj2 === "object" || typeof obj2 === "function") && typeof obj2.then === "function";
}
const _sfc_main$v = {
  name: "ef-modal",
  emits: ["update:visible"],
  components: {
    EfIcon,
    EfButton
  },
  computed: {
    bodyStyle() {
      let res = {};
      this.width && (res.width = this.width);
      return res;
    }
  },
  props: {
    onCancel: {
      type: Function,
      default: () => {
      }
    },
    onConfirm: {
      type: Function,
      default: () => {
      }
    },
    width: {
      type: [String, Number],
      default: ""
    },
    visible: {
      type: Boolean,
      default: false
    },
    title: {
      type: String,
      default: "TITLE"
    },
    showHeader: {
      type: Boolean,
      default: true
    },
    showFooter: {
      type: Boolean,
      default: true
    },
    showCloseIcon: {
      type: Boolean,
      default: true
    },
    showCancelButton: {
      type: Boolean,
      default: true
    },
    cancelText: {
      type: String,
      default: "\u53D6\u6D88"
    },
    confirmText: {
      type: String,
      default: "\u786E\u5B9A"
    },
    showMark: {
      type: Boolean,
      default: true
    },
    ifShowConfirmLoading: {
      type: Boolean,
      default: true
    },
    autoClose: {
      type: Boolean,
      default: true
    }
  },
  watch: {
    visible(newVal) {
      if (newVal) {
        this.showMark && (this.markInstance = EfMark());
        this.hiddenBodyScroll();
      } else {
        this.handleClose();
      }
    }
  },
  data() {
    return {
      markInstance: null,
      showConfirmLoading: false,
      hoverCloseIcon: false
    };
  },
  methods: {
    handleCancel() {
      typeof this.onCancel == "function" && this.onCancel(this);
      this.handleClose();
    },
    handleConfirm() {
      if (this.showConfirmLoading === true)
        return;
      if (typeof this.onConfirm == "function") {
        let res = this.onConfirm(this);
        if (res === false)
          return;
        if (isPromise(res)) {
          this.ifShowConfirmLoading && (this.showConfirmLoading = true);
          res.then((res2) => {
            if (res2 === false) {
              this.showConfirmLoading = false;
            } else {
              this.handleClose();
            }
          });
          return;
        }
      }
      this.handleClose();
    },
    handleClose() {
      this.$emit("update:visible", false);
      this.markInstance && (this.markInstance.data.visible = false);
      this.showConfirmLoading = false;
      this.showBodyScroll();
    },
    test() {
    },
    hiddenBodyScroll() {
      document.body.classList.add("overflow-hidden");
    },
    showBodyScroll() {
      document.body.classList.remove("overflow-hidden");
    }
  },
  mounted() {
  },
  created() {
  }
};
const _hoisted_1$q = { class: "ef-modal-wrapper" };
const _hoisted_2$m = {
  key: 0,
  class: "header flex-base"
};
const _hoisted_3$g = {
  key: 1,
  class: "title"
};
const _hoisted_4$c = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_5$7 = { class: "body" };
const _hoisted_6$6 = {
  key: 1,
  class: "footer"
};
const _hoisted_7$6 = {
  key: 1,
  class: "flex-base"
};
const _hoisted_8$5 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
function _sfc_render$l(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  const _component_ef_button = resolveComponent("ef-button");
  return openBlock(), createBlock(Transition, { name: "modalbox-fade" }, {
    default: withCtx(() => [
      withDirectives(createElementVNode("div", _hoisted_1$q, [
        createElementVNode("div", {
          style: normalizeStyle($options.bodyStyle),
          class: "inner pos-relative"
        }, [
          $props.showHeader ? (openBlock(), createElementBlock("div", _hoisted_2$m, [
            _ctx.$slots.header ? renderSlot(_ctx.$slots, "header", { key: 0 }) : (openBlock(), createElementBlock("span", _hoisted_3$g, toDisplayString($props.title), 1)),
            _hoisted_4$c,
            withDirectives(createVNode(_component_ef_icon, {
              onMouseleave: _cache[0] || (_cache[0] = ($event) => $data.hoverCloseIcon = false),
              onMouseenter: _cache[1] || (_cache[1] = ($event) => $data.hoverCloseIcon = true),
              style: { "margin-left": "10px", "cursor": "pointer" },
              color: $data.hoverCloseIcon ? "#404040" : "#cfd2d6",
              type: "icon-close",
              onClick: $options.handleClose
            }, null, 8, ["color", "onClick"]), [
              [vShow, $props.showCloseIcon]
            ])
          ])) : createCommentVNode("", true),
          createElementVNode("div", _hoisted_5$7, [
            renderSlot(_ctx.$slots, "default")
          ]),
          $props.showFooter ? (openBlock(), createElementBlock("div", _hoisted_6$6, [
            _ctx.$slots.footer ? renderSlot(_ctx.$slots, "footer", { key: 0 }) : (openBlock(), createElementBlock("div", _hoisted_7$6, [
              _hoisted_8$5,
              withDirectives(createVNode(_component_ef_button, {
                style: { "min-width": "70px" },
                onClick: $options.handleCancel,
                size: "small"
              }, {
                default: withCtx(() => [
                  createTextVNode(toDisplayString($props.cancelText), 1)
                ]),
                _: 1
              }, 8, ["onClick"]), [
                [vShow, $props.showCancelButton]
              ]),
              createVNode(_component_ef_button, {
                style: { "min-width": "70px" },
                loading: $data.showConfirmLoading,
                onClick: $options.handleConfirm,
                type: "primary",
                size: "small",
                class: "margin-left"
              }, {
                default: withCtx(() => [
                  createTextVNode(toDisplayString($props.confirmText), 1)
                ]),
                _: 1
              }, 8, ["loading", "onClick"])
            ]))
          ])) : createCommentVNode("", true)
        ], 4)
      ], 512), [
        [vShow, $props.visible]
      ])
    ]),
    _: 3
  });
}
var component$2 = /* @__PURE__ */ _export_sfc(_sfc_main$v, [["render", _sfc_render$l]]);
const obj$2 = {
  install(app, options) {
    app.component(component$2.name, component$2);
  }
};
var VNodes = {
  props: {
    vnodes: {
      type: Object,
      default: null
    }
  },
  setup(props2, {
    attrs,
    slots,
    emit
  }) {
    return () => h(props2.vnodes[0], {}, null);
  }
};
let EFzIndex = 2e3;
const getZIndex = () => {
  return ++EFzIndex;
};
function useComputeContentPosition({
  props: props2,
  contentVisible,
  emit
}) {
  const interval2 = toRef(props2, "interval");
  const triggerRef = ref(null);
  const contentRef = ref(null);
  const arrowRef = ref(null);
  const contentStyle = shallowRef({});
  const allowStyle = shallowRef({});
  const zIndex2 = getZIndex();
  const placement = ref(props2.placement);
  let placementBackup = props2.placement;
  let nextPlacementIndex = {
    value: 0
  };
  const resetPlacement = (_) => {
    placementBackup = props2.placement;
    placement.value = props2.placement;
    nextPlacementIndex = {
      value: 0
    };
  };
  const onResize = (_) => {
    computeContentStyle();
  };
  const onScroll = (_) => {
    computeContentStyle();
  };
  const computeContentStyle = () => {
    if (!contentVisible.value) {
      return;
    }
    computeContentPosition({
      triggerRef,
      contentRef,
      arrowRef,
      allowStyle,
      contentStyle,
      placement,
      interval: interval2,
      zIndex: zIndex2,
      placementBackup,
      nextPlacementIndex,
      resetPlacement
    });
  };
  computeContentStyle();
  watch(contentVisible, (newVal, oldVal) => {
    newVal && computeContentStyle();
  });
  watch((_) => props2.placement, (newVal, oldVal) => {
    resetPlacement();
  });
  watch(placement, (newVal, oldVal) => {
    computeContentStyle();
  });
  onMounted(() => {
    window.addEventListener("resize", onResize);
    window.addEventListener("scroll", onScroll);
  });
  onUnmounted(() => {
    window.removeEventListener("resize", onResize);
    window.removeEventListener("scroll", onScroll);
  });
  return {
    interval: interval2,
    triggerRef,
    contentRef,
    arrowRef,
    contentStyle,
    allowStyle,
    computeContentStyle,
    placement
  };
}
function computeContentPosition({
  triggerRef,
  contentRef,
  arrowRef,
  allowStyle,
  contentStyle,
  placement,
  interval: interval2,
  zIndex: zIndex2,
  placementBackup,
  nextPlacementIndex,
  resetPlacement
}) {
  nextTick(() => {
    let x, y, allowX, allowY;
    let contentRes = {};
    let allowRes = {};
    let inViewport = true;
    contentRef.value.getBoundingClientRect();
    const triggerRect = triggerRef.value.$el.getBoundingClientRect();
    const triggerComputedStyle = window.getComputedStyle(triggerRef.value.$el);
    const contentComputedStyle = window.getComputedStyle(contentRef.value);
    const arrowComputedStyle = window.getComputedStyle(arrowRef.value);
    const viewportRect = getViewportRect();
    const getInViewport = (placement2) => {
      let tempPlacement = placement2.split("-")[0];
      let inViewport2 = true;
      if (tempPlacement == "top") {
        inViewport2 = parseInt(contentComputedStyle.height) + interval2.value <= triggerRect.top;
      } else if (tempPlacement == "bottom") {
        inViewport2 = parseInt(contentComputedStyle.height) + interval2.value <= viewportRect.height - triggerRect.bottom;
      } else if (tempPlacement == "left") {
        inViewport2 = parseInt(contentComputedStyle.width) + interval2.value <= triggerRect.left;
      } else if (tempPlacement == "right") {
        inViewport2 = parseInt(contentComputedStyle.width) + interval2.value <= viewportRect.width - triggerRect.right;
      }
      return inViewport2;
    };
    if (getInViewport(placementBackup) && placementBackup != placement.value) {
      resetPlacement();
      return;
    }
    inViewport = getInViewport(placement.value);
    if (placement.value == "top") {
      x = triggerRect.left + getDocumentScrollLeft() + parseInt(triggerComputedStyle.width) / 2 - parseInt(contentComputedStyle.width) / 2;
      y = triggerRect.top + getDocumentScrollTop() - interval2.value - parseInt(contentComputedStyle.height);
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowX = parseInt(contentComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
      allowRes = {
        bottom: 0,
        left: 0,
        transform: `translate(${allowX}px, 50%) rotate(45deg)`
      };
    } else if (placement.value == "top-start") {
      x = triggerRect.left + getDocumentScrollLeft();
      y = triggerRect.top + getDocumentScrollTop() - interval2.value - parseInt(contentComputedStyle.height);
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
      allowRes = {
        bottom: 0,
        left: 0,
        transform: `translate(${allowX}px, 50%) rotate(45deg)`
      };
    } else if (placement.value == "top-end") {
      x = triggerRect.right + getDocumentScrollLeft() - parseInt(contentComputedStyle.width);
      y = triggerRect.top + getDocumentScrollTop() - interval2.value - parseInt(contentComputedStyle.height);
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
      allowRes = {
        bottom: 0,
        right: 0,
        transform: `translate(-${allowX}px, 50%) rotate(45deg)`
      };
    } else if (placement.value == "bottom") {
      x = triggerRect.left + getDocumentScrollLeft() + parseInt(triggerComputedStyle.width) / 2 - parseInt(contentComputedStyle.width) / 2;
      y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) + interval2.value;
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowX = parseInt(contentComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
      allowRes = {
        top: 0,
        left: 0,
        transform: `translate(${allowX}px, -50%) rotate(45deg)`
      };
    } else if (placement.value == "bottom-start") {
      x = triggerRect.left + getDocumentScrollLeft();
      y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) + interval2.value;
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
      allowRes = {
        top: 0,
        left: 0,
        transform: `translate(${allowX}px, -50%) rotate(45deg)`
      };
    } else if (placement.value == "bottom-end") {
      x = triggerRect.right + getDocumentScrollLeft() - parseInt(contentComputedStyle.width);
      y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) + interval2.value;
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowX = parseInt(triggerComputedStyle.width) / 2 - parseInt(arrowComputedStyle.width) / 2;
      allowRes = {
        top: 0,
        right: 0,
        transform: `translate(-${allowX}px, -50%) rotate(45deg)`
      };
    } else if (placement.value == "left") {
      x = triggerRect.left + getDocumentScrollLeft() - interval2.value - parseInt(contentComputedStyle.width);
      if (triggerRect.top <= viewportRect.height && triggerRect.top + parseInt(contentComputedStyle.height) / 2 + parseInt(triggerComputedStyle.height) / 2 >= viewportRect.height) {
        y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
        allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) + parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else if (triggerRect.top + parseInt(triggerComputedStyle.height) >= 0 && triggerRect.top + parseInt(triggerComputedStyle.height) / 2 <= parseInt(contentComputedStyle.height) / 2) {
        y = getDocumentScrollTop();
        allowY = triggerRect.top + parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else {
        y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) / 2 - parseInt(contentComputedStyle.height) / 2;
        allowY = parseInt(contentComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      }
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowRes = {
        top: 0,
        right: 0,
        transform: `translate(50%, ${allowY}px) rotate(45deg)`
      };
    } else if (placement.value == "left-start") {
      x = triggerRect.left + getDocumentScrollLeft() - interval2.value - parseInt(contentComputedStyle.width);
      if (triggerRect.top <= viewportRect.height && triggerRect.top + parseInt(contentComputedStyle.height) >= viewportRect.height) {
        y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
        allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) + parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else {
        y = triggerRect.top + getDocumentScrollTop();
        allowY = parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      }
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowRes = {
        top: 0,
        right: 0,
        transform: `translate(50%, ${allowY}px) rotate(45deg)`
      };
    } else if (placement.value == "left-end") {
      x = triggerRect.left + getDocumentScrollLeft() - interval2.value - parseInt(contentComputedStyle.width);
      if (triggerRect.top + parseInt(triggerComputedStyle.height) >= 0 && triggerRect.top + parseInt(triggerComputedStyle.height) <= parseInt(contentComputedStyle.height)) {
        y = getDocumentScrollTop();
        allowY = 0 - (parseInt(contentComputedStyle.height) - triggerRect.top - parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2);
      } else {
        y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) - parseInt(contentComputedStyle.height);
        allowY = 0 - parseInt(triggerComputedStyle.height) / 2 + parseInt(arrowComputedStyle.height) / 2;
      }
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowRes = {
        bottom: 0,
        right: 0,
        transform: `translate(50%, ${allowY}px) rotate(45deg)`
      };
    } else if (placement.value == "right") {
      x = triggerRect.right + getDocumentScrollLeft() + interval2.value;
      if (triggerRect.top <= viewportRect.height && triggerRect.top + parseInt(contentComputedStyle.height) / 2 + parseInt(triggerComputedStyle.height) / 2 >= viewportRect.height) {
        y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
        allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) + parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else if (triggerRect.top + parseInt(triggerComputedStyle.height) >= 0 && triggerRect.top + parseInt(triggerComputedStyle.height) / 2 <= parseInt(contentComputedStyle.height) / 2) {
        y = getDocumentScrollTop();
        allowY = triggerRect.top + parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else {
        y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) / 2 - parseInt(contentComputedStyle.height) / 2;
        allowY = parseInt(contentComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      }
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowRes = {
        top: 0,
        left: 0,
        transform: `translate(-50%, ${allowY}px) rotate(45deg)`
      };
    } else if (placement.value == "right-start") {
      x = triggerRect.right + getDocumentScrollLeft() + interval2.value;
      if (triggerRect.top <= viewportRect.height && triggerRect.top + parseInt(contentComputedStyle.height) >= viewportRect.height) {
        y = viewportRect.height - parseInt(contentComputedStyle.height) + getDocumentScrollTop();
        allowY = parseInt(contentComputedStyle.height) - (viewportRect.height - triggerRect.top) + parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else {
        y = triggerRect.top + getDocumentScrollTop();
        allowY = parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      }
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowRes = {
        top: 0,
        left: 0,
        transform: `translate(-50%, ${allowY}px) rotate(45deg)`
      };
    } else if (placement.value == "right-end") {
      x = triggerRect.right + getDocumentScrollLeft() + interval2.value;
      if (triggerRect.top + parseInt(triggerComputedStyle.height) >= 0 && triggerRect.top + parseInt(triggerComputedStyle.height) <= parseInt(contentComputedStyle.height)) {
        y = getDocumentScrollTop();
        allowY = parseInt(contentComputedStyle.height) - triggerRect.top - parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      } else {
        y = triggerRect.top + getDocumentScrollTop() + parseInt(triggerComputedStyle.height) - parseInt(contentComputedStyle.height);
        allowY = parseInt(triggerComputedStyle.height) / 2 - parseInt(arrowComputedStyle.height) / 2;
      }
      contentRes = {
        left: `${x}px`,
        top: `${y}px`,
        zIndex: zIndex2
      };
      allowRes = {
        bottom: 0,
        left: 0,
        transform: `translate(-50%, -${allowY}px) rotate(45deg)`
      };
    }
    if (isElementInViewport(triggerRef.value.$el)) {
      if (inViewport || nextPlacementIndex.value > 3) {
        allowStyle.value = allowRes;
        contentStyle.value = contentRes;
      } else {
        placement.value = getNextPlacemnet(placementBackup, nextPlacementIndex);
      }
    } else {
      allowStyle.value = allowRes;
      contentStyle.value = contentRes;
    }
  });
}
function getNextPlacemnet(placementBackup, nextPlacementIndex) {
  let placementMap = {
    top: ["bottom", "right", "left"],
    bottom: ["top", "right", "left"],
    left: ["right", "bottom", "top"],
    right: ["left", "bottom", "top"]
  };
  let tempArr = placementMap[placementBackup.split("-")[0]];
  if (tempArr && nextPlacementIndex.value < tempArr.length) {
    return tempArr[nextPlacementIndex.value++];
  }
  return placementBackup;
}
function useContentVisible({
  props: props2,
  emit
}) {
  let contentVisible = void 0;
  if (props2.visible === void 0) {
    contentVisible = ref(false);
  } else {
    contentVisible = computed({
      get() {
        return props2.visible;
      },
      set(newVal, oldVal) {
        emit("update:visible", newVal);
      }
    });
  }
  watch(contentVisible, (newVal, oldVal) => {
    emit("visibleChange", newVal);
  });
  let contentHideTimer = null;
  const delayHideTimes = toRef(props2, "delayHideTimes");
  const openDelayhideContent = () => {
    contentHideTimer = setTimeout(() => {
      contentVisible.value = false;
    }, delayHideTimes.value);
  };
  const clearContentHideTimer = () => {
    contentHideTimer && clearTimeout(contentHideTimer);
  };
  const immediateHideContent = () => {
    contentVisible.value = false;
  };
  const showContent = () => {
    clearContentHideTimer();
    contentVisible.value = true;
  };
  const toggleContent = () => {
    contentVisible.value = !contentVisible.value;
  };
  const bodyClickEvent = (e) => {
    if (!["manual", "hover"].includes(props2.trigger)) {
      immediateHideContent();
    }
  };
  onMounted(() => {
    document.body.addEventListener("click", bodyClickEvent);
  });
  onUnmounted(() => {
    clearContentHideTimer();
    document.body.removeEventListener("click", bodyClickEvent);
  });
  return {
    contentVisible,
    openDelayhideContent,
    clearContentHideTimer,
    immediateHideContent,
    showContent,
    toggleContent
  };
}
const props$4 = {
  trigger: {
    type: String,
    default: "hover",
    validator: (val) => ["", "click", "hover", "manual"].includes(val)
  },
  placement: {
    type: String,
    default: "top",
    validator: (val) => [
      "",
      "top",
      "top-start",
      "top-end",
      "bottom",
      "bottom-start",
      "bottom-end",
      "left",
      "left-start",
      "left-end",
      "right",
      "right-start",
      "right-end"
    ].includes(val)
  },
  delayHideTimes: {
    type: Number,
    default: 200
  },
  transitionName: {
    type: String,
    default: "fade-in-linear",
    validator: (val) => ["", "fade-in-linear", "zoom-in"].includes(val)
  },
  theme: {
    type: String,
    default: "light",
    validator: (val) => ["", "dark", "light"].includes(val)
  },
  visible: {
    type: Boolean,
    default: void 0
  },
  showArrow: {
    type: Boolean,
    default: true
  },
  closeAllOnClick: {
    type: Boolean,
    default: true
  },
  interval: {
    type: Number,
    default: 15
  },
  disabled: Boolean
};
const _sfc_main$u = {
  name: "ef-popper",
  emits: ["update:visible", "visibleChange"],
  props: props$4,
  components: {
    VNode: VNodes
  },
  setup(props2, { attrs, slots, emit }) {
    const {
      contentVisible,
      openDelayhideContent,
      clearContentHideTimer,
      immediateHideContent,
      showContent,
      toggleContent
    } = useContentVisible({ props: props2, emit });
    const {
      interval: interval2,
      triggerRef,
      contentRef,
      arrowRef,
      contentStyle,
      allowStyle,
      computeContentStyle,
      placement: componentPlacement
    } = useComputeContentPosition({ props: props2, contentVisible, emit });
    const onTriggerClick = (e) => {
      if (props2.disabled)
        return;
      props2.closeAllOnClick && document.body.click();
      if (!["manual", "click"].includes(props2.trigger))
        return;
      toggleContent();
    };
    const onTriggerMouseenter = (e) => {
      if (props2.disabled)
        return;
      if (props2.trigger != "hover")
        return;
      showContent();
    };
    const onTriggerMouseleave = (e) => {
      if (props2.disabled)
        return;
      if (props2.trigger != "hover")
        return;
      openDelayhideContent();
    };
    const onContentClick = (e) => {
    };
    const onContentMouseenter = (e) => {
      clearContentHideTimer();
    };
    const onContentMouseleave = (e) => {
      if (props2.trigger != "hover")
        return;
      openDelayhideContent();
    };
    return {
      triggerRef,
      contentRef,
      contentStyle,
      allowStyle,
      contentVisible,
      arrowRef,
      componentPlacement,
      onTriggerClick,
      onTriggerMouseenter,
      onTriggerMouseleave,
      onContentMouseenter,
      onContentMouseleave,
      onContentClick,
      openDelayhideContent,
      immediateHideContent,
      showContent,
      toggleContent,
      computeContentStyle
    };
  }
};
const _hoisted_1$p = ["data-popper-placement", "data-theme"];
const _hoisted_2$l = { class: "content translate3d-0" };
const _hoisted_3$f = ["data-theme"];
function _sfc_render$k(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_node = resolveComponent("v-node");
  return openBlock(), createElementBlock(Fragment, null, [
    createVNode(_component_v_node, mergeProps(_ctx.$attrs, {
      ref: "triggerRef",
      onClick: withModifiers($setup.onTriggerClick, ["stop"]),
      onMouseenter: $setup.onTriggerMouseenter,
      onMouseleave: $setup.onTriggerMouseleave,
      vnodes: _ctx.$slots.trigger()
    }), null, 16, ["onClick", "onMouseenter", "onMouseleave", "vnodes"]),
    (openBlock(), createBlock(Teleport, { to: "body" }, [
      createVNode(Transition, {
        name: "ef-" + _ctx.transitionName
      }, {
        default: withCtx(() => [
          withDirectives(createElementVNode("div", {
            class: "ef-popper-content-wrapper",
            ref: "contentRef",
            onClick: _cache[0] || (_cache[0] = withModifiers((...args) => $setup.onContentClick && $setup.onContentClick(...args), ["stop"])),
            onMouseenter: _cache[1] || (_cache[1] = (...args) => $setup.onContentMouseenter && $setup.onContentMouseenter(...args)),
            onMouseleave: _cache[2] || (_cache[2] = (...args) => $setup.onContentMouseleave && $setup.onContentMouseleave(...args)),
            style: normalizeStyle($setup.contentStyle),
            "data-popper-placement": $setup.componentPlacement,
            "data-theme": _ctx.theme
          }, [
            createElementVNode("div", _hoisted_2$l, [
              renderSlot(_ctx.$slots, "default")
            ]),
            withDirectives(createElementVNode("div", {
              class: normalizeClass(["arrow", $setup.componentPlacement]),
              style: normalizeStyle($setup.allowStyle),
              ref: "arrowRef",
              "data-theme": _ctx.theme
            }, null, 14, _hoisted_3$f), [
              [vShow, _ctx.showArrow]
            ])
          ], 44, _hoisted_1$p), [
            [vShow, $setup.contentVisible]
          ])
        ]),
        _: 3
      }, 8, ["name"])
    ]))
  ], 64);
}
var EfPopper = /* @__PURE__ */ _export_sfc(_sfc_main$u, [["render", _sfc_render$k]]);
const props$3 = {
  ...props$4,
  content: {
    type: String,
    default: "this is tooltips,this is tooltips,this is tooltips"
  },
  width: {
    type: [Number, String],
    default: void 0
  },
  theme: {
    type: String,
    default: "dark"
  }
};
const _sfc_main$t = {
  name: "ef-tooltip",
  components: {
    EfPopper,
    VNode: VNodes
  },
  props: props$3,
  setup(props2, { attrs, slots, emit }) {
    const tooltipStyle = shallowRef({});
    const efPopper = ref(null);
    const tooltipComputedStyle = computed(() => {
      let res = {
        ...tooltipStyle.value
      };
      if (props2.width != void 0) {
        res.width = parseInt(props2.width) + "px";
      }
      return res;
    });
    return {
      tooltipComputedStyle,
      efPopper
    };
  }
};
const _hoisted_1$o = ["data-theme"];
const _hoisted_2$k = { key: 0 };
const _hoisted_3$e = {
  key: 1,
  class: "ef-tooltip-content"
};
function _sfc_render$j(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_node = resolveComponent("v-node");
  const _component_ef_popper = resolveComponent("ef-popper");
  return openBlock(), createBlock(_component_ef_popper, mergeProps({ ref: "efPopper" }, _ctx.$props), {
    trigger: withCtx(() => [
      createVNode(_component_v_node, {
        vnodes: _ctx.$slots.default()
      }, null, 8, ["vnodes"])
    ]),
    default: withCtx(() => [
      createElementVNode("div", {
        "data-theme": _ctx.theme,
        class: "ef-tooltip-wrapper",
        style: normalizeStyle($setup.tooltipComputedStyle)
      }, [
        _ctx.$slots.content ? (openBlock(), createElementBlock("div", _hoisted_2$k, [
          renderSlot(_ctx.$slots, "content")
        ])) : (openBlock(), createElementBlock("p", _hoisted_3$e, toDisplayString(_ctx.content), 1))
      ], 12, _hoisted_1$o)
    ]),
    _: 3
  }, 16);
}
var Tooltip = /* @__PURE__ */ _export_sfc(_sfc_main$t, [["render", _sfc_render$j]]);
const _sfc_main$s = {
  name: "ef-menu",
  emits: ["change"],
  components: {
    EfIcon,
    EfPopper,
    EfTooltip: Tooltip
  },
  props: {
    popperInterval: {
      type: Number,
      default: 2
    },
    delayCloseTimes: {
      type: Number,
      default: 100
    },
    collapsePlacement: {
      type: String,
      default: "right"
    },
    collapseTransitionName: {
      type: String,
      default: "zoom-in"
    },
    collapseVertical: {
      type: Boolean,
      default: false
    },
    liHeight: {
      type: [Number, String],
      default: 50
    },
    menuItemPaddingLeft: {
      type: [String, Number],
      default: "20px"
    },
    collapse: {
      type: Boolean,
      default: false
    },
    role: {
      type: String,
      default: ""
    },
    route: {
      type: Boolean,
      default: true
    },
    subLevel: {
      type: Number,
      default: 1
    },
    isSubMenu: {
      type: Boolean,
      default: false
    },
    menuList: {
      type: Array,
      default: []
    }
  },
  computed: {
    muneItemStyle() {
      let styleObj = {
        height: parseInt(this.liHeight) + "px",
        lineHeight: parseInt(this.liHeight) + "px",
        paddingLeft: (this.subLevel - 0) * parseInt(this.menuItemPaddingLeft) + "px"
      };
      if (this.collapseVertical && this.subLevel > 1) {
        styleObj.paddingLeft = "20px";
      }
      if (this.collapseVertical && this.subLevel == 1) {
        styleObj.paddingRight = "20px";
      }
      return styleObj;
    }
  },
  watch: {
    menuList: {
      handler(newVal, oldVal) {
        if (this.subLevel == 1) {
          this.menuInfo.cloneMenuList = newVal || [];
          this.menuInfo.firstParent = this;
          this.handleMenuList(newVal);
          this.$nextTick((_) => {
            this.createMenuLevelMap(this.menuInfo.cloneMenuList);
          });
        }
      },
      immediate: true
    },
    collapseVertical(newVal, oldVal) {
      this.handleCollapseVerticalChange(newVal);
    }
  },
  provide() {
    let res = this.subLevel == 1 ? { menuData: this.menuInfo } : {};
    return res;
  },
  inject: {
    menuData: {
      default: () => ({
        key: "",
        cloneMenuList: [],
        menuLevelMap: {},
        firstParent: null,
        keyLevelMap: {},
        closeAllPopperTimer: null
      })
    }
  },
  data() {
    return {
      menuInfo: this.menuData
    };
  },
  methods: {
    setEfPopperRef(el) {
    },
    onMouseleaveForCollapseMenu(menu) {
      menu.popperClose();
      let path = this.findParentPath(this.menuInfo.cloneMenuList, menu.key);
      if (path.length > 0) {
        path.forEach((menu2) => {
          menu2.popperClose();
        });
      }
    },
    onMouseenterForCollapseMenu(menu) {
      menu.popperClearClose();
      let path = this.findParentPath(this.menuInfo.cloneMenuList, menu.key);
      if (path.length > 0) {
        path.forEach((menu2) => {
          menu2.poppervisible = true;
          menu2.popperClearClose();
        });
      }
    },
    onMouseenterForCollapseLi(menu) {
      menu.poppervisible = true;
      menu.popperClearClose();
    },
    selectMenu(key) {
      this.$nextTick((_) => {
        if (Object.prototype.toString.call(key) == "[object Object]") {
          key = key.key;
        }
        this.menuInfo.key = key;
        if (this.collapseVertical) {
          return;
        }
        let p = this.findParentPath(this.menuInfo.cloneMenuList, key);
        if (p != null) {
          let isChild = p.length > 0 ? true : false;
          this.openSubMenu(String(key), true, isChild);
          p.forEach((menu) => {
            if (menu) {
              menu.showSubMenu = true;
            }
          });
        }
      });
    },
    openSubMenu(key, isOpen = true, isChild = false) {
      if (isOpen && this.collapse) {
        let currentLevel = isChild ? this.menuInfo.keyLevelMap[key] - 1 : this.menuInfo.keyLevelMap[key];
        this.closeSameLevelMenu(currentLevel, [key]);
      }
      this.$nextTick((_) => {
        let p = this.findParentPath(this.menuInfo.cloneMenuList, key, true);
        if (Array.isArray(p)) {
          let firstParentEl = this.menuInfo.firstParent.$el;
          for (let i = p.length - 1; i >= 0; i--) {
            let menu = p[i];
            if (!menu.hasOwnProperty("children") || menu.children.length < 1) {
              continue;
            }
            let subMenuDom = firstParentEl.querySelector(`ul[role=ef-sub-menu-${menu.key}]`);
            let nextMenuDom = subMenuDom;
            if (!menu.showSubMenu) {
              nextMenuDom.style.height = "0px";
            } else {
              let cloneDom = nextMenuDom.cloneNode(true);
              cloneDom.style.visibility = "hidden";
              cloneDom.style.position = "absolute";
              cloneDom.style.height = "auto";
              firstParentEl.appendChild(cloneDom);
              let height = cloneDom.offsetHeight;
              nextMenuDom.style.height = height + "px";
              cloneDom.remove();
            }
          }
        }
      });
    },
    onChildChange(currentKey, menu) {
      this.$emit("change", currentKey, menu);
    },
    onClickMenuItem(menu, index) {
      this.menuInfo.key = menu.key;
      !this.collapseVertical && this.collapse && this.openSubMenu(menu.key, true, false);
      this.$emit("change", this.menuInfo.key, menu);
      if (this.route && menu.fullRoutePath && this.$router) {
        this.$router.push({
          path: menu.fullRoutePath
        });
      }
      if (this.collapseVertical) {
        let path = this.findParentPath(this.menuInfo.cloneMenuList, menu.key);
        if (path.length > 0) {
          path.forEach((menu2) => {
            menu2.poppervisible = false;
          });
        }
      }
    },
    onToggleSubmenu(menu, index, e) {
      if (!menu.hasOwnProperty("showSubMenu")) {
        menu.showSubMenu = false;
      }
      menu.showSubMenu = !menu.showSubMenu;
      let isOpen = menu.showSubMenu ? true : false;
      this.openSubMenu(menu.key, isOpen);
    },
    closeSameLevelMenu(level = 1, exclude = []) {
      let closeMenus = [];
      if (closeMenus = this.menuInfo.menuLevelMap[level]) {
        let firstParentEl = this.menuInfo.firstParent.$el;
        closeMenus.forEach((item) => {
          if (!exclude.includes(item.key)) {
            if (item.children && item.children.length > 0) {
              item.showSubMenu = false;
              this.$nextTick((_) => {
                let subMenuDom = firstParentEl.querySelector(`ul[role=ef-sub-menu-${item.key}]`);
                subMenuDom && subMenuDom.style && (subMenuDom.style.height = "0px");
              });
            }
          }
        });
      }
    },
    closeAllMenu(menuList = void 0) {
      if (!menuList) {
        menuList = this.menuList;
      }
      menuList.forEach((menu) => {
        if (menu.children && menu.children.length > 0) {
          menu.showSubMenu = false;
          this.closeAllMenu(menu.children);
        }
      });
    },
    handleCollapseVerticalChange(val) {
      if (!val) {
        this.menuInfo.key && this.selectMenu(this.menuInfo.key);
      } else {
        this.closeAllMenu();
      }
    },
    handleMenuList(menuList = []) {
      menuList.forEach((menu) => {
        if (menu.children && menu.children.length > 0) {
          menu.poppervisible = false;
          menu.popperClose = () => {
            menu.popperCloseTimer && clearTimeout(menu.popperCloseTimer);
            menu.popperCloseTimer = setTimeout((_) => {
              menu.poppervisible = false;
            }, this.delayCloseTimes);
          };
          menu.popperClearClose = () => {
            menu.popperCloseTimer && clearTimeout(menu.popperCloseTimer);
          };
          this.handleMenuList(menu.children);
        }
      });
    },
    closeAllPopper() {
      if (this.collapseVertical) {
        this.menuInfo.closeAllPopperTimer && clearTimeout(this.menuInfo.closeAllPopperTimer);
        this.menuInfo.closeAllPopperTimer = setTimeout((_) => {
          this.menuList.forEach((menu) => {
            if (menu.children && menu.children.length > 0) {
              menu.popperClose && menu.popperClose();
            }
          });
        }, this.delayCloseTimes);
      }
    },
    clearCloseAllPopperTimer() {
      this.menuInfo.closeAllPopperTimer && clearTimeout(this.menuInfo.closeAllPopperTimer);
    },
    findFirstParent() {
      let parent = this;
      while (parent.$parent && parent.$parent.$options.name == "ef-menu") {
        parent = parent.$parent;
      }
      return parent;
    },
    findParentPath(list, keyCopy, includeSelf = false) {
      for (let i = 0; i < list.length; i++) {
        let menu = list[i];
        let parent = includeSelf ? [menu] : [];
        if (menu.key == keyCopy) {
          return parent;
        } else {
          if (Array.isArray(menu.children) && menu.children.length > 0) {
            !includeSelf && parent.push(menu);
            let deepRes = this.findParentPath(menu.children, keyCopy, includeSelf);
            if (deepRes != null) {
              return [...parent, ...deepRes];
            }
          }
        }
      }
      return null;
    },
    test() {
    },
    createMenuLevelMap(list = [], currentLevel = 1) {
      list.forEach((item) => {
        let key = item.key;
        if (!this.menuInfo.menuLevelMap.hasOwnProperty(currentLevel)) {
          this.menuInfo.menuLevelMap[currentLevel] = [];
        }
        this.menuInfo.menuLevelMap[currentLevel].push(item);
        this.menuInfo.keyLevelMap[key] = currentLevel;
        if (Array.isArray(item.children)) {
          this.createMenuLevelMap(item.children, currentLevel + 1);
        }
      });
    }
  },
  mounted() {
  },
  created() {
  }
};
const _hoisted_1$n = ["role"];
const _hoisted_2$j = ["onMouseenter", "onMouseleave"];
const _hoisted_3$d = {
  key: 1,
  class: "ef-menu-li ef-menu-sub-li"
};
const _hoisted_4$b = ["onClick"];
const _hoisted_5$6 = { class: "ef-menu-li-label" };
const _hoisted_6$5 = ["onClick"];
const _hoisted_7$5 = ["onClick"];
const _hoisted_8$4 = { class: "ef-menu-li-label" };
function _sfc_render$i(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  const _component_ef_menu = resolveComponent("ef-menu");
  const _component_ef_popper = resolveComponent("ef-popper");
  const _component_ef_tooltip = resolveComponent("ef-tooltip");
  return openBlock(), createElementBlock("ul", {
    role: $props.role,
    class: normalizeClass({ "ef-menu": true, "ef-sub-menu": $props.isSubMenu })
  }, [
    (openBlock(true), createElementBlock(Fragment, null, renderList($props.menuList, (menu, index) => {
      return openBlock(), createElementBlock(Fragment, null, [
        Array.isArray(menu.children) && menu.children.length > 0 ? (openBlock(), createElementBlock(Fragment, { key: 0 }, [
          $props.collapseVertical ? (openBlock(), createBlock(_component_ef_popper, {
            key: 0,
            placement: $props.collapsePlacement + "-start",
            trigger: "manual",
            delayHideTimes: $props.delayCloseTimes,
            transitionName: $props.collapseTransitionName,
            theme: "light",
            showArrow: false,
            interval: $props.popperInterval,
            visible: menu.poppervisible,
            "onUpdate:visible": ($event) => menu.poppervisible = $event
          }, {
            trigger: withCtx(() => [
              createElementVNode("li", {
                class: "ef-menu-li ef-menu-sub-li",
                onMouseenter: ($event) => $options.onMouseenterForCollapseLi(menu),
                onMouseleave: ($event) => menu.popperClose()
              }, [
                createElementVNode("div", {
                  style: normalizeStyle($options.muneItemStyle),
                  class: "ef-sub-menu-title"
                }, [
                  menu.customIconClass ? (openBlock(), createElementBlock("i", {
                    key: 0,
                    class: normalizeClass(["ef-menu-icon", menu.customIconClass])
                  }, null, 2)) : menu.icon ? (openBlock(), createBlock(_component_ef_icon, {
                    key: 1,
                    class: "ef-menu-icon",
                    type: menu.icon
                  }, null, 8, ["type"])) : createCommentVNode("", true),
                  withDirectives(createElementVNode("span", { class: "ef-menu-li-label" }, toDisplayString(menu.label), 513), [
                    [vShow, $props.subLevel != 1]
                  ]),
                  withDirectives(createElementVNode("span", {
                    class: normalizeClass(["ef-icon-arrow-wrapper", { "icon-pos": !menu.showSubMenu }])
                  }, [
                    createVNode(_component_ef_icon, {
                      class: "ef-icon-arrow",
                      type: "icon-arrow-up",
                      rotate: 90
                    })
                  ], 2), [
                    [vShow, $props.subLevel != 1]
                  ])
                ], 4)
              ], 40, _hoisted_2$j)
            ]),
            default: withCtx(() => [
              createVNode(_component_ef_menu, {
                onMouseenter: ($event) => $options.onMouseenterForCollapseMenu(menu),
                onMouseleave: ($event) => $options.onMouseleaveForCollapseMenu(menu),
                collapseTransitionName: $props.collapseTransitionName,
                collapsePlacement: $props.collapsePlacement,
                collapseVertical: $props.collapseVertical,
                collapse: $props.collapse,
                role: "ef-sub-menu-" + menu.key,
                onChange: $options.onChildChange,
                subLevel: $props.subLevel + 1,
                menuList: menu.children,
                isSubMenu: true,
                menuItemPaddingLeft: $props.menuItemPaddingLeft,
                liHeight: $props.liHeight,
                route: $props.route,
                class: normalizeClass({ "ef-sub-menu-close": !menu.showSubMenu, "ef-sub-menu-open": menu.showSubMenu })
              }, null, 8, ["onMouseenter", "onMouseleave", "collapseTransitionName", "collapsePlacement", "collapseVertical", "collapse", "role", "onChange", "subLevel", "menuList", "menuItemPaddingLeft", "liHeight", "route", "class"])
            ]),
            _: 2
          }, 1032, ["placement", "delayHideTimes", "transitionName", "interval", "visible", "onUpdate:visible"])) : (openBlock(), createElementBlock("li", _hoisted_3$d, [
            createElementVNode("div", {
              style: normalizeStyle($options.muneItemStyle),
              class: "ef-sub-menu-title",
              onClick: withModifiers(($event) => $options.onToggleSubmenu(menu, index, $event), ["stop"])
            }, [
              menu.customIconClass ? (openBlock(), createElementBlock("i", {
                key: 0,
                class: normalizeClass(["ef-menu-icon", menu.customIconClass])
              }, null, 2)) : menu.icon ? (openBlock(), createBlock(_component_ef_icon, {
                key: 1,
                class: "ef-menu-icon",
                type: menu.icon
              }, null, 8, ["type"])) : createCommentVNode("", true),
              createElementVNode("span", _hoisted_5$6, toDisplayString(menu.label), 1),
              createElementVNode("span", {
                class: normalizeClass(["ef-icon-arrow-wrapper", { "icon-pos": !menu.showSubMenu }])
              }, [
                createVNode(_component_ef_icon, {
                  class: "ef-icon-arrow",
                  type: "icon-arrow-up",
                  rotate: menu.showSubMenu ? 0 : 180
                }, null, 8, ["rotate"])
              ], 2)
            ], 12, _hoisted_4$b),
            createVNode(_component_ef_menu, {
              collapseTransitionName: $props.collapseTransitionName,
              collapsePlacement: $props.collapsePlacement,
              collapseVertical: $props.collapseVertical,
              collapse: $props.collapse,
              role: "ef-sub-menu-" + menu.key,
              onChange: $options.onChildChange,
              subLevel: $props.subLevel + 1,
              menuList: menu.children,
              isSubMenu: true,
              menuItemPaddingLeft: $props.menuItemPaddingLeft,
              liHeight: $props.liHeight,
              route: $props.route,
              class: normalizeClass({ "ef-sub-menu-close": !menu.showSubMenu, "ef-sub-menu-open": menu.showSubMenu })
            }, null, 8, ["collapseTransitionName", "collapsePlacement", "collapseVertical", "collapse", "role", "onChange", "subLevel", "menuList", "menuItemPaddingLeft", "liHeight", "route", "class"])
          ]))
        ], 64)) : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
          $props.collapseVertical && $props.subLevel == 1 ? (openBlock(), createBlock(_component_ef_tooltip, {
            key: 0,
            placement: $props.collapsePlacement,
            content: menu.label,
            theme: "dark"
          }, {
            default: withCtx(() => [
              (openBlock(), createElementBlock("li", {
                onClick: withModifiers(($event) => $options.onClickMenuItem(menu, index), ["stop"]),
                style: normalizeStyle([$options.muneItemStyle, { "padding-right": "20px" }]),
                class: normalizeClass(["ef-menu-li", { active: menu.key == $data.menuInfo.key }]),
                key: menu.key || index
              }, [
                menu.customIconClass ? (openBlock(), createElementBlock("i", {
                  key: 0,
                  class: normalizeClass(["ef-menu-icon", menu.customIconClass])
                }, null, 2)) : menu.icon ? (openBlock(), createBlock(_component_ef_icon, {
                  key: 1,
                  class: "ef-menu-icon",
                  type: menu.icon
                }, null, 8, ["type"])) : createCommentVNode("", true)
              ], 14, _hoisted_6$5))
            ]),
            _: 2
          }, 1032, ["placement", "content"])) : (openBlock(), createElementBlock("li", {
            onClick: withModifiers(($event) => $options.onClickMenuItem(menu, index), ["stop"]),
            style: normalizeStyle($options.muneItemStyle),
            class: normalizeClass(["ef-menu-li", { active: menu.key == $data.menuInfo.key, notNollapseVertical: !$props.collapseVertical }]),
            key: menu.key || index
          }, [
            menu.customIconClass ? (openBlock(), createElementBlock("i", {
              key: 0,
              class: normalizeClass(["ef-menu-icon", menu.customIconClass])
            }, null, 2)) : menu.icon ? (openBlock(), createBlock(_component_ef_icon, {
              key: 1,
              class: "ef-menu-icon",
              type: menu.icon
            }, null, 8, ["type"])) : createCommentVNode("", true),
            createElementVNode("span", _hoisted_8$4, toDisplayString(menu.label), 1)
          ], 14, _hoisted_7$5))
        ], 64))
      ], 64);
    }), 256))
  ], 10, _hoisted_1$n);
}
var component$1 = /* @__PURE__ */ _export_sfc(_sfc_main$s, [["render", _sfc_render$i]]);
const obj$1 = {
  install(app, options) {
    app.component(component$1.name, component$1);
  }
};
const _sfc_main$r = {
  name: "ef-step",
  components: {
    EfIcon
  },
  props: {
    steps: {
      type: Array,
      default: []
    },
    lineWidth: {
      type: Number,
      default: 60
    },
    circleWidth: {
      type: Number,
      default: 40
    }
  },
  data() {
    return {
      current: 0
    };
  },
  computed: {
    lastStepIndex() {
      return this.steps.length - 1;
    },
    circleWidthStyle() {
      return {
        width: this.circleWidth + "px",
        height: this.circleWidth + "px"
      };
    }
  },
  methods: {
    onClickItem(index) {
      if (this.getStatus(index) == "wait") {
        return;
      } else {
        this.current = index;
      }
    },
    to(current) {
      let next = current;
      if (current > this.lastStepIndex + 1) {
        next = this.lastStepIndex + 1;
      } else if (current < 0) {
        next = 0;
      }
      this.current = next;
    },
    next() {
      let next;
      if (this.current + 1 > this.lastStepIndex + 1) {
        next = this.lastStepIndex + 1;
      } else {
        next = this.current + 1;
      }
      this.current = next;
    },
    prev() {
      if (this.current - 1 < 0) {
        this.current = 0;
      } else {
        this.current--;
      }
    },
    getStatus(index) {
      if (index == this.current) {
        return "current";
      } else if (index < this.current) {
        return "finish";
      } else {
        return "wait";
      }
    }
  },
  mounted() {
  },
  created() {
  }
};
const _hoisted_1$m = { class: "ef-steps-wrapper" };
const _hoisted_2$i = ["onClick"];
const _hoisted_3$c = ["onClick"];
const _hoisted_4$a = ["onClick"];
function _sfc_render$h(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return openBlock(), createElementBlock("div", _hoisted_1$m, [
    (openBlock(true), createElementBlock(Fragment, null, renderList($props.steps, (item, index) => {
      return openBlock(), createElementBlock("div", {
        key: index,
        class: normalizeClass(["step-item", $options.getStatus(index) == "wait" ? "step-item-wait" : ""])
      }, [
        $options.getStatus(index) == "finish" ? (openBlock(), createElementBlock("div", {
          key: 0,
          style: normalizeStyle($options.circleWidthStyle),
          onClick: ($event) => $options.onClickItem(index),
          class: "box box-finish"
        }, [
          createVNode(_component_ef_icon, {
            style: { "position": "relative", "left": "1px", "top": "1px" },
            type: "icon-seleted"
          })
        ], 12, _hoisted_2$i)) : (openBlock(), createElementBlock("span", {
          key: 1,
          style: normalizeStyle($options.circleWidthStyle),
          onClick: ($event) => $options.onClickItem(index),
          class: normalizeClass([$options.getStatus(index), "box"])
        }, toDisplayString(index + 1), 15, _hoisted_3$c)),
        createElementVNode("span", {
          onClick: ($event) => $options.onClickItem(index),
          class: "text"
        }, toDisplayString(item.title), 9, _hoisted_4$a),
        index != $options.lastStepIndex ? (openBlock(), createElementBlock("span", {
          key: 2,
          style: normalizeStyle({ width: $props.lineWidth + "px" }),
          class: normalizeClass(["line", { "ok": $options.getStatus(index + 1) != "wait" }])
        }, null, 6)) : createCommentVNode("", true)
      ], 2);
    }), 128))
  ]);
}
var component = /* @__PURE__ */ _export_sfc(_sfc_main$r, [["render", _sfc_render$h]]);
const obj = {
  install(app, options) {
    app.component(component.name, component);
  }
};
const useForm = () => {
  const formContext = inject("efForm", void 0);
  const formItemContext = inject("efFormItem", void 0);
  return {
    formContext,
    formItemContext
  };
};
const DEFAULT_EXCLUDE_KEYS = ["class", "style"];
const LISTENER_PREFIX = /^on[A-Z]/;
var useAttrs = (params = {}) => {
  const {
    excludeListeners = false,
    excludeKeys = []
  } = params;
  const instance = getCurrentInstance();
  const attrs = shallowRef({});
  const allExcludeKeys = excludeKeys.concat(DEFAULT_EXCLUDE_KEYS);
  instance.attrs = reactive(instance.attrs);
  watchEffect(() => {
    const res = entries(instance.attrs).reduce((acm, [key, val]) => {
      if (!allExcludeKeys.includes(key) && !(excludeListeners && LISTENER_PREFIX.test(key))) {
        acm[key] = val;
      }
      return acm;
    }, {});
    attrs.value = res;
  });
  return attrs;
};
let hiddenTextarea;
const HIDDEN_STYLE = `
  height:0 !important;
  visibility:hidden !important;
  overflow:hidden !important;
  position:absolute !important;
  z-index:-1000 !important;
  top:0 !important;
  right:0 !important;
`;
const CONTEXT_STYLE = [
  "letter-spacing",
  "line-height",
  "padding-top",
  "padding-bottom",
  "font-family",
  "font-weight",
  "font-size",
  "text-rendering",
  "text-transform",
  "width",
  "text-indent",
  "padding-left",
  "padding-right",
  "border-width",
  "box-sizing"
];
function calculateNodeStyling(targetElement) {
  const style = window.getComputedStyle(targetElement);
  const boxSizing = style.getPropertyValue("box-sizing");
  const paddingSize = parseFloat(style.getPropertyValue("padding-bottom")) + parseFloat(style.getPropertyValue("padding-top"));
  const borderSize = parseFloat(style.getPropertyValue("border-bottom-width")) + parseFloat(style.getPropertyValue("border-top-width"));
  const contextStyle = CONTEXT_STYLE.map((name) => `${name}:${style.getPropertyValue(name)}`).join(";");
  return {
    contextStyle,
    paddingSize,
    borderSize,
    boxSizing
  };
}
function calcTextareaHeight(targetElement, minRows = 1, maxRows = null) {
  if (!hiddenTextarea) {
    hiddenTextarea = document.createElement("textarea");
    document.body.appendChild(hiddenTextarea);
  }
  const {
    paddingSize,
    borderSize,
    boxSizing,
    contextStyle
  } = calculateNodeStyling(targetElement);
  hiddenTextarea.setAttribute("style", `${contextStyle};${HIDDEN_STYLE}`);
  hiddenTextarea.value = targetElement.value || targetElement.placeholder || "";
  let height = hiddenTextarea.scrollHeight;
  const result = {};
  if (boxSizing === "border-box") {
    height = height + borderSize;
  } else if (boxSizing === "content-box") {
    height = height - paddingSize;
  }
  hiddenTextarea.value = "";
  const singleRowHeight = hiddenTextarea.scrollHeight - paddingSize;
  if (minRows !== null) {
    let minHeight = singleRowHeight * minRows;
    if (boxSizing === "border-box") {
      minHeight = minHeight + paddingSize + borderSize;
    }
    height = Math.max(minHeight, height);
    result.minHeight = `${minHeight}px`;
  }
  if (maxRows !== null) {
    let maxHeight = singleRowHeight * maxRows;
    if (boxSizing === "border-box") {
      maxHeight = maxHeight + paddingSize + borderSize;
    }
    height = Math.min(maxHeight, height);
  }
  result.height = `${height}px`;
  hiddenTextarea.parentNode.removeChild(hiddenTextarea);
  hiddenTextarea = null;
  return result;
}
const _sfc_main$q = {
  name: "ef-input",
  inheritAttrs: false,
  emits: ["update:modelValue", "input", "change", "focus", "blur", "clear", "mouseleave", "mouseenter", "keydown"],
  components: {
    EfIcon
  },
  props: {
    modelValue: {
      type: [String, Number],
      default: ""
    },
    type: {
      type: String,
      default: "text"
    },
    size: {
      type: String,
      validator: isValidComponentSize
    },
    autosize: {
      type: [Boolean, Object],
      default: false
    },
    autocomplete: {
      type: String,
      default: "off"
    },
    placeholder: {
      type: String,
      default: "\u8BF7\u8F93\u5165"
    },
    form: {
      type: String,
      default: ""
    },
    disabled: {
      type: Boolean,
      default: false
    },
    readonly: {
      type: Boolean,
      default: false
    },
    clearable: {
      type: Boolean,
      default: false
    },
    showPassword: {
      type: Boolean,
      default: false
    },
    showWordLimit: {
      type: Boolean,
      default: false
    },
    suffixIcon: {
      type: String,
      default: ""
    },
    prefixIcon: {
      type: String,
      default: ""
    },
    suffixIconColor: {
      type: String,
      default: "#c0c4cc"
    },
    prefixIconColor: {
      type: String,
      default: "#c0c4cc"
    },
    label: {
      type: String
    },
    tabindex: {
      type: [Number, String]
    },
    validateEvent: {
      type: Boolean,
      default: true
    },
    inputStyle: {
      type: Object,
      default: () => {
      }
    },
    maxlength: {
      type: [Number, String]
    },
    validate: {
      type: Object,
      default: null
    },
    clearToFocus: {
      type: Boolean,
      default: true
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const attrsObj = useAttrs();
    const { formContext, formItemContext } = useForm();
    const passwordVisible = ref(false);
    const input = ref(null);
    const textarea = ref(null);
    const showValidateErrmsg = ref(false);
    const ifHover = ref(false);
    const inputValue = computed({
      get() {
        return props2.modelValue;
      },
      set(newVal, oldVal) {
        emit("update:modelValue", newVal);
        nextTick(resizeTextarea);
        if (props2.validateEvent) {
          formItemContext == null ? void 0 : formItemContext.validate("change").then(({ valid, errmsg, detail }) => {
          });
        }
      }
    });
    const inputOrTextarea = computed(() => input.value || textarea.value);
    const inputDisabled = computed(() => props2.disabled);
    const inputSize = computed(() => props2.size);
    const iconSize = computed(() => {
      const iconSizeMap = {
        large: 20,
        middle: 18,
        small: 16,
        mini: 14
      };
      return iconSizeMap[inputSize.value] || 18;
    });
    const showClear = computed(() => {
      return props2.clearable && !inputDisabled.value && !props2.readonly && inputValue.value;
    });
    const suffixIconType = computed(() => {
      return props2.showPassword ? passwordVisible.value ? "icon-browse" : "icon-Notvisible" : props2.suffixIcon;
    });
    const _textareaCalcStyle = shallowRef(props2.inputStyle);
    const computedTextareaStyle = computed(() => ({
      ...props2.inputStyle,
      ..._textareaCalcStyle.value,
      resize: props2.resize
    }));
    const inputClasses = computed((_) => ({
      "validate-fail": (formItemContext == null ? void 0 : formItemContext.validateStatus) === "NOTPASS"
    }));
    const onMouseleave = (e) => {
      ifHover.value = false;
      emit("mouseleave", e);
    };
    const onMouseenter = (e) => {
      ifHover.value = true;
      emit("mouseenter", e);
    };
    const onClear = () => {
      clear();
      props2.clearToFocus && focus();
    };
    const onSuffixIconClick = () => {
      if (props2.showPassword) {
        passwordVisible.value = !passwordVisible.value;
      }
    };
    const onChange = (e) => {
      emit("change", e.target.value);
      if (props2.validate) {
        if (!props2.validate.validator || typeof props2.validate.validator != "function") {
          console.error("props.validate.validator\u4E0D\u5B58\u5728\u6216\u8005\u7C7B\u578B\u9519\u8BEF");
          return;
        }
        if (!props2.validate.errmsg) {
          console.error("props.validate.errmsg\u4E0D\u5B58\u5728");
          return;
        }
        if (props2.validate.validator(e.target.value)) {
          showValidateErrmsg.value = false;
        } else {
          showValidateErrmsg.value = true;
        }
      }
    };
    const onInput = (e) => {
      emit("input", e.target.value);
    };
    const onFocus = (e) => {
      emit("focus", e);
    };
    const onBlur = (e) => {
      if (props2.validateEvent) {
        formItemContext == null ? void 0 : formItemContext.validate("blur");
      }
      emit("blur", e);
    };
    const onKeydown = (e) => {
      emit("keydown", e);
    };
    const clear = () => {
      inputValue.value = "";
      emit("clear", "");
    };
    const focus = () => {
      nextTick(() => {
        inputOrTextarea.value.focus();
      });
    };
    const blur = () => {
      inputOrTextarea.value.blur();
    };
    const select = () => {
      inputOrTextarea.value.select();
    };
    const resizeTextarea = () => {
      const { type: type4, autosize } = props2;
      if (window === "undefined" || type4 !== "textarea")
        return;
      if (autosize) {
        const minRows = isObject(autosize) ? autosize.minRows : void 0;
        const maxRows = isObject(autosize) ? autosize.maxRows : void 0;
        _textareaCalcStyle.value = {
          ...calcTextareaHeight(textarea.value, minRows, maxRows)
        };
      } else {
        _textareaCalcStyle.value = {
          minHeight: calcTextareaHeight(textarea.value).minHeight
        };
      }
    };
    const onInputClick = (e) => {
      console.log("input click");
    };
    watch(() => props2.type, () => {
      nextTick(resizeTextarea);
    });
    onMounted(() => {
      nextTick(resizeTextarea);
    });
    return {
      input,
      textarea,
      passwordVisible,
      inputValue,
      attrsObj,
      inputDisabled,
      inputSize,
      iconSize,
      showClear,
      suffixIconType,
      computedTextareaStyle,
      showValidateErrmsg,
      ifHover,
      inputClasses,
      blur,
      select,
      focus,
      onMouseenter,
      onMouseleave,
      onClear,
      onSuffixIconClick,
      onChange,
      onInput,
      onFocus,
      onBlur,
      onKeydown,
      onInputClick
    };
  }
};
const _hoisted_1$l = {
  key: 0,
  class: "ef-input-group__prepend ef-input-group__append_prepend"
};
const _hoisted_2$h = { class: "ef-input-inner-wrapper" };
const _hoisted_3$b = { class: "prefix-wrapper" };
const _hoisted_4$9 = ["placeholder", "readonly", "type", "disabled", "autocomplete", "tabindex", "aria-label"];
const _hoisted_5$5 = {
  class: /* @__PURE__ */ normalizeClass(["suffix-wrapper"])
};
const _hoisted_6$4 = {
  key: 0,
  class: "validate-errmsg"
};
const _hoisted_7$4 = {
  key: 1,
  class: "ef-input-group__append ef-input-group__append_prepend"
};
const _hoisted_8$3 = ["tabindex", "disabled", "readonly", "autocomplete", "aria-label", "placeholder"];
function _sfc_render$g(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return openBlock(), createElementBlock("div", {
    class: normalizeClass([
      $props.type === "textarea" ? "ef-textarea-wrapper" : "ef-input-wrapper",
      $setup.inputSize ? "ef-input--" + $setup.inputSize : "",
      {
        "is-disabled": $setup.inputDisabled,
        "ef-input-group": _ctx.$slots.prepend || _ctx.$slots.append,
        "ef-input-group--append": _ctx.$slots.append,
        "ef-input-group--prepend": _ctx.$slots.prepend,
        "ef-input--prefix": _ctx.$slots.prefix || $props.prefixIcon,
        "ef-input--suffix": _ctx.$slots.suffix || $setup.suffixIconType || $props.showPassword,
        "ef-input--clearable": $props.clearable,
        "ef-input--suffix--password-clear": $props.clearable && $props.showPassword
      },
      _ctx.$attrs.class
    ]),
    style: normalizeStyle(_ctx.$attrs.style),
    onMouseenter: _cache[12] || (_cache[12] = (...args) => $setup.onMouseenter && $setup.onMouseenter(...args)),
    onMouseleave: _cache[13] || (_cache[13] = (...args) => $setup.onMouseleave && $setup.onMouseleave(...args))
  }, [
    $props.type !== "textarea" ? (openBlock(), createElementBlock(Fragment, { key: 0 }, [
      _ctx.$slots.prepend ? (openBlock(), createElementBlock("div", _hoisted_1$l, [
        renderSlot(_ctx.$slots, "prepend")
      ])) : createCommentVNode("", true),
      createElementVNode("div", _hoisted_2$h, [
        createElementVNode("span", _hoisted_3$b, [
          _ctx.$slots.prefix ? renderSlot(_ctx.$slots, "prefix", { key: 0 }) : $props.prefixIcon ? (openBlock(), createBlock(_component_ef_icon, {
            key: 1,
            type: $props.prefixIcon,
            color: $props.prefixIconColor,
            fontSize: $setup.iconSize
          }, null, 8, ["type", "color", "fontSize"])) : createCommentVNode("", true)
        ]),
        withDirectives(createElementVNode("input", mergeProps({
          ref: "input",
          placeholder: $props.placeholder,
          class: ["ef-input-inner", $setup.inputClasses],
          "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => $setup.inputValue = $event),
          readonly: $props.readonly
        }, $setup.attrsObj, {
          type: $props.showPassword ? $setup.passwordVisible ? "text" : "password" : $props.type,
          disabled: $setup.inputDisabled,
          style: $props.inputStyle,
          autocomplete: $props.autocomplete,
          tabindex: $props.tabindex,
          "aria-label": $props.label,
          onChange: _cache[1] || (_cache[1] = (...args) => $setup.onChange && $setup.onChange(...args)),
          onInput: _cache[2] || (_cache[2] = (...args) => $setup.onInput && $setup.onInput(...args)),
          onFocus: _cache[3] || (_cache[3] = (...args) => $setup.onFocus && $setup.onFocus(...args)),
          onBlur: _cache[4] || (_cache[4] = (...args) => $setup.onBlur && $setup.onBlur(...args)),
          onKeydown: _cache[5] || (_cache[5] = (...args) => $setup.onKeydown && $setup.onKeydown(...args))
        }), null, 16, _hoisted_4$9), [
          [vModelDynamic, $setup.inputValue]
        ]),
        createElementVNode("span", _hoisted_5$5, [
          $setup.showClear && $setup.ifHover ? (openBlock(), createBlock(_component_ef_icon, {
            key: 0,
            class: "show-pointer",
            type: "icon-reeor",
            color: "#c0c4cc",
            fontSize: "16",
            onClick: withModifiers($setup.onClear, ["stop"])
          }, null, 8, ["onClick"])) : createCommentVNode("", true),
          _ctx.$slots.suffix ? renderSlot(_ctx.$slots, "suffix", { key: 1 }) : $setup.suffixIconType ? (openBlock(), createBlock(_component_ef_icon, {
            key: 2,
            class: normalizeClass([{ "show-pointer": $props.showPassword }]),
            type: $setup.suffixIconType,
            color: $props.suffixIconColor,
            fontSize: $setup.iconSize,
            onClick: $setup.onSuffixIconClick
          }, null, 8, ["class", "type", "color", "fontSize", "onClick"])) : createCommentVNode("", true)
        ]),
        createVNode(Transition, { name: "ef-form-errmsg" }, {
          default: withCtx(() => [
            $props.validate && $setup.showValidateErrmsg ? (openBlock(), createElementBlock("span", _hoisted_6$4, toDisplayString($props.validate.errmsg), 1)) : createCommentVNode("", true)
          ]),
          _: 1
        })
      ]),
      _ctx.$slots.append ? (openBlock(), createElementBlock("div", _hoisted_7$4, [
        renderSlot(_ctx.$slots, "append")
      ])) : createCommentVNode("", true)
    ], 64)) : withDirectives((openBlock(), createElementBlock("textarea", mergeProps({
      key: 1,
      ref: "textarea",
      "onUpdate:modelValue": _cache[6] || (_cache[6] = ($event) => $setup.inputValue = $event),
      class: ["ef-textarea__inner", $setup.inputClasses]
    }, $setup.attrsObj, {
      tabindex: $props.tabindex,
      disabled: $setup.inputDisabled,
      readonly: $props.readonly,
      autocomplete: $props.autocomplete,
      style: $setup.computedTextareaStyle,
      "aria-label": $props.label,
      placeholder: $props.placeholder,
      onChange: _cache[7] || (_cache[7] = (...args) => $setup.onChange && $setup.onChange(...args)),
      onInput: _cache[8] || (_cache[8] = (...args) => $setup.onInput && $setup.onInput(...args)),
      onFocus: _cache[9] || (_cache[9] = (...args) => $setup.onFocus && $setup.onFocus(...args)),
      onBlur: _cache[10] || (_cache[10] = (...args) => $setup.onBlur && $setup.onBlur(...args)),
      onKeydown: _cache[11] || (_cache[11] = (...args) => $setup.onKeydown && $setup.onKeydown(...args))
    }), null, 16, _hoisted_8$3)), [
      [vModelText, $setup.inputValue]
    ])
  ], 38);
}
var EfInput = /* @__PURE__ */ _export_sfc(_sfc_main$q, [["render", _sfc_render$g]]);
EfInput.install = (app, options) => {
  app.component(EfInput.name, EfInput);
};
EfPopper.install = (app, options) => {
  app.component(EfPopper.name, EfPopper);
};
const props$2 = {
  ...props$4,
  title: {
    type: String,
    default: "Title"
  },
  content: {
    type: String,
    default: "this is content,this is content,this is content"
  },
  width: {
    type: [Number, String],
    default: void 0
  }
};
const _sfc_main$p = {
  name: "ef-popover",
  emits: ["update:visible", "visibleChange"],
  components: {
    EfPopper,
    VNode: VNodes
  },
  props: props$2,
  setup(props2, { attrs, slots, emit }) {
    const popoverStyle = shallowRef({});
    const efPopper = ref(null);
    const popperVisible = ref(false);
    const popoverComputedStyle = computed(() => {
      let res = {
        ...popoverStyle.value
      };
      if (props2.width != void 0) {
        res.width = parseInt(props2.width) + "px";
      }
      return res;
    });
    watch((_) => props2.visible, (newVal) => {
      popperVisible.value = newVal;
    }, { immediate: true });
    const onPopperVisibleChange = (val) => {
      emit("update:visible", val);
      emit("visibleChange", val);
    };
    return {
      popoverComputedStyle,
      efPopper,
      popperVisible,
      onPopperVisibleChange
    };
  }
};
const _hoisted_1$k = ["data-theme"];
const _hoisted_2$g = { class: "ef-popover-title" };
const _hoisted_3$a = { key: 0 };
const _hoisted_4$8 = {
  key: 1,
  class: "ef-popover-content"
};
function _sfc_render$f(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_node = resolveComponent("v-node");
  const _component_ef_popper = resolveComponent("ef-popper");
  return openBlock(), createBlock(_component_ef_popper, mergeProps({ ref: "efPopper" }, _ctx.$props, {
    visible: $setup.popperVisible,
    "onUpdate:visible": _cache[0] || (_cache[0] = ($event) => $setup.popperVisible = $event),
    onVisibleChange: $setup.onPopperVisibleChange
  }), {
    trigger: withCtx(() => [
      createVNode(_component_v_node, {
        vnodes: _ctx.$slots.trigger()
      }, null, 8, ["vnodes"])
    ]),
    default: withCtx(() => [
      createElementVNode("div", {
        "data-theme": _ctx.theme,
        class: "ef-popover-wrapper",
        style: normalizeStyle($setup.popoverComputedStyle)
      }, [
        createElementVNode("p", _hoisted_2$g, toDisplayString(_ctx.title), 1),
        _ctx.$slots.content ? (openBlock(), createElementBlock("div", _hoisted_3$a, [
          renderSlot(_ctx.$slots, "content")
        ])) : (openBlock(), createElementBlock("p", _hoisted_4$8, toDisplayString(_ctx.content), 1))
      ], 12, _hoisted_1$k)
    ]),
    _: 3
  }, 16, ["visible", "onVisibleChange"]);
}
var Popover = /* @__PURE__ */ _export_sfc(_sfc_main$p, [["render", _sfc_render$f]]);
Popover.install = (app, options) => {
  app.component(Popover.name, Popover);
};
Tooltip.install = (app, options) => {
  app.component(Tooltip.name, Tooltip);
};
const props$1 = {
  ...props$4,
  maxHeight: {
    type: [Number, String],
    default: 350
  },
  itemHeight: {
    type: [Number, String],
    default: 50
  },
  placement: {
    type: String,
    default: "bottom",
    validator: (val) => [
      "",
      "top",
      "top-start",
      "top-end",
      "bottom",
      "bottom-start",
      "bottom-end",
      "left",
      "left-start",
      "left-end",
      "right",
      "right-start",
      "right-end"
    ].includes(val)
  },
  transitionName: {
    type: String,
    default: "zoom-in",
    validator: (val) => ["", "fade-in-linear", "zoom-in"].includes(val)
  }
};
const _sfc_main$o = {
  name: "ef-dropdown",
  components: {
    EfPopper,
    VNode: VNodes
  },
  props: props$1,
  emits: ["command"],
  setup(props2, { attrs, slots, emit }) {
    const wrapperStyle = computed(() => {
      let style = {};
      props2.maxHeight && (style.maxHeight = parseInt(props2.maxHeight) + "px");
      return style;
    });
    const efPopper = ref(null);
    const dropdown = ref(null);
    provide("efDropdown", {
      props: props2,
      emit,
      efPopper
    });
    const onVisibleChange = (val) => {
    };
    return {
      wrapperStyle,
      efPopper,
      onVisibleChange,
      dropdown
    };
  }
};
function _sfc_render$e(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_node = resolveComponent("v-node");
  const _component_ef_popper = resolveComponent("ef-popper");
  return openBlock(), createBlock(_component_ef_popper, mergeProps({ ref: "efPopper" }, _ctx.$props), {
    trigger: withCtx(() => [
      createVNode(_component_v_node, {
        vnodes: _ctx.$slots.default()
      }, null, 8, ["vnodes"])
    ]),
    default: withCtx(() => [
      createElementVNode("div", {
        class: "ef-dropdown-wrapper scrollBarStyle",
        style: normalizeStyle($setup.wrapperStyle),
        ref: "dropdown"
      }, [
        renderSlot(_ctx.$slots, "dropdown")
      ], 4)
    ]),
    _: 3
  }, 16);
}
var Dropdown = /* @__PURE__ */ _export_sfc(_sfc_main$o, [["render", _sfc_render$e]]);
const _sfc_main$n = {
  name: "ef-dropdown-menu",
  setup(props2, { attrs, slots, emit }) {
  }
};
const _hoisted_1$j = { class: "ef-dropdown-menu-wrapper" };
function _sfc_render$d(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("ul", _hoisted_1$j, [
    renderSlot(_ctx.$slots, "default")
  ]);
}
var DropdownMenu = /* @__PURE__ */ _export_sfc(_sfc_main$n, [["render", _sfc_render$d]]);
const _sfc_main$m = {
  name: "ef-dropdown-item",
  components: {
    EfIcon
  },
  props: {
    command: {
      type: [Object, String, Number],
      default: () => ({})
    },
    disabled: Boolean,
    divided: Boolean,
    iconObj: {
      type: Object,
      default: () => null
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const efDropdown = inject("efDropdown");
    const itemStyle = computed(() => {
      let res = {};
      res.height = parseInt(efDropdown.props.itemHeight) + "px";
      return res;
    });
    const onItemClick = (e) => {
      if (props2.disabled) {
        return;
      }
      efDropdown.efPopper.value.immediateHideContent();
      efDropdown.emit("command", props2.command);
    };
    return {
      onItemClick,
      itemStyle
    };
  }
};
function _sfc_render$c(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return openBlock(), createElementBlock("li", {
    class: normalizeClass(["ef-dropdown-item-wrapper flex-base", { disabled: $props.disabled, divided: $props.divided }]),
    style: normalizeStyle($setup.itemStyle),
    onClick: _cache[0] || (_cache[0] = (...args) => $setup.onItemClick && $setup.onItemClick(...args))
  }, [
    $props.iconObj ? (openBlock(), createBlock(_component_ef_icon, mergeProps({ key: 0 }, $props.iconObj, { style: { "margin-right": "3px" } }), null, 16)) : createCommentVNode("", true),
    renderSlot(_ctx.$slots, "default")
  ], 6);
}
var DropdownItem = /* @__PURE__ */ _export_sfc(_sfc_main$m, [["render", _sfc_render$c]]);
Dropdown.install = (app, options) => {
  app.component(Dropdown.name, Dropdown);
};
DropdownMenu.install = (app, options) => {
  app.component(DropdownMenu.name, DropdownMenu);
};
DropdownItem.install = (app, options) => {
  app.component(DropdownItem.name, DropdownItem);
};
var EfDropdown = {
  install(app, options) {
    app.use(Dropdown);
    app.use(DropdownMenu);
    app.use(DropdownItem);
  }
};
const props = {
  ...props$4,
  trigger: {
    type: String,
    default: "click",
    validator: (val) => ["", "click", "hover", "manual"].includes(val)
  },
  placement: {
    type: String,
    default: "bottom",
    validator: (val) => [
      "",
      "top",
      "top-start",
      "top-end",
      "bottom",
      "bottom-start",
      "bottom-end",
      "left",
      "left-start",
      "left-end",
      "right",
      "right-start",
      "right-end"
    ].includes(val)
  },
  transitionName: {
    type: String,
    default: "zoom-in",
    validator: (val) => ["", "fade-in-linear", "zoom-in"].includes(val)
  },
  width: {
    type: [Number, String],
    default: 240
  },
  maxHeight: {
    type: [Number, String],
    default: 300
  },
  itemHeight: {
    type: [Number, String],
    default: 34
  },
  modelValue: {
    type: [String, Number, Boolean, Object],
    default: void 0
  },
  multiple: Boolean,
  disabled: Boolean,
  filterable: Boolean,
  clearable: Boolean,
  filterMethod: {
    type: Function,
    default: (inputVal, label) => {
      return label.indexOf(inputVal) > -1;
    }
  },
  singleRowHeight: {
    type: [Number, String],
    default: 45
  },
  allowCreate: Boolean,
  placeholder: {
    type: String,
    default: "\u8BF7\u9009\u62E9"
  },
  height: {
    type: [Number, String],
    default: void 0
  },
  size: {
    type: String,
    default: ""
  }
};
function useSelect({
  props: props2,
  emit
}) {
  const optionsMap = ref(/* @__PURE__ */ new Map());
  const efPopper = ref(null);
  const popperVisible = ref(false);
  const tagRef = ref(null);
  const contentRef = ref(null);
  const disabledKeys = ref([]);
  const { formContext, formItemContext } = useForm();
  let selectedList = void 0;
  if (props2.modelValue === void 0) {
    selectedList = ref([]);
  } else {
    selectedList = computed({
      get() {
        return props2.multiple ? props2.modelValue : props2.modelValue ? [props2.modelValue] : [];
      },
      set(newVal, oldVal) {
        emit("update:modelValue", props2.multiple ? newVal : newVal[0]);
        formItemContext == null ? void 0 : formItemContext.validate("change");
      }
    });
  }
  const triggerStyle = computed(() => {
    let style = {};
    props2.width && (style.width = parseInt(props2.width) + "px");
    return style;
  });
  const contentStyle = computed(() => {
    let style = {};
    props2.maxHeight && (style.maxHeight = parseInt(props2.maxHeight) + "px");
    props2.width && (style.width = parseInt(props2.width) + "px");
    return style;
  });
  watch(selectedList, (newVal) => {
    emit("change", props2.multiple ? newVal : newVal[0]);
  }, {
    deep: true
  });
  watch(popperVisible, (newVal, oldVal) => {
    if (newVal) {
      setSelectedOptionPosition();
    }
  });
  const setSelectedOptionPosition = () => {
    if (selectedList.value.length < 1)
      return;
    nextTick((_) => {
      let optionEl = contentRef.value.querySelector(`li.selected`);
      let contentScrollTop = getChildToParentTopDistance(contentRef.value, optionEl) - contentRef.value.offsetHeight;
      contentRef.value.scrollTop = contentScrollTop > 0 ? contentScrollTop : 0;
    });
  };
  const addDisabledKey = (value) => {
    disabledKeys.value.push(value);
  };
  const isDisabledKey = (value) => disabledKeys.value.includes(value);
  const clearSelectedData = (_) => {
    selectedList.value = [];
    emit("clear");
  };
  const setSelectedData = (value) => {
    selectedList.value = [value];
  };
  const removeSelectedData = (value) => {
    let index = selectedList.value.indexOf(value);
    if (index > -1) {
      selectedList.value.splice(index, 1);
    }
  };
  const toggleSelectedData = (value) => {
    let index = selectedList.value.indexOf(value);
    if (index > -1) {
      selectedList.value.splice(index, 1);
    } else {
      selectedList.value.push(value);
    }
  };
  const addOption = ({
    value,
    label
  }) => {
    optionsMap.value.set(value, label);
  };
  const getOptionLabel = (value) => {
    return optionsMap.value.get(value);
  };
  const removeOption = (value) => {
    optionsMap.value.has(value) && optionsMap.value.delete(value);
  };
  const clearOptions = (_) => {
    optionsMap.value.clear();
  };
  const hasOption = (val) => optionsMap.value.has(val);
  return {
    optionsMap,
    selectedList,
    addOption,
    removeOption,
    clearOptions,
    getOptionLabel,
    clearSelectedData,
    setSelectedData,
    toggleSelectedData,
    removeSelectedData,
    hasOption,
    efPopper,
    popperVisible,
    tagRef,
    triggerStyle,
    contentStyle,
    addDisabledKey,
    isDisabledKey,
    contentRef
  };
}
function useInput({
  popperVisible,
  props: props2,
  selectedList,
  optionsMap,
  getOptionLabel,
  tagRef,
  efPopper,
  emit,
  clearSelectedData
}) {
  const inputStyle = shallowRef({
    height: parseInt(props2.height) + "px"
  });
  const ifFocus = ref(false);
  const inputRef = ref(null);
  const inputValue = ref("");
  const inputPlaceholder = ref(props2.placeholder);
  const filterValue = ref("");
  const isEmpty = computed((_) => selectedList.value.length < 1);
  const inputSuffixIconRotate = computed(() => {
    return popperVisible.value ? -180 : 0;
  });
  const inputClearable = computed(() => {
    return !isEmpty.value && props2.clearable;
  });
  const inputDisabled = computed(() => {
    return props2.disabled;
  });
  watch(selectedList, (newVal, oldVal) => {
    if (!props2.multiple) {
      if (newVal.length > 0) {
        inputValue.value = getOptionLabel(newVal[0]);
      }
    } else {
      computedInputStyle();
      if (newVal.length > 0) {
        props2.clearable && (inputValue.value = " ");
      }
    }
  }, {
    deep: true
  });
  watch(popperVisible, (newVal) => {
    if (!newVal) {
      if (props2.filterable && !isEmpty.value) {
        if (!props2.multiple) {
          inputValue.value = getOptionLabel(selectedList.value[0]);
          inputRef.value.blur();
        }
      }
    }
  });
  watch(optionsMap, (newVal) => {
    if (isEmpty.value) {
      return;
    }
    if (!props2.multiple && getOptionLabel(selectedList.value[0])) {
      inputValue.value = getOptionLabel(selectedList.value[0]);
    }
  }, {
    deep: true
  });
  const clearFocus = () => {
    ifFocus.value = false;
  };
  const openInputFocus = () => {
    ifFocus.value = true;
  };
  const computedInputStyle = (isClear = false) => {
    nextTick((_) => {
      let inputTmpStyle = {};
      let tagStyle = window.getComputedStyle(tagRef.value);
      let height = parseInt(tagStyle.height) + 10;
      if (height < parseInt(props2.singleRowHeight)) {
        height = parseInt(props2.singleRowHeight);
      }
      inputTmpStyle.height = height + "px";
      inputStyle.value = inputTmpStyle;
      efPopper.value.computeContentStyle();
    });
  };
  const onInputChange = (val) => {
  };
  const onInputInput = (val) => {
    filterValue.value = val;
  };
  const onInputFocus = (e) => {
    if (!props2.filterable) {
      e.currentTarget.blur();
    } else {
      if (!isEmpty.value) {
        if (!props2.multiple) {
          inputPlaceholder.value = inputValue.value;
          inputValue.value = "";
          filterValue.value = "";
        }
      }
    }
  };
  const onInputBlur = (e) => {
  };
  const onClear = () => {
    clearSelectedData();
    if (!props2.multiple)
      ;
    else {
      computedInputStyle(true);
    }
  };
  const onInputClick = (e) => {
  };
  onMounted((_) => {
    if (!props2.multiple) {
      inputValue.value = getOptionLabel(selectedList.value[0]);
    } else {
      inputPlaceholder.value = "";
      inputValue.value = "";
      props2.clearable && !isEmpty.value && (inputValue.value = " ");
      !isEmpty.value && computedInputStyle();
      if (isEmpty.value) {
        inputValue.value = "";
        inputPlaceholder.value = props2.placeholder;
      }
    }
    document.body.addEventListener("click", clearFocus);
  });
  onUnmounted(() => {
    document.body.removeEventListener("click", clearFocus);
  });
  return {
    inputValue,
    inputSuffixIconRotate,
    inputClearable,
    inputDisabled,
    inputPlaceholder,
    onInputChange,
    onInputInput,
    onInputFocus,
    onInputBlur,
    inputRef,
    filterValue,
    ifFocus,
    inputStyle,
    openInputFocus,
    onClear,
    onInputClick
  };
}
function useTag({
  props: props2,
  inputRef,
  selectedList
}) {
  const tagWrapperStyle = shallowRef({});
  const isEmpty = computed((_) => selectedList.value.length < 1);
  const computeTagWrapperStyle = () => {
    let tagStyle = {};
    let computedInputStyle = window.getComputedStyle(inputRef.value.$el.querySelector(".ef-input-inner"));
    let inputWidth = parseInt(computedInputStyle.width);
    let paddingRight = parseInt(computedInputStyle.paddingRight);
    let paddingLeft = parseInt(computedInputStyle.paddingLeft);
    if (selectedList.value.length == 1)
      paddingRight += 20;
    tagStyle.maxWidth = inputWidth - paddingRight - paddingLeft + "px";
    tagStyle.paddingLeft = paddingLeft + "px";
    tagWrapperStyle.value = tagStyle;
  };
  watch(selectedList, (newVal, oldVal) => {
    if (!props2.multiple)
      ;
    else {
      if (!isEmpty.value) {
        computeTagWrapperStyle();
      }
    }
  }, {
    deep: true
  });
  onMounted((_) => {
    computeTagWrapperStyle();
  });
  return {
    tagWrapperStyle
  };
}
function useTagInput({
  props: props2,
  onInputInput,
  popperVisible,
  selectedList,
  filterValue
}) {
  const tagInputValue = ref("");
  const tagInputPlaceholder = ref(props2.placeholder);
  const tagInputRef = ref(null);
  const isEmpty = computed((_) => selectedList.value.length < 1);
  watch(selectedList, (newVal, oldVal) => {
    if (!props2.multiple)
      ;
    else {
      if (props2.filterable) {
        tagInputPlaceholder.value = "";
        focusTagInput();
        if (newVal.length < 1) {
          tagInputPlaceholder.value = props2.placeholder;
        }
      }
    }
  }, {
    deep: true
  });
  watch(popperVisible, (newVal) => {
    if (!newVal) {
      if (props2.filterable && props2.multiple) {
        if (!isEmpty.value) {
          clearData();
        } else {
          resetData();
        }
      }
    } else {
      filterValue.value = "";
    }
  });
  const clearData = (_) => {
    tagInputValue.value = "";
    tagInputPlaceholder.value = "";
  };
  const resetData = (_) => {
    tagInputValue.value = "";
    tagInputPlaceholder.value = props2.placeholder;
  };
  const focusTagInput = (_) => {
    tagInputRef.value.focus();
  };
  const onTagInputInput = (e) => {
    let val = e.target.value;
    tagInputValue.value = val;
    onInputInput(val);
  };
  const onTagInputClick = (e) => {
    if (popperVisible.value) {
      e.stopPropagation();
    }
  };
  onMounted((_) => {
    if (!isEmpty.value) {
      clearData();
    }
  });
  return {
    onTagInputInput,
    onTagInputClick,
    tagInputValue,
    tagInputPlaceholder,
    tagInputRef
  };
}
function useNewOption({
  props: props2,
  selectedList,
  tagInputValue,
  hasOption,
  toggleSelectedData,
  addOption
}) {
  const newOptionsMap = ref(/* @__PURE__ */ new Map());
  const newOptionVisible = computed(() => props2.multiple && props2.allowCreate && tagInputValue.value && (!hasOption(tagInputValue.value) || newOptionsMap.value.has(tagInputValue.value)));
  const newOptionItemStyle = computed(() => {
    let res = {};
    res.height = parseInt(props2.itemHeight) + "px";
    return res;
  });
  const newOptionIsSelected = computed((_) => selectedList.value.includes(tagInputValue.value));
  const onNewOptionClick = (e) => {
    if (props2.multiple) {
      newOptionsMap.value.set(tagInputValue.value, tagInputValue.value);
      addOption({
        value: tagInputValue.value,
        label: tagInputValue.value
      });
      toggleSelectedData(tagInputValue.value);
    }
  };
  return {
    newOptionItemStyle,
    newOptionVisible,
    newOptionIsSelected,
    onNewOptionClick,
    newOptionsMap
  };
}
function useOnKeydown({
  props: props2,
  tagInputValue,
  newOptionsMap,
  optionsMap,
  setSelectedData,
  toggleSelectedData,
  popperVisible,
  isDisabledKey,
  selectedList,
  filterValue,
  newOptionVisible,
  addOption,
  contentRef
}) {
  const optionsKeys = ref([]);
  const hoverIndex = ref(-1);
  const hoverKey = computed((_) => optionsKeys.value[hoverIndex.value]);
  watch(filterValue, (newVal, oldVal) => {
    genOptionsKeys();
    hoverIndex.value = 0;
  });
  watch(optionsMap, (newVal, oldVal) => {
    genOptionsKeys();
  }, {
    deep: true
  });
  watch(popperVisible, (newVal, oldVal) => {
    if (!newVal) {
      hoverIndex.value = -1;
    } else {
      setHoverIndex();
    }
  });
  const onKeydown = (e) => {
    const keyCode = e.keyCode;
    if (keyCode == 38) {
      hoverIndex.value = hoverIndex.value - 1 < 0 ? optionsKeys.value.length - 1 : hoverIndex.value - 1;
      setContentScrollTop();
    } else if (keyCode == 40) {
      hoverIndex.value = hoverIndex.value + 1 > optionsKeys.value.length - 1 ? 0 : hoverIndex.value + 1;
      setContentScrollTop();
    } else if (keyCode == 13) {
      if (isDisabledKey(hoverKey.value)) {
        return;
      }
      if (!props2.multiple && (hoverIndex.value == -1 || hoverKey.value == selectedList.value[0])) {
        return;
      }
      if (!props2.multiple) {
        setSelectedData(hoverKey.value);
        popperVisible.value = false;
      } else {
        if (newOptionVisible.value) {
          newOptionsMap.value.set(tagInputValue.value, tagInputValue.value);
          addOption({
            value: tagInputValue.value,
            label: tagInputValue.value
          });
        }
        toggleSelectedData(hoverKey.value);
      }
    }
  };
  const setContentScrollTop = (_) => {
    nextTick((_2) => {
      contentRef.value.scrollTop;
      let optionEl = contentRef.value.querySelector(`li.hover`);
      let optionToParentTopDistance = getChildToParentTopDistance(contentRef.value, optionEl);
      let contentScrollTop = optionToParentTopDistance - contentRef.value.offsetHeight;
      contentRef.value.scrollTop = contentScrollTop > 0 ? contentScrollTop : 0;
    });
  };
  const setHoverIndex = (_) => {
    if (selectedList.value.length < 1)
      return;
    let index = optionsKeys.value.indexOf(selectedList.value[0]);
    if (index > -1) {
      hoverIndex.value = index;
    }
  };
  const genOptionsKeys = (_) => {
    optionsKeys.value = [...optionsMap.value.keys()].filter((key) => {
      if (![...newOptionsMap.value.keys()].includes(key)) {
        if (props2.filterable) {
          return props2.filterMethod(filterValue.value, optionsMap.value.get(key));
        } else {
          return true;
        }
      } else {
        return false;
      }
    });
    if (props2.multiple && props2.filterable && props2.allowCreate && tagInputValue.value && newOptionVisible.value) {
      optionsKeys.value.unshift(tagInputValue.value);
    }
  };
  onMounted((_) => {
    genOptionsKeys();
  });
  onUnmounted((_) => {
  });
  return {
    onKeydown,
    hoverKey
  };
}
const _sfc_main$l = {
  name: "ef-select",
  emits: ["update:modelValue", "change", "clear"],
  props,
  components: {
    EfPopper,
    EfInput,
    VNode: VNodes
  },
  setup(props2, { attrs, slots, emit }) {
    const {
      optionsMap,
      selectedList,
      addOption,
      removeOption,
      clearOptions,
      getOptionLabel,
      clearSelectedData,
      setSelectedData,
      toggleSelectedData,
      removeSelectedData,
      hasOption,
      efPopper,
      popperVisible,
      tagRef,
      triggerStyle,
      contentStyle,
      addDisabledKey,
      isDisabledKey,
      contentRef
    } = useSelect({ props: props2, emit });
    const {
      inputValue,
      inputSuffixIconRotate,
      inputClearable,
      inputDisabled,
      inputPlaceholder,
      onInputChange,
      onInputInput,
      onInputFocus,
      onInputBlur,
      inputRef,
      filterValue,
      ifFocus,
      inputStyle,
      openInputFocus,
      onClear,
      onInputClick
    } = useInput({
      popperVisible,
      props: props2,
      selectedList,
      optionsMap,
      getOptionLabel,
      tagRef,
      efPopper,
      emit,
      clearSelectedData
    });
    const { tagWrapperStyle } = useTag({ props: props2, inputRef, selectedList });
    const { onTagInputInput, onTagInputClick, tagInputValue, tagInputPlaceholder, tagInputRef } = useTagInput({
      props: props2,
      onInputInput,
      popperVisible,
      selectedList,
      filterValue
    });
    const {
      newOptionItemStyle,
      newOptionVisible,
      newOptionIsSelected,
      onNewOptionClick,
      newOptionsMap
    } = useNewOption({
      props: props2,
      selectedList,
      tagInputValue,
      hasOption,
      toggleSelectedData,
      addOption
    });
    const { hoverKey, onKeydown } = useOnKeydown({
      props: props2,
      tagInputValue,
      newOptionsMap,
      optionsMap,
      newOptionsMap,
      setSelectedData,
      toggleSelectedData,
      popperVisible,
      isDisabledKey,
      selectedList,
      filterValue,
      newOptionVisible,
      addOption,
      contentRef
    });
    provide("efSelect", {
      props: props2,
      emit,
      efPopper,
      optionsMap,
      selectedList,
      setSelectedData,
      addOption,
      inputValue,
      popperVisible,
      filterValue,
      toggleSelectedData,
      hoverKey,
      addDisabledKey,
      isDisabledKey
    });
    return {
      contentStyle,
      triggerStyle,
      efPopper,
      popperVisible,
      inputValue,
      inputSuffixIconRotate,
      inputClearable,
      inputDisabled,
      inputPlaceholder,
      onInputChange,
      onInputInput,
      onInputFocus,
      onInputBlur,
      inputRef,
      ifFocus,
      inputStyle,
      openInputFocus,
      onClear,
      onInputClick,
      optionsMap,
      selectedList,
      addOption,
      removeOption,
      clearOptions,
      clearSelectedData,
      setSelectedData,
      toggleSelectedData,
      removeSelectedData,
      getOptionLabel,
      contentRef,
      tagWrapperStyle,
      tagRef,
      onTagInputInput,
      onTagInputClick,
      tagInputValue,
      tagInputPlaceholder,
      tagInputRef,
      newOptionItemStyle,
      newOptionVisible,
      newOptionIsSelected,
      onNewOptionClick,
      hoverKey,
      onKeydown
    };
  }
};
const _hoisted_1$i = { class: "tag" };
const _hoisted_2$f = { class: "tag-text" };
const _hoisted_3$9 = ["value"];
const _hoisted_4$7 = ["data-value"];
function _sfc_render$b(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  const _component_ef_input = resolveComponent("ef-input");
  const _component_ef_popper = resolveComponent("ef-popper");
  return openBlock(), createBlock(_component_ef_popper, mergeProps({ ref: "efPopper" }, _ctx.$props, {
    visible: $setup.popperVisible,
    "onUpdate:visible": _cache[8] || (_cache[8] = ($event) => $setup.popperVisible = $event)
  }), {
    trigger: withCtx(() => [
      createElementVNode("div", {
        class: "ef-select-trigger-wrapper",
        style: normalizeStyle($setup.triggerStyle),
        onClick: _cache[6] || (_cache[6] = (...args) => $setup.openInputFocus && $setup.openInputFocus(...args))
      }, [
        _ctx.multiple ? (openBlock(), createElementBlock("div", {
          key: 0,
          class: "tag-wrapper",
          ref: "tagRef",
          style: normalizeStyle($setup.tagWrapperStyle),
          onMouseenter: _cache[4] || (_cache[4] = ($event) => _ctx.$refs["inputRef"].onMouseenter())
        }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList($setup.selectedList, (item) => {
            return openBlock(), createElementBlock("span", _hoisted_1$i, [
              createElementVNode("span", _hoisted_2$f, toDisplayString($setup.getOptionLabel(item)), 1),
              createVNode(_component_ef_icon, {
                fontSize: "14",
                type: "icon-close",
                color: "#909399",
                onClick: withModifiers(($event) => $setup.removeSelectedData(item), ["stop"])
              }, null, 8, ["onClick"])
            ]);
          }), 256)),
          _ctx.filterable ? (openBlock(), createElementBlock("input", {
            key: 0,
            ref: "tagInputRef",
            value: $setup.tagInputValue,
            type: "text",
            class: "tag-input",
            onInput: _cache[0] || (_cache[0] = (...args) => $setup.onTagInputInput && $setup.onTagInputInput(...args)),
            onChange: _cache[1] || (_cache[1] = (...args) => $setup.onTagInputInput && $setup.onTagInputInput(...args)),
            onClick: _cache[2] || (_cache[2] = (...args) => $setup.onTagInputClick && $setup.onTagInputClick(...args)),
            onKeydown: _cache[3] || (_cache[3] = (...args) => $setup.onKeydown && $setup.onKeydown(...args))
          }, null, 40, _hoisted_3$9)) : createCommentVNode("", true)
        ], 36)) : createCommentVNode("", true),
        createVNode(_component_ef_input, {
          ref: "inputRef",
          modelValue: $setup.inputValue,
          "onUpdate:modelValue": _cache[5] || (_cache[5] = ($event) => $setup.inputValue = $event),
          clearable: $setup.inputClearable,
          disabled: $setup.inputDisabled,
          placeholder: $setup.inputPlaceholder,
          type: "text",
          onChange: $setup.onInputChange,
          onInput: $setup.onInputInput,
          onFocus: $setup.onInputFocus,
          onBlur: $setup.onInputBlur,
          onClear: $setup.onClear,
          onClick: $setup.onInputClick,
          onKeydown: $setup.onKeydown,
          class: normalizeClass({ focus: $setup.ifFocus }),
          inputStyle: $setup.inputStyle,
          clearToFocus: false,
          size: _ctx.size
        }, {
          suffix: withCtx(() => [
            createVNode(_component_ef_icon, {
              type: "icon-arrow-down",
              fontSize: "20",
              color: "#c0c4cc",
              rotate: $setup.inputSuffixIconRotate
            }, null, 8, ["rotate"])
          ]),
          _: 1
        }, 8, ["modelValue", "clearable", "disabled", "placeholder", "onChange", "onInput", "onFocus", "onBlur", "onClear", "onClick", "onKeydown", "class", "inputStyle", "size"])
      ], 4)
    ]),
    default: withCtx(() => [
      createElementVNode("ul", {
        class: "ef-select-content-wrapper scrollBarStyle",
        style: normalizeStyle($setup.contentStyle),
        ref: "contentRef"
      }, [
        $setup.newOptionVisible ? (openBlock(), createElementBlock("li", {
          key: 0,
          class: normalizeClass(["ef-option-wrapper flex-base", { selected: $setup.newOptionIsSelected, hover: $setup.hoverKey == $setup.tagInputValue }]),
          style: normalizeStyle($setup.newOptionItemStyle),
          onClick: _cache[7] || (_cache[7] = withModifiers((...args) => $setup.onNewOptionClick && $setup.onNewOptionClick(...args), ["stop"])),
          "data-value": $setup.tagInputValue
        }, [
          createElementVNode("span", null, toDisplayString($setup.tagInputValue), 1)
        ], 14, _hoisted_4$7)) : createCommentVNode("", true),
        renderSlot(_ctx.$slots, "default")
      ], 4)
    ]),
    _: 3
  }, 16, ["visible"]);
}
var Select = /* @__PURE__ */ _export_sfc(_sfc_main$l, [["render", _sfc_render$b]]);
const _sfc_main$k = {
  name: "ef-option",
  components: {
    EfIcon
  },
  props: {
    disabled: Boolean,
    iconObj: {
      type: Object,
      default: () => null
    },
    value: {
      type: [String, Number, Boolean, Object],
      default: void 0
    },
    label: {
      type: [String, Number],
      default: void 0
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const efSelect = inject("efSelect");
    const { formContext, formItemContext } = useForm();
    efSelect.addOption({
      value: props2.value,
      label: props2.label
    });
    props2.disabled && efSelect.addDisabledKey(props2.value);
    const optionVisible = ref(true);
    const filterMethod = ([...args]) => {
      let inputVal = args[0];
      optionVisible.value = efSelect.props.filterable ? efSelect.props.filterMethod(inputVal, props2.label) : true;
    };
    const throttleFilterMethod = throttle(filterMethod, 300, 300);
    const itemStyle = computed(() => {
      let res = {};
      res.height = parseInt(efSelect.props.itemHeight) + "px";
      return res;
    });
    const isSelected = computed((_) => efSelect.selectedList.value.includes(props2.value));
    watch(efSelect.filterValue, (newVal, oldVal) => throttleFilterMethod(newVal));
    const onItemClick = (e) => {
      if (props2.disabled) {
        return;
      }
      if (!efSelect.props.multiple) {
        efSelect.setSelectedData(props2.value);
        efSelect.popperVisible.value = false;
      } else {
        efSelect.toggleSelectedData(props2.value);
        formItemContext == null ? void 0 : formItemContext.validate("change");
      }
    };
    return {
      onItemClick,
      itemStyle,
      optionVisible,
      isSelected,
      hoverKey: efSelect.hoverKey
    };
  }
};
const _hoisted_1$h = ["data-value"];
function _sfc_render$a(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return withDirectives((openBlock(), createElementBlock("li", {
    class: normalizeClass(["ef-option-wrapper flex-base", { disabled: $props.disabled, selected: $setup.isSelected, hover: $setup.hoverKey == $props.value }]),
    style: normalizeStyle($setup.itemStyle),
    onClick: _cache[0] || (_cache[0] = withModifiers((...args) => $setup.onItemClick && $setup.onItemClick(...args), ["stop"])),
    "data-value": $props.value
  }, [
    _ctx.$slots.default ? renderSlot(_ctx.$slots, "default", { key: 0 }) : (openBlock(), createElementBlock(Fragment, { key: 1 }, [
      $props.iconObj ? (openBlock(), createBlock(_component_ef_icon, mergeProps({ key: 0 }, $props.iconObj, { style: { "margin-right": "3px" } }), null, 16)) : createCommentVNode("", true),
      createElementVNode("span", null, toDisplayString($props.label), 1)
    ], 64))
  ], 14, _hoisted_1$h)), [
    [vShow, $setup.optionVisible]
  ]);
}
var Option = /* @__PURE__ */ _export_sfc(_sfc_main$k, [["render", _sfc_render$a]]);
const _sfc_main$j = {
  name: "ef-option-group",
  props: {
    label: {
      type: String,
      default: ""
    }
  },
  setup(props2, { attrs, slots, emit }) {
  }
};
const _hoisted_1$g = { class: "ef-option-group-wrapper" };
const _hoisted_2$e = { class: "ef-option-group-label" };
const _hoisted_3$8 = { class: "ef-select-group" };
function _sfc_render$9(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("ul", _hoisted_1$g, [
    createElementVNode("li", _hoisted_2$e, toDisplayString($props.label), 1),
    createElementVNode("li", null, [
      createElementVNode("ul", _hoisted_3$8, [
        renderSlot(_ctx.$slots, "default")
      ])
    ])
  ]);
}
var OptionGroup = /* @__PURE__ */ _export_sfc(_sfc_main$j, [["render", _sfc_render$9]]);
Select.install = (app, options) => {
  app.component(Select.name, Select);
};
Option.install = (app, options) => {
  app.component(Option.name, Option);
};
OptionGroup.install = (app, options) => {
  app.component(OptionGroup.name, OptionGroup);
};
var EfSelect = {
  install(app, options) {
    app.use(Select);
    app.use(Option);
    app.use(OptionGroup);
  }
};
var commonjsGlobal = typeof globalThis !== "undefined" ? globalThis : typeof window !== "undefined" ? window : typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : {};
var dayjs_min = { exports: {} };
(function(module, exports) {
  !function(t, e) {
    module.exports = e();
  }(commonjsGlobal, function() {
    var t = 1e3, e = 6e4, n = 36e5, r = "millisecond", i = "second", s = "minute", u = "hour", a = "day", o = "week", f = "month", h2 = "quarter", c = "year", d = "date", $ = "Invalid Date", l = /^(\d{4})[-/]?(\d{1,2})?[-/]?(\d{0,2})[Tt\s]*(\d{1,2})?:?(\d{1,2})?:?(\d{1,2})?[.:]?(\d+)?$/, y = /\[([^\]]+)]|Y{1,4}|M{1,4}|D{1,2}|d{1,4}|H{1,2}|h{1,2}|a|A|m{1,2}|s{1,2}|Z{1,2}|SSS/g, M = { name: "en", weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"), months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_") }, m = function(t2, e2, n2) {
      var r2 = String(t2);
      return !r2 || r2.length >= e2 ? t2 : "" + Array(e2 + 1 - r2.length).join(n2) + t2;
    }, g = { s: m, z: function(t2) {
      var e2 = -t2.utcOffset(), n2 = Math.abs(e2), r2 = Math.floor(n2 / 60), i2 = n2 % 60;
      return (e2 <= 0 ? "+" : "-") + m(r2, 2, "0") + ":" + m(i2, 2, "0");
    }, m: function t2(e2, n2) {
      if (e2.date() < n2.date())
        return -t2(n2, e2);
      var r2 = 12 * (n2.year() - e2.year()) + (n2.month() - e2.month()), i2 = e2.clone().add(r2, f), s2 = n2 - i2 < 0, u2 = e2.clone().add(r2 + (s2 ? -1 : 1), f);
      return +(-(r2 + (n2 - i2) / (s2 ? i2 - u2 : u2 - i2)) || 0);
    }, a: function(t2) {
      return t2 < 0 ? Math.ceil(t2) || 0 : Math.floor(t2);
    }, p: function(t2) {
      return { M: f, y: c, w: o, d: a, D: d, h: u, m: s, s: i, ms: r, Q: h2 }[t2] || String(t2 || "").toLowerCase().replace(/s$/, "");
    }, u: function(t2) {
      return t2 === void 0;
    } }, v = "en", D = {};
    D[v] = M;
    var p = function(t2) {
      return t2 instanceof _;
    }, S = function t2(e2, n2, r2) {
      var i2;
      if (!e2)
        return v;
      if (typeof e2 == "string") {
        var s2 = e2.toLowerCase();
        D[s2] && (i2 = s2), n2 && (D[s2] = n2, i2 = s2);
        var u2 = e2.split("-");
        if (!i2 && u2.length > 1)
          return t2(u2[0]);
      } else {
        var a2 = e2.name;
        D[a2] = e2, i2 = a2;
      }
      return !r2 && i2 && (v = i2), i2 || !r2 && v;
    }, w = function(t2, e2) {
      if (p(t2))
        return t2.clone();
      var n2 = typeof e2 == "object" ? e2 : {};
      return n2.date = t2, n2.args = arguments, new _(n2);
    }, O = g;
    O.l = S, O.i = p, O.w = function(t2, e2) {
      return w(t2, { locale: e2.$L, utc: e2.$u, x: e2.$x, $offset: e2.$offset });
    };
    var _ = function() {
      function M2(t2) {
        this.$L = S(t2.locale, null, true), this.parse(t2);
      }
      var m2 = M2.prototype;
      return m2.parse = function(t2) {
        this.$d = function(t3) {
          var e2 = t3.date, n2 = t3.utc;
          if (e2 === null)
            return new Date(NaN);
          if (O.u(e2))
            return new Date();
          if (e2 instanceof Date)
            return new Date(e2);
          if (typeof e2 == "string" && !/Z$/i.test(e2)) {
            var r2 = e2.match(l);
            if (r2) {
              var i2 = r2[2] - 1 || 0, s2 = (r2[7] || "0").substring(0, 3);
              return n2 ? new Date(Date.UTC(r2[1], i2, r2[3] || 1, r2[4] || 0, r2[5] || 0, r2[6] || 0, s2)) : new Date(r2[1], i2, r2[3] || 1, r2[4] || 0, r2[5] || 0, r2[6] || 0, s2);
            }
          }
          return new Date(e2);
        }(t2), this.$x = t2.x || {}, this.init();
      }, m2.init = function() {
        var t2 = this.$d;
        this.$y = t2.getFullYear(), this.$M = t2.getMonth(), this.$D = t2.getDate(), this.$W = t2.getDay(), this.$H = t2.getHours(), this.$m = t2.getMinutes(), this.$s = t2.getSeconds(), this.$ms = t2.getMilliseconds();
      }, m2.$utils = function() {
        return O;
      }, m2.isValid = function() {
        return !(this.$d.toString() === $);
      }, m2.isSame = function(t2, e2) {
        var n2 = w(t2);
        return this.startOf(e2) <= n2 && n2 <= this.endOf(e2);
      }, m2.isAfter = function(t2, e2) {
        return w(t2) < this.startOf(e2);
      }, m2.isBefore = function(t2, e2) {
        return this.endOf(e2) < w(t2);
      }, m2.$g = function(t2, e2, n2) {
        return O.u(t2) ? this[e2] : this.set(n2, t2);
      }, m2.unix = function() {
        return Math.floor(this.valueOf() / 1e3);
      }, m2.valueOf = function() {
        return this.$d.getTime();
      }, m2.startOf = function(t2, e2) {
        var n2 = this, r2 = !!O.u(e2) || e2, h3 = O.p(t2), $2 = function(t3, e3) {
          var i2 = O.w(n2.$u ? Date.UTC(n2.$y, e3, t3) : new Date(n2.$y, e3, t3), n2);
          return r2 ? i2 : i2.endOf(a);
        }, l2 = function(t3, e3) {
          return O.w(n2.toDate()[t3].apply(n2.toDate("s"), (r2 ? [0, 0, 0, 0] : [23, 59, 59, 999]).slice(e3)), n2);
        }, y2 = this.$W, M3 = this.$M, m3 = this.$D, g2 = "set" + (this.$u ? "UTC" : "");
        switch (h3) {
          case c:
            return r2 ? $2(1, 0) : $2(31, 11);
          case f:
            return r2 ? $2(1, M3) : $2(0, M3 + 1);
          case o:
            var v2 = this.$locale().weekStart || 0, D2 = (y2 < v2 ? y2 + 7 : y2) - v2;
            return $2(r2 ? m3 - D2 : m3 + (6 - D2), M3);
          case a:
          case d:
            return l2(g2 + "Hours", 0);
          case u:
            return l2(g2 + "Minutes", 1);
          case s:
            return l2(g2 + "Seconds", 2);
          case i:
            return l2(g2 + "Milliseconds", 3);
          default:
            return this.clone();
        }
      }, m2.endOf = function(t2) {
        return this.startOf(t2, false);
      }, m2.$set = function(t2, e2) {
        var n2, o2 = O.p(t2), h3 = "set" + (this.$u ? "UTC" : ""), $2 = (n2 = {}, n2[a] = h3 + "Date", n2[d] = h3 + "Date", n2[f] = h3 + "Month", n2[c] = h3 + "FullYear", n2[u] = h3 + "Hours", n2[s] = h3 + "Minutes", n2[i] = h3 + "Seconds", n2[r] = h3 + "Milliseconds", n2)[o2], l2 = o2 === a ? this.$D + (e2 - this.$W) : e2;
        if (o2 === f || o2 === c) {
          var y2 = this.clone().set(d, 1);
          y2.$d[$2](l2), y2.init(), this.$d = y2.set(d, Math.min(this.$D, y2.daysInMonth())).$d;
        } else
          $2 && this.$d[$2](l2);
        return this.init(), this;
      }, m2.set = function(t2, e2) {
        return this.clone().$set(t2, e2);
      }, m2.get = function(t2) {
        return this[O.p(t2)]();
      }, m2.add = function(r2, h3) {
        var d2, $2 = this;
        r2 = Number(r2);
        var l2 = O.p(h3), y2 = function(t2) {
          var e2 = w($2);
          return O.w(e2.date(e2.date() + Math.round(t2 * r2)), $2);
        };
        if (l2 === f)
          return this.set(f, this.$M + r2);
        if (l2 === c)
          return this.set(c, this.$y + r2);
        if (l2 === a)
          return y2(1);
        if (l2 === o)
          return y2(7);
        var M3 = (d2 = {}, d2[s] = e, d2[u] = n, d2[i] = t, d2)[l2] || 1, m3 = this.$d.getTime() + r2 * M3;
        return O.w(m3, this);
      }, m2.subtract = function(t2, e2) {
        return this.add(-1 * t2, e2);
      }, m2.format = function(t2) {
        var e2 = this, n2 = this.$locale();
        if (!this.isValid())
          return n2.invalidDate || $;
        var r2 = t2 || "YYYY-MM-DDTHH:mm:ssZ", i2 = O.z(this), s2 = this.$H, u2 = this.$m, a2 = this.$M, o2 = n2.weekdays, f2 = n2.months, h3 = function(t3, n3, i3, s3) {
          return t3 && (t3[n3] || t3(e2, r2)) || i3[n3].slice(0, s3);
        }, c2 = function(t3) {
          return O.s(s2 % 12 || 12, t3, "0");
        }, d2 = n2.meridiem || function(t3, e3, n3) {
          var r3 = t3 < 12 ? "AM" : "PM";
          return n3 ? r3.toLowerCase() : r3;
        }, l2 = { YY: String(this.$y).slice(-2), YYYY: this.$y, M: a2 + 1, MM: O.s(a2 + 1, 2, "0"), MMM: h3(n2.monthsShort, a2, f2, 3), MMMM: h3(f2, a2), D: this.$D, DD: O.s(this.$D, 2, "0"), d: String(this.$W), dd: h3(n2.weekdaysMin, this.$W, o2, 2), ddd: h3(n2.weekdaysShort, this.$W, o2, 3), dddd: o2[this.$W], H: String(s2), HH: O.s(s2, 2, "0"), h: c2(1), hh: c2(2), a: d2(s2, u2, true), A: d2(s2, u2, false), m: String(u2), mm: O.s(u2, 2, "0"), s: String(this.$s), ss: O.s(this.$s, 2, "0"), SSS: O.s(this.$ms, 3, "0"), Z: i2 };
        return r2.replace(y, function(t3, e3) {
          return e3 || l2[t3] || i2.replace(":", "");
        });
      }, m2.utcOffset = function() {
        return 15 * -Math.round(this.$d.getTimezoneOffset() / 15);
      }, m2.diff = function(r2, d2, $2) {
        var l2, y2 = O.p(d2), M3 = w(r2), m3 = (M3.utcOffset() - this.utcOffset()) * e, g2 = this - M3, v2 = O.m(this, M3);
        return v2 = (l2 = {}, l2[c] = v2 / 12, l2[f] = v2, l2[h2] = v2 / 3, l2[o] = (g2 - m3) / 6048e5, l2[a] = (g2 - m3) / 864e5, l2[u] = g2 / n, l2[s] = g2 / e, l2[i] = g2 / t, l2)[y2] || g2, $2 ? v2 : O.a(v2);
      }, m2.daysInMonth = function() {
        return this.endOf(f).$D;
      }, m2.$locale = function() {
        return D[this.$L];
      }, m2.locale = function(t2, e2) {
        if (!t2)
          return this.$L;
        var n2 = this.clone(), r2 = S(t2, e2, true);
        return r2 && (n2.$L = r2), n2;
      }, m2.clone = function() {
        return O.w(this.$d, this);
      }, m2.toDate = function() {
        return new Date(this.valueOf());
      }, m2.toJSON = function() {
        return this.isValid() ? this.toISOString() : null;
      }, m2.toISOString = function() {
        return this.$d.toISOString();
      }, m2.toString = function() {
        return this.$d.toUTCString();
      }, M2;
    }(), T = _.prototype;
    return w.prototype = T, [["$ms", r], ["$s", i], ["$m", s], ["$H", u], ["$W", a], ["$M", f], ["$y", c], ["$D", d]].forEach(function(t2) {
      T[t2[1]] = function(e2) {
        return this.$g(e2, t2[0], t2[1]);
      };
    }), w.extend = function(t2, e2) {
      return t2.$i || (t2(e2, _, w), t2.$i = true), w;
    }, w.locale = S, w.isDayjs = p, w.unix = function(t2) {
      return w(1e3 * t2);
    }, w.en = D[v], w.Ls = D, w.p = {}, w;
  });
})(dayjs_min);
var dayjs = dayjs_min.exports;
var isSameOrBefore$1 = { exports: {} };
(function(module, exports) {
  !function(e, i) {
    module.exports = i();
  }(commonjsGlobal, function() {
    return function(e, i) {
      i.prototype.isSameOrBefore = function(e2, i2) {
        return this.isSame(e2, i2) || this.isBefore(e2, i2);
      };
    };
  });
})(isSameOrBefore$1);
var isSameOrBefore = isSameOrBefore$1.exports;
var isSameOrAfter$1 = { exports: {} };
(function(module, exports) {
  !function(e, t) {
    module.exports = t();
  }(commonjsGlobal, function() {
    return function(e, t) {
      t.prototype.isSameOrAfter = function(e2, t2) {
        return this.isSame(e2, t2) || this.isAfter(e2, t2);
      };
    };
  });
})(isSameOrAfter$1);
var isSameOrAfter = isSameOrAfter$1.exports;
var customParseFormat$1 = { exports: {} };
(function(module, exports) {
  !function(e, t) {
    module.exports = t();
  }(commonjsGlobal, function() {
    var e = { LTS: "h:mm:ss A", LT: "h:mm A", L: "MM/DD/YYYY", LL: "MMMM D, YYYY", LLL: "MMMM D, YYYY h:mm A", LLLL: "dddd, MMMM D, YYYY h:mm A" }, t = /(\[[^[]*\])|([-_:/.,()\s]+)|(A|a|YYYY|YY?|MM?M?M?|Do|DD?|hh?|HH?|mm?|ss?|S{1,3}|z|ZZ?)/g, n = /\d\d/, r = /\d\d?/, i = /\d*[^-_:/,()\s\d]+/, o = {}, s = function(e2) {
      return (e2 = +e2) + (e2 > 68 ? 1900 : 2e3);
    };
    var a = function(e2) {
      return function(t2) {
        this[e2] = +t2;
      };
    }, f = [/[+-]\d\d:?(\d\d)?|Z/, function(e2) {
      (this.zone || (this.zone = {})).offset = function(e3) {
        if (!e3)
          return 0;
        if (e3 === "Z")
          return 0;
        var t2 = e3.match(/([+-]|\d\d)/g), n2 = 60 * t2[1] + (+t2[2] || 0);
        return n2 === 0 ? 0 : t2[0] === "+" ? -n2 : n2;
      }(e2);
    }], h2 = function(e2) {
      var t2 = o[e2];
      return t2 && (t2.indexOf ? t2 : t2.s.concat(t2.f));
    }, u = function(e2, t2) {
      var n2, r2 = o.meridiem;
      if (r2) {
        for (var i2 = 1; i2 <= 24; i2 += 1)
          if (e2.indexOf(r2(i2, 0, t2)) > -1) {
            n2 = i2 > 12;
            break;
          }
      } else
        n2 = e2 === (t2 ? "pm" : "PM");
      return n2;
    }, d = { A: [i, function(e2) {
      this.afternoon = u(e2, false);
    }], a: [i, function(e2) {
      this.afternoon = u(e2, true);
    }], S: [/\d/, function(e2) {
      this.milliseconds = 100 * +e2;
    }], SS: [n, function(e2) {
      this.milliseconds = 10 * +e2;
    }], SSS: [/\d{3}/, function(e2) {
      this.milliseconds = +e2;
    }], s: [r, a("seconds")], ss: [r, a("seconds")], m: [r, a("minutes")], mm: [r, a("minutes")], H: [r, a("hours")], h: [r, a("hours")], HH: [r, a("hours")], hh: [r, a("hours")], D: [r, a("day")], DD: [n, a("day")], Do: [i, function(e2) {
      var t2 = o.ordinal, n2 = e2.match(/\d+/);
      if (this.day = n2[0], t2)
        for (var r2 = 1; r2 <= 31; r2 += 1)
          t2(r2).replace(/\[|\]/g, "") === e2 && (this.day = r2);
    }], M: [r, a("month")], MM: [n, a("month")], MMM: [i, function(e2) {
      var t2 = h2("months"), n2 = (h2("monthsShort") || t2.map(function(e3) {
        return e3.slice(0, 3);
      })).indexOf(e2) + 1;
      if (n2 < 1)
        throw new Error();
      this.month = n2 % 12 || n2;
    }], MMMM: [i, function(e2) {
      var t2 = h2("months").indexOf(e2) + 1;
      if (t2 < 1)
        throw new Error();
      this.month = t2 % 12 || t2;
    }], Y: [/[+-]?\d+/, a("year")], YY: [n, function(e2) {
      this.year = s(e2);
    }], YYYY: [/\d{4}/, a("year")], Z: f, ZZ: f };
    function c(n2) {
      var r2, i2;
      r2 = n2, i2 = o && o.formats;
      for (var s2 = (n2 = r2.replace(/(\[[^\]]+])|(LTS?|l{1,4}|L{1,4})/g, function(t2, n3, r3) {
        var o2 = r3 && r3.toUpperCase();
        return n3 || i2[r3] || e[r3] || i2[o2].replace(/(\[[^\]]+])|(MMMM|MM|DD|dddd)/g, function(e2, t3, n4) {
          return t3 || n4.slice(1);
        });
      })).match(t), a2 = s2.length, f2 = 0; f2 < a2; f2 += 1) {
        var h3 = s2[f2], u2 = d[h3], c2 = u2 && u2[0], l = u2 && u2[1];
        s2[f2] = l ? { regex: c2, parser: l } : h3.replace(/^\[|\]$/g, "");
      }
      return function(e2) {
        for (var t2 = {}, n3 = 0, r3 = 0; n3 < a2; n3 += 1) {
          var i3 = s2[n3];
          if (typeof i3 == "string")
            r3 += i3.length;
          else {
            var o2 = i3.regex, f3 = i3.parser, h4 = e2.slice(r3), u3 = o2.exec(h4)[0];
            f3.call(t2, u3), e2 = e2.replace(u3, "");
          }
        }
        return function(e3) {
          var t3 = e3.afternoon;
          if (t3 !== void 0) {
            var n4 = e3.hours;
            t3 ? n4 < 12 && (e3.hours += 12) : n4 === 12 && (e3.hours = 0), delete e3.afternoon;
          }
        }(t2), t2;
      };
    }
    return function(e2, t2, n2) {
      n2.p.customParseFormat = true, e2 && e2.parseTwoDigitYear && (s = e2.parseTwoDigitYear);
      var r2 = t2.prototype, i2 = r2.parse;
      r2.parse = function(e3) {
        var t3 = e3.date, r3 = e3.utc, s2 = e3.args;
        this.$u = r3;
        var a2 = s2[1];
        if (typeof a2 == "string") {
          var f2 = s2[2] === true, h3 = s2[3] === true, u2 = f2 || h3, d2 = s2[2];
          h3 && (d2 = s2[2]), o = this.$locale(), !f2 && d2 && (o = n2.Ls[d2]), this.$d = function(e4, t4, n3) {
            try {
              if (["x", "X"].indexOf(t4) > -1)
                return new Date((t4 === "X" ? 1e3 : 1) * e4);
              var r4 = c(t4)(e4), i3 = r4.year, o2 = r4.month, s3 = r4.day, a3 = r4.hours, f3 = r4.minutes, h4 = r4.seconds, u3 = r4.milliseconds, d3 = r4.zone, l2 = new Date(), m2 = s3 || (i3 || o2 ? 1 : l2.getDate()), M2 = i3 || l2.getFullYear(), Y = 0;
              i3 && !o2 || (Y = o2 > 0 ? o2 - 1 : l2.getMonth());
              var p = a3 || 0, v = f3 || 0, D = h4 || 0, g = u3 || 0;
              return d3 ? new Date(Date.UTC(M2, Y, m2, p, v, D, g + 60 * d3.offset * 1e3)) : n3 ? new Date(Date.UTC(M2, Y, m2, p, v, D, g)) : new Date(M2, Y, m2, p, v, D, g);
            } catch (e5) {
              return new Date("");
            }
          }(t3, a2, r3), this.init(), d2 && d2 !== true && (this.$L = this.locale(d2).$L), u2 && t3 != this.format(a2) && (this.$d = new Date("")), o = {};
        } else if (a2 instanceof Array)
          for (var l = a2.length, m = 1; m <= l; m += 1) {
            s2[1] = a2[m - 1];
            var M = n2.apply(this, s2);
            if (M.isValid()) {
              this.$d = M.$d, this.$L = M.$L, this.init();
              break;
            }
            m === l && (this.$d = new Date(""));
          }
        else
          i2.call(this, e3);
      };
    };
  });
})(customParseFormat$1);
var customParseFormat = customParseFormat$1.exports;
var isBetween$1 = { exports: {} };
(function(module, exports) {
  !function(e, i) {
    module.exports = i();
  }(commonjsGlobal, function() {
    return function(e, i, t) {
      i.prototype.isBetween = function(e2, i2, s, f) {
        var n = t(e2), o = t(i2), r = (f = f || "()")[0] === "(", u = f[1] === ")";
        return (r ? this.isAfter(n, s) : !this.isBefore(n, s)) && (u ? this.isBefore(o, s) : !this.isAfter(o, s)) || (r ? this.isBefore(n, s) : !this.isAfter(n, s)) && (u ? this.isAfter(o, s) : !this.isBefore(o, s));
      };
    };
  });
})(isBetween$1);
var isBetween = isBetween$1.exports;
var zhCn = { exports: {} };
(function(module, exports) {
  !function(e, _) {
    module.exports = _(dayjs_min.exports);
  }(commonjsGlobal, function(e) {
    function _(e2) {
      return e2 && typeof e2 == "object" && "default" in e2 ? e2 : { default: e2 };
    }
    var t = _(e), d = { name: "zh-cn", weekdays: "\u661F\u671F\u65E5_\u661F\u671F\u4E00_\u661F\u671F\u4E8C_\u661F\u671F\u4E09_\u661F\u671F\u56DB_\u661F\u671F\u4E94_\u661F\u671F\u516D".split("_"), weekdaysShort: "\u5468\u65E5_\u5468\u4E00_\u5468\u4E8C_\u5468\u4E09_\u5468\u56DB_\u5468\u4E94_\u5468\u516D".split("_"), weekdaysMin: "\u65E5_\u4E00_\u4E8C_\u4E09_\u56DB_\u4E94_\u516D".split("_"), months: "\u4E00\u6708_\u4E8C\u6708_\u4E09\u6708_\u56DB\u6708_\u4E94\u6708_\u516D\u6708_\u4E03\u6708_\u516B\u6708_\u4E5D\u6708_\u5341\u6708_\u5341\u4E00\u6708_\u5341\u4E8C\u6708".split("_"), monthsShort: "1\u6708_2\u6708_3\u6708_4\u6708_5\u6708_6\u6708_7\u6708_8\u6708_9\u6708_10\u6708_11\u6708_12\u6708".split("_"), ordinal: function(e2, _2) {
      return _2 === "W" ? e2 + "\u5468" : e2 + "\u65E5";
    }, weekStart: 1, yearStart: 4, formats: { LT: "HH:mm", LTS: "HH:mm:ss", L: "YYYY/MM/DD", LL: "YYYY\u5E74M\u6708D\u65E5", LLL: "YYYY\u5E74M\u6708D\u65E5Ah\u70B9mm\u5206", LLLL: "YYYY\u5E74M\u6708D\u65E5ddddAh\u70B9mm\u5206", l: "YYYY/M/D", ll: "YYYY\u5E74M\u6708D\u65E5", lll: "YYYY\u5E74M\u6708D\u65E5 HH:mm", llll: "YYYY\u5E74M\u6708D\u65E5dddd HH:mm" }, relativeTime: { future: "%s\u5185", past: "%s\u524D", s: "\u51E0\u79D2", m: "1 \u5206\u949F", mm: "%d \u5206\u949F", h: "1 \u5C0F\u65F6", hh: "%d \u5C0F\u65F6", d: "1 \u5929", dd: "%d \u5929", M: "1 \u4E2A\u6708", MM: "%d \u4E2A\u6708", y: "1 \u5E74", yy: "%d \u5E74" }, meridiem: function(e2, _2) {
      var t2 = 100 * e2 + _2;
      return t2 < 600 ? "\u51CC\u6668" : t2 < 900 ? "\u65E9\u4E0A" : t2 < 1100 ? "\u4E0A\u5348" : t2 < 1300 ? "\u4E2D\u5348" : t2 < 1800 ? "\u4E0B\u5348" : "\u665A\u4E0A";
    } };
    return t.default.locale(d, null, true), d;
  });
})(zhCn);
const getYearByYear = (year) => {
  const columns = 4;
  let yearData = [
    []
  ];
  let startYear = Math.floor(year / 10) * 10;
  const startYearDayjs = dayjs(startYear.toString(), "YYYY");
  for (let i = 0; i < 10; i++) {
    if (yearData[yearData.length - 1].length >= columns) {
      yearData.push([]);
    }
    let lastYearArr = yearData[yearData.length - 1];
    let currentDayjs = startYearDayjs.add(i, "year");
    let yearObj = {
      year: currentDayjs.format("YYYY"),
      value: currentDayjs.format("YY")
    };
    lastYearArr.push(yearObj);
  }
  return yearData;
};
const getMonthByYear = (year) => {
  const columns = 4;
  const rows = 3;
  let monthData = [
    []
  ];
  const startMonth = year + "01";
  const startMonthDayjs = dayjs(startMonth, "YYYYMM");
  for (let i = 0; i < columns * rows; i++) {
    if (monthData[monthData.length - 1].length >= columns) {
      monthData.push([]);
    }
    let lastMonthArr = monthData[monthData.length - 1];
    let currentDayjs = startMonthDayjs.add(i, "month");
    let monthObj = {
      month: currentDayjs.format("YYYYMM"),
      value: currentDayjs.format("M")
    };
    lastMonthArr.push(monthObj);
  }
  return monthData;
};
const getCalendarByMonth = (ym) => {
  const commonFormat = "YYYYMMDD";
  const columns = 7;
  const rows = 6;
  let calendarData = [
    []
  ];
  const daysInMonth = dayjs(ym, "YYYYMM").daysInMonth();
  const startDateOfMonth = ym + "01";
  const endDateOfMonth = "" + ym + daysInMonth;
  const startDateOfMonthDayjs = dayjs(startDateOfMonth, commonFormat);
  const endDateOfMonthDayjs = dayjs(endDateOfMonth, commonFormat);
  const startWeekdayOfMonth = startDateOfMonthDayjs.day();
  endDateOfMonthDayjs.day();
  const startDateDayjs = startDateOfMonthDayjs.subtract(startWeekdayOfMonth == 7 ? 0 : startWeekdayOfMonth, "day");
  for (let i = 0; i < columns * rows; i++) {
    if (calendarData[calendarData.length - 1].length >= columns) {
      calendarData.push([]);
    }
    let lastWeekArr = calendarData[calendarData.length - 1];
    let currentDayjs = startDateDayjs.add(i, "day");
    let dateObj = {
      date: currentDayjs.format(commonFormat),
      value: currentDayjs.format("D"),
      isCurrentMonth: currentDayjs.isSameOrAfter(startDateOfMonthDayjs) && currentDayjs.isSameOrBefore(endDateOfMonthDayjs)
    };
    lastWeekArr.push(dateObj);
  }
  return calendarData;
};
const _sfc_main$i = {
  name: "ef-basic-date-table",
  emits: ["update:modelValue", "pick", "hoverCell"],
  components: {
    VNodes
  },
  props: {
    month: {
      type: String,
      default: ""
    },
    modelValue: {
      type: [String, Array],
      default: ""
    },
    weekLabels: {
      type: Array,
      default: (_) => ["\u65E5", "\u4E00", "\u4E8C", "\u4E09", "\u56DB", "\u4E94", "\u516D"]
    },
    disabledDate: {
      type: Function,
      default: (_) => false
    },
    multiple: Boolean,
    inRange: {
      type: Function,
      default: (_) => false
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const efDatePicker = inject("efDateCommonPick", {});
    const today = ref(dayjs().format("YYYYMMDD"));
    const multipleDateArray = ref(props2.modelValue || []);
    const calendar = computed((_) => getCalendarByMonth(props2.month || dayjs().format("YYYYMM")));
    const multiple = computed((_) => props2.multiple ? true : false);
    const selectedDate = computed({
      get() {
        return props2.modelValue;
      },
      set(newVal, oldVal) {
        emit("update:modelValue", newVal);
      }
    });
    watch((_) => props2.modelValue, (newVal, oldVal) => {
      if (multiple.value) {
        multipleDateArray.value = newVal;
      }
    });
    const onClickCell = (row) => {
      if (!row.isCurrentMonth)
        return;
      if (props2.disabledDate(row.date))
        return;
      if (multiple.value) {
        let index = multipleDateArray.value.indexOf(row.date);
        if (index == -1) {
          multipleDateArray.value.push(row.date);
        } else {
          multipleDateArray.value.splice(index, 1);
        }
        selectedDate.value = multipleDateArray.value;
        emit("pick", multipleDateArray.value, row);
      } else {
        selectedDate.value = row.date;
        emit("pick", row.date, row);
      }
    };
    const onHoverCell = (row) => {
      emit("hoverCell", row);
    };
    return {
      calendar,
      today,
      selectedDate,
      multiple,
      multipleDateArray,
      efDatePickerSlot: efDatePicker.slots,
      onClickCell,
      onHoverCell
    };
  }
};
const _hoisted_1$f = {
  cellspacing: "0",
  cellpadding: "0",
  class: "ef-date-table"
};
const _hoisted_2$d = ["onClick", "onMouseenter"];
function _sfc_render$8(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_nodes = resolveComponent("v-nodes");
  return openBlock(), createElementBlock("table", _hoisted_1$f, [
    createElementVNode("tbody", null, [
      createElementVNode("tr", null, [
        (openBlock(true), createElementBlock(Fragment, null, renderList($props.weekLabels, (week) => {
          return openBlock(), createElementBlock("th", { key: week }, toDisplayString(week), 1);
        }), 128))
      ]),
      (openBlock(true), createElementBlock(Fragment, null, renderList($setup.calendar, (item, index) => {
        return openBlock(), createElementBlock("tr", {
          key: index,
          class: "ef-date-table-row"
        }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList(item, (row) => {
            return openBlock(), createElementBlock("td", {
              key: row.date,
              class: normalizeClass({
                valid: row.isCurrentMonth,
                notCurrentMonth: !row.isCurrentMonth,
                today: $setup.today == row.date,
                disabled: $props.disabledDate(row.date)
              }),
              onClick: ($event) => $setup.onClickCell(row),
              onMouseenter: ($event) => $setup.onHoverCell(row)
            }, [
              createElementVNode("div", {
                class: normalizeClass(["ef-date-table-cell", { inRange: row.isCurrentMonth && $props.inRange(row.date) }])
              }, [
                $setup.efDatePickerSlot && $setup.efDatePickerSlot.date ? (openBlock(), createBlock(_component_v_nodes, {
                  key: 0,
                  class: normalizeClass(["ef-date-table-cell-text", {
                    selectedDate: $setup.multiple ? $setup.multipleDateArray.includes(row.date) : $setup.selectedDate == row.date
                  }]),
                  vnodes: $setup.efDatePickerSlot.date(row)
                }, null, 8, ["class", "vnodes"])) : (openBlock(), createElementBlock("span", {
                  key: 1,
                  class: normalizeClass(["ef-date-table-cell-text", {
                    selectedDate: $setup.multiple ? $setup.multipleDateArray.includes(row.date) : $setup.selectedDate == row.date
                  }])
                }, toDisplayString(row.value), 3))
              ], 2)
            ], 42, _hoisted_2$d);
          }), 128))
        ]);
      }), 128))
    ])
  ]);
}
var BasicDateTable = /* @__PURE__ */ _export_sfc(_sfc_main$i, [["render", _sfc_render$8]]);
const _sfc_main$h = {
  name: "ef-basic-month-table",
  emits: ["update:modelValue", "pick", "hoverCell"],
  components: {
    VNodes
  },
  props: {
    year: {
      type: String,
      default: ""
    },
    modelValue: {
      type: [String, Array],
      default: ""
    },
    disabledDate: {
      type: Function,
      default: (_) => false
    },
    multiple: Boolean,
    inRange: {
      type: Function,
      default: (_) => false
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const efDatePicker = inject("efDateCommonPick", {});
    const today = ref(dayjs().format("YYYYMM"));
    const multipleMonthArray = ref(props2.modelValue || []);
    const monthArr = computed((_) => getMonthByYear(props2.year || dayjs().format("YYYY")));
    const multiple = computed((_) => props2.multiple ? true : false);
    const selectedMonth = computed({
      get() {
        return props2.modelValue;
      },
      set(newVal, oldVal) {
        emit("update:modelValue", newVal);
      }
    });
    watch((_) => props2.modelValue, (newVal, oldVal) => {
      if (multiple.value) {
        multipleMonthArray.value = newVal;
      }
    });
    const onClickCell = (mon) => {
      if (props2.disabledDate(mon.month))
        return;
      if (multiple.value) {
        let index = multipleMonthArray.value.indexOf(mon.month);
        if (index == -1) {
          multipleMonthArray.value.push(mon.month);
        } else {
          multipleMonthArray.value.splice(index, 1);
        }
        selectedMonth.value = multipleMonthArray.value;
        emit("pick", multipleMonthArray.value, mon);
      } else {
        selectedMonth.value = mon.month;
        emit("pick", mon.month, mon);
      }
    };
    const onHoverCell = (row) => {
      emit("hoverCell", row);
    };
    return {
      monthArr,
      today,
      multiple,
      multipleMonthArray,
      selectedMonth,
      efDatePickerSlot: efDatePicker.slots,
      onClickCell,
      onHoverCell
    };
  }
};
const _hoisted_1$e = {
  cellspacing: "0",
  cellpadding: "0",
  class: "ef-month-table"
};
const _hoisted_2$c = ["onClick", "onMouseenter"];
function _sfc_render$7(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_nodes = resolveComponent("v-nodes");
  return openBlock(), createElementBlock("table", _hoisted_1$e, [
    createElementVNode("tbody", null, [
      (openBlock(true), createElementBlock(Fragment, null, renderList($setup.monthArr, (item, index) => {
        return openBlock(), createElementBlock("tr", { key: index }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList(item, (mon) => {
            return openBlock(), createElementBlock("td", {
              onClick: ($event) => $setup.onClickCell(mon),
              key: mon.month,
              class: normalizeClass({ disabled: $props.disabledDate(mon.month) }),
              onMouseenter: ($event) => $setup.onHoverCell(mon)
            }, [
              createElementVNode("div", {
                class: normalizeClass(["ef-month-table-cell", { inRange: $props.inRange(mon.month) }])
              }, [
                $setup.efDatePickerSlot && $setup.efDatePickerSlot.month ? (openBlock(), createBlock(_component_v_nodes, {
                  key: 0,
                  vnodes: $setup.efDatePickerSlot.month(mon),
                  class: normalizeClass(["ef-month-table-text", {
                    today: $setup.today == mon.month,
                    selectedMonth: $setup.multiple ? $setup.multipleMonthArray.includes(mon.month) : $setup.selectedMonth == mon.month
                  }])
                }, null, 8, ["vnodes", "class"])) : (openBlock(), createElementBlock("span", {
                  key: 1,
                  class: normalizeClass(["ef-month-table-text", {
                    today: $setup.today == mon.month,
                    selectedMonth: $setup.multiple ? $setup.multipleMonthArray.includes(mon.month) : $setup.selectedMonth == mon.month
                  }])
                }, toDisplayString(mon.value + "\u6708"), 3))
              ], 2)
            ], 42, _hoisted_2$c);
          }), 128))
        ]);
      }), 128))
    ])
  ]);
}
var BasicMonthTable = /* @__PURE__ */ _export_sfc(_sfc_main$h, [["render", _sfc_render$7]]);
const _sfc_main$g = {
  name: "ef-basic-year-table",
  emits: ["update:modelValue", "pick"],
  components: {
    VNodes
  },
  props: {
    year: {
      type: String,
      default: ""
    },
    modelValue: {
      type: [String, Array],
      default: ""
    },
    disabledDate: {
      type: Function,
      default: (_) => false
    },
    multiple: Boolean,
    inRange: {
      type: Function,
      default: (_) => false
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const efDatePicker = inject("efDateCommonPick", {});
    const today = ref(dayjs().format("YYYY"));
    const multipleYearArray = ref(props2.modelValue || []);
    const yearArr = computed((_) => getYearByYear(props2.year || dayjs().format("YYYY")));
    const multiple = computed((_) => props2.multiple ? true : false);
    const selectedYear = computed({
      get() {
        return props2.modelValue;
      },
      set(newVal, oldVal) {
        emit("update:modelValue", newVal);
      }
    });
    const onClickCell = (year) => {
      if (props2.disabledDate(year.year))
        return;
      if (multiple.value) {
        let index = multipleYearArray.value.indexOf(year.year);
        if (index == -1) {
          multipleYearArray.value.push(year.year);
        } else {
          multipleYearArray.value.splice(index, 1);
        }
        selectedYear.value = multipleYearArray.value;
        emit("pick", multipleYearArray.value);
      } else {
        selectedYear.value = year.year;
        emit("pick", year.year);
      }
    };
    return {
      yearArr,
      today,
      multiple,
      multipleYearArray,
      selectedYear,
      efDatePickerSlot: efDatePicker.slots,
      onClickCell
    };
  }
};
const _hoisted_1$d = {
  cellspacing: "0",
  cellpadding: "0",
  class: "ef-month-table"
};
const _hoisted_2$b = ["onClick"];
function _sfc_render$6(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_v_nodes = resolveComponent("v-nodes");
  return openBlock(), createElementBlock("table", _hoisted_1$d, [
    createElementVNode("tbody", null, [
      (openBlock(true), createElementBlock(Fragment, null, renderList($setup.yearArr, (item, index) => {
        return openBlock(), createElementBlock("tr", { key: index }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList(item, (year) => {
            return openBlock(), createElementBlock("td", {
              onClick: ($event) => $setup.onClickCell(year),
              key: year.year,
              class: normalizeClass({ disabled: $props.disabledDate(year.year) })
            }, [
              createElementVNode("div", {
                class: normalizeClass(["ef-month-table-cell", { inRange: $props.inRange(year.year) }])
              }, [
                $setup.efDatePickerSlot && $setup.efDatePickerSlot.year ? (openBlock(), createBlock(_component_v_nodes, {
                  key: 0,
                  vnodes: $setup.efDatePickerSlot.year(year),
                  class: normalizeClass(["ef-month-table-text", {
                    today: $setup.today == year.year,
                    selectedMonth: $setup.multiple ? $setup.multipleYearArray.includes(year.year) : $setup.selectedYear == year.year
                  }])
                }, null, 8, ["vnodes", "class"])) : (openBlock(), createElementBlock("span", {
                  key: 1,
                  class: normalizeClass(["ef-month-table-text", {
                    today: $setup.today == year.year,
                    selectedMonth: $setup.multiple ? $setup.multipleYearArray.includes(year.year) : $setup.selectedYear == year.year
                  }])
                }, toDisplayString(year.year), 3))
              ], 2)
            ], 10, _hoisted_2$b);
          }), 128))
        ]);
      }), 128))
    ])
  ]);
}
var BasicYearTable = /* @__PURE__ */ _export_sfc(_sfc_main$g, [["render", _sfc_render$6]]);
const defaulDateFormat = {
  year: "YYYY",
  month: "YYYY-MM",
  date: "YYYY-MM-DD",
  datetime: "YYYY-MM-DD HH:mm:ss",
  daterange: "YYYY-MM-DD",
  monthrange: "YYYY-MM",
  datetimerange: "YYYY-MM-DD HH:mm:ss",
  time: "HH:mm:ss",
  timerange: "HH:mm:ss"
};
const devDateFormat = {
  year: "YYYY",
  month: "YYYYMM",
  date: "YYYYMMDD",
  datetime: "YYYYMMDD HH:mm:ss",
  daterange: "YYYYMMDD",
  monthrange: "YYYYMM",
  datetimerange: "YYYYMMDD HH:mm:ss",
  time: "HH:mm:ss",
  timerange: "HH:mm:ss"
};
const _hoisted_1$c = { class: "ef-time-picker-wrapper" };
const _hoisted_2$a = ["onClick"];
const _hoisted_3$7 = ["onClick"];
const _hoisted_4$6 = ["onClick"];
const __default__$9 = {
  name: "ef-time-picker-panel"
};
const _sfc_main$f = /* @__PURE__ */ Object.assign(__default__$9, {
  props: {
    disabledHours: {
      type: Function,
      default: (item) => false
    },
    disabledMinutes: {
      type: Function,
      default: (item) => false
    },
    disabledSeconds: {
      type: Function,
      default: (item) => false
    },
    modelValue: {
      type: String,
      default: ""
    }
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { expose, emit }) {
    const props2 = __props;
    const efDate = inject("efDateCommonPick");
    const options = reactive({
      hours: Array.from({ length: 24 }, (v, i) => i < 10 ? "0" + i : "" + i),
      minutes: Array.from({ length: 60 }, (v, i) => i < 10 ? "0" + i : "" + i),
      seconds: Array.from({ length: 60 }, (v, i) => i < 10 ? "0" + i : "" + i)
    });
    const viewData = reactive({
      hours: "00",
      minutes: "00",
      seconds: "00"
    });
    const hoursRef = ref();
    const minutesRef = ref();
    const secondsRef = ref();
    const hoverMap = reactive({
      hours: false,
      minutes: false,
      seconds: false
    });
    const hms = computed((_) => `${viewData.hours}:${viewData.minutes}:${viewData.seconds}`);
    const adjustPositon = () => {
      nextTick((_) => {
        let hoursScrollTop = parseInt(viewData.hours) * 32;
        let minutesScrollTop = parseInt(viewData.minutes) * 32;
        let secondsScrollTop = parseInt(viewData.seconds) * 32;
        hoursRef.value.scrollTop = Math.max(hoursScrollTop, 0);
        minutesRef.value.scrollTop = Math.max(minutesScrollTop, 0);
        secondsRef.value.scrollTop = Math.max(secondsScrollTop, 0);
      });
    };
    watch((_) => viewData.hours, (newVal) => {
      adjustPositon();
    });
    watch((_) => viewData.minutes, (newVal) => {
      adjustPositon();
    });
    watch((_) => viewData.seconds, (newVal) => {
      adjustPositon();
    });
    watch((_) => props2.modelValue, (newVal) => {
      let dayjsCase = dayjs(newVal, "HH:mm:ss");
      if (!newVal) {
        dayjsCase = dayjs();
      }
      viewData.hours = dayjsCase.format("HH");
      viewData.minutes = dayjsCase.format("mm");
      viewData.seconds = dayjsCase.format("ss");
    }, { immediate: true });
    watch(efDate.popperVisible, (newVal) => {
      newVal && adjustPositon();
    });
    const onClickItem = (val, type4, disabled) => {
      if (disabled)
        return;
      viewData[type4] = val;
      emit("update:modelValue", hms.value);
      emit("change", hms.value, type4, val);
    };
    expose({
      adjustPositon
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", _hoisted_1$c, [
        createElementVNode("ul", {
          onMouseenter: _cache[0] || (_cache[0] = ($event) => hoverMap.hours = true),
          onMouseleave: _cache[1] || (_cache[1] = ($event) => hoverMap.hours = false),
          class: normalizeClass(["item-wrapper scrollBarStyle", { hiddenScrollbar: !hoverMap.hours }]),
          ref_key: "hoursRef",
          ref: hoursRef
        }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList(options.hours, (item) => {
            return openBlock(), createElementBlock("li", {
              onClick: ($event) => onClickItem(item, "hours", __props.disabledHours(item)),
              class: normalizeClass(["item", { active: viewData.hours == item, disabled: __props.disabledHours(item) }])
            }, toDisplayString(item), 11, _hoisted_2$a);
          }), 256))
        ], 34),
        createElementVNode("ul", {
          onMouseenter: _cache[2] || (_cache[2] = ($event) => hoverMap.minutes = true),
          onMouseleave: _cache[3] || (_cache[3] = ($event) => hoverMap.minutes = false),
          class: normalizeClass([{ hiddenScrollbar: !hoverMap.minutes }, "item-wrapper scrollBarStyle minutes"]),
          ref_key: "minutesRef",
          ref: minutesRef
        }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList(options.minutes, (item) => {
            return openBlock(), createElementBlock("li", {
              onClick: ($event) => onClickItem(item, "minutes", __props.disabledMinutes(item)),
              class: normalizeClass(["item", { active: viewData.minutes == item, disabled: __props.disabledMinutes(item) }])
            }, toDisplayString(item), 11, _hoisted_3$7);
          }), 256))
        ], 34),
        createElementVNode("ul", {
          class: normalizeClass(["item-wrapper scrollBarStyle", { hiddenScrollbar: !hoverMap.seconds }]),
          ref_key: "secondsRef",
          ref: secondsRef,
          onMouseenter: _cache[4] || (_cache[4] = ($event) => hoverMap.seconds = true),
          onMouseleave: _cache[5] || (_cache[5] = ($event) => hoverMap.seconds = false)
        }, [
          (openBlock(true), createElementBlock(Fragment, null, renderList(options.seconds, (item) => {
            return openBlock(), createElementBlock("li", {
              onClick: ($event) => onClickItem(item, "seconds", __props.disabledSeconds(item)),
              class: normalizeClass(["item", { active: viewData.seconds == item, disabled: __props.disabledSeconds(item) }])
            }, toDisplayString(item), 11, _hoisted_4$6);
          }), 256))
        ], 34)
      ]);
    };
  }
});
const _sfc_main$e = {
  name: "ef-panel-date-pick",
  emits: ["update:modelValue", "change"],
  components: {
    EfIcon,
    EfBasicYearTable: BasicYearTable,
    EfBasicMonthTable: BasicMonthTable,
    EfBasicDateTable: BasicDateTable,
    TimePick: _sfc_main$f
  },
  props: {
    multiple: Boolean,
    disabledDate: {
      type: Function,
      default: (_) => false
    },
    type: {
      type: String,
      default: "date",
      validator: (val) => ["", "year", "month", "date", "datetime"].includes(val)
    },
    modelValue: {
      type: [String, Array],
      default: ""
    },
    labelFormat: {
      type: String,
      default: void 0
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const currentView = ref(props2.type);
    const viewYear = ref(dayjs().format("YYYY"));
    const viewMonth = ref(dayjs().format("MM"));
    const data = reactive({
      year: props2.multiple ? [] : "",
      month: props2.multiple ? [] : "",
      date: props2.multiple ? [] : ""
    });
    const dateInputValue = ref("");
    const timeInputValue = ref("");
    const timeValue = ref("");
    const timePickVisible = ref(false);
    const timePickRef = ref();
    const efDate = inject("efDateCommonPick", {});
    const viewYearMonth = computed((_) => "" + viewYear.value + viewMonth.value);
    const viewYearForYearTable = computed((_) => (Math.floor(viewYear.value / 10) * 10).toString());
    const labelForViewYear = computed((_) => "" + viewYearForYearTable.value + " - " + (parseInt(viewYearForYearTable.value) + 9));
    const timePickStyle = computed((_) => {
      return {
        zIndex: getZIndex()
      };
    });
    const datetimeValue = computed((_) => `${data.date} ${timeValue.value}`);
    const clear = (_) => {
      dateInputValue.value = "";
      timeInputValue.value = "";
      timeValue.value = "";
      data.year = "";
      data.month = "";
      data.date = "";
    };
    watch((_) => props2.modelValue, (newVal, oldVal) => {
      if (!newVal || Array.isArray(newVal) && newVal.length < 1) {
        clear();
        return;
      }
      if (props2.type != "datetime" && props2.multiple) {
        if (!Array.isArray(newVal)) {
          throw new Error(`${newVal} \u65F6\u95F4\u683C\u5F0F\u9A8C\u8BC1\u9519\u8BEF \u591A\u9009\u5FC5\u987B\u4E3A\u6570\u7EC4`);
        }
        data[props2.type] = newVal;
      } else {
        if (!newVal)
          return;
        let dayjsCase = dayjs(newVal, devDateFormat[props2.type]);
        viewYear.value = dayjsCase.format("YYYY");
        viewMonth.value = dayjsCase.format("MM");
        if (props2.type == "datetime") {
          data.date = dayjsCase.format("YYYYMMDD");
          dateInputValue.value = dayjsCase.format("YYYY-MM-DD");
          timeValue.value = dayjsCase.format("HH:mm:ss");
          timeInputValue.value = dayjsCase.format("HH:mm:ss");
        } else {
          data[props2.type] = newVal;
        }
      }
    }, { immediate: true });
    watch((_) => props2.type, (newVal, oldVal) => {
      currentView.value = newVal;
    });
    watch(efDate.popperVisible, (newVal) => {
      if (!newVal) {
        timePickVisible.value = false;
      }
    });
    const onPreYear = () => {
      if (currentView.value == "year") {
        let preYearDayjs = dayjs(viewYearForYearTable.value, "YYYY").add(-10, "year");
        viewYear.value = preYearDayjs.format("YYYY");
      } else {
        let preYearDayjs = dayjs(viewYear.value, "YYYY").add(-1, "year");
        viewYear.value = preYearDayjs.format("YYYY");
      }
    };
    const onPreMonth = () => {
      let preYearMonthDayjs = dayjs(viewYearMonth.value, "YYYYMM").add(-1, "month");
      viewYear.value = preYearMonthDayjs.format("YYYY");
      viewMonth.value = preYearMonthDayjs.format("MM");
    };
    const onNextYear = () => {
      if (currentView.value == "year") {
        let nextYearDayjs = dayjs(viewYearForYearTable.value, "YYYY").add(10, "year");
        viewYear.value = nextYearDayjs.format("YYYY");
      } else {
        let nextYearDayjs = dayjs(viewYear.value, "YYYY").add(1, "year");
        viewYear.value = nextYearDayjs.format("YYYY");
      }
    };
    const onNextMonth = () => {
      let nextYearMonthDayjs = dayjs(viewYearMonth.value, "YYYYMM").add(1, "month");
      viewYear.value = nextYearMonthDayjs.format("YYYY");
      viewMonth.value = nextYearMonthDayjs.format("MM");
    };
    const onToggleToMonth = () => {
      currentView.value = "month";
    };
    const onToggleToYear = () => {
      currentView.value = "year";
    };
    const onYearPick = (val) => {
      if (props2.type == "year") {
        emit("update:modelValue", val);
        emit("change", val);
      } else {
        viewYear.value = val;
        currentView.value = "month";
      }
    };
    const onMonthPick = (val) => {
      if (props2.type == "month") {
        emit("update:modelValue", val);
        emit("change", val);
      } else {
        viewMonth.value = dayjs(val, "YYYYMM").format("MM");
        currentView.value = "date";
      }
    };
    const onDatePick = (val) => {
      if (props2.type == "datetime") {
        dateInputValue.value = dayjs(val, "YYYYMMDD").format("YYYY-MM-DD");
        if (!timeInputValue.value) {
          let nowTime = dayjs().format("HH:mm:ss");
          timeInputValue.value = nowTime;
          timeValue.value = nowTime;
        }
      }
      let value = props2.type == "datetime" ? datetimeValue.value : val;
      emit("update:modelValue", value);
      emit("change", value);
    };
    const onTimeInputClick = () => {
      if (!timePickVisible.value) {
        timePickVisible.value = true;
        timePickRef.value.adjustPositon();
        timeInputValue.value && (timeValue.value = timeInputValue.value);
      }
    };
    const onTimeChange = (val, type4, typeVal) => {
      if (props2.type == "datetime") {
        timeInputValue.value = val;
        if (!dateInputValue.value) {
          dateInputValue.value = dayjs().format("YYYY-MM-DD");
          data.date = dayjs().format("YYYYMMDD");
        }
      }
      emit("update:modelValue", datetimeValue.value);
      emit("change", datetimeValue.value);
    };
    const onInputDateChange = (e) => {
      let val = e.target.value;
      let dayjsCase = dayjs(val, "YYYY-MM-DD", "zh-cn", true);
      if (!dayjsCase.isValid()) {
        if (data.date) {
          let formatDate = dayjs(data.date, "YYYYMMDD").format("YYYY-MM-DD");
          dateInputValue.value = "";
          dateInputValue.value = formatDate;
        } else {
          dateInputValue.value = "";
        }
      } else {
        data.date = val;
        emit("update:modelValue", datetimeValue.value);
        emit("change", datetimeValue.value);
      }
    };
    const onInputTimeChange = (e) => {
      let val = e.target.value;
      let dayjsCase = dayjs(val, "HH:mm:ss", "zh-cn", true);
      if (!dayjsCase.isValid()) {
        if (timeValue.value) {
          timeInputValue.value = "";
          timeInputValue.value = timeValue.value;
        } else {
          timeInputValue.value = "";
        }
      } else {
        timeValue.value = val;
        emit("update:modelValue", datetimeValue.value);
        emit("change", datetimeValue.value);
      }
    };
    return {
      currentView,
      data,
      viewYear,
      viewMonth,
      viewYearMonth,
      viewYearForYearTable,
      labelForViewYear,
      dateInputValue,
      timeValue,
      timeInputValue,
      timePickStyle,
      timePickVisible,
      timePickRef,
      onPreYear,
      onPreMonth,
      onNextYear,
      onNextMonth,
      onToggleToMonth,
      onToggleToYear,
      onYearPick,
      onMonthPick,
      onDatePick,
      onTimeChange,
      onTimeInputClick,
      onInputDateChange,
      onInputTimeChange
    };
  }
};
const _hoisted_1$b = {
  key: 0,
  class: "ef-panel-date-pick-datetime-header"
};
const _hoisted_2$9 = { class: "item" };
const _hoisted_3$6 = ["value"];
const _hoisted_4$5 = { class: "item" };
const _hoisted_5$4 = ["value"];
const _hoisted_6$3 = { class: "ef-panel-date-pick-header flex-base" };
const _hoisted_7$3 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_8$2 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_9$2 = { class: "ef-panel-date-pick-content" };
function _sfc_render$5(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_time_pick = resolveComponent("time-pick");
  const _component_ef_icon = resolveComponent("ef-icon");
  const _component_ef_basic_year_table = resolveComponent("ef-basic-year-table");
  const _component_ef_basic_month_table = resolveComponent("ef-basic-month-table");
  const _component_ef_basic_date_table = resolveComponent("ef-basic-date-table");
  return openBlock(), createElementBlock("div", {
    class: "ef-panel-date-pick",
    onClick: _cache[10] || (_cache[10] = ($event) => $setup.timePickVisible = false)
  }, [
    $props.type == "datetime" ? (openBlock(), createElementBlock("div", _hoisted_1$b, [
      createElementVNode("div", _hoisted_2$9, [
        createElementVNode("input", {
          placeholder: "\u9009\u62E9\u65E5\u671F",
          class: "item-input",
          type: "text",
          value: $setup.dateInputValue,
          onChange: _cache[0] || (_cache[0] = (...args) => $setup.onInputDateChange && $setup.onInputDateChange(...args))
        }, null, 40, _hoisted_3$6)
      ]),
      createElementVNode("div", _hoisted_4$5, [
        createElementVNode("input", {
          placeholder: "\u9009\u62E9\u65F6\u95F4",
          class: "item-input",
          type: "text",
          value: $setup.timeInputValue,
          onClick: _cache[1] || (_cache[1] = withModifiers((...args) => $setup.onTimeInputClick && $setup.onTimeInputClick(...args), ["stop"])),
          onChange: _cache[2] || (_cache[2] = (...args) => $setup.onInputTimeChange && $setup.onInputTimeChange(...args))
        }, null, 40, _hoisted_5$4),
        withDirectives(createVNode(_component_time_pick, {
          ref: "timePickRef",
          style: normalizeStyle($setup.timePickStyle),
          class: "time-pick-wrapper",
          modelValue: $setup.timeValue,
          "onUpdate:modelValue": _cache[3] || (_cache[3] = ($event) => $setup.timeValue = $event),
          onChange: $setup.onTimeChange,
          onClick: _cache[4] || (_cache[4] = withModifiers(() => {
          }, ["stop"]))
        }, null, 8, ["style", "modelValue", "onChange"]), [
          [vShow, $setup.timePickVisible]
        ])
      ])
    ])) : createCommentVNode("", true),
    createElementVNode("div", _hoisted_6$3, [
      createVNode(_component_ef_icon, {
        onClick: $setup.onPreYear,
        type: "icon-double-arrow-left",
        size: "small"
      }, null, 8, ["onClick"]),
      withDirectives(createVNode(_component_ef_icon, {
        onClick: $setup.onPreMonth,
        type: "icon-arrow-left",
        size: "small"
      }, null, 8, ["onClick"]), [
        [vShow, ["date", "datetime"].includes($setup.currentView)]
      ]),
      _hoisted_7$3,
      withDirectives(createElementVNode("span", { class: "ef-panel-date-pick-header-title" }, toDisplayString($setup.labelForViewYear), 513), [
        [vShow, $setup.currentView == "year"]
      ]),
      withDirectives(createElementVNode("span", {
        onClick: _cache[5] || (_cache[5] = (...args) => $setup.onToggleToYear && $setup.onToggleToYear(...args)),
        class: "ef-panel-date-pick-header-title"
      }, toDisplayString($setup.viewYear + "\u5E74"), 513), [
        [vShow, ["date", "datetime"].includes($setup.currentView) || $setup.currentView == "month"]
      ]),
      withDirectives(createElementVNode("span", {
        onClick: _cache[6] || (_cache[6] = (...args) => $setup.onToggleToMonth && $setup.onToggleToMonth(...args)),
        class: "ef-panel-date-pick-header-title margin-left-sm"
      }, toDisplayString($setup.viewMonth + "\u6708"), 513), [
        [vShow, ["date", "datetime"].includes($setup.currentView)]
      ]),
      _hoisted_8$2,
      withDirectives(createVNode(_component_ef_icon, {
        onClick: $setup.onNextMonth,
        type: "icon-arrow-right",
        size: "small"
      }, null, 8, ["onClick"]), [
        [vShow, ["date", "datetime"].includes($setup.currentView)]
      ]),
      createVNode(_component_ef_icon, {
        onClick: $setup.onNextYear,
        type: "icon-double-arro-right",
        size: "small"
      }, null, 8, ["onClick"])
    ]),
    createElementVNode("div", _hoisted_9$2, [
      $setup.currentView == "year" ? (openBlock(), createBlock(_component_ef_basic_year_table, {
        key: 0,
        disabledDate: $props.disabledDate,
        multiple: $props.multiple,
        year: $setup.viewYearForYearTable,
        modelValue: $setup.data.year,
        "onUpdate:modelValue": _cache[7] || (_cache[7] = ($event) => $setup.data.year = $event),
        onPick: $setup.onYearPick
      }, null, 8, ["disabledDate", "multiple", "year", "modelValue", "onPick"])) : $setup.currentView == "month" ? (openBlock(), createBlock(_component_ef_basic_month_table, {
        key: 1,
        disabledDate: $props.disabledDate,
        multiple: $props.multiple,
        year: $setup.viewYear,
        modelValue: $setup.data.month,
        "onUpdate:modelValue": _cache[8] || (_cache[8] = ($event) => $setup.data.month = $event),
        onPick: $setup.onMonthPick
      }, null, 8, ["disabledDate", "multiple", "year", "modelValue", "onPick"])) : ["date", "datetime"].includes($setup.currentView) ? (openBlock(), createBlock(_component_ef_basic_date_table, {
        key: 2,
        disabledDate: $props.disabledDate,
        multiple: $props.multiple,
        month: $setup.viewYearMonth,
        modelValue: $setup.data.date,
        "onUpdate:modelValue": _cache[9] || (_cache[9] = ($event) => $setup.data.date = $event),
        onPick: $setup.onDatePick
      }, null, 8, ["disabledDate", "multiple", "month", "modelValue", "onPick"])) : createCommentVNode("", true)
    ])
  ]);
}
var PanelDatePick = /* @__PURE__ */ _export_sfc(_sfc_main$e, [["render", _sfc_render$5]]);
const _hoisted_1$a = {
  key: 0,
  class: "ef-panel-date-range-pick-header"
};
const _hoisted_2$8 = { class: "left-box" };
const _hoisted_3$5 = { class: "item" };
const _hoisted_4$4 = ["disabled", "value"];
const _hoisted_5$3 = { class: "item" };
const _hoisted_6$2 = ["disabled", "value", "onClick"];
const _hoisted_7$2 = { class: "right-box" };
const _hoisted_8$1 = { class: "item" };
const _hoisted_9$1 = ["disabled", "value"];
const _hoisted_10$1 = { class: "item" };
const _hoisted_11$1 = ["disabled", "value", "onClick"];
const _hoisted_12$1 = { class: "ef-panel-date-range-pick-content" };
const _hoisted_13$1 = { class: "ef-panel-date-range-pick-content-left" };
const _hoisted_14$1 = { class: "basic-date-table-header flex-base" };
const _hoisted_15$1 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_16 = { class: "basic-date-table-header-title" };
const _hoisted_17 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_18 = { class: "basic-date-table-wrapper" };
const _hoisted_19 = { class: "ef-panel-date-range-pick-content-right" };
const _hoisted_20 = { class: "basic-date-table-header flex-base" };
const _hoisted_21 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_22 = { class: "basic-date-table-header-title" };
const _hoisted_23 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_24 = { class: "basic-date-table-wrapper" };
const _hoisted_25 = /* @__PURE__ */ createElementVNode("div", { class: "ef-panel-date-range-pick-footer" }, null, -1);
const __default__$8 = {
  name: "panel-date-range-pick"
};
const _sfc_main$d = /* @__PURE__ */ Object.assign(__default__$8, {
  props: {
    modelValue: {
      type: Array,
      default: []
    },
    disabledDate: {
      type: Function,
      default: (_) => false
    },
    type: {
      type: String,
      default: "daterange",
      validator: (val) => ["", "daterange", "datetimerange"].includes(val)
    }
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const leftViewMonth = ref(dayjs().format("YYYYMM"));
    const rightViewMonth = ref(dayjs().add(1, "month").format("YYYYMM"));
    const leftSelectedArray = ref([]);
    const rightSelectedArray = ref([]);
    const hoverDate = ref();
    const startDate = ref();
    const endDate = ref();
    const startDateInputValue = ref("");
    const startTimeInputValue = ref("");
    const endDateInputValue = ref("");
    const endTimeInputValue = ref("");
    const startTimeValue = ref("");
    const endTimeValue = ref("");
    const endTimePickVisible = ref(false);
    const startTimePickVisible = ref(false);
    const startTimePickRef = ref();
    const endTimePickRef = ref();
    const efDate = inject("efDateCommonPick");
    const mergeSelectedArray = computed((_) => [...leftSelectedArray.value, ...rightSelectedArray.value].sort((a, b) => a > b ? 1 : -1));
    const leftViewMonthLabel = computed((_) => dayjs(leftViewMonth.value, "YYYYMM").format("YYYY\u5E74MM\u6708"));
    const rightViewMonthLabel = computed((_) => dayjs(rightViewMonth.value, "YYYYMM").format("YYYY\u5E74MM\u6708"));
    const ifDisableLeftNextYear = computed((_) => dayjs(leftViewMonth.value, "YYYYMM").add(1, "year").isSameOrAfter(rightViewMonth.value));
    const ifDisableLeftNextMonth = computed((_) => dayjs(leftViewMonth.value, "YYYYMM").add(1, "month").isSameOrAfter(rightViewMonth.value));
    const ifDisableRightPreYear = computed((_) => dayjs(rightViewMonth.value, "YYYYMM").subtract(1, "year").isSameOrBefore(leftViewMonth.value));
    const ifDisableRightPreMonth = computed((_) => dayjs(rightViewMonth.value, "YYYYMM").subtract(1, "month").isSameOrBefore(leftViewMonth.value));
    const datetimerangeValue = computed((_) => mergeSelectedArray.value.map((item, index) => {
      let tempMap = {
        0: startTimeValue.value,
        1: endTimeValue.value
      };
      return `${item} ${tempMap[index]}`;
    }));
    const timePickStyle = computed((_) => {
      return {
        zIndex: getZIndex()
      };
    });
    const inRange = (date4) => {
      if (startDate.value && (endDate.value || hoverDate.value)) {
        return dayjs(date4).isBetween(startDate.value, endDate.value || hoverDate.value) || dayjs(date4).isSame(startDate.value) || dayjs(date4).isSame(endDate.value || hoverDate.value);
      } else {
        return false;
      }
    };
    const clearSelectedData = (_) => {
      leftSelectedArray.value = [];
      rightSelectedArray.value = [];
      startDate.value = void 0;
      endDate.value = void 0;
    };
    const clear = (_) => {
      startDateInputValue.value = "";
      startTimeInputValue.value = "";
      startTimeValue.value = "";
      endDateInputValue.value = "";
      endTimeInputValue.value = "";
      endTimeValue.value = "";
      leftSelectedArray.value = [];
      rightSelectedArray.value = [];
      startDate.value = void 0;
      endDate.value = void 0;
    };
    const handlePick = (currentDate, direction) => {
      if (startDate.value && endDate.value) {
        clearSelectedData();
        direction == "left" ? leftSelectedArray.value = [currentDate.date] : rightSelectedArray.value = [currentDate.date];
        startDate.value = currentDate.date;
      } else if (mergeSelectedArray.value.length == 2) {
        endDate.value = currentDate.date;
        if (props2.type == "datetimerange") {
          startDateInputValue.value = dayjs(mergeSelectedArray.value[0], "YYYYMMDD").format("YYYY-MM-DD");
          endDateInputValue.value = dayjs(mergeSelectedArray.value[1], "YYYYMMDD").format("YYYY-MM-DD");
          let formatTime = dayjs().format("HH:mm:ss");
          if (!startTimeValue.value) {
            startTimeValue.value = formatTime;
            startTimeInputValue.value = formatTime;
          }
          if (!endTimeValue.value) {
            endTimeValue.value = formatTime;
            endTimeInputValue.value = formatTime;
          }
          emit("update:modelValue", datetimerangeValue.value);
          emit("change", datetimerangeValue.value);
        } else {
          emit("update:modelValue", mergeSelectedArray.value);
          emit("change", mergeSelectedArray.value);
        }
      } else if (mergeSelectedArray.value.length == 1) {
        startDate.value = currentDate.date;
      } else if (mergeSelectedArray.value.length == 0) {
        startDate.value = currentDate.date;
        endDate.value = currentDate.date;
        direction == "left" ? leftSelectedArray.value = [currentDate.date, currentDate.date] : rightSelectedArray.value = [currentDate.date, currentDate.date];
        if (props2.type == "datetimerange") {
          startDateInputValue.value = dayjs(mergeSelectedArray.value[0], "YYYYMMDD").format("YYYY-MM-DD");
          endDateInputValue.value = dayjs(mergeSelectedArray.value[1], "YYYYMMDD").format("YYYY-MM-DD");
          let formatTime = dayjs().format("HH:mm:ss");
          if (!startTimeValue.value) {
            startTimeValue.value = formatTime;
            startTimeInputValue.value = formatTime;
          }
          if (!endTimeValue.value) {
            endTimeValue.value = formatTime;
            endTimeInputValue.value = formatTime;
          }
          emit("update:modelValue", datetimerangeValue.value);
          emit("change", datetimerangeValue.value);
        } else {
          emit("update:modelValue", mergeSelectedArray.value);
          emit("change", mergeSelectedArray.value);
        }
      }
    };
    const onLeftPreYear = (_) => {
      leftViewMonth.value = dayjs(leftViewMonth.value, "YYYYMM").subtract(1, "year").format("YYYYMM");
    };
    const onLeftPreMonth = (_) => {
      leftViewMonth.value = dayjs(leftViewMonth.value, "YYYYMM").subtract(1, "month").format("YYYYMM");
    };
    const onLeftNextMonth = (_) => {
      if (ifDisableLeftNextMonth.value)
        return;
      leftViewMonth.value = dayjs(leftViewMonth.value, "YYYYMM").add(1, "month").format("YYYYMM");
    };
    const onLeftNextYear = (_) => {
      if (ifDisableLeftNextYear.value)
        return;
      leftViewMonth.value = dayjs(leftViewMonth.value, "YYYYMM").add(1, "year").format("YYYYMM");
    };
    const onRightPreYear = (_) => {
      if (ifDisableRightPreYear.value)
        return;
      rightViewMonth.value = dayjs(rightViewMonth.value, "YYYYMM").subtract(1, "year").format("YYYYMM");
    };
    const onRightPreMonth = (_) => {
      if (ifDisableRightPreMonth.value)
        return;
      rightViewMonth.value = dayjs(rightViewMonth.value, "YYYYMM").subtract(1, "month").format("YYYYMM");
    };
    const onRightNextMonth = (_) => {
      rightViewMonth.value = dayjs(rightViewMonth.value, "YYYYMM").add(1, "month").format("YYYYMM");
    };
    const onRightNextYear = (_) => {
      rightViewMonth.value = dayjs(rightViewMonth.value, "YYYYMM").add(1, "year").format("YYYYMM");
    };
    const onLeftPick = (val, currentDate) => {
      handlePick(currentDate, "left");
    };
    const onRightPick = (val, currentDate) => {
      handlePick(currentDate, "right");
    };
    const onHoverCell = (row) => {
      hoverDate.value = row.date;
    };
    const onStartInputDateChange = (e) => {
      let val = e.target.value;
      let dayjsCase = dayjs(val, "YYYY-MM-DD", "zh-cn", true);
      if (!dayjsCase.isValid()) {
        if (startDate.value) {
          let formatDate = dayjs(startDate.value, "YYYYMMDD").format("YYYY-MM-DD");
          startDateInputValue.value = "";
          startDateInputValue.value = formatDate;
        } else {
          startDateInputValue.value = "";
        }
      } else {
        startDate.value = dayjsCase.format("YYYYMMDD");
        if (!endDate.value || endDate.value < startDate.value) {
          endDate.value = startDate.value;
        }
        let tempArr = [startDate.value, endDate.value].sort((a, b) => a > b ? 1 : -1).map((item, index) => {
          let tempMap = {
            0: startTimeValue.value,
            1: endTimeValue.value
          };
          return `${item} ${tempMap[index]}`;
        });
        emit("update:modelValue", tempArr);
        emit("change", tempArr);
      }
    };
    const onStartTimeInputClick = (_) => {
      if (endTimePickVisible.value) {
        endTimePickVisible.value = false;
      }
      if (!startTimePickVisible.value) {
        startTimePickVisible.value = true;
        startTimePickRef.value.adjustPositon();
        startTimeInputValue.value && (startTimeValue.value = startTimeInputValue.value);
      }
    };
    const onStartInputTimeChange = (e) => {
      let val = e.target.value;
      let dayjsCase = dayjs(val, "HH:mm:ss", "zh-cn", true);
      if (!dayjsCase.isValid()) {
        if (startTimeValue.value) {
          startTimeInputValue.value = "";
          startTimeInputValue.value = startTimeValue.value;
        } else {
          startTimeInputValue.value = "";
        }
      } else {
        startTimeValue.value = val;
        emit("update:modelValue", datetimerangeValue.value);
        emit("change", datetimerangeValue.value);
      }
    };
    const onEndInputDateChange = (e) => {
      let val = e.target.value;
      let dayjsCase = dayjs(val, "YYYY-MM-DD", "zh-cn", true);
      if (!dayjsCase.isValid()) {
        if (endDate.value) {
          let formatDate = dayjs(endDate.value, "YYYYMMDD").format("YYYY-MM-DD");
          endDateInputValue.value = "";
          endDateInputValue.value = formatDate;
        } else {
          endDateInputValue.value = "";
        }
      } else {
        endDate.value = dayjsCase.format("YYYYMMDD");
        if (!startDate.value || endDate.value < startDate.value) {
          startDate.value = endDate.value;
        }
        let tempArr = [startDate.value, endDate.value].sort((a, b) => a > b ? 1 : -1).map((item, index) => {
          let tempMap = {
            0: startTimeValue.value,
            1: endTimeValue.value
          };
          return `${item} ${tempMap[index]}`;
        });
        emit("update:modelValue", tempArr);
        emit("change", tempArr);
      }
    };
    const onEndTimeInputClick = (_) => {
      if (startTimePickVisible.value) {
        startTimePickVisible.value = false;
      }
      if (!endTimePickVisible.value) {
        endTimePickVisible.value = true;
        endTimePickRef.value.adjustPositon();
        endTimeInputValue.value && (endTimeValue.value = endTimeInputValue.value);
      }
    };
    const onEndInputTimeChange = (e) => {
      let val = e.target.value;
      let dayjsCase = dayjs(val, "HH:mm:ss", "zh-cn", true);
      if (!dayjsCase.isValid()) {
        if (endTimeValue.value) {
          endTimeInputValue.value = "";
          endTimeInputValue.value = endTimeValue.value;
        } else {
          endTimeInputValue.value = "";
        }
      } else {
        endTimeValue.value = val;
        emit("update:modelValue", datetimerangeValue.value);
        emit("change", datetimerangeValue.value);
      }
    };
    const onStartTimeChange = (val, type4, typeVal) => {
      if (props2.type == "datetimerange") {
        startTimeInputValue.value = val;
        let nowFormatDate = dayjs().format("YYYYMMDD");
        if (!startDateInputValue.value || !endDateInputValue.value) {
          leftSelectedArray.value = [nowFormatDate, nowFormatDate];
        }
        if (!endTimeInputValue.value) {
          endTimeValue.value = val;
        }
      }
      if (startDate.value == endDate.value && endTimeValue.value < val) {
        endTimeValue.value = val;
      }
      emit("update:modelValue", datetimerangeValue.value);
      emit("change", datetimerangeValue.value);
    };
    const onEndTimeChange = (val, type4, typeVal) => {
      if (props2.type == "datetimerange") {
        endTimeInputValue.value = val;
        let nowFormatDate = dayjs().format("YYYYMMDD");
        if (!startDateInputValue.value || !endDateInputValue.value) {
          leftSelectedArray.value = [nowFormatDate, nowFormatDate];
        }
        if (!startTimeInputValue.value) {
          startTimeValue.value = val;
        }
      }
      if (startDate.value == endDate.value && startTimeValue.value > val) {
        startTimeValue.value = val;
      }
      emit("update:modelValue", datetimerangeValue.value);
      emit("change", datetimerangeValue.value);
    };
    watch((_) => props2.modelValue, (newVal, oldVal) => {
      if (Array.isArray(newVal) && newVal.length == 2) {
        let [start, end] = newVal.sort((a, b) => a > b ? 1 : -1);
        let formatString = props2.type == "daterange" ? "YYYYMMDD" : "YYYYMMDD HH:mm:ss";
        let startDayjs = dayjs(start, formatString);
        let endDayjs = dayjs(end, formatString);
        startDate.value = startDayjs.format("YYYYMMDD");
        endDate.value = endDayjs.format("YYYYMMDD");
        if (startDayjs.format("YYYYMM") == endDayjs.format("YYYYMM")) {
          leftViewMonth.value = startDayjs.format("YYYYMM");
          rightViewMonth.value = startDayjs.add(1, "month").format("YYYYMM");
          leftSelectedArray.value = [startDate.value, endDate.value];
          rightSelectedArray.value = [];
        } else {
          leftViewMonth.value = startDayjs.format("YYYYMM");
          rightViewMonth.value = endDayjs.format("YYYYMM");
          leftSelectedArray.value = [startDate.value];
          rightSelectedArray.value = [endDate.value];
        }
        if (props2.type == "datetimerange") {
          startTimeValue.value = startDayjs.format("HH:mm:ss");
          startTimeInputValue.value = startDayjs.format("HH:mm:ss");
          endTimeValue.value = endDayjs.format("HH:mm:ss");
          endTimeInputValue.value = endDayjs.format("HH:mm:ss");
          startDateInputValue.value = startDayjs.format("YYYY-MM-DD");
          endDateInputValue.value = endDayjs.format("YYYY-MM-DD");
        }
      } else if (Array.isArray(newVal) && newVal.length == 1) {
        throw new Error("\u8BF7\u81F3\u5C11\u4F20\u5165\u4E24\u4E2A\u65F6\u95F4");
      } else if (!newVal || Array.isArray(newVal) && newVal.length == 0) {
        clear();
      }
    }, { immediate: true });
    watch(efDate.popperVisible, (newVal) => {
      if (!newVal) {
        startTimePickVisible.value = false;
        endTimePickVisible.value = false;
      }
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", {
        class: "ef-panel-date-range-pick",
        onClick: _cache[6] || (_cache[6] = ($event) => {
          startTimePickVisible.value = false;
          endTimePickVisible.value = false;
        })
      }, [
        __props.type == "datetimerange" ? (openBlock(), createElementBlock("div", _hoisted_1$a, [
          createElementVNode("div", _hoisted_2$8, [
            createElementVNode("div", _hoisted_3$5, [
              createElementVNode("input", {
                placeholder: "\u9009\u62E9\u65E5\u671F",
                class: "item-input",
                type: "text",
                disabled: unref(mergeSelectedArray).length == 1,
                value: startDateInputValue.value,
                onChange: onStartInputDateChange
              }, null, 40, _hoisted_4$4)
            ]),
            createElementVNode("div", _hoisted_5$3, [
              createElementVNode("input", {
                placeholder: "\u9009\u62E9\u65F6\u95F4",
                class: "item-input",
                type: "text",
                disabled: unref(mergeSelectedArray).length == 1,
                value: startTimeInputValue.value,
                onClick: withModifiers(onStartTimeInputClick, ["stop"]),
                onChange: onStartInputTimeChange
              }, null, 40, _hoisted_6$2),
              withDirectives(createVNode(_sfc_main$f, {
                ref_key: "startTimePickRef",
                ref: startTimePickRef,
                style: normalizeStyle(unref(timePickStyle)),
                class: "time-pick-wrapper",
                modelValue: startTimeValue.value,
                "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => startTimeValue.value = $event),
                onChange: onStartTimeChange,
                onClick: _cache[1] || (_cache[1] = withModifiers(() => {
                }, ["stop"]))
              }, null, 8, ["style", "modelValue"]), [
                [vShow, startTimePickVisible.value]
              ])
            ])
          ]),
          createElementVNode("div", _hoisted_7$2, [
            createElementVNode("div", _hoisted_8$1, [
              createElementVNode("input", {
                placeholder: "\u9009\u62E9\u65E5\u671F",
                class: "item-input",
                type: "text",
                disabled: unref(mergeSelectedArray).length == 1,
                value: endDateInputValue.value,
                onChange: onEndInputDateChange
              }, null, 40, _hoisted_9$1)
            ]),
            createElementVNode("div", _hoisted_10$1, [
              createElementVNode("input", {
                placeholder: "\u9009\u62E9\u65F6\u95F4",
                class: "item-input",
                type: "text",
                disabled: unref(mergeSelectedArray).length == 1,
                value: endTimeInputValue.value,
                onClick: withModifiers(onEndTimeInputClick, ["stop"]),
                onChange: onEndInputTimeChange
              }, null, 40, _hoisted_11$1),
              withDirectives(createVNode(_sfc_main$f, {
                ref_key: "endTimePickRef",
                ref: endTimePickRef,
                style: normalizeStyle(unref(timePickStyle)),
                class: "time-pick-wrapper",
                modelValue: endTimeValue.value,
                "onUpdate:modelValue": _cache[2] || (_cache[2] = ($event) => endTimeValue.value = $event),
                onChange: onEndTimeChange,
                onClick: _cache[3] || (_cache[3] = withModifiers(() => {
                }, ["stop"]))
              }, null, 8, ["style", "modelValue"]), [
                [vShow, endTimePickVisible.value]
              ])
            ])
          ])
        ])) : createCommentVNode("", true),
        createElementVNode("div", _hoisted_12$1, [
          createElementVNode("div", _hoisted_13$1, [
            createElementVNode("div", _hoisted_14$1, [
              createVNode(EfIcon, {
                onClick: onLeftPreYear,
                type: "icon-double-arrow-left",
                size: "small"
              }),
              createVNode(EfIcon, {
                onClick: onLeftPreMonth,
                type: "icon-arrow-left",
                size: "small"
              }),
              _hoisted_15$1,
              createElementVNode("span", _hoisted_16, toDisplayString(unref(leftViewMonthLabel)), 1),
              _hoisted_17,
              createVNode(EfIcon, {
                onClick: onLeftNextMonth,
                disabled: unref(ifDisableLeftNextMonth),
                type: "icon-arrow-right",
                size: "small"
              }, null, 8, ["disabled"]),
              createVNode(EfIcon, {
                onClick: onLeftNextYear,
                disabled: unref(ifDisableLeftNextYear),
                type: "icon-double-arro-right",
                size: "small"
              }, null, 8, ["disabled"])
            ]),
            createElementVNode("div", _hoisted_18, [
              createVNode(BasicDateTable, {
                month: leftViewMonth.value,
                multiple: "",
                modelValue: leftSelectedArray.value,
                "onUpdate:modelValue": _cache[4] || (_cache[4] = ($event) => leftSelectedArray.value = $event),
                disabledDate: __props.disabledDate,
                inRange,
                onPick: onLeftPick,
                onHoverCell
              }, null, 8, ["month", "modelValue", "disabledDate"])
            ])
          ]),
          createElementVNode("div", _hoisted_19, [
            createElementVNode("div", _hoisted_20, [
              createVNode(EfIcon, {
                onClick: onRightPreYear,
                disabled: unref(ifDisableRightPreYear),
                type: "icon-double-arrow-left",
                size: "small"
              }, null, 8, ["disabled"]),
              createVNode(EfIcon, {
                onClick: onRightPreMonth,
                disabled: unref(ifDisableRightPreMonth),
                type: "icon-arrow-left",
                size: "small"
              }, null, 8, ["disabled"]),
              _hoisted_21,
              createElementVNode("span", _hoisted_22, toDisplayString(unref(rightViewMonthLabel)), 1),
              _hoisted_23,
              createVNode(EfIcon, {
                onClick: onRightNextMonth,
                type: "icon-arrow-right",
                size: "small"
              }),
              createVNode(EfIcon, {
                onClick: onRightNextYear,
                type: "icon-double-arro-right",
                size: "small"
              })
            ]),
            createElementVNode("div", _hoisted_24, [
              createVNode(BasicDateTable, {
                month: rightViewMonth.value,
                multiple: "",
                modelValue: rightSelectedArray.value,
                "onUpdate:modelValue": _cache[5] || (_cache[5] = ($event) => rightSelectedArray.value = $event),
                disabledDate: __props.disabledDate,
                inRange,
                onPick: onRightPick,
                onHoverCell
              }, null, 8, ["month", "modelValue", "disabledDate"])
            ])
          ])
        ]),
        _hoisted_25
      ]);
    };
  }
});
const _hoisted_1$9 = { class: "ef-panel-date-range-pick" };
const _hoisted_2$7 = { class: "ef-panel-date-range-pick-content" };
const _hoisted_3$4 = { class: "ef-panel-date-range-pick-content-left" };
const _hoisted_4$3 = { class: "basic-date-table-header flex-base" };
const _hoisted_5$2 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_6$1 = { class: "basic-date-table-header-title" };
const _hoisted_7$1 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_8 = { class: "basic-date-table-wrapper" };
const _hoisted_9 = { class: "ef-panel-date-range-pick-content-right" };
const _hoisted_10 = { class: "basic-date-table-header flex-base" };
const _hoisted_11 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_12 = { class: "basic-date-table-header-title" };
const _hoisted_13 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
const _hoisted_14 = { class: "basic-date-table-wrapper" };
const _hoisted_15 = /* @__PURE__ */ createElementVNode("div", { class: "ef-panel-date-range-pick-footer" }, null, -1);
const __default__$7 = {
  name: "panel-month-range-pick"
};
const _sfc_main$c = /* @__PURE__ */ Object.assign(__default__$7, {
  props: {
    modelValue: {
      type: Array,
      default: []
    },
    disabledDate: {
      type: Function,
      default: (_) => false
    }
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const leftViewYear = ref(dayjs().format("YYYY"));
    const rightViewYear = ref(dayjs().add(1, "year").format("YYYY"));
    const leftSelectedArray = ref([]);
    const rightSelectedArray = ref([]);
    const hoverDate = ref();
    const startDate = ref();
    const endDate = ref();
    const mergeSelectedArray = computed((_) => [...leftSelectedArray.value, ...rightSelectedArray.value].sort((a, b) => a > b ? 1 : -1));
    const leftViewYearLabel = computed((_) => dayjs(leftViewYear.value, "YYYY").format("YYYY\u5E74"));
    const rightViewYearLabel = computed((_) => dayjs(rightViewYear.value, "YYYY").format("YYYY\u5E74"));
    const ifDisableLeftNextYear = computed((_) => dayjs(leftViewYear.value, "YYYY").add(1, "year").isSameOrAfter(rightViewYear.value));
    const ifDisableRightPreYear = computed((_) => dayjs(rightViewYear.value, "YYYY").subtract(1, "year").isSameOrBefore(leftViewYear.value));
    const clear = (_) => {
      leftSelectedArray.value = [];
      rightSelectedArray.value = [];
      startDate.value = void 0;
      endDate.value = void 0;
    };
    watch((_) => props2.modelValue, (newVal, oldVal) => {
      if (!newVal || Array.isArray(newVal) && newVal.length < 1) {
        clear();
        return;
      }
      if (Array.isArray(newVal) && newVal.length == 2) {
        let [start, end] = newVal.sort((a, b) => a > b ? 1 : -1);
        let formatString = "YYYYMM";
        let startDayjs = dayjs(start, formatString);
        let endDayjs = dayjs(end, formatString);
        startDate.value = startDayjs.format("YYYYMM");
        endDate.value = endDayjs.format("YYYYMM");
        if (startDayjs.format("YYYY") == endDayjs.format("YYYY")) {
          leftViewYear.value = startDayjs.format("YYYY");
          rightViewYear.value = startDayjs.add(1, "year").format("YYYY");
          leftSelectedArray.value = [startDate.value, endDate.value];
          rightSelectedArray.value = [];
        } else {
          leftViewYear.value = startDayjs.format("YYYY");
          rightViewYear.value = endDayjs.format("YYYY");
          leftSelectedArray.value = [startDate.value];
          rightSelectedArray.value = [endDate.value];
        }
      }
    }, { immediate: true });
    const inRange = (date4) => {
      if (startDate.value && (endDate.value || hoverDate.value)) {
        return dayjs(date4).isBetween(startDate.value, endDate.value || hoverDate.value) || dayjs(date4).isSame(startDate.value) || dayjs(date4).isSame(endDate.value || hoverDate.value);
      } else {
        return false;
      }
    };
    const clearSelectedData = (_) => {
      leftSelectedArray.value = [];
      rightSelectedArray.value = [];
      startDate.value = void 0;
      endDate.value = void 0;
    };
    const handlePick = (currentDate, direction) => {
      if (startDate.value && endDate.value) {
        clearSelectedData();
        direction == "left" ? leftSelectedArray.value = [currentDate.month] : rightSelectedArray.value = [currentDate.month];
        startDate.value = currentDate.month;
      } else if (mergeSelectedArray.value.length == 2) {
        endDate.value = currentDate.month;
        emit("update:modelValue", mergeSelectedArray.value);
        emit("change", mergeSelectedArray.value);
      } else if (mergeSelectedArray.value.length == 1) {
        startDate.value = currentDate.month;
      } else if (mergeSelectedArray.value.length == 0) {
        startDate.value = currentDate.month;
        endDate.value = currentDate.month;
        direction == "left" ? leftSelectedArray.value = [currentDate.month, currentDate.month] : rightSelectedArray.value = [currentDate.month, currentDate.month];
        emit("update:modelValue", mergeSelectedArray.value);
        emit("change", mergeSelectedArray.value);
      }
    };
    const onHoverCell = (row) => {
      hoverDate.value = row.month;
    };
    const onLeftPick = (val, currentDate) => {
      handlePick(currentDate, "left");
    };
    const onRightPick = (val, currentDate) => {
      handlePick(currentDate, "right");
    };
    const onLeftPreYear = (_) => {
      leftViewYear.value = dayjs(leftViewYear.value, "YYYY").subtract(1, "year").format("YYYY");
    };
    const onLeftNextYear = (_) => {
      if (ifDisableLeftNextYear.value)
        return;
      leftViewYear.value = dayjs(leftViewYear.value, "YYYY").add(1, "year").format("YYYY");
    };
    const onRightPreYear = (_) => {
      if (ifDisableRightPreYear.value)
        return;
      rightViewYear.value = dayjs(rightViewYear.value, "YYYY").subtract(1, "year").format("YYYY");
    };
    const onRightNextYear = (_) => {
      rightViewYear.value = dayjs(rightViewYear.value, "YYYY").add(1, "year").format("YYYY");
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", _hoisted_1$9, [
        createElementVNode("div", _hoisted_2$7, [
          createElementVNode("div", _hoisted_3$4, [
            createElementVNode("div", _hoisted_4$3, [
              createVNode(EfIcon, {
                onClick: onLeftPreYear,
                type: "icon-double-arrow-left",
                size: "small"
              }),
              _hoisted_5$2,
              createElementVNode("span", _hoisted_6$1, toDisplayString(unref(leftViewYearLabel)), 1),
              _hoisted_7$1,
              createVNode(EfIcon, {
                onClick: onLeftNextYear,
                disabled: unref(ifDisableLeftNextYear),
                type: "icon-double-arro-right",
                size: "small"
              }, null, 8, ["disabled"])
            ]),
            createElementVNode("div", _hoisted_8, [
              createVNode(BasicMonthTable, {
                year: leftViewYear.value,
                multiple: "",
                modelValue: leftSelectedArray.value,
                "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => leftSelectedArray.value = $event),
                disabledDate: __props.disabledDate,
                onPick: onLeftPick,
                inRange,
                onHoverCell
              }, null, 8, ["year", "modelValue", "disabledDate"])
            ])
          ]),
          createElementVNode("div", _hoisted_9, [
            createElementVNode("div", _hoisted_10, [
              createVNode(EfIcon, {
                onClick: onRightPreYear,
                disabled: unref(ifDisableRightPreYear),
                type: "icon-double-arrow-left",
                size: "small"
              }, null, 8, ["disabled"]),
              _hoisted_11,
              createElementVNode("span", _hoisted_12, toDisplayString(unref(rightViewYearLabel)), 1),
              _hoisted_13,
              createVNode(EfIcon, {
                onClick: onRightNextYear,
                type: "icon-double-arro-right",
                size: "small"
              })
            ]),
            createElementVNode("div", _hoisted_14, [
              createVNode(BasicMonthTable, {
                year: rightViewYear.value,
                multiple: "",
                modelValue: rightSelectedArray.value,
                "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => rightSelectedArray.value = $event),
                disabledDate: __props.disabledDate,
                onPick: onRightPick,
                inRange,
                onHoverCell
              }, null, 8, ["year", "modelValue", "disabledDate"])
            ])
          ])
        ]),
        _hoisted_15
      ]);
    };
  }
});
const _hoisted_1$8 = { class: "ef-time-range-picker-wrapper" };
const _hoisted_2$6 = { class: "left-box" };
const _hoisted_3$3 = /* @__PURE__ */ createElementVNode("div", { class: "title" }, "\u5F00\u59CB\u65F6\u95F4", -1);
const _hoisted_4$2 = { class: "time-pick-wrapper" };
const _hoisted_5$1 = { class: "right-box" };
const _hoisted_6 = /* @__PURE__ */ createElementVNode("div", { class: "title" }, "\u7ED3\u675F\u65F6\u95F4", -1);
const _hoisted_7 = { class: "time-pick-wrapper" };
const __default__$6 = {
  name: "ef-time-range-picker"
};
const _sfc_main$b = /* @__PURE__ */ Object.assign(__default__$6, {
  props: {
    disabledStartHours: {
      type: Function,
      default: (item) => false
    },
    disabledStartMinutes: {
      type: Function,
      default: (item) => false
    },
    disabledStartSeconds: {
      type: Function,
      default: (item) => false
    },
    disabledEndHours: {
      type: Function,
      default: (item) => false
    },
    disabledEndMinutes: {
      type: Function,
      default: (item) => false
    },
    disabledEndSeconds: {
      type: Function,
      default: (item) => false
    },
    modelValue: {
      type: Array,
      default: (_) => []
    }
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const viewTime = reactive({
      start: "",
      end: ""
    });
    const values = computed((_) => {
      if (viewTime.start && !viewTime.end) {
        return [viewTime.start, viewTime.start];
      } else if (viewTime.end && !viewTime.start) {
        return [viewTime.end, viewTime.end];
      } else {
        return [viewTime.start, viewTime.end];
      }
    });
    watch((_) => props2.modelValue, (newVal) => {
      if (Array.isArray(newVal) && newVal.length == 2) {
        let [start, end] = newVal.sort((a, b) => a > b ? 1 : -1);
        let now = dayjs().format("HH:mm:ss");
        viewTime.start = start || now;
        viewTime.end = end || now;
      }
    }, { immediate: true });
    const handleTimeChange = (direction, val, type4, typeVal) => {
      if (direction == "start" && viewTime.start > viewTime.end) {
        viewTime.end = viewTime.start;
      } else if (direction == "end" && viewTime.start > viewTime.end) {
        viewTime.start = viewTime.end;
      }
      emit("update:modelValue", values.value);
      emit("change", values.value, direction, val, type4, typeVal);
    };
    const onStartTimeChange = (val, type4, typeVal) => {
      handleTimeChange("start", val, type4, typeVal);
    };
    const onEndTimeChange = (val, type4, typeVal) => {
      handleTimeChange("end", val, type4, typeVal);
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", _hoisted_1$8, [
        createElementVNode("div", _hoisted_2$6, [
          _hoisted_3$3,
          createElementVNode("div", _hoisted_4$2, [
            createVNode(_sfc_main$f, {
              disabledHours: __props.disabledStartHours,
              disabledMinutes: __props.disabledStartMinutes,
              disabledSeconds: __props.disabledStartSeconds,
              modelValue: viewTime.start,
              "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => viewTime.start = $event),
              onChange: onStartTimeChange
            }, null, 8, ["disabledHours", "disabledMinutes", "disabledSeconds", "modelValue"])
          ])
        ]),
        createElementVNode("div", _hoisted_5$1, [
          _hoisted_6,
          createElementVNode("div", _hoisted_7, [
            createVNode(_sfc_main$f, {
              disabledHours: __props.disabledEndHours,
              disabledMinutes: __props.disabledEndMinutes,
              disabledSeconds: __props.disabledEndSeconds,
              modelValue: viewTime.end,
              "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => viewTime.end = $event),
              onChange: onEndTimeChange
            }, null, 8, ["disabledHours", "disabledMinutes", "disabledSeconds", "modelValue"])
          ])
        ])
      ]);
    };
  }
});
const _sfc_main$a = {
  name: "ef-date-picker",
  emits: ["change", "update:modelValue"],
  components: {
    EfPopper,
    PanelDatePick,
    EfInput,
    PanelDateRangePick: _sfc_main$d,
    EfIcon,
    PanelMonthRangePick: _sfc_main$c,
    TimePick: _sfc_main$f,
    TimeRangePick: _sfc_main$b
  },
  props: {
    clearable: Boolean,
    rangeSeparator: {
      type: String,
      default: "-"
    },
    transitionName: {
      type: String,
      default: "zoom-in"
    },
    trigger: {
      type: String,
      default: "click"
    },
    placement: {
      type: String,
      default: "bottom"
    },
    multiple: Boolean,
    disabledDate: {
      type: Function,
      default: (_) => false
    },
    modelValue: {
      type: [String, Array],
      default: ""
    },
    type: {
      type: String,
      default: "date",
      validator: (val) => [
        "",
        "year",
        "month",
        "date",
        "datetime",
        "daterange",
        "monthrange",
        "datetimerange",
        "time",
        "timerange"
      ].includes(val)
    },
    width: {
      type: [String, Number],
      default: void 0
    },
    height: {
      type: [String, Number],
      default: 40
    },
    valueFormatString: {
      type: String,
      default: void 0
    },
    labelFormatString: {
      type: String,
      default: void 0
    },
    placeholder: {
      type: String,
      default: "\u9009\u62E9\u65E5\u671F"
    },
    startPlaceholder: {
      type: String,
      default: "\u5F00\u59CB\u65E5\u671F"
    },
    endPlaceholder: {
      type: String,
      default: "\u7ED3\u675F\u65E5\u671F"
    }
  },
  setup(props2, { attrs, slots, emit }) {
    const inputValue = ref("");
    const popperVisible = ref(false);
    const efDatePickerInput = ref(null);
    const dateRangeValue = ref();
    const monthRangeValue = ref();
    const rangeStartInputValue = ref("");
    const rangeEndInputValue = ref("");
    const timeValue = ref();
    const timeRangeValues = ref([]);
    const isHover = ref(false);
    const { formContext, formItemContext } = useForm();
    const prefixIcon = computed((_) => ["time", "timerange", "datetime", "datetimerange"].includes(props2.type) ? "icon-clock" : "icon-calendar");
    const isRange = computed((_) => ["daterange", "monthrange", "datetimerange", "timerange"].includes(props2.type));
    const inputStyle = computed((_) => {
      let style = {};
      props2.height && (style.height = parseInt(props2.height) + "px");
      props2.height && (style.lineHeight = parseInt(props2.height) + "px");
      return style;
    });
    const inputNativeStyle = computed((_) => {
      let typeToWidthMap = {
        year: 220,
        month: 220,
        date: 220,
        time: 220,
        datetime: 220,
        daterange: 300,
        monthrange: 300,
        datetimerange: 400,
        timerange: 300
      };
      return {
        width: parseInt(props2.width || typeToWidthMap[props2.type]) + "px"
      };
    });
    const labelFormat = computed((_) => props2.labelFormatString || defaulDateFormat[props2.type]);
    const valueFormat = computed((_) => props2.valueFormatString || defaulDateFormat[props2.type]);
    const datePickValue = ref(isRange.value || props2.multiple ? [] : "");
    const isEmptyValue2 = computed((_) => {
      if (props2.modelValue) {
        if (Array.isArray(props2.modelValue)) {
          return props2.modelValue.length == 0;
        } else {
          return false;
        }
      } else {
        return true;
      }
    });
    const onDateRangeChange = (val) => {
      let formatArray = val.map((item) => dayjs(item, devDateFormat[props2.type]).format(valueFormat.value));
      emit("update:modelValue", formatArray);
      emit("change", formatArray);
      if (props2.type != "datetimerange") {
        popperVisible.value = false;
      }
    };
    const onMonthRangeChange = (val) => {
      let formatArray = val.map((item) => dayjs(item, devDateFormat[props2.type]).format(valueFormat.value));
      emit("update:modelValue", formatArray);
      emit("change", formatArray);
      popperVisible.value = false;
    };
    const valueFormatToLabel = (val) => {
      return dayjs(val, devDateFormat[props2.type]).format(valueFormat.value);
    };
    const onDatePickChange = (val) => {
      if (props2.multiple) {
        let formatArray = val.map((item) => dayjs(item, devDateFormat[props2.type]).format(valueFormat.value));
        emit("update:modelValue", formatArray);
        emit("change", formatArray);
      } else {
        let formatVal = dayjs(val, devDateFormat[props2.type]).format(valueFormat.value);
        emit("update:modelValue", formatVal);
        emit("change", formatVal);
        props2.type != "datetime" && (popperVisible.value = false);
      }
    };
    const onInputChange = (val) => {
      let dayjsCase = dayjs(val, labelFormat.value, "zh-cn", true);
      if (!dayjsCase.isValid()) {
        let val2 = props2.type != "time" ? datePickValue.value : timeValue.value;
        inputValue.value = dayjs(val2, devDateFormat[props2.type]).format(labelFormat.value);
      } else {
        emit("update:modelValue", dayjsCase.format(valueFormat.value));
        emit("change", dayjsCase.format(valueFormat.value));
        props2.type != "time" && (popperVisible.value = false);
      }
    };
    const onInputRangeChange = (direction) => {
      let rangeValuesMap = {
        daterange: dateRangeValue.value,
        datetimerange: dateRangeValue.value,
        monthrange: monthRangeValue.value,
        timerange: timeRangeValues.value
      };
      let currentValues = rangeValuesMap[props2.type];
      let val = direction == "start" ? rangeStartInputValue.value : rangeEndInputValue.value;
      let dayjsCase = dayjs(val, labelFormat.value, "zh-cn", true);
      if (dayjsCase.isValid()) {
        let formatVal = dayjsCase.format(devDateFormat[props2.type]);
        if (direction == "start") {
          currentValues[0] = formatVal;
        } else {
          currentValues[1] = formatVal;
        }
      }
      let formatValues = currentValues.map((item) => dayjs(item, devDateFormat[props2.type]).format(valueFormat.value));
      emit("update:modelValue", formatValues);
      emit("change", formatValues);
    };
    const onInputFocus = (_) => {
      if (props2.multiple) {
        efDatePickerInput.value.blur();
      }
    };
    const onInputClick = (e) => {
      !popperVisible.value && (popperVisible.value = true);
      e.stopPropagation();
    };
    const onTimeChange = (val, type4, typeVal) => {
      let formatVal = dayjs(val, devDateFormat[props2.type]).format(valueFormat.value);
      emit("update:modelValue", formatVal);
      emit("change", formatVal, type4, typeVal);
    };
    const onTimeRangeChange = (values, direction, val, type4, typeVal) => {
      let formatArray = values.map((item) => dayjs(item, devDateFormat[props2.type]).format(valueFormat.value));
      emit("update:modelValue", formatArray);
      emit("change", formatArray, direction, val, type4, typeVal);
    };
    const clear = () => {
      let clearVal;
      if (props2.multiple || isRange.value) {
        clearVal = [];
      } else {
        clearVal = "";
      }
      datePickValue.value = clearVal;
      dateRangeValue.value = clearVal;
      monthRangeValue.value = clearVal;
      timeRangeValues.value = clearVal;
      inputValue.value = "";
      rangeStartInputValue.value = "";
      rangeEndInputValue.value = "";
    };
    const onClear = () => {
      let clearVal;
      if (props2.multiple || isRange.value) {
        clearVal = [];
      } else {
        clearVal = "";
      }
      emit("update:modelValue", clearVal);
      emit("change", clearVal);
      formItemContext == null ? void 0 : formItemContext.validate("change");
    };
    watch((_) => props2.modelValue, (newVal, oldVal) => {
      if (!newVal || Array.isArray(newVal) && newVal.length < 1) {
        clear();
        return;
      }
      formItemContext == null ? void 0 : formItemContext.validate("change");
      if (["year", "month", "date"].includes(props2.type)) {
        if (props2.multiple) {
          if (!Array.isArray(newVal)) {
            throw new Error(`${newVal} \u65F6\u95F4\u683C\u5F0F\u9A8C\u8BC1\u9519\u8BEF \u591A\u9009\u5FC5\u987B\u4E3A\u6570\u7EC4`);
          }
          let datePickValueForMultiple = [];
          let inputValueForMultiple = [];
          for (let i = 0; i < newVal.length; i++) {
            let date4 = newVal[i];
            let dayjsCase = dayjs(date4, valueFormat.value);
            if (!dayjsCase.isValid()) {
              throw new Error(`${newVal} \u65F6\u95F4\u683C\u5F0F\u9A8C\u8BC1\u9519\u8BEF ${valueFormat.value}`);
            }
            datePickValueForMultiple.push(dayjsCase.format(devDateFormat[props2.type]));
            inputValueForMultiple.push(dayjsCase.format(labelFormat.value));
          }
          datePickValue.value = datePickValueForMultiple;
          inputValue.value = inputValueForMultiple.join(",");
        } else {
          let dayjsCase = dayjs(newVal, valueFormat.value);
          if (!dayjsCase.isValid()) {
            throw new Error(`${newVal} \u65F6\u95F4\u683C\u5F0F\u9A8C\u8BC1\u9519\u8BEF ${valueFormat.value}`);
          }
          datePickValue.value = dayjsCase.format(devDateFormat[props2.type]);
          inputValue.value = dayjsCase.format(labelFormat.value);
        }
      } else if (["daterange", "datetimerange"].includes(props2.type)) {
        newVal = newVal.sort((a, b) => a > b ? 1 : -1);
        dateRangeValue.value = newVal.map((item) => dayjs(item, valueFormat.value).format(devDateFormat[props2.type]));
        rangeStartInputValue.value = dayjs(newVal[0], valueFormat.value).format(labelFormat.value);
        rangeEndInputValue.value = dayjs(newVal[1], valueFormat.value).format(labelFormat.value);
      } else if (["monthrange"].includes(props2.type)) {
        newVal = newVal.sort((a, b) => a > b ? 1 : -1);
        monthRangeValue.value = newVal.map((item) => dayjs(item, valueFormat.value).format(devDateFormat[props2.type]));
        rangeStartInputValue.value = dayjs(newVal[0], valueFormat.value).format(labelFormat.value);
        rangeEndInputValue.value = dayjs(newVal[1], valueFormat.value).format(labelFormat.value);
      } else if (["time"].includes(props2.type)) {
        let dayjsCase = dayjs(newVal, valueFormat.value);
        if (!dayjsCase.isValid()) {
          throw new Error(`${newVal} \u65F6\u95F4\u683C\u5F0F\u9A8C\u8BC1\u9519\u8BEF ${valueFormat.value}`);
        }
        timeValue.value = dayjsCase.format(devDateFormat[props2.type]);
        inputValue.value = dayjsCase.format(labelFormat.value);
      } else if (["timerange"].includes(props2.type)) {
        newVal = newVal.sort((a, b) => a > b ? 1 : -1);
        timeRangeValues.value = newVal.map((item) => dayjs(item, valueFormat.value).format(devDateFormat[props2.type]));
        rangeStartInputValue.value = dayjs(newVal[0], valueFormat.value).format(labelFormat.value);
        rangeEndInputValue.value = dayjs(newVal[1], valueFormat.value).format(labelFormat.value);
      } else if (["datetime"].includes(props2.type)) {
        let dayjsCase = dayjs(newVal, valueFormat.value);
        if (dayjsCase.isValid()) {
          datePickValue.value = dayjsCase.format(devDateFormat[props2.type]);
          inputValue.value = dayjsCase.format(labelFormat.value);
        }
      }
    }, { immediate: true });
    provide("efDateCommonPick", {
      popperVisible,
      slots
    });
    return {
      datePickValue,
      isRange,
      inputStyle,
      inputNativeStyle,
      inputValue,
      popperVisible,
      efDatePickerInput,
      dateRangeValue,
      rangeEndInputValue,
      rangeStartInputValue,
      monthRangeValue,
      timeValue,
      timeRangeValues,
      labelFormat,
      prefixIcon,
      isEmptyValue: isEmptyValue2,
      isHover,
      formItemContext,
      onDatePickChange,
      onInputChange,
      onInputFocus,
      onDateRangeChange,
      onInputClick,
      onMonthRangeChange,
      onTimeChange,
      onTimeRangeChange,
      onInputRangeChange,
      onClear,
      valueFormatToLabel
    };
  }
};
const _hoisted_1$7 = ["placeholder"];
const _hoisted_2$5 = { class: "range-separator flex-0-0-auto" };
const _hoisted_3$2 = ["placeholder"];
function _sfc_render$4(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_input = resolveComponent("ef-input");
  const _component_ef_icon = resolveComponent("ef-icon");
  const _component_panel_date_pick = resolveComponent("panel-date-pick");
  const _component_panel_date_range_pick = resolveComponent("panel-date-range-pick");
  const _component_panel_month_range_pick = resolveComponent("panel-month-range-pick");
  const _component_time_pick = resolveComponent("time-pick");
  const _component_time_range_pick = resolveComponent("time-range-pick");
  const _component_ef_popper = resolveComponent("ef-popper");
  return openBlock(), createBlock(_component_ef_popper, {
    visible: $setup.popperVisible,
    "onUpdate:visible": _cache[13] || (_cache[13] = ($event) => $setup.popperVisible = $event),
    placement: $props.placement,
    trigger: $props.trigger,
    transitionName: $props.transitionName
  }, {
    trigger: withCtx(() => {
      var _a;
      return [
        createElementVNode("div", {
          style: { "display": "inline-block" },
          onMouseenter: _cache[6] || (_cache[6] = ($event) => $setup.isHover = true),
          onMouseleave: _cache[7] || (_cache[7] = ($event) => $setup.isHover = false)
        }, [
          !$setup.isRange ? (openBlock(), createBlock(_component_ef_input, {
            key: 0,
            ref: "efDatePickerInput",
            placeholder: $props.placeholder,
            style: normalizeStyle($setup.inputNativeStyle),
            inputStyle: $setup.inputStyle,
            modelValue: $setup.inputValue,
            "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => $setup.inputValue = $event),
            "prefix-icon": $setup.prefixIcon,
            clearable: $props.clearable,
            clearToFocus: false,
            onChange: $setup.onInputChange,
            onFocus: $setup.onInputFocus,
            onClick: $setup.onInputClick,
            onClear: $setup.onClear
          }, null, 8, ["placeholder", "style", "inputStyle", "modelValue", "prefix-icon", "clearable", "onChange", "onFocus", "onClick", "onClear"])) : (openBlock(), createElementBlock("div", {
            key: 1,
            class: normalizeClass(["ef-date-pick-range-trigger-wrapper", {
              "primary-border-color": $setup.popperVisible,
              clearable: $props.clearable,
              "validate-fail": ((_a = $setup.formItemContext) == null ? void 0 : _a.validateStatus) === "NOTPASS"
            }]),
            style: normalizeStyle($setup.inputNativeStyle),
            onClick: _cache[5] || (_cache[5] = (...args) => $setup.onInputClick && $setup.onInputClick(...args))
          }, [
            createVNode(_component_ef_icon, {
              color: "#c0c4cc",
              type: $setup.prefixIcon,
              size: "small",
              class: "flex-0-0-auto"
            }, null, 8, ["type"]),
            withDirectives(createElementVNode("input", {
              style: normalizeStyle($setup.inputStyle),
              class: "input flex-1",
              type: "text",
              "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => $setup.rangeStartInputValue = $event),
              placeholder: $props.startPlaceholder,
              onChange: _cache[2] || (_cache[2] = ($event) => $setup.onInputRangeChange("start"))
            }, null, 44, _hoisted_1$7), [
              [vModelText, $setup.rangeStartInputValue]
            ]),
            createElementVNode("span", _hoisted_2$5, toDisplayString($props.rangeSeparator), 1),
            withDirectives(createElementVNode("input", {
              style: normalizeStyle($setup.inputStyle),
              class: "input flex-1",
              type: "text",
              "onUpdate:modelValue": _cache[3] || (_cache[3] = ($event) => $setup.rangeEndInputValue = $event),
              placeholder: $props.endPlaceholder,
              onChange: _cache[4] || (_cache[4] = ($event) => $setup.onInputRangeChange("end"))
            }, null, 44, _hoisted_3$2), [
              [vModelText, $setup.rangeEndInputValue]
            ]),
            $props.clearable && !$setup.isEmptyValue && $setup.isHover ? (openBlock(), createBlock(_component_ef_icon, {
              key: 0,
              class: normalizeClass(["show-pointer", "flex-0-0-auto"]),
              fontSize: "16",
              color: "#c0c4cc",
              type: "icon-reeor",
              style: { "position": "absolute", "right": "5px", "top": "50%", "transform": "translateY(-50%)" },
              onClick: withModifiers($setup.onClear, ["stop"])
            }, null, 8, ["onClick"])) : createCommentVNode("", true)
          ], 6))
        ], 32)
      ];
    }),
    default: withCtx(() => [
      ["year", "month", "date", "datetime"].includes($props.type) ? (openBlock(), createBlock(_component_panel_date_pick, {
        key: 0,
        multiple: $props.multiple,
        disabledDate: $props.disabledDate,
        type: $props.type,
        labelFormat: $setup.labelFormat,
        modelValue: $setup.datePickValue,
        "onUpdate:modelValue": _cache[8] || (_cache[8] = ($event) => $setup.datePickValue = $event),
        onChange: $setup.onDatePickChange
      }, null, 8, ["multiple", "disabledDate", "type", "labelFormat", "modelValue", "onChange"])) : ["daterange", "datetimerange"].includes($props.type) ? (openBlock(), createBlock(_component_panel_date_range_pick, {
        key: 1,
        type: $props.type,
        disabledDate: $props.disabledDate,
        modelValue: $setup.dateRangeValue,
        "onUpdate:modelValue": _cache[9] || (_cache[9] = ($event) => $setup.dateRangeValue = $event),
        onChange: $setup.onDateRangeChange
      }, null, 8, ["type", "disabledDate", "modelValue", "onChange"])) : ["monthrange"].includes($props.type) ? (openBlock(), createBlock(_component_panel_month_range_pick, {
        key: 2,
        disabledDate: $props.disabledDate,
        modelValue: $setup.monthRangeValue,
        "onUpdate:modelValue": _cache[10] || (_cache[10] = ($event) => $setup.monthRangeValue = $event),
        onChange: $setup.onMonthRangeChange
      }, null, 8, ["disabledDate", "modelValue", "onChange"])) : ["time"].includes($props.type) ? (openBlock(), createBlock(_component_time_pick, {
        key: 3,
        modelValue: $setup.timeValue,
        "onUpdate:modelValue": _cache[11] || (_cache[11] = ($event) => $setup.timeValue = $event),
        onChange: $setup.onTimeChange
      }, null, 8, ["modelValue", "onChange"])) : ["timerange"].includes($props.type) ? (openBlock(), createBlock(_component_time_range_pick, {
        key: 4,
        modelValue: $setup.timeRangeValues,
        "onUpdate:modelValue": _cache[12] || (_cache[12] = ($event) => $setup.timeRangeValues = $event),
        onChange: $setup.onTimeRangeChange
      }, null, 8, ["modelValue", "onChange"])) : createCommentVNode("", true)
    ]),
    _: 1
  }, 8, ["visible", "placement", "trigger", "transitionName"]);
}
var DatePicker = /* @__PURE__ */ _export_sfc(_sfc_main$a, [["render", _sfc_render$4]]);
dayjs.extend(isSameOrBefore);
dayjs.extend(isSameOrAfter);
dayjs.extend(customParseFormat);
dayjs.extend(isBetween);
BasicDateTable.install = (app, options) => {
  app.component(BasicDateTable.name, BasicDateTable);
};
BasicMonthTable.install = (app, options) => {
  app.component(BasicMonthTable.name, BasicMonthTable);
};
BasicYearTable.install = (app, options) => {
  app.component(BasicYearTable.name, BasicYearTable);
};
PanelDatePick.install = (app, options) => {
  app.component(PanelDatePick.name, PanelDatePick);
};
DatePicker.install = (app, options) => {
  app.component(DatePicker.name, DatePicker);
};
_sfc_main$d.install = (app, options) => {
  app.component(_sfc_main$d.name, _sfc_main$d);
};
_sfc_main$c.install = (app, options) => {
  app.component(_sfc_main$c.name, _sfc_main$c);
};
const EfBasicDateTable = BasicDateTable;
const EfBasicMonthTable = BasicMonthTable;
const EfBasicYearTable = BasicYearTable;
const EfPanelDatePick = PanelDatePick;
const EfPanelDateRangePick = _sfc_main$d;
const EfPanelMonthRangePick = _sfc_main$c;
var EfDatePicker = {
  install(app, options) {
    app.use(DatePicker);
  }
};
_sfc_main$f.install = (app, options) => {
  app.component(_sfc_main$f.name, _sfc_main$f);
};
_sfc_main$b.install = (app, options) => {
  app.component(_sfc_main$b.name, _sfc_main$b);
};
var EfTimePicker = {
  install(app, options) {
    app.use(_sfc_main$f);
    app.use(_sfc_main$b);
  }
};
const _hoisted_1$6 = { class: "ef-radio-wrapper-input" };
const _hoisted_2$4 = ["value", "disabled"];
const __default__$5 = {
  name: "ef-radio"
};
const _sfc_main$9 = /* @__PURE__ */ Object.assign(__default__$5, {
  props: {
    value: {
      type: [String, Number],
      default: ""
    },
    modelValue: {
      type: [String, Number],
      default: ""
    },
    border: Boolean,
    disabled: Boolean
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const { formContext, formItemContext } = useForm();
    const efRadioGroup = inject("efRadioGroup", void 0);
    const radioRef = ref();
    const isGroup = computed(() => !!efRadioGroup);
    const model = computed({
      get() {
        return props2.modelValue;
      },
      set(newVal) {
        emit("update:modelValue", newVal);
        radioRef.value.checked = props2.modelValue === props2.value;
        formItemContext == null ? void 0 : formItemContext.validate("change");
      }
    });
    const isChecked = computed((_) => model.value === props2.value);
    const onChange = (e) => {
      nextTick(() => emit("change", model.value));
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("label", {
        class: normalizeClass(["ef-radio-wrapper", { checked: unref(isChecked), border: __props.border, group: unref(isGroup) }])
      }, [
        createElementVNode("span", _hoisted_1$6, [
          createElementVNode("span", {
            class: normalizeClass(["inner", { checked: unref(isChecked), disabled: __props.disabled }])
          }, null, 2),
          withDirectives(createElementVNode("input", {
            ref_key: "radioRef",
            ref: radioRef,
            class: "input",
            type: "radio",
            "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => isRef(model) ? model.value = $event : null),
            value: __props.value,
            tabindex: "-1",
            onChange,
            disabled: __props.disabled
          }, null, 40, _hoisted_2$4), [
            [vModelRadio, unref(model)]
          ])
        ]),
        createElementVNode("span", {
          class: normalizeClass(["ef-radio-wrapper-text", { checked: unref(isChecked), disabled: __props.disabled }])
        }, [
          renderSlot(_ctx.$slots, "default", {}, () => [
            createTextVNode(toDisplayString(__props.value), 1)
          ])
        ], 2)
      ], 2);
    };
  }
});
const _hoisted_1$5 = { key: 1 };
const __default__$4 = {
  name: "ef-radio-group"
};
const _sfc_main$8 = /* @__PURE__ */ Object.assign(__default__$4, {
  props: {
    options: {
      type: Array,
      default: (_) => []
    },
    modelValue: {
      type: [String, Number],
      default: ""
    },
    direction: {
      type: String,
      default: "horizontal"
    },
    border: Boolean,
    disabled: Boolean
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const modelValue = computed({
      get() {
        return props2.modelValue;
      },
      set(newVal) {
        emit("update:modelValue", newVal);
      }
    });
    const onChange = (val) => {
      emit("change", val);
    };
    provide("efRadioGroup", {
      border: props2.border,
      disabled: props2.disabled
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", {
        class: normalizeClass(["ef-radio-group-wrapper", [__props.direction]])
      }, [
        (openBlock(true), createElementBlock(Fragment, null, renderList(__props.options, (item) => {
          return openBlock(), createBlock(_sfc_main$9, {
            disabled: __props.disabled,
            border: __props.border,
            value: item.value,
            modelValue: unref(modelValue),
            "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => isRef(modelValue) ? modelValue.value = $event : null),
            onChange,
            class: normalizeClass([__props.direction])
          }, {
            default: withCtx(() => [
              _ctx.$slots.default ? renderSlot(_ctx.$slots, "default", {
                key: 0,
                option: item
              }) : (openBlock(), createElementBlock("span", _hoisted_1$5, toDisplayString(item.label), 1))
            ]),
            _: 2
          }, 1032, ["disabled", "border", "value", "modelValue", "class"]);
        }), 256))
      ], 2);
    };
  }
});
_sfc_main$9.install = (app, options) => {
  app.component(_sfc_main$9.name, _sfc_main$9);
};
_sfc_main$8.install = (app, options) => {
  app.component(_sfc_main$8.name, _sfc_main$8);
};
var EfRadio = {
  install(app) {
    app.use(_sfc_main$9);
    app.use(_sfc_main$8);
  }
};
var cascaderProps = {
  modelValue: {
    type: Array,
    default: (_) => []
  },
  options: {
    type: Array,
    default: (_) => []
  },
  props: {
    type: Object,
    default: (_) => ({
      expandTrigger: "click",
      leaf: "leaf",
      disabled: "disabled",
      children: "children",
      value: "value",
      label: "label",
      lazy: false,
      lazyLoad: (_2) => {
      }
    })
  },
  disabled: Boolean
};
function useStore({
  props: props2
}) {
  const panelList = ref([]);
  const viewItemList = ref([]);
  const selectedValueList = ref([]);
  const selectedLabelList = ref([]);
  watch((_) => props2.options, (newVal) => {
    if (!props2.props.lazy) {
      panelList.value = [newVal];
    }
  }, {
    immediate: true
  });
  const clear = (_) => {
    viewItemList.value = [];
    selectedLabelList.value = [];
    selectedValueList.value = [];
  };
  watch((_) => props2.modelValue, (newVal) => {
    selectedValueList.value = newVal;
    if (!newVal || newVal.length < 1) {
      clear();
    }
  }, {
    immediate: true
  });
  watch((_) => props2.props.lazy, (newVal) => {
    if (newVal) {
      let node = {
        level: 0
      };
      const resolve = (nodes) => {
        nodes.length > 0 && (panelList.value = [nodes]);
      };
      props2.props.lazyLoad(node, resolve);
    }
  }, {
    immediate: true
  });
  const addNextPanel = (currentPanelIndex, item) => {
    let tmpArr = panelList.value.slice(0, currentPanelIndex + 1);
    viewItemList.value[currentPanelIndex] = item;
    if (!props2.props.lazy && Array.isArray(item.children) && item.children.length > 0) {
      panelList.value = [...tmpArr, item.children];
    } else if (props2.props.lazy && !item[props2.props.leaf]) {
      item.lazyLoading = true;
      let node = {
        level: currentPanelIndex + 1,
        ...item
      };
      const resolve = (nodes) => {
        nodes.length > 0 && (panelList.value = [...tmpArr, nodes]);
        item.lazyLoading = false;
      };
      props2.props.lazyLoad(node, resolve);
    } else {
      panelList.value = panelList.value.slice(0, currentPanelIndex + 1);
      viewItemList.value = viewItemList.value.slice(0, currentPanelIndex + 1);
      selectedValueList.value = viewItemList.value.map((item2) => item2[props2.props.value]);
      selectedLabelList.value = viewItemList.value.map((item2) => item2[props2.props.label]);
    }
  };
  return {
    addNextPanel,
    panelList,
    viewItemList,
    selectedLabelList,
    selectedValueList
  };
}
const _hoisted_1$4 = { class: "ef-cascader-panel-wrapper" };
const _hoisted_2$3 = ["onClick", "onMouseenter"];
const _hoisted_3$1 = {
  key: 1,
  class: "ef-cascader-item-label"
};
const _hoisted_4$1 = { class: "ef-cascader-item-icon" };
const __default__$3 = {
  name: "ef-cascader-panel"
};
const _sfc_main$7 = /* @__PURE__ */ Object.assign(__default__$3, {
  props: cascaderProps,
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const efCascaderIndex = inject("efCascaderIndex", void 0);
    const { addNextPanel, panelList, viewItemList, selectedLabelList, selectedValueList } = useStore({ props: props2 });
    const prop = computed((_) => props2.props);
    const itemIconType = (item, optionsIndex) => {
      if ((item[prop.value.leaf] || !prop.value.lazy && (!Array.isArray(item[prop.value.children]) || item[prop.value.children].length < 1)) && isSelected(item, optionsIndex)) {
        return "icon-seleted";
      } else {
        return "icon-arrow-right";
      }
    };
    const isSelected = (item, optionsIndex) => {
      if (optionsIndex == 0) {
        return item[prop.value.value] == selectedValueList.value[0];
      } else {
        return selectedValueList.value.slice(0, optionsIndex + 1).join() == viewItemList.value.map((item2) => item2[prop.value.value]).slice(0, optionsIndex).join() + "," + item[prop.value.value];
      }
    };
    const onRowClick = (item, currentPanelIndex) => {
      if (item[prop.value.disabled])
        return;
      if ((!Array.isArray(item[prop.value.children]) || item[prop.value.children].length < 1) && (item[prop.value.leaf] === void 0 || item[prop.value.leaf])) {
        addNextPanel(currentPanelIndex, item);
        let model = {
          selectedLabelList: selectedLabelList.value,
          selectedValueList: selectedValueList.value
        };
        emit("update:modelValue", selectedValueList.value);
        emit("change", model);
      } else {
        if (prop.value.expandTrigger != "click")
          return;
        addNextPanel(currentPanelIndex, item);
      }
    };
    const onMouseenterRow = (item, currentPanelIndex) => {
      if (item[prop.value.disabled])
        return;
      if (prop.value.expandTrigger != "hover")
        return;
      if (prop.value.lazy && !item[prop.value.leaf] || Array.isArray(item[prop.value.children]) && item[prop.value.children].length > 0) {
        addNextPanel(currentPanelIndex, item);
      }
    };
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", _hoisted_1$4, [
        (openBlock(true), createElementBlock(Fragment, null, renderList(unref(panelList), (options, optionsIndex) => {
          return openBlock(), createElementBlock("ul", {
            key: optionsIndex,
            class: "ef-cascader-panel scrollBarStyle"
          }, [
            (openBlock(true), createElementBlock(Fragment, null, renderList(options, (item, index) => {
              return openBlock(), createElementBlock("li", {
                onClick: ($event) => onRowClick(item, optionsIndex),
                onMouseenter: ($event) => onMouseenterRow(item, optionsIndex),
                key: item[unref(prop).value],
                class: normalizeClass(["ef-cascader-item", {
                  selected: isSelected(item, optionsIndex),
                  hovered: unref(viewItemList).includes(item),
                  disabled: item[unref(prop).disabled]
                }])
              }, [
                unref(efCascaderIndex) && unref(efCascaderIndex).slots.default ? (openBlock(), createBlock(unref(VNodes), {
                  key: 0,
                  class: "ef-cascader-item-label",
                  vnodes: unref(efCascaderIndex).slots.default(item)
                }, null, 8, ["vnodes"])) : (openBlock(), createElementBlock("span", _hoisted_3$1, toDisplayString(item[unref(prop).label]), 1)),
                createElementVNode("div", _hoisted_4$1, [
                  item.lazyLoading ? (openBlock(), createBlock(EfIcon, {
                    key: 0,
                    type: "icon-loading",
                    size: "mini",
                    fontSize: "18",
                    spin: ""
                  })) : !item.lazyLoading && (Array.isArray(item[unref(prop).children]) && item[unref(prop).children].length > 0 || unref(prop).lazy && !item[unref(prop).leaf] || isSelected(item, optionsIndex)) ? (openBlock(), createBlock(EfIcon, {
                    key: 1,
                    type: itemIconType(item, optionsIndex)
                  }, null, 8, ["type"])) : createCommentVNode("", true)
                ])
              ], 42, _hoisted_2$3);
            }), 128))
          ]);
        }), 128))
      ]);
    };
  }
});
const _hoisted_1$3 = { style: { "display": "inline-block" } };
const __default__$2 = {
  name: "ef-cascader"
};
const _sfc_main$6 = /* @__PURE__ */ Object.assign(__default__$2, {
  props: {
    ...cascaderProps,
    transitionName: {
      type: String,
      default: "zoom-in"
    },
    trigger: {
      type: String,
      default: "click"
    },
    placement: {
      type: String,
      default: "bottom-start"
    },
    placeholder: {
      type: String,
      default: "\u8BF7\u9009\u62E9"
    },
    width: {
      type: [String, Number],
      default: void 0
    },
    height: {
      type: [String, Number],
      default: 40
    },
    separator: {
      type: String,
      default: "/"
    },
    clearable: Boolean
  },
  emits: ["update:modelValue", "change"],
  setup(__props, { emit }) {
    const props2 = __props;
    const slots = useSlots();
    useAttrs$1();
    const popperVisible = ref(false);
    const cascaderValues = ref([]);
    const efInput = ref();
    const inputValue = ref("");
    const { formContext, formItemContext } = useForm();
    const inputStyle = computed((_) => {
      let style = {};
      props2.height && (style.height = parseInt(props2.height) + "px");
      props2.height && (style.lineHeight = parseInt(props2.height) + "px");
      return style;
    });
    const inputNativeStyle = computed((_) => {
      return {
        width: parseInt(props2.width || "260") + "px"
      };
    });
    const prop = computed((_) => props2.props);
    const genLabel = () => {
      let findArr = [];
      const find = (list, level = 0) => {
        list.forEach((item, index) => {
          let preCascaderValues = cascaderValues.value.slice(0, level + 1);
          let preFindArr = findArr.slice(0, level);
          if ([...preFindArr.map((item2) => item2[props2.props.value]), item[props2.props.value]].join() == preCascaderValues.join()) {
            findArr[level] = item;
            if (Array.isArray(item[props2.props.children]) && item[props2.props.children].length > 0) {
              find(item.children, level + 1);
            }
          }
        });
      };
      find(props2.options);
      return findArr.map((item) => item[props2.props.label]).join(props2.separator);
    };
    watch((_) => props2.modelValue, (newVal) => {
      cascaderValues.value = newVal;
      if (!props2.props.lazy && !inputValue.value) {
        inputValue.value = genLabel();
      }
    }, { immediate: true });
    const onCascaderChange = ({ selectedLabelList, selectedValueList }) => {
      inputValue.value = selectedLabelList.join(props2.separator);
      emit("update:modelValue", selectedValueList);
      emit("change", selectedValueList);
      formItemContext == null ? void 0 : formItemContext.validate("change");
      popperVisible.value = false;
    };
    const onInputFocus = (e) => {
      efInput.value.blur();
    };
    const onInputChange = (val) => {
    };
    const onInputClick = (e) => {
    };
    const onClear = (_) => {
      emit("update:modelValue", []);
      emit("change", []);
      formItemContext == null ? void 0 : formItemContext.validate("change");
      cascaderValues.value = [];
      popperVisible.value = false;
    };
    provide("efCascaderIndex", {
      slots
    });
    return (_ctx, _cache) => {
      return openBlock(), createBlock(EfPopper, {
        visible: popperVisible.value,
        "onUpdate:visible": _cache[2] || (_cache[2] = ($event) => popperVisible.value = $event),
        placement: __props.placement,
        trigger: __props.trigger,
        transitionName: __props.transitionName,
        disabled: _ctx.disabled
      }, {
        trigger: withCtx(() => [
          createElementVNode("div", _hoisted_1$3, [
            createVNode(EfInput, {
              ref_key: "efInput",
              ref: efInput,
              clearToFocus: false,
              clearable: __props.clearable,
              placeholder: __props.placeholder,
              style: normalizeStyle(unref(inputNativeStyle)),
              inputStyle: unref(inputStyle),
              modelValue: inputValue.value,
              "onUpdate:modelValue": _cache[0] || (_cache[0] = ($event) => inputValue.value = $event),
              onChange: onInputChange,
              onFocus: onInputFocus,
              onClick: onInputClick,
              onClear,
              disabled: _ctx.disabled
            }, null, 8, ["clearable", "placeholder", "style", "inputStyle", "modelValue", "disabled"])
          ])
        ]),
        default: withCtx(() => [
          createVNode(_sfc_main$7, {
            modelValue: cascaderValues.value,
            "onUpdate:modelValue": _cache[1] || (_cache[1] = ($event) => cascaderValues.value = $event),
            options: _ctx.options,
            props: unref(prop),
            onChange: onCascaderChange
          }, null, 8, ["modelValue", "options", "props"])
        ]),
        _: 1
      }, 8, ["visible", "placement", "trigger", "transitionName", "disabled"]);
    };
  }
});
_sfc_main$7.install = (app, options) => {
  app.component(_sfc_main$7.name, _sfc_main$7);
};
_sfc_main$6.install = (app, options) => {
  app.component(_sfc_main$6.name, _sfc_main$6);
};
var EfCascader = {
  install(app) {
    app.use(_sfc_main$7);
    app.use(_sfc_main$6);
  }
};
const _sfc_main$5 = {
  name: "ef-tree"
};
function _sfc_render$3(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("div", null, "ef-tree");
}
var Tree = /* @__PURE__ */ _export_sfc(_sfc_main$5, [["render", _sfc_render$3]]);
Tree.install = (app, options) => {
  app.component(Tree.name, Tree);
};
const _sfc_main$4 = defineComponent({
  name: "ef-collapse-transition",
  setup() {
    return {
      on: {
        beforeEnter(el) {
          if (!el.dataset)
            el.dataset = {};
          el.dataset.oldPaddingTop = el.style.paddingTop;
          el.dataset.oldPaddingBottom = el.style.paddingBottom;
          el.style.maxHeight = 0;
          el.style.paddingTop = 0;
          el.style.paddingBottom = 0;
        },
        enter(el) {
          el.dataset.oldOverflow = el.style.overflow;
          if (el.scrollHeight !== 0) {
            el.style.maxHeight = `${el.scrollHeight}px`;
            el.style.paddingTop = el.dataset.oldPaddingTop;
            el.style.paddingBottom = el.dataset.oldPaddingBottom;
          } else {
            el.style.maxHeight = 0;
            el.style.paddingTop = el.dataset.oldPaddingTop;
            el.style.paddingBottom = el.dataset.oldPaddingBottom;
          }
          el.style.overflow = "hidden";
        },
        afterEnter(el) {
          el.style.maxHeight = "";
          el.style.overflow = el.dataset.oldOverflow;
        },
        beforeLeave(el) {
          if (!el.dataset)
            el.dataset = {};
          el.dataset.oldPaddingTop = el.style.paddingTop;
          el.dataset.oldPaddingBottom = el.style.paddingBottom;
          el.dataset.oldOverflow = el.style.overflow;
          el.style.maxHeight = `${el.scrollHeight}px`;
          el.style.overflow = "hidden";
        },
        leave(el) {
          if (el.scrollHeight !== 0) {
            el.style.maxHeight = 0;
            el.style.paddingTop = 0;
            el.style.paddingBottom = 0;
          }
        },
        afterLeave(el) {
          el.style.maxHeight = "";
          el.style.overflow = el.dataset.oldOverflow;
          el.style.paddingTop = el.dataset.oldPaddingTop;
          el.style.paddingBottom = el.dataset.oldPaddingBottom;
        }
      }
    };
  }
});
function _sfc_render$2(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createBlock(Transition, mergeProps({ name: "ef-collapse-transition" }, toHandlers(_ctx.on)), {
    default: withCtx(() => [
      renderSlot(_ctx.$slots, "default")
    ]),
    _: 3
  }, 16);
}
var CollapseTransition = /* @__PURE__ */ _export_sfc(_sfc_main$4, [["render", _sfc_render$2]]);
CollapseTransition.install = (app, options) => {
  app.component(CollapseTransition.name, CollapseTransition);
};
const formProps = {
  model: Object,
  rules: Object,
  itemWidth: {
    type: [String, Number],
    default: ""
  },
  labelPosition: String,
  labelWidth: {
    type: [String, Number],
    default: ""
  },
  labelSuffix: {
    type: String,
    default: ""
  },
  inline: Boolean,
  inlineMessage: Boolean,
  statusIcon: Boolean,
  showMessage: {
    type: Boolean,
    default: true
  },
  size: {
    type: String,
    values: ["", "small", "middle", "large"]
  },
  disabled: Boolean,
  validateOnRuleChange: {
    type: Boolean,
    default: true
  },
  hideRequiredAsterisk: {
    type: Boolean,
    default: false
  },
  scrollToError: {
    type: Boolean,
    default: false
  }
};
const __default__$1 = defineComponent({
  name: "ef-form"
});
const _sfc_main$3 = /* @__PURE__ */ Object.assign(__default__$1, {
  props: formProps,
  emits: ["validate"],
  setup(__props, { expose, emit }) {
    const props2 = __props;
    useSlots();
    useAttrs$1();
    const fieldsMap = {};
    const addField = (propPath, formItemContext) => {
      fieldsMap[propPath] = formItemContext;
    };
    const removeField = (propPath) => {
      delete fieldsMap[propPath];
    };
    const formClasses = computed(() => {
      return ["ef-form"];
    });
    const scrollToField = (prop) => {
      var _a;
      const field = fieldsMap[prop];
      if (field) {
        (_a = field.$el) == null ? void 0 : _a.scrollIntoView();
      }
    };
    const validateField = (field) => {
      return new Promise((resolve, reject) => {
        let fieldProps = Object.keys(fieldsMap);
        if (field) {
          fieldProps = fieldProps.filter((prop) => field === prop);
        }
        let validateDetail = {};
        let successCounts = 0;
        let failCounts = 0;
        let firstFailProp = void 0;
        const handleValidate = (res, prop) => {
          let { valid, errmsg } = res;
          validateDetail[prop] = res;
          if (valid === true) {
            successCounts++;
          } else {
            if (!firstFailProp) {
              firstFailProp = prop;
            }
            failCounts++;
          }
          if (successCounts + failCounts == fieldProps.length) {
            if (failCounts > 0) {
              if (props2.scrollToError === true) {
                scrollToField(firstFailProp);
              }
              resolve({
                valid: false,
                validateDetail
              });
            } else {
              resolve({
                valid: true,
                validateDetail
              });
            }
          }
        };
        fieldProps.forEach((prop) => {
          let fieldItemContext = fieldsMap[prop];
          fieldItemContext.validate().then((res) => {
            handleValidate(res, prop);
          });
        });
      });
    };
    const validate = () => {
      return validateField(void 0);
    };
    const clearValidate = () => {
      Object.values(fieldsMap).forEach((field) => {
        field.clearValidate();
      });
    };
    provide("efForm", reactive({
      ...toRefs(props2),
      emit,
      addField,
      removeField
    }));
    expose({
      clearValidate,
      validateField,
      validate,
      scrollToField
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("form", {
        class: normalizeClass(unref(formClasses))
      }, [
        renderSlot(_ctx.$slots, "default")
      ], 2);
    };
  }
});
function _extends() {
  _extends = Object.assign ? Object.assign.bind() : function(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }
    return target;
  };
  return _extends.apply(this, arguments);
}
function _inheritsLoose(subClass, superClass) {
  subClass.prototype = Object.create(superClass.prototype);
  subClass.prototype.constructor = subClass;
  _setPrototypeOf(subClass, superClass);
}
function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf2(o2) {
    return o2.__proto__ || Object.getPrototypeOf(o2);
  };
  return _getPrototypeOf(o);
}
function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf2(o2, p2) {
    o2.__proto__ = p2;
    return o2;
  };
  return _setPrototypeOf(o, p);
}
function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct)
    return false;
  if (Reflect.construct.sham)
    return false;
  if (typeof Proxy === "function")
    return true;
  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function() {
    }));
    return true;
  } catch (e) {
    return false;
  }
}
function _construct(Parent, args, Class) {
  if (_isNativeReflectConstruct()) {
    _construct = Reflect.construct.bind();
  } else {
    _construct = function _construct2(Parent2, args2, Class2) {
      var a = [null];
      a.push.apply(a, args2);
      var Constructor = Function.bind.apply(Parent2, a);
      var instance = new Constructor();
      if (Class2)
        _setPrototypeOf(instance, Class2.prototype);
      return instance;
    };
  }
  return _construct.apply(null, arguments);
}
function _isNativeFunction(fn) {
  return Function.toString.call(fn).indexOf("[native code]") !== -1;
}
function _wrapNativeSuper(Class) {
  var _cache = typeof Map === "function" ? /* @__PURE__ */ new Map() : void 0;
  _wrapNativeSuper = function _wrapNativeSuper2(Class2) {
    if (Class2 === null || !_isNativeFunction(Class2))
      return Class2;
    if (typeof Class2 !== "function") {
      throw new TypeError("Super expression must either be null or a function");
    }
    if (typeof _cache !== "undefined") {
      if (_cache.has(Class2))
        return _cache.get(Class2);
      _cache.set(Class2, Wrapper);
    }
    function Wrapper() {
      return _construct(Class2, arguments, _getPrototypeOf(this).constructor);
    }
    Wrapper.prototype = Object.create(Class2.prototype, {
      constructor: {
        value: Wrapper,
        enumerable: false,
        writable: true,
        configurable: true
      }
    });
    return _setPrototypeOf(Wrapper, Class2);
  };
  return _wrapNativeSuper(Class);
}
var formatRegExp = /%[sdj%]/g;
var warning = function warning2() {
};
if (typeof process !== "undefined" && process.env && false) {
  warning = function warning3(type4, errors) {
    if (typeof console !== "undefined" && console.warn && typeof ASYNC_VALIDATOR_NO_WARNING === "undefined") {
      if (errors.every(function(e) {
        return typeof e === "string";
      })) {
        console.warn(type4, errors);
      }
    }
  };
}
function convertFieldsError(errors) {
  if (!errors || !errors.length)
    return null;
  var fields = {};
  errors.forEach(function(error) {
    var field = error.field;
    fields[field] = fields[field] || [];
    fields[field].push(error);
  });
  return fields;
}
function format(template) {
  for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
    args[_key - 1] = arguments[_key];
  }
  var i = 0;
  var len = args.length;
  if (typeof template === "function") {
    return template.apply(null, args);
  }
  if (typeof template === "string") {
    var str = template.replace(formatRegExp, function(x) {
      if (x === "%%") {
        return "%";
      }
      if (i >= len) {
        return x;
      }
      switch (x) {
        case "%s":
          return String(args[i++]);
        case "%d":
          return Number(args[i++]);
        case "%j":
          try {
            return JSON.stringify(args[i++]);
          } catch (_) {
            return "[Circular]";
          }
          break;
        default:
          return x;
      }
    });
    return str;
  }
  return template;
}
function isNativeStringType(type4) {
  return type4 === "string" || type4 === "url" || type4 === "hex" || type4 === "email" || type4 === "date" || type4 === "pattern";
}
function isEmptyValue(value, type4) {
  if (value === void 0 || value === null) {
    return true;
  }
  if (type4 === "array" && Array.isArray(value) && !value.length) {
    return true;
  }
  if (isNativeStringType(type4) && typeof value === "string" && !value) {
    return true;
  }
  return false;
}
function asyncParallelArray(arr, func, callback) {
  var results = [];
  var total = 0;
  var arrLength = arr.length;
  function count(errors) {
    results.push.apply(results, errors || []);
    total++;
    if (total === arrLength) {
      callback(results);
    }
  }
  arr.forEach(function(a) {
    func(a, count);
  });
}
function asyncSerialArray(arr, func, callback) {
  var index = 0;
  var arrLength = arr.length;
  function next(errors) {
    if (errors && errors.length) {
      callback(errors);
      return;
    }
    var original = index;
    index = index + 1;
    if (original < arrLength) {
      func(arr[original], next);
    } else {
      callback([]);
    }
  }
  next([]);
}
function flattenObjArr(objArr) {
  var ret = [];
  Object.keys(objArr).forEach(function(k) {
    ret.push.apply(ret, objArr[k] || []);
  });
  return ret;
}
var AsyncValidationError = /* @__PURE__ */ function(_Error) {
  _inheritsLoose(AsyncValidationError2, _Error);
  function AsyncValidationError2(errors, fields) {
    var _this;
    _this = _Error.call(this, "Async Validation Error") || this;
    _this.errors = errors;
    _this.fields = fields;
    return _this;
  }
  return AsyncValidationError2;
}(/* @__PURE__ */ _wrapNativeSuper(Error));
function asyncMap(objArr, option, func, callback, source) {
  if (option.first) {
    var _pending = new Promise(function(resolve, reject) {
      var next = function next2(errors) {
        callback(errors);
        return errors.length ? reject(new AsyncValidationError(errors, convertFieldsError(errors))) : resolve(source);
      };
      var flattenArr = flattenObjArr(objArr);
      asyncSerialArray(flattenArr, func, next);
    });
    _pending["catch"](function(e) {
      return e;
    });
    return _pending;
  }
  var firstFields = option.firstFields === true ? Object.keys(objArr) : option.firstFields || [];
  var objArrKeys = Object.keys(objArr);
  var objArrLength = objArrKeys.length;
  var total = 0;
  var results = [];
  var pending = new Promise(function(resolve, reject) {
    var next = function next2(errors) {
      results.push.apply(results, errors);
      total++;
      if (total === objArrLength) {
        callback(results);
        return results.length ? reject(new AsyncValidationError(results, convertFieldsError(results))) : resolve(source);
      }
    };
    if (!objArrKeys.length) {
      callback(results);
      resolve(source);
    }
    objArrKeys.forEach(function(key) {
      var arr = objArr[key];
      if (firstFields.indexOf(key) !== -1) {
        asyncSerialArray(arr, func, next);
      } else {
        asyncParallelArray(arr, func, next);
      }
    });
  });
  pending["catch"](function(e) {
    return e;
  });
  return pending;
}
function isErrorObj(obj2) {
  return !!(obj2 && obj2.message !== void 0);
}
function getValue(value, path) {
  var v = value;
  for (var i = 0; i < path.length; i++) {
    if (v == void 0) {
      return v;
    }
    v = v[path[i]];
  }
  return v;
}
function complementError(rule, source) {
  return function(oe) {
    var fieldValue;
    if (rule.fullFields) {
      fieldValue = getValue(source, rule.fullFields);
    } else {
      fieldValue = source[oe.field || rule.fullField];
    }
    if (isErrorObj(oe)) {
      oe.field = oe.field || rule.fullField;
      oe.fieldValue = fieldValue;
      return oe;
    }
    return {
      message: typeof oe === "function" ? oe() : oe,
      fieldValue,
      field: oe.field || rule.fullField
    };
  };
}
function deepMerge(target, source) {
  if (source) {
    for (var s in source) {
      if (source.hasOwnProperty(s)) {
        var value = source[s];
        if (typeof value === "object" && typeof target[s] === "object") {
          target[s] = _extends({}, target[s], value);
        } else {
          target[s] = value;
        }
      }
    }
  }
  return target;
}
var required$1 = function required(rule, value, source, errors, options, type4) {
  if (rule.required && (!source.hasOwnProperty(rule.field) || isEmptyValue(value, type4 || rule.type))) {
    errors.push(format(options.messages.required, rule.fullField));
  }
};
var whitespace = function whitespace2(rule, value, source, errors, options) {
  if (/^\s+$/.test(value) || value === "") {
    errors.push(format(options.messages.whitespace, rule.fullField));
  }
};
var urlReg;
var getUrlRegex = function() {
  if (urlReg) {
    return urlReg;
  }
  var word = "[a-fA-F\\d:]";
  var b = function b2(options) {
    return options && options.includeBoundaries ? "(?:(?<=\\s|^)(?=" + word + ")|(?<=" + word + ")(?=\\s|$))" : "";
  };
  var v4 = "(?:25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]\\d|\\d)(?:\\.(?:25[0-5]|2[0-4]\\d|1\\d\\d|[1-9]\\d|\\d)){3}";
  var v6seg = "[a-fA-F\\d]{1,4}";
  var v6 = ("\n(?:\n(?:" + v6seg + ":){7}(?:" + v6seg + "|:)|                                    // 1:2:3:4:5:6:7::  1:2:3:4:5:6:7:8\n(?:" + v6seg + ":){6}(?:" + v4 + "|:" + v6seg + "|:)|                             // 1:2:3:4:5:6::    1:2:3:4:5:6::8   1:2:3:4:5:6::8  1:2:3:4:5:6::1.2.3.4\n(?:" + v6seg + ":){5}(?::" + v4 + "|(?::" + v6seg + "){1,2}|:)|                   // 1:2:3:4:5::      1:2:3:4:5::7:8   1:2:3:4:5::8    1:2:3:4:5::7:1.2.3.4\n(?:" + v6seg + ":){4}(?:(?::" + v6seg + "){0,1}:" + v4 + "|(?::" + v6seg + "){1,3}|:)| // 1:2:3:4::        1:2:3:4::6:7:8   1:2:3:4::8      1:2:3:4::6:7:1.2.3.4\n(?:" + v6seg + ":){3}(?:(?::" + v6seg + "){0,2}:" + v4 + "|(?::" + v6seg + "){1,4}|:)| // 1:2:3::          1:2:3::5:6:7:8   1:2:3::8        1:2:3::5:6:7:1.2.3.4\n(?:" + v6seg + ":){2}(?:(?::" + v6seg + "){0,3}:" + v4 + "|(?::" + v6seg + "){1,5}|:)| // 1:2::            1:2::4:5:6:7:8   1:2::8          1:2::4:5:6:7:1.2.3.4\n(?:" + v6seg + ":){1}(?:(?::" + v6seg + "){0,4}:" + v4 + "|(?::" + v6seg + "){1,6}|:)| // 1::              1::3:4:5:6:7:8   1::8            1::3:4:5:6:7:1.2.3.4\n(?::(?:(?::" + v6seg + "){0,5}:" + v4 + "|(?::" + v6seg + "){1,7}|:))             // ::2:3:4:5:6:7:8  ::2:3:4:5:6:7:8  ::8             ::1.2.3.4\n)(?:%[0-9a-zA-Z]{1,})?                                             // %eth0            %1\n").replace(/\s*\/\/.*$/gm, "").replace(/\n/g, "").trim();
  var v46Exact = new RegExp("(?:^" + v4 + "$)|(?:^" + v6 + "$)");
  var v4exact = new RegExp("^" + v4 + "$");
  var v6exact = new RegExp("^" + v6 + "$");
  var ip = function ip2(options) {
    return options && options.exact ? v46Exact : new RegExp("(?:" + b(options) + v4 + b(options) + ")|(?:" + b(options) + v6 + b(options) + ")", "g");
  };
  ip.v4 = function(options) {
    return options && options.exact ? v4exact : new RegExp("" + b(options) + v4 + b(options), "g");
  };
  ip.v6 = function(options) {
    return options && options.exact ? v6exact : new RegExp("" + b(options) + v6 + b(options), "g");
  };
  var protocol = "(?:(?:[a-z]+:)?//)";
  var auth = "(?:\\S+(?::\\S*)?@)?";
  var ipv4 = ip.v4().source;
  var ipv6 = ip.v6().source;
  var host = "(?:(?:[a-z\\u00a1-\\uffff0-9][-_]*)*[a-z\\u00a1-\\uffff0-9]+)";
  var domain = "(?:\\.(?:[a-z\\u00a1-\\uffff0-9]-*)*[a-z\\u00a1-\\uffff0-9]+)*";
  var tld = "(?:\\.(?:[a-z\\u00a1-\\uffff]{2,}))";
  var port = "(?::\\d{2,5})?";
  var path = '(?:[/?#][^\\s"]*)?';
  var regex = "(?:" + protocol + "|www\\.)" + auth + "(?:localhost|" + ipv4 + "|" + ipv6 + "|" + host + domain + tld + ")" + port + path;
  urlReg = new RegExp("(?:^" + regex + "$)", "i");
  return urlReg;
};
var pattern$2 = {
  email: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+\.)+[a-zA-Z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]{2,}))$/,
  hex: /^#?([a-f0-9]{6}|[a-f0-9]{3})$/i
};
var types = {
  integer: function integer(value) {
    return types.number(value) && parseInt(value, 10) === value;
  },
  "float": function float(value) {
    return types.number(value) && !types.integer(value);
  },
  array: function array(value) {
    return Array.isArray(value);
  },
  regexp: function regexp(value) {
    if (value instanceof RegExp) {
      return true;
    }
    try {
      return !!new RegExp(value);
    } catch (e) {
      return false;
    }
  },
  date: function date(value) {
    return typeof value.getTime === "function" && typeof value.getMonth === "function" && typeof value.getYear === "function" && !isNaN(value.getTime());
  },
  number: function number(value) {
    if (isNaN(value)) {
      return false;
    }
    return typeof value === "number";
  },
  object: function object(value) {
    return typeof value === "object" && !types.array(value);
  },
  method: function method(value) {
    return typeof value === "function";
  },
  email: function email(value) {
    return typeof value === "string" && value.length <= 320 && !!value.match(pattern$2.email);
  },
  url: function url(value) {
    return typeof value === "string" && value.length <= 2048 && !!value.match(getUrlRegex());
  },
  hex: function hex(value) {
    return typeof value === "string" && !!value.match(pattern$2.hex);
  }
};
var type$1 = function type(rule, value, source, errors, options) {
  if (rule.required && value === void 0) {
    required$1(rule, value, source, errors, options);
    return;
  }
  var custom = ["integer", "float", "array", "regexp", "object", "method", "email", "number", "date", "url", "hex"];
  var ruleType = rule.type;
  if (custom.indexOf(ruleType) > -1) {
    if (!types[ruleType](value)) {
      errors.push(format(options.messages.types[ruleType], rule.fullField, rule.type));
    }
  } else if (ruleType && typeof value !== rule.type) {
    errors.push(format(options.messages.types[ruleType], rule.fullField, rule.type));
  }
};
var range = function range2(rule, value, source, errors, options) {
  var len = typeof rule.len === "number";
  var min = typeof rule.min === "number";
  var max = typeof rule.max === "number";
  var spRegexp = /[\uD800-\uDBFF][\uDC00-\uDFFF]/g;
  var val = value;
  var key = null;
  var num = typeof value === "number";
  var str = typeof value === "string";
  var arr = Array.isArray(value);
  if (num) {
    key = "number";
  } else if (str) {
    key = "string";
  } else if (arr) {
    key = "array";
  }
  if (!key) {
    return false;
  }
  if (arr) {
    val = value.length;
  }
  if (str) {
    val = value.replace(spRegexp, "_").length;
  }
  if (len) {
    if (val !== rule.len) {
      errors.push(format(options.messages[key].len, rule.fullField, rule.len));
    }
  } else if (min && !max && val < rule.min) {
    errors.push(format(options.messages[key].min, rule.fullField, rule.min));
  } else if (max && !min && val > rule.max) {
    errors.push(format(options.messages[key].max, rule.fullField, rule.max));
  } else if (min && max && (val < rule.min || val > rule.max)) {
    errors.push(format(options.messages[key].range, rule.fullField, rule.min, rule.max));
  }
};
var ENUM$1 = "enum";
var enumerable$1 = function enumerable(rule, value, source, errors, options) {
  rule[ENUM$1] = Array.isArray(rule[ENUM$1]) ? rule[ENUM$1] : [];
  if (rule[ENUM$1].indexOf(value) === -1) {
    errors.push(format(options.messages[ENUM$1], rule.fullField, rule[ENUM$1].join(", ")));
  }
};
var pattern$1 = function pattern(rule, value, source, errors, options) {
  if (rule.pattern) {
    if (rule.pattern instanceof RegExp) {
      rule.pattern.lastIndex = 0;
      if (!rule.pattern.test(value)) {
        errors.push(format(options.messages.pattern.mismatch, rule.fullField, value, rule.pattern));
      }
    } else if (typeof rule.pattern === "string") {
      var _pattern = new RegExp(rule.pattern);
      if (!_pattern.test(value)) {
        errors.push(format(options.messages.pattern.mismatch, rule.fullField, value, rule.pattern));
      }
    }
  }
};
var rules = {
  required: required$1,
  whitespace,
  type: type$1,
  range,
  "enum": enumerable$1,
  pattern: pattern$1
};
var string = function string2(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value, "string") && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options, "string");
    if (!isEmptyValue(value, "string")) {
      rules.type(rule, value, source, errors, options);
      rules.range(rule, value, source, errors, options);
      rules.pattern(rule, value, source, errors, options);
      if (rule.whitespace === true) {
        rules.whitespace(rule, value, source, errors, options);
      }
    }
  }
  callback(errors);
};
var method2 = function method3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules.type(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var number2 = function number3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (value === "") {
      value = void 0;
    }
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules.type(rule, value, source, errors, options);
      rules.range(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var _boolean = function _boolean2(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules.type(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var regexp2 = function regexp3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (!isEmptyValue(value)) {
      rules.type(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var integer2 = function integer3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules.type(rule, value, source, errors, options);
      rules.range(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var floatFn = function floatFn2(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules.type(rule, value, source, errors, options);
      rules.range(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var array2 = function array3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if ((value === void 0 || value === null) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options, "array");
    if (value !== void 0 && value !== null) {
      rules.type(rule, value, source, errors, options);
      rules.range(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var object2 = function object3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules.type(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var ENUM = "enum";
var enumerable2 = function enumerable3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (value !== void 0) {
      rules[ENUM](rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var pattern2 = function pattern3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value, "string") && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (!isEmptyValue(value, "string")) {
      rules.pattern(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var date2 = function date3(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value, "date") && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
    if (!isEmptyValue(value, "date")) {
      var dateObject;
      if (value instanceof Date) {
        dateObject = value;
      } else {
        dateObject = new Date(value);
      }
      rules.type(rule, dateObject, source, errors, options);
      if (dateObject) {
        rules.range(rule, dateObject.getTime(), source, errors, options);
      }
    }
  }
  callback(errors);
};
var required2 = function required3(rule, value, callback, source, options) {
  var errors = [];
  var type4 = Array.isArray(value) ? "array" : typeof value;
  rules.required(rule, value, source, errors, options, type4);
  callback(errors);
};
var type2 = function type3(rule, value, callback, source, options) {
  var ruleType = rule.type;
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value, ruleType) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options, ruleType);
    if (!isEmptyValue(value, ruleType)) {
      rules.type(rule, value, source, errors, options);
    }
  }
  callback(errors);
};
var any = function any2(rule, value, callback, source, options) {
  var errors = [];
  var validate = rule.required || !rule.required && source.hasOwnProperty(rule.field);
  if (validate) {
    if (isEmptyValue(value) && !rule.required) {
      return callback();
    }
    rules.required(rule, value, source, errors, options);
  }
  callback(errors);
};
var validators = {
  string,
  method: method2,
  number: number2,
  "boolean": _boolean,
  regexp: regexp2,
  integer: integer2,
  "float": floatFn,
  array: array2,
  object: object2,
  "enum": enumerable2,
  pattern: pattern2,
  date: date2,
  url: type2,
  hex: type2,
  email: type2,
  required: required2,
  any
};
function newMessages() {
  return {
    "default": "Validation error on field %s",
    required: "%s is required",
    "enum": "%s must be one of %s",
    whitespace: "%s cannot be empty",
    date: {
      format: "%s date %s is invalid for format %s",
      parse: "%s date could not be parsed, %s is invalid ",
      invalid: "%s date %s is invalid"
    },
    types: {
      string: "%s is not a %s",
      method: "%s is not a %s (function)",
      array: "%s is not an %s",
      object: "%s is not an %s",
      number: "%s is not a %s",
      date: "%s is not a %s",
      "boolean": "%s is not a %s",
      integer: "%s is not an %s",
      "float": "%s is not a %s",
      regexp: "%s is not a valid %s",
      email: "%s is not a valid %s",
      url: "%s is not a valid %s",
      hex: "%s is not a valid %s"
    },
    string: {
      len: "%s must be exactly %s characters",
      min: "%s must be at least %s characters",
      max: "%s cannot be longer than %s characters",
      range: "%s must be between %s and %s characters"
    },
    number: {
      len: "%s must equal %s",
      min: "%s cannot be less than %s",
      max: "%s cannot be greater than %s",
      range: "%s must be between %s and %s"
    },
    array: {
      len: "%s must be exactly %s in length",
      min: "%s cannot be less than %s in length",
      max: "%s cannot be greater than %s in length",
      range: "%s must be between %s and %s in length"
    },
    pattern: {
      mismatch: "%s value %s does not match pattern %s"
    },
    clone: function clone() {
      var cloned = JSON.parse(JSON.stringify(this));
      cloned.clone = this.clone;
      return cloned;
    }
  };
}
var messages = newMessages();
var Schema = /* @__PURE__ */ function() {
  function Schema2(descriptor) {
    this.rules = null;
    this._messages = messages;
    this.define(descriptor);
  }
  var _proto = Schema2.prototype;
  _proto.define = function define(rules2) {
    var _this = this;
    if (!rules2) {
      throw new Error("Cannot configure a schema with no rules");
    }
    if (typeof rules2 !== "object" || Array.isArray(rules2)) {
      throw new Error("Rules must be an object");
    }
    this.rules = {};
    Object.keys(rules2).forEach(function(name) {
      var item = rules2[name];
      _this.rules[name] = Array.isArray(item) ? item : [item];
    });
  };
  _proto.messages = function messages2(_messages) {
    if (_messages) {
      this._messages = deepMerge(newMessages(), _messages);
    }
    return this._messages;
  };
  _proto.validate = function validate(source_, o, oc) {
    var _this2 = this;
    if (o === void 0) {
      o = {};
    }
    if (oc === void 0) {
      oc = function oc2() {
      };
    }
    var source = source_;
    var options = o;
    var callback = oc;
    if (typeof options === "function") {
      callback = options;
      options = {};
    }
    if (!this.rules || Object.keys(this.rules).length === 0) {
      if (callback) {
        callback(null, source);
      }
      return Promise.resolve(source);
    }
    function complete(results) {
      var errors = [];
      var fields = {};
      function add(e) {
        if (Array.isArray(e)) {
          var _errors;
          errors = (_errors = errors).concat.apply(_errors, e);
        } else {
          errors.push(e);
        }
      }
      for (var i = 0; i < results.length; i++) {
        add(results[i]);
      }
      if (!errors.length) {
        callback(null, source);
      } else {
        fields = convertFieldsError(errors);
        callback(errors, fields);
      }
    }
    if (options.messages) {
      var messages$1 = this.messages();
      if (messages$1 === messages) {
        messages$1 = newMessages();
      }
      deepMerge(messages$1, options.messages);
      options.messages = messages$1;
    } else {
      options.messages = this.messages();
    }
    var series = {};
    var keys = options.keys || Object.keys(this.rules);
    keys.forEach(function(z) {
      var arr = _this2.rules[z];
      var value = source[z];
      arr.forEach(function(r) {
        var rule = r;
        if (typeof rule.transform === "function") {
          if (source === source_) {
            source = _extends({}, source);
          }
          value = source[z] = rule.transform(value);
        }
        if (typeof rule === "function") {
          rule = {
            validator: rule
          };
        } else {
          rule = _extends({}, rule);
        }
        rule.validator = _this2.getValidationMethod(rule);
        if (!rule.validator) {
          return;
        }
        rule.field = z;
        rule.fullField = rule.fullField || z;
        rule.type = _this2.getType(rule);
        series[z] = series[z] || [];
        series[z].push({
          rule,
          value,
          source,
          field: z
        });
      });
    });
    var errorFields = {};
    return asyncMap(series, options, function(data, doIt) {
      var rule = data.rule;
      var deep = (rule.type === "object" || rule.type === "array") && (typeof rule.fields === "object" || typeof rule.defaultField === "object");
      deep = deep && (rule.required || !rule.required && data.value);
      rule.field = data.field;
      function addFullField(key, schema) {
        return _extends({}, schema, {
          fullField: rule.fullField + "." + key,
          fullFields: rule.fullFields ? [].concat(rule.fullFields, [key]) : [key]
        });
      }
      function cb(e) {
        if (e === void 0) {
          e = [];
        }
        var errorList = Array.isArray(e) ? e : [e];
        if (!options.suppressWarning && errorList.length) {
          Schema2.warning("async-validator:", errorList);
        }
        if (errorList.length && rule.message !== void 0) {
          errorList = [].concat(rule.message);
        }
        var filledErrors = errorList.map(complementError(rule, source));
        if (options.first && filledErrors.length) {
          errorFields[rule.field] = 1;
          return doIt(filledErrors);
        }
        if (!deep) {
          doIt(filledErrors);
        } else {
          if (rule.required && !data.value) {
            if (rule.message !== void 0) {
              filledErrors = [].concat(rule.message).map(complementError(rule, source));
            } else if (options.error) {
              filledErrors = [options.error(rule, format(options.messages.required, rule.field))];
            }
            return doIt(filledErrors);
          }
          var fieldsSchema = {};
          if (rule.defaultField) {
            Object.keys(data.value).map(function(key) {
              fieldsSchema[key] = rule.defaultField;
            });
          }
          fieldsSchema = _extends({}, fieldsSchema, data.rule.fields);
          var paredFieldsSchema = {};
          Object.keys(fieldsSchema).forEach(function(field) {
            var fieldSchema = fieldsSchema[field];
            var fieldSchemaList = Array.isArray(fieldSchema) ? fieldSchema : [fieldSchema];
            paredFieldsSchema[field] = fieldSchemaList.map(addFullField.bind(null, field));
          });
          var schema = new Schema2(paredFieldsSchema);
          schema.messages(options.messages);
          if (data.rule.options) {
            data.rule.options.messages = options.messages;
            data.rule.options.error = options.error;
          }
          schema.validate(data.value, data.rule.options || options, function(errs) {
            var finalErrors = [];
            if (filledErrors && filledErrors.length) {
              finalErrors.push.apply(finalErrors, filledErrors);
            }
            if (errs && errs.length) {
              finalErrors.push.apply(finalErrors, errs);
            }
            doIt(finalErrors.length ? finalErrors : null);
          });
        }
      }
      var res;
      if (rule.asyncValidator) {
        res = rule.asyncValidator(rule, data.value, cb, data.source, options);
      } else if (rule.validator) {
        try {
          res = rule.validator(rule, data.value, cb, data.source, options);
        } catch (error) {
          console.error == null ? void 0 : console.error(error);
          if (!options.suppressValidatorError) {
            setTimeout(function() {
              throw error;
            }, 0);
          }
          cb(error.message);
        }
        if (res === true) {
          cb();
        } else if (res === false) {
          cb(typeof rule.message === "function" ? rule.message(rule.fullField || rule.field) : rule.message || (rule.fullField || rule.field) + " fails");
        } else if (res instanceof Array) {
          cb(res);
        } else if (res instanceof Error) {
          cb(res.message);
        }
      }
      if (res && res.then) {
        res.then(function() {
          return cb();
        }, function(e) {
          return cb(e);
        });
      }
    }, function(results) {
      complete(results);
    }, source);
  };
  _proto.getType = function getType(rule) {
    if (rule.type === void 0 && rule.pattern instanceof RegExp) {
      rule.type = "pattern";
    }
    if (typeof rule.validator !== "function" && rule.type && !validators.hasOwnProperty(rule.type)) {
      throw new Error(format("Unknown rule type %s", rule.type));
    }
    return rule.type || "string";
  };
  _proto.getValidationMethod = function getValidationMethod(rule) {
    if (typeof rule.validator === "function") {
      return rule.validator;
    }
    var keys = Object.keys(rule);
    var messageIndex = keys.indexOf("message");
    if (messageIndex !== -1) {
      keys.splice(messageIndex, 1);
    }
    if (keys.length === 1 && keys[0] === "required") {
      return validators.required;
    }
    return validators[this.getType(rule)] || void 0;
  };
  return Schema2;
}();
Schema.register = function register(type4, validator) {
  if (typeof validator !== "function") {
    throw new Error("Cannot register a validator by type, validator is not a function");
  }
  validators[type4] = validator;
};
Schema.warning = warning;
Schema.messages = messages;
Schema.validators = validators;
const formItemProps = {
  label: String,
  itemWidth: {
    type: [String, Number],
    default: ""
  },
  labelWidth: {
    type: [String, Number],
    default: ""
  },
  prop: [String, Array],
  required: {
    type: Boolean,
    default: void 0
  },
  error: String,
  for: String,
  inlineMessage: {
    type: [String, Boolean],
    default: ""
  },
  showMessage: {
    type: Boolean,
    default: true
  },
  size: {
    type: String,
    values: ["", "small", "middle", "large"]
  },
  rules: Object
};
const _hoisted_1$2 = ["for"];
const _hoisted_2$2 = { class: "ef-form-item__error" };
const __default__ = defineComponent({
  name: "ef-form-item"
});
const _sfc_main$2 = /* @__PURE__ */ Object.assign(__default__, {
  props: formItemProps,
  emits: [],
  setup(__props, { expose, emit }) {
    const props2 = __props;
    useSlots();
    useAttrs$1();
    const validateStatus = ref();
    const validateErrMsg = ref();
    const formItemRef = ref();
    const formContext = inject("efForm", {});
    const validateValue = computed((_) => {
      const model = formContext.model;
      if (!model || !props2.prop) {
        return;
      }
      return getObjValByPath(model, props2.prop).value;
    });
    const validateRule = computed((_) => {
      const rules2 = props2.rules ? castArray(props2.rules) : [];
      const formRules = formContext.rules;
      if (formRules && props2.prop) {
        const _rules = getObjValByPath(formRules, props2.prop).value;
        if (_rules) {
          rules2.push(...castArray(_rules));
        }
      }
      if (props2.required !== void 0) {
        rules2.push({ required: !!props2.required });
      }
      return rules2;
    });
    const validateField = computed((_) => {
      if (!props2.prop)
        return "";
      return Array.isArray(props2.prop) ? props2.prop.join(".") : props2.prop;
    });
    const isRequired = computed(() => validateRule.value.some((rule) => rule.required === true));
    const labelFor = computed(() => props2.for || validateField.value);
    const showErrMsg = computed((_) => (props2.showMessage || formContext.showMessage) && validateStatus.value == "NOTPASS");
    const formItemClasses = computed(() => {
      return [
        "ef-form-item",
        {
          inline: formContext.inline === true,
          "label-top": formContext.labelPosition === "top"
        }
      ];
    });
    const currentLabel = computed(() => `${props2.label || ""}${formContext.labelSuffix || ""}`);
    const labelStyle = computed(() => {
      if (formContext.labelPosition === "top") {
        return {};
      }
      const labelWidth = props2.labelWidth || formContext.labelWidth || "";
      if (labelWidth)
        return { width: addUnit(labelWidth) };
      return {};
    });
    const contentStyle = computed(() => {
      const labelWidth = props2.labelWidth || formContext.labelWidth || "";
      if (labelWidth && !props2.label && formContext.labelPosition != "top")
        return { marginLeft: addUnit(labelWidth) };
      return {};
    });
    const formItemLabelClasses = computed(() => {
      return [
        "ef-form-item__label",
        {
          "label-left": formContext.labelPosition === "left",
          "label-top": formContext.labelPosition === "top",
          required: !formContext.hideRequiredAsterisk && (isRequired.value || props2.required)
        }
      ];
    });
    const itemStyle = computed(() => {
      let style = {};
      const itemWidth = props2.itemWidth || formContext.itemWidth || "";
      if (itemWidth) {
        style.width = addUnit(itemWidth);
        if (formContext.labelPosition === "top" && formContext.inline === true) {
          style.display = "inline-block";
        }
      }
      return style;
    });
    const setValidationStatus = (status) => {
      validateStatus.value = status;
    };
    const onValidationSucceeded = () => {
      setValidationStatus("PASS");
      formContext.emit("validate", validateField.value, true, "");
    };
    const onValidationFailed = (errMsg) => {
      setValidationStatus("NOTPASS");
      validateErrMsg.value = errMsg;
      formContext.emit("validate", validateField.value, false, errMsg);
    };
    const getFilteredRules = (trigger) => {
      const rules2 = validateRule.value;
      return rules2.filter((rule) => {
        if (!rule.trigger || !trigger)
          return true;
        if (Array.isArray(rule.trigger)) {
          return rule.trigger.includes(trigger);
        } else {
          return rule.trigger === trigger;
        }
      }).map(({ trigger: trigger2, ...rule }) => rule);
    };
    const validate = (trigger) => {
      return new Promise((res, rej) => {
        const fieldName = validateField.value;
        let filterTriggers = getFilteredRules(trigger);
        const validator = new Schema({
          [fieldName]: filterTriggers
        });
        validator.validate({ [fieldName]: validateValue.value }, { firstFields: true }).then(() => {
          if (validateStatus.value == "NOTPASS" && filterTriggers.length == 0)
            ;
          else {
            onValidationSucceeded();
          }
          res({
            valid: true
          });
        }).catch((detail) => {
          let { errors, fields } = detail;
          let errmsg = errors[0]["message"];
          onValidationFailed(errmsg);
          res({
            valid: false,
            errmsg,
            detail
          });
        });
      });
    };
    const clearValidate = () => {
      setValidationStatus("");
      validateErrMsg.value = "";
    };
    const context = reactive({
      ...toRefs(props2),
      $el: formItemRef,
      validateStatus,
      clearValidate,
      validate
    });
    provide("efFormItem", context);
    onMounted(() => {
      if (props2.prop) {
        formContext.addField(validateField.value, context);
      }
    });
    onBeforeUnmount(() => {
      formContext.removeField(validateField.value);
    });
    expose({
      validateStatus,
      clearValidate,
      validate
    });
    return (_ctx, _cache) => {
      return openBlock(), createElementBlock("div", {
        style: normalizeStyle(unref(itemStyle)),
        class: normalizeClass(unref(formItemClasses)),
        ref_key: "formItemRef",
        ref: formItemRef
      }, [
        _ctx.label || _ctx.$slots.label ? (openBlock(), createElementBlock("label", {
          key: 0,
          for: unref(labelFor),
          class: normalizeClass(unref(formItemLabelClasses)),
          style: normalizeStyle(unref(labelStyle))
        }, [
          renderSlot(_ctx.$slots, "label", { label: unref(currentLabel) }, () => [
            createTextVNode(toDisplayString(unref(currentLabel)), 1)
          ])
        ], 14, _hoisted_1$2)) : createCommentVNode("", true),
        createElementVNode("div", {
          class: normalizeClass(["ef-form-item__content", { "valid-fail": validateStatus.value === "NOTPASS" }]),
          style: normalizeStyle(unref(contentStyle))
        }, [
          renderSlot(_ctx.$slots, "default"),
          createVNode(Transition, { name: "ef-zoom-in-top" }, {
            default: withCtx(() => [
              unref(showErrMsg) ? renderSlot(_ctx.$slots, "error", {
                key: 0,
                error: validateErrMsg.value
              }, () => [
                createElementVNode("div", _hoisted_2$2, toDisplayString(validateErrMsg.value), 1)
              ]) : createCommentVNode("", true)
            ]),
            _: 3
          })
        ], 6)
      ], 6);
    };
  }
});
_sfc_main$3.install = (app, options) => {
  app.component(_sfc_main$3.name, _sfc_main$3);
};
_sfc_main$2.install = (app, options) => {
  app.component(_sfc_main$2.name, _sfc_main$2);
};
var EfForm = {
  install(app, options) {
    app.use(_sfc_main$3);
    app.use(_sfc_main$2);
  }
};
const _sfc_main$1 = {
  components: {
    EfIcon
  },
  props: {
    onDestroy: {
      type: Function,
      default: () => {
      }
    },
    ifHoverCloseIcon: {
      type: Boolean,
      default: false
    },
    message: {
      type: [String, Object],
      default: ""
    },
    duration: {
      type: Number,
      default: 3e3
    },
    type: {
      type: String,
      default: "warn"
    },
    iconClass: {
      type: String,
      default: ""
    },
    customClass: {
      type: String,
      default: ""
    },
    onClose: {
      type: Function,
      default: () => {
      }
    },
    showClose: {
      type: Boolean,
      default: false
    },
    verticalOffset: {
      type: Number,
      default: 20
    },
    zIndex: {
      type: Number,
      default: 0
    },
    dangerouslyUseHTMLString: {
      type: Boolean,
      default: false
    }
  },
  data() {
    return {
      iconTypeMap: {
        success: "icon-success-fill",
        warn: "icon-prompt-fill1",
        error: "icon-reeor-fill"
      },
      iconColorMap: {
        success: "#67c23a",
        warn: "#e6a23c",
        error: "#f56c6c"
      },
      visible: false,
      timer: null,
      closed: false
    };
  },
  computed: {
    positionStyle() {
      return {
        top: `${this.verticalOffset}px`,
        zIndex: this.zIndex
      };
    }
  },
  watch: {
    closed(newVal) {
      if (newVal) {
        this.visible = false;
      }
    }
  },
  methods: {
    handleAfterLeave() {
      this.onDestroy();
    },
    close() {
      this.closed = true;
      if (typeof this.onClose === "function") {
        this.onClose(this);
      }
    },
    clearTimer() {
      clearTimeout(this.timer);
    },
    startTimer() {
      if (this.duration > 0) {
        this.timer = setTimeout(() => {
          if (!this.closed) {
            this.close();
          }
        }, this.duration);
      }
    },
    keydown(e) {
      if (e.keyCode === 27) {
        if (!this.closed) {
          this.close();
        }
      }
    }
  },
  mounted() {
    this.visible = true;
    this.startTimer();
    document.addEventListener("keydown", this.keydown);
  },
  unmounted() {
    document.removeEventListener("keydown", this.keydown);
  }
};
const _hoisted_1$1 = { key: 0 };
const _hoisted_2$1 = ["innerHTML"];
function _sfc_render$1(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  return openBlock(), createBlock(Transition, {
    name: "ef-message-fade",
    onAfterLeave: $options.handleAfterLeave
  }, {
    default: withCtx(() => [
      withDirectives(createElementVNode("div", {
        class: normalizeClass(["ef-message", $props.type ? `ef-message-${$props.type}` : "", $props.customClass]),
        style: normalizeStyle($options.positionStyle),
        onMouseenter: _cache[2] || (_cache[2] = (...args) => $options.clearTimer && $options.clearTimer(...args)),
        onMouseleave: _cache[3] || (_cache[3] = (...args) => $options.startTimer && $options.startTimer(...args)),
        role: "message"
      }, [
        createVNode(_component_ef_icon, {
          type: $data.iconTypeMap[$props.type],
          color: $data.iconColorMap[$props.type],
          style: { "margin-right": "5px" }
        }, null, 8, ["type", "color"]),
        renderSlot(_ctx.$slots, "default", {}, () => [
          !$props.dangerouslyUseHTMLString ? (openBlock(), createElementBlock("p", _hoisted_1$1, toDisplayString($props.message), 1)) : (openBlock(), createElementBlock("p", {
            key: 1,
            innerHTML: $props.message
          }, null, 8, _hoisted_2$1))
        ]),
        $props.showClose ? (openBlock(), createBlock(_component_ef_icon, {
          key: 0,
          onMouseenter: _cache[0] || (_cache[0] = ($event) => $props.ifHoverCloseIcon = true),
          onMouseleave: _cache[1] || (_cache[1] = ($event) => $props.ifHoverCloseIcon = false),
          style: { "margin-left": "15px", "cursor": "pointer" },
          color: $props.ifHoverCloseIcon ? "#666" : "#cfd2d6",
          fontSize: "16",
          type: "icon-close",
          onClick: $options.close
        }, null, 8, ["color", "onClick"])) : createCommentVNode("", true)
      ], 38), [
        [vShow, $data.visible]
      ])
    ]),
    _: 3
  }, 8, ["onAfterLeave"]);
}
var MessageTempalte = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render$1]]);
let instances = [];
let seed = 1;
let zIndex = 1001;
const interval = 16;
const Message = function(options = {}) {
  if (typeof window === "undefined") {
    return;
  }
  if (typeof options === "string") {
    options = {
      message: options
    };
  }
  let id = "ef_message_" + seed++;
  let verticalOffset = options.offset || interval;
  instances.forEach((item) => {
    verticalOffset += item.el.offsetHeight + interval;
  });
  const props2 = {
    ...options,
    id,
    verticalOffset,
    zIndex: ++zIndex,
    onClose: () => {
      Message.close(id, options.onClose);
    },
    onDestroy: () => {
      render(null, container);
    }
  };
  const container = document.createElement("div");
  const isFunction2 = (val) => typeof val === "function";
  const messageContent = props2.message;
  const vm = createVNode(MessageTempalte, props2, isFunction2(messageContent) ? {
    default: messageContent
  } : isVNode(messageContent) ? {
    default: () => messageContent
  } : null);
  render(vm, container);
  document.body.appendChild(vm.el);
  instances.push(vm);
  return vm;
};
["success", "warn", "error"].forEach((type4) => {
  Message[type4] = (options) => {
    if (typeof options === "string") {
      options = {
        message: options
      };
    }
    options.type = type4;
    return Message(options);
  };
});
Message.close = function(id, userOnClose) {
  let len = instances.length;
  let index = -1;
  let removedHeight;
  for (let i = 0; i < len; i++) {
    if (id === instances[i].props.id) {
      removedHeight = instances[i].el.offsetHeight;
      index = i;
      if (typeof userOnClose === "function") {
        userOnClose(instances[i]);
      }
      instances.splice(i, 1);
      break;
    }
  }
  if (len <= 1 || index === -1 || index > instances.length - 1)
    return;
  for (let i = index; i < len - 1; i++) {
    const pos = parseInt(instances[i].el.style["top"], 10) - removedHeight - interval;
    instances[i].component.props.verticalOffset = pos;
  }
};
Message.closeAll = function() {
  for (let i = instances.length - 1; i >= 0; i--) {
    instances[i].component.ctx.close();
  }
};
Message.install = (app, opts) => {
  app.config.globalProperties.$message = Message;
};
const _sfc_main = {
  name: "ef-alert",
  components: {
    EfIcon,
    EfButton
  },
  computed: {
    bodyStyle() {
      let res = {};
      this.width && (res.width = this.width);
      return res;
    }
  },
  props: {
    title: {
      type: String,
      default: ""
    },
    body: {
      type: String,
      default: ""
    },
    ifBodyHtml: {
      type: Boolean,
      default: true
    },
    confirmText: {
      type: String,
      default: "\u786E\u5B9A"
    },
    cancelText: {
      type: String,
      default: "\u53D6\u6D88"
    },
    showCancelButton: {
      type: Boolean,
      default: true
    },
    ifShowConfirmLoading: {
      type: Boolean,
      default: true
    },
    onCancel: {
      type: Function,
      default: () => {
      }
    },
    onConfirm: {
      type: Function,
      default: () => {
      }
    },
    onDestroy: {
      type: Function,
      default: () => {
      }
    },
    width: {
      type: String,
      default: ""
    },
    showMark: {
      type: Boolean,
      default: true
    },
    showIcon: {
      type: Boolean,
      default: true
    },
    iconType: {
      type: String,
      default: "error"
    }
  },
  data() {
    return {
      iconTypeMap: {
        success: "icon-success-fill",
        warn: "icon-prompt-fill1",
        error: "icon-reeor-fill"
      },
      iconColorMap: {
        success: "#67c23a",
        warn: "#e6a23c",
        error: "#f56c6c"
      },
      markInstance: null,
      showConfirmLoading: false,
      visible: false
    };
  },
  methods: {
    afterLeave() {
      this.onDestroy();
    },
    handleCancel() {
      typeof this.onCancel == "function" && this.onCancel();
      this.doClose();
    },
    handleConfirm() {
      if (typeof this.onConfirm == "function") {
        let res = this.onConfirm();
        if (res === false) {
          return;
        }
        if (isPromise(res)) {
          this.ifShowConfirmLoading && (this.showConfirmLoading = true);
          res.then((res2) => {
            this.doClose();
          });
        } else {
          this.doClose();
        }
      } else {
        this.doClose();
      }
    },
    doClose() {
      this.visible = false;
      this.markInstance && (this.markInstance.data.visible = false);
      this.showConfirmLoading = false;
      this.showBodyScroll();
    },
    showLoading() {
      this.showConfirmLoading = true;
    },
    test() {
    },
    hiddenBodyScroll() {
      document.body.classList.add("overflow-hidden");
    },
    showBodyScroll() {
      document.body.classList.remove("overflow-hidden");
    }
  },
  mounted() {
    this.hiddenBodyScroll();
  },
  created() {
  }
};
const _hoisted_1 = {
  key: 0,
  class: "ef-alert-wrapper"
};
const _hoisted_2 = { class: "header flex-base flex-0-0-auto" };
const _hoisted_3 = ["innerHTML"];
const _hoisted_4 = { class: "footer flex-base flex-0-0-auto margin-top" };
const _hoisted_5 = /* @__PURE__ */ createElementVNode("span", { class: "flex-1" }, null, -1);
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_ef_icon = resolveComponent("ef-icon");
  const _component_ef_button = resolveComponent("ef-button");
  return openBlock(), createBlock(Transition, {
    name: "msgbox-fade",
    onAfterLeave: $options.afterLeave
  }, {
    default: withCtx(() => [
      $data.visible ? (openBlock(), createElementBlock("div", _hoisted_1, [
        createElementVNode("div", {
          class: "inner pos-relative",
          style: normalizeStyle($options.bodyStyle)
        }, [
          createElementVNode("div", _hoisted_2, [
            withDirectives(createVNode(_component_ef_icon, {
              size: "middle",
              type: $data.iconTypeMap[$props.iconType],
              color: $data.iconColorMap[$props.iconType]
            }, null, 8, ["type", "color"]), [
              [vShow, $props.showIcon]
            ]),
            createElementVNode("span", {
              class: normalizeClass(["title", { "icon-padding": $props.showIcon }])
            }, toDisplayString($props.title), 3)
          ]),
          $props.ifBodyHtml ? (openBlock(), createElementBlock("div", {
            key: 0,
            class: normalizeClass(["body margin-top-sm", {
              "inner-icon-padding": $props.showIcon
            }]),
            innerHTML: $props.body
          }, null, 10, _hoisted_3)) : (openBlock(), createElementBlock("div", {
            key: 1,
            class: normalizeClass(["body marin-top-sm", {
              "inner-icon-padding": $props.showIcon
            }])
          }, toDisplayString($props.body), 3)),
          createElementVNode("div", _hoisted_4, [
            _hoisted_5,
            withDirectives(createVNode(_component_ef_button, {
              style: { "min-width": "70px" },
              onClick: $options.handleCancel,
              size: "small"
            }, {
              default: withCtx(() => [
                createTextVNode(toDisplayString($props.cancelText), 1)
              ]),
              _: 1
            }, 8, ["onClick"]), [
              [vShow, $props.showCancelButton]
            ]),
            createVNode(_component_ef_button, {
              style: { "min-width": "70px" },
              loading: $data.showConfirmLoading,
              onClick: $options.handleConfirm,
              type: "primary",
              size: "small",
              class: "margin-left"
            }, {
              default: withCtx(() => [
                createTextVNode(toDisplayString($props.confirmText), 1)
              ]),
              _: 1
            }, 8, ["loading", "onClick"])
          ])
        ], 4)
      ])) : createCommentVNode("", true)
    ]),
    _: 1
  }, 8, ["onAfterLeave"]);
}
var alertVue = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render]]);
const Alert = function(ops = {}) {
  const userDestroy = ops.onDestroy || function() {
  };
  const props2 = {
    ...ops,
    onDestroy: () => {
      userDestroy();
      render(null, container);
    }
  };
  const container = document.createElement("div");
  const vm = createVNode(alertVue, props2, null);
  render(vm, container);
  if (ops.showMark != false) {
    vm.component.data.markInstance = EfMark();
  }
  vm.component.data.visible = true;
  document.body.appendChild(vm.el);
  return vm.component;
};
Alert.install = (app, opts) => {
  app.config.globalProperties.$alert = Alert;
};
function genColumns(columnsConfig) {
  if (!columnsConfig) {
    columnsConfig = {
      key: "aaa|bbb|ccc|ddd|fff",
      title: "\u5DE5\u53F7|\u85AA\u8D441|\u85AA\u8D442|\u85AA\u8D443|\u603B\u5DE5\u8D44",
      fixed: "left|--|--|--|right",
      width: "150|200|-|150|-"
    };
  }
  if (!columnsConfig) {
    throw new Error("columnsConfig \u4E0D\u80FD\u4E3A\u7A7A");
  }
  let cols = columnsConfig.key;
  if (!cols) {
    throw new Error("columnsConfig.key \u4E0D\u80FD\u4E3A\u7A7A");
  }
  let columnsConfigArrMap = {};
  for (let configkey in columnsConfig) {
    let configValue = columnsConfig[configkey];
    if (configValue) {
      let configArr = configValue.split("|");
      columnsConfigArrMap[configkey] = configArr;
    }
  }
  let colsArrLen = columnsConfigArrMap["key"].length;
  let res = [];
  for (let i = 0; i < colsArrLen; i++) {
    let item = {};
    for (let configkey in columnsConfigArrMap) {
      let configArr = columnsConfigArrMap[configkey];
      let val = configArr[i].trim().replace(/-/g, "");
      if (val) {
        item[configkey] = val;
        if (configkey == "key") {
          item["dataIndex"] = val || "";
        }
      }
    }
    res.push(item);
  }
  return res;
}
function genColumnsV2({
  titleMap = {},
  widthMap = {},
  fixedMap = {},
  customRowRenderMap = {},
  customTitleRenderMap = {},
  sortMap = {}
}) {
  return Object.keys(titleMap).map((key) => {
    let row = {
      key,
      dataIndex: key
    };
    row.title = titleMap[key] || "";
    widthMap[key] && (row.width = widthMap[key]);
    fixedMap[key] && (row.fixed = fixedMap[key]);
    customRowRenderMap[key] && (row.customRowRender = customRowRenderMap[key]);
    customTitleRenderMap[key] && (row.customTitleRender = customTitleRenderMap[key]);
    sortMap[key] && (row.sort = sortMap[key]);
    return row;
  });
}
var Util = /* @__PURE__ */ Object.freeze(/* @__PURE__ */ Object.defineProperty({
  __proto__: null,
  genColumns,
  genColumnsV2
}, Symbol.toStringTag, { value: "Module" }));
const Util1 = {
  ...Util,
  install(app, opts) {
    app.config.globalProperties.$efUtil = Util;
  }
};
const componentsArray = [
  tableObj,
  obj$8,
  obj$7,
  obj$6,
  obj$5,
  obj$4,
  obj$3,
  obj$2,
  obj$1,
  obj,
  EfInput,
  EfPopper,
  Popover,
  Tooltip,
  EfDropdown,
  EfSelect,
  EfDatePicker,
  EfTimePicker,
  EfRadio,
  EfCascader,
  Tree,
  CollapseTransition,
  EfForm,
  Util1,
  Message,
  Alert,
  Mark
];
var _default = {
  install(app, option) {
    componentsArray.forEach((component2) => {
      app.use(component2);
    });
  }
};
export { Alert as EfAlert, EfBasicDateTable, EfBasicMonthTable, EfBasicYearTable, obj$3 as EfButton, EfCascader, obj$6 as EfCheckbox, obj$5 as EfCheckboxGroup, CollapseTransition as EfCollapseTransition, EfDatePicker, EfDropdown, EfForm, obj$8 as EfIcon, EfInput, obj$7 as EfLoading, Mark as EfMark, obj$1 as EfMenu, Message as EfMessage, obj$2 as EfModal, EfPanelDatePick, EfPanelDateRangePick, EfPanelMonthRangePick, Popover as EfPopover, EfPopper, EfRadio, EfSelect, obj as EfStep, tableObj as EfTable, EfTimePicker, Tooltip as EfTooltip, Tree as EfTree, obj$4 as EfTriangle, Util1 as EfUtil, _default as default };
