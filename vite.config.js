import {
	defineConfig
} from 'vite'
import {
	resolve
} from 'path';
import DefineOptions from 'unplugin-vue-define-options/vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
	resolve: {
		alias: {
			'@': resolve(__dirname, './src'),
			'#': resolve(__dirname, './package'),
			'&': resolve(__dirname, './theme')
		},
	},
	plugins: [vue(), DefineOptions()]
})
