import EFUI, * as efComponents from '@emmafgy/efui';
import "@emmafgy/efui/theme/index.css";
import './common/index.css';
export default function({
	app,
	router,
	siteData
}) {

	// console.error("clientAppEnhance");

	console.log({
		EFUI,
		efComponents
	})

	// app 是由 createApp 创建的 Vue 应用实例。
	// router 是由 createRouter 创建的路由实例。
	// siteData 是一个根据用户配置生成的对象，包含 base, lang, title, description, head 和 locales。
	app.use(EFUI);
}
