const path = require('path');
const fs = require('fs');

const baseDir = path.resolve(__dirname, '../../docs');


const isFile = (filePath) => {
	try {
		return fs.statSync(filePath).isFile();
	} catch (err) {
		return false;
	}
}

/**
 * 按照分类获取侧边栏
 * @param {*} dirName 
 * @param {*} classifyMap 
 * @param {*} orderMap 
 */
const getSidebarsByClassify = (dirName, classifyMap, orderMap) => {

	let result = [];
	let dir = path.resolve(baseDir, dirName);
	const classifyDirList = fs.readdirSync(dir);
	// order
	classifyDirList.sort((a, b) => orderMap[a] > orderMap[b] ? 1 : -1);
	classifyDirList.forEach(classifyDir => {
		// let sidebarItem = {};
		let sidebarText = classifyMap[classifyDir] || classifyDir;
		let sidebarChildren = [];
		const files = fs.readdirSync(path.resolve(dir, classifyDir));
		files.forEach(fileName => {
			// let child = {};
			let fileContent = fs.readFileSync(path.resolve(dir, classifyDir, fileName))
				.toString();
			let line1 = fileContent.split('\n')[0];
			let childText = fileName;
			if (line1) {
				childText = line1.replace(/#/, '');
			}
			let childLink = `/docs/${dirName}/${classifyDir}/${fileName}`;
			sidebarChildren.push({
				text: childText,
				link: childLink
			});
		});
		sidebarChildren.sort((a, b) => a.text.localeCompare(b.text));
		console.log({
			sidebarChildren
		})
		result.push({
			text: sidebarText,
			children: sidebarChildren
		});
	});
	return result;

}


/**
 * 导航栏分类，排序配置
 */
let guideClassify = {
	'basic': "基础",
	'advance': '进阶',
	'introduce': '介绍'
};

let guideOrder = {
	'introduce': 1,
	'basic': 2,
	'advance': 3
};

let blogsClassify = {
	'frontend': '前端',
	'backend': '后端',
	'server': '服务器',
	'git': 'git'
};

let blogsOrder = {
	'frontend': 1,
	'backend': 2,
	'server': 3,
	'git': 4
};

let componentsClassify = {
	'basic': "基础组件",
	'data-display': "数据展示组件",
	'feedback': "反馈组件",
	'form': "表单组件",
	'other': "其他组件"
};

let componentsOrder = {
	'basic': 1,
	'data-display': 3,
	'feedback': 4,
	'form': 2,
	'other': 5,
};

module.exports = {
	guideSidebars: getSidebarsByClassify('guide', guideClassify, guideOrder),
	componentsSidebars: getSidebarsByClassify('components', componentsClassify, componentsOrder),
	blogsSidebars: getSidebarsByClassify('blogs', blogsClassify, blogsOrder)
}