import{_ as n,x as s}from"./app.7c4c2116.js";const a={},e=s(`<h1 id="linux-\u6DFB\u52A0jar\u5305\u670D\u52A1" tabindex="-1"><a class="header-anchor" href="#linux-\u6DFB\u52A0jar\u5305\u670D\u52A1" aria-hidden="true">#</a> linux \u6DFB\u52A0jar\u5305\u670D\u52A1</h1><ol><li>\u5728/etc/init.d/\u65B0\u5EFAsh\u6587\u4EF6</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token shebang important">#!/bin/sh</span>
<span class="token comment"># chkconfig: 2345 85 15</span>
<span class="token comment"># description:auto_run</span>

<span class="token comment">#JAR\u6839\u4F4D\u7F6E</span>
<span class="token assign-left variable">JAR_ROOT</span><span class="token operator">=</span>/opt/test-server/

<span class="token comment">#JAR\u4F4D\u7F6E</span>
<span class="token assign-left variable">JAR_PATH</span><span class="token operator">=</span><span class="token string">&quot;<span class="token variable">$JAR_ROOT</span>&quot;</span>test-sso-server.jar

<span class="token comment">#LOG\u4F4D\u7F6E</span>
<span class="token assign-left variable">LOG_PATH</span><span class="token operator">=</span><span class="token string">&quot;<span class="token variable">$JAR_ROOT</span>&quot;</span>test-server-nohup.out

<span class="token comment">#\u914D\u7F6E\u6587\u4EF6</span>
<span class="token assign-left variable">APP_CONFIG_PATH</span><span class="token operator">=</span><span class="token string">&quot;<span class="token variable">$JAR_ROOT</span>&quot;</span>application-prod.yml

<span class="token comment">#\u5F00\u59CB\u65B9\u6CD5</span>
<span class="token function-name function">start</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token builtin class-name">cd</span> <span class="token variable">$JAR_ROOT</span>
    <span class="token function">nohup</span> java -Dfile.encoding<span class="token operator">=</span>utf-8 -jar <span class="token variable">$JAR_PATH</span> --spring.config.location<span class="token operator">=</span><span class="token variable">$APP_CONFIG_PATH</span>  <span class="token operator">&gt;</span>  <span class="token variable">$LOG_PATH</span> <span class="token operator"><span class="token file-descriptor important">2</span>&gt;</span><span class="token file-descriptor important">&amp;1</span> <span class="token operator">&amp;</span>
   <span class="token comment"># nohup java -Dfile.encoding=utf-8 -jar -Xms128m -Xmx256m -XX:PermSize=32M -XX:MaxPermSize=64M $JAR_PATH &gt;$LOG_PATH &amp;</span>
    <span class="token builtin class-name">echo</span> <span class="token string">&quot;<span class="token variable">$JAR_PATH</span> start success.&quot;</span>
<span class="token punctuation">}</span>

<span class="token comment">#\u7ED3\u675F\u65B9\u6CD5</span>
<span class="token function-name function">stop</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token function">kill</span> -9 <span class="token variable"><span class="token variable">\`</span><span class="token function">ps</span> -ef<span class="token operator">|</span><span class="token function">grep</span> $JAR_PATH<span class="token operator">|</span><span class="token function">grep</span> -v <span class="token function">grep</span><span class="token operator">|</span><span class="token function">grep</span> -v stop<span class="token operator">|</span><span class="token function">awk</span> <span class="token string">&#39;{print $2}&#39;</span><span class="token variable">\`</span></span>
    <span class="token builtin class-name">echo</span> <span class="token string">&quot;<span class="token variable">$JAR_PATH</span> stop success.&quot;</span>
<span class="token punctuation">}</span>

<span class="token keyword">case</span> <span class="token string">&quot;<span class="token variable">$1</span>&quot;</span> <span class="token keyword">in</span>
start<span class="token punctuation">)</span>
    start
    <span class="token punctuation">;</span><span class="token punctuation">;</span>
stop<span class="token punctuation">)</span>
    stop
    <span class="token punctuation">;</span><span class="token punctuation">;</span>
restart<span class="token punctuation">)</span>
    stop
    start
    <span class="token punctuation">;</span><span class="token punctuation">;</span>
*<span class="token punctuation">)</span>
    <span class="token builtin class-name">echo</span> <span class="token string">&quot;Userage: <span class="token variable">$0</span> {start|stop|restart}&quot;</span>
    <span class="token builtin class-name">exit</span> <span class="token number">1</span>
<span class="token keyword">esac</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br></div></div><ol start="2"><li>\u7ED9sh\u6587\u4EF6\u548Cjar\u53EF\u6267\u884C\u6743\u9650</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token function">chmod</span> +x /etc/init.d/sh\u6587\u4EF6
<span class="token function">chmod</span> +x jar\u5305\u4F4D\u7F6E
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><ol start="3"><li>\u8BBE\u7F6E\u5F00\u673A\u542F\u52A8</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>\u6DFB\u52A0\u4E3A\u7CFB\u7EDF\u670D\u52A1
<span class="token function">chkconfig</span> --add sh\u6587\u4EF6

\u5F00\u673A\u81EA\u542F\u52A8
<span class="token function">chkconfig</span> sh\u6587\u4EF6 on

\u67E5\u770B
<span class="token function">chkconfig</span> --list

\u542F\u52A8
<span class="token function">service</span> sh\u6587\u4EF6 start

\u505C\u7528
<span class="token function">service</span> sh\u6587\u4EF6 stop

\u91CD\u542F
<span class="token function">service</span> sh\u6587\u4EF6 restart
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br></div></div>`,7);function p(l,t){return e}var r=n(a,[["render",p],["__file","autoRunJar.html.vue"]]);export{r as default};
