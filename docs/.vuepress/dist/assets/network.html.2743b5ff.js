import{_ as s,x as n}from"./app.7c4c2116.js";const a={},e=n(`<h1 id="\u7F51\u5361\u914D\u7F6E" tabindex="-1"><a class="header-anchor" href="#\u7F51\u5361\u914D\u7F6E" aria-hidden="true">#</a> \u7F51\u5361\u914D\u7F6E</h1><h2 id="\u5355\u7F51\u5361\u914D\u7F6E" tabindex="-1"><a class="header-anchor" href="#\u5355\u7F51\u5361\u914D\u7F6E" aria-hidden="true">#</a> \u5355\u7F51\u5361\u914D\u7F6E</h2><ol><li>nmcli dev \u6216\u8005 nmcli con \u6216\u8005 ifconfig \u67E5\u8BE2\u5F00\u542F\u7684\u7F51\u5361\u540D\u79F0</li><li>\u8FDB\u5165/etc/sysconfig/network-scripts/\u76EE\u5F55 \u627E\u5230\u5BF9\u5E94\u7684\u7F51\u5361\u914D\u7F6E\u6587\u4EF6\uFF08\u4F8B\uFF1Aifcfg-eth0\uFF09\uFF0C\u5E76\u53C2\u7167\u5982\u4E0B\u793A\u4F8B\u7F16\u8F91</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token assign-left variable">TYPE</span><span class="token operator">=</span>Ethernet
<span class="token assign-left variable">PROXY_METHOD</span><span class="token operator">=</span>none
<span class="token assign-left variable">BROWSER_ONLY</span><span class="token operator">=</span>no
<span class="token assign-left variable">BOOTPROTO</span><span class="token operator">=</span>none
<span class="token assign-left variable">DEFROUTE</span><span class="token operator">=</span>yes
<span class="token assign-left variable">IPV4_FAILURE_FATAL</span><span class="token operator">=</span>no
<span class="token assign-left variable">IPV6INIT</span><span class="token operator">=</span>no
<span class="token assign-left variable">IPV6_AUTOCONF</span><span class="token operator">=</span>no
<span class="token assign-left variable">IPV6_DEFROUTE</span><span class="token operator">=</span>no
<span class="token assign-left variable">IPV6_FAILURE_FATAL</span><span class="token operator">=</span>no
<span class="token assign-left variable">NAME</span><span class="token operator">=</span>ens1f1np1
<span class="token assign-left variable">UUID</span><span class="token operator">=</span>02ec3c03-cc86-4435-8e70-e693602dd366
<span class="token assign-left variable">DEVICE</span><span class="token operator">=</span>ens1f1np1
<span class="token assign-left variable">ONBOOT</span><span class="token operator">=</span>yes
<span class="token assign-left variable">IPADDR</span><span class="token operator">=</span><span class="token number">192.18</span>.0.28
<span class="token assign-left variable">NETMASK</span><span class="token operator">=</span><span class="token number">255.255</span>.255.0
<span class="token assign-left variable">DNS1</span><span class="token operator">=</span><span class="token number">100.0</span>.100.10
<span class="token assign-left variable">GATEWAY</span><span class="token operator">=</span><span class="token number">192.18</span>.0.1
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br></div></div><ol start="3"><li>\u91CD\u542F\u7F51\u7EDC</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>centos7: <span class="token function">service</span> network restart

centos8:
<span class="token number">1</span>. nmcli c reload
<span class="token number">2</span>. nmcli c up ethX

</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br></div></div><h2 id="\u7F51\u7EDC\u805A\u5408\u914D\u7F6E" tabindex="-1"><a class="header-anchor" href="#\u7F51\u7EDC\u805A\u5408\u914D\u7F6E" aria-hidden="true">#</a> \u7F51\u7EDC\u805A\u5408\u914D\u7F6E</h2><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>
<span class="token number">1</span>- nmcli device status <span class="token punctuation">(</span>nmcli dev<span class="token punctuation">)</span>\u67E5\u770B\u7F51\u53E3\u72B6\u6001\u662F\u5426\u8FDE\u63A5

<span class="token number">1</span>-1 nmcli device connect em1 <span class="token builtin class-name">:</span> \u6253\u5F00\u9700\u8981\u5F00\u542F\u7684\u7F51\u53E3
 
<span class="token number">2</span>- nmcli connection <span class="token function">add</span> <span class="token builtin class-name">type</span> bond ifname bond0 mode <span class="token number">0</span>
  \u521B\u5EFA\u4E00\u4E2A\u903B\u8F91\u7F51\u53E3\uFF0C\u805A\u5408\u7C7B\u578B\u4E3Abond\uFF0C\u805A\u5408\u53E3\u540D\u79F0\u4E3Abond0\uFF0C\u6A21\u5F0F\u4E3A1\u3002
\uFF081\u4E3A\u4E3B\u5907\uFF0C\u6211\u4EEC\u5B9E\u65BD\u65F6\u4F7F\u7528\u6700\u591A\u662F\u6A21\u5F0F0\uFF0C0\u4E3A\u8D1F\u8F7D\u5206\u62C5\uFF0Cbond\u603B\u5171\u67097\u79CD\u6A21\u5F0F

<span class="token number">3</span>- nmcli connection <span class="token function">add</span> <span class="token builtin class-name">type</span> bond-slave ifname em1 master bond0\uFF0C
  \u5C06\u771F\u5B9E\u7F51\u5361ens33\u548Cens37\u4F5C\u4E3A\u6210\u5458\u53E3\u6DFB\u52A0\u5230\u805A\u5408\u53E3bond0

<span class="token number">4</span>-\u4FEE\u6539\u805A\u5408\u53E3\u914D\u7F6E\u6587\u4EF6\uFF1A<span class="token punctuation">(</span>\u4E3B\u8981\u914D\u7F6E\u597DIP\u7B49\u4FE1\u606F<span class="token punctuation">)</span> 
  \u6587\u4EF6\u8DEF\u5F84\uFF1A/etc/sysconfig/network-scripts/ifcfg-bond-bond0
  <span class="token assign-left variable">BONDING_OPTS</span><span class="token operator">=</span>mode<span class="token operator">=</span>balance-rr
  <span class="token assign-left variable">TYPE</span><span class="token operator">=</span>Bond
  <span class="token assign-left variable">BONDING_MASTER</span><span class="token operator">=</span>yes
  <span class="token assign-left variable">PROXY_METHOD</span><span class="token operator">=</span>none
  <span class="token assign-left variable">BROWSER_ONLY</span><span class="token operator">=</span>no
  <span class="token assign-left variable">BOOTPROTO</span><span class="token operator">=</span>none
  <span class="token assign-left variable">DEFROUTE</span><span class="token operator">=</span>yes
  <span class="token assign-left variable">IPV4_FAILURE_FATAL</span><span class="token operator">=</span>no
  <span class="token assign-left variable">IPV6INIT</span><span class="token operator">=</span>no
  <span class="token assign-left variable">IPV6_AUTOCONF</span><span class="token operator">=</span>no
  <span class="token assign-left variable">IPV6_DEFROUTE</span><span class="token operator">=</span>no
  <span class="token assign-left variable">IPV6_FAILURE_FATAL</span><span class="token operator">=</span>no
  <span class="token assign-left variable">IPV6_ADDR_GEN_MODE</span><span class="token operator">=</span>stable-privacy
  <span class="token assign-left variable">NAME</span><span class="token operator">=</span>bond-bond0
  <span class="token assign-left variable">UUID</span><span class="token operator">=</span>22e50880-cc3c-4886-b9a0-a7a929570f73
  <span class="token assign-left variable">DEVICE</span><span class="token operator">=</span>bond0
  <span class="token assign-left variable">ONBOOT</span><span class="token operator">=</span>yes
  <span class="token assign-left variable">IPADDR</span><span class="token operator">=</span><span class="token number">192.18</span>.0.27
  <span class="token assign-left variable">NETMASK</span><span class="token operator">=</span><span class="token number">255.255</span>.255.0
  <span class="token assign-left variable">DNS1</span><span class="token operator">=</span><span class="token number">100.0</span>.100.10
  <span class="token assign-left variable">GATEWAY</span><span class="token operator">=</span><span class="token number">192.18</span>.0.1

<span class="token number">5</span>-\u91CD\u542F\u7F51\u7EDC
  centos7: <span class="token function">service</span> network restart
  centos8:
<span class="token number">1</span>. nmcli c reload
<span class="token number">2</span>. nmcli c up ethX

\u5176\u4ED6\u547D\u4EE4\u8BF4\u660E
nmcli dev <span class="token builtin class-name">:</span> \u67E5\u770B\u7F51\u8DEF\u8BBE\u5907\u8FDE\u63A5\u72B6\u6001
nmcli con \uFF1A\u67E5\u770B\u7F51\u7EDC\u8FDE\u63A5\u72B6\u6001
<span class="token function">ip</span> a <span class="token operator">|</span> <span class="token function">grep</span> bond :\u67E5\u770B\u7F51\u7EDC\u63A5\u53E3\u72B6\u6001

</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br></div></div>`,8);function p(l,r){return e}var c=s(a,[["render",p],["__file","network.html.vue"]]);export{c as default};
