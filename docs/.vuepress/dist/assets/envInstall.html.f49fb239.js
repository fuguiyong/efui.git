import{_ as r,r as i,o as t,b as p,f as e,d as a,w as l,F as c,a as s,x as d}from"./app.7c4c2116.js";const u={},m=e("h1",{id:"linux-\u73AF\u5883-\u8F6F\u4EF6\u5B89\u88C5",tabindex:"-1"},[e("a",{class:"header-anchor",href:"#linux-\u73AF\u5883-\u8F6F\u4EF6\u5B89\u88C5","aria-hidden":"true"},"#"),s(" linux \u73AF\u5883/\u8F6F\u4EF6\u5B89\u88C5")],-1),b={class:"table-of-contents"},o=s("Centos\u5B89\u88C5php7"),g=s("Centos\u5B89\u88C5nginx"),v=s("Centos\u79BB\u7EBF\u5B89\u88C5nginx"),x=s("Centos\u79BB\u7EBF\u5B89\u88C5reids"),h=s("Centos\u79BB\u7EBF\u5B89\u88C5gcc"),y=s("Centos\u79BB\u7EBF\u5B89\u88C5mysql"),f=s("\u5B89\u88C5java\u73AF\u5883"),q=d(`<h2 id="centos\u5B89\u88C5php7" tabindex="-1"><a class="header-anchor" href="#centos\u5B89\u88C5php7" aria-hidden="true">#</a> Centos\u5B89\u88C5php7</h2><ol><li>\u66F4\u6362rpm\u6E90</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token comment">#Centos 5.X\uFF1A</span>
<span class="token function">rpm</span> -Uvh http://mirror.webtatic.com/yum/el5/latest.rpm
<span class="token comment">#CentOs 6.x\uFF1A</span>
<span class="token function">rpm</span> -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm
<span class="token comment">#CentOs 7.X\uFF1A</span>
<span class="token function">rpm</span> -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
<span class="token function">rpm</span> -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

<span class="token comment"># \u67E5\u770B\u53EF\u7528php\u7248\u672C</span>
yum provides php
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br></div></div><ol start="2"><li>\u5220\u9664\u5DF2\u5B89\u88C5php\u7248\u672C</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>yum remove php*
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><ol start="3"><li>\u5B89\u88C5php7.2</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>yum <span class="token function">install</span> -y php72w php72w-opcache php72w-xml php72w-mcrypt php72w-gd php72w-devel php72w-mysql php72w-intl php72w-mbstring
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><ol start="4"><li>\u67E5\u770B\u7248\u672C</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>php -v
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><ol start="5"><li>\u5B89\u88C5php-fpm</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>yum <span class="token function">install</span> php72w-fpm
systemctl start php-fpm.service \u3010\u542F\u52A8\u3011
systemctl <span class="token builtin class-name">enable</span> php-fpm.service\u3010\u5F00\u673A\u81EA\u542F\u52A8\u3011
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><h2 id="centos\u5B89\u88C5nginx" tabindex="-1"><a class="header-anchor" href="#centos\u5B89\u88C5nginx" aria-hidden="true">#</a> Centos\u5B89\u88C5nginx</h2><p>1.\u6DFB\u52A0\u6E90</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>sudo rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>2.\u5B89\u88C5</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>yum install -y nginx
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>3.\u542F\u52A8\u5E76\u4E14\u8BBE\u7F6E\u5F00\u673A\u91CD\u542F</p><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code><span class="token function">sudo</span> systemctl start nginx.service
<span class="token function">sudo</span> systemctl <span class="token builtin class-name">enable</span> nginx.service
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><h2 id="centos\u79BB\u7EBF\u5B89\u88C5nginx" tabindex="-1"><a class="header-anchor" href="#centos\u79BB\u7EBF\u5B89\u88C5nginx" aria-hidden="true">#</a> Centos\u79BB\u7EBF\u5B89\u88C5nginx</h2><p>1.\u4E0B\u8F7D\u5B89\u88C5\u5305</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>http://nginx.org/packages/centos/7/x86_64/RPMS/
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>2.\u4E0A\u4F20&amp;\u5B89\u88C5</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>sudo yum install -y nginx-1.16.1-1.el7.ngx.x86_64.rpm
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>3.\u542F\u52A8</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>sudo service nginx start
#\u6216\u8005
sudo systemctl start nginx.service
sudo systemctl enable nginx.service
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div><h2 id="centos\u79BB\u7EBF\u5B89\u88C5reids" tabindex="-1"><a class="header-anchor" href="#centos\u79BB\u7EBF\u5B89\u88C5reids" aria-hidden="true">#</a> Centos\u79BB\u7EBF\u5B89\u88C5reids</h2><p>1.\u68C0\u67E5\u73AF\u5883\u662F\u5426\u5B58\u5728gcc-c++</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>rpm -qa | grep gcc-c++ (\u672A\u8F93\u5165\u5185\u5BB9\u5219\u65E0)
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>2.\u4E0B\u8F7Dredis</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>https://download.redis.io/releases/
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>3.\u4E0A\u4F20\u81F3\u670D\u52A1\u5668</p><p>4.\u89E3\u538B\u6587\u4EF6</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>tar -zxvf
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>5.\u7F16\u8BD1</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u8FDB\u5165\u89E3\u538B\u76EE\u5F55
make MALLOC=libc
cd src &amp;&amp; make install
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><p>6.\u914D\u7F6E\u5BC6\u7801\u53CA\u540E\u53F0\u542F\u52A8</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u6253\u5F00redis.conf,\u5C06 daemonize \u8BBE\u7F6E\u4E3A yes,requirepass \u201C12345\u201D
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>7.\u542F\u52A8</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u8FDB\u5165bin\uFF08/usr/local/bin\uFF09\u76EE\u5F55
./redis-server [\u914D\u7F6E\u6587\u4EF6\u76EE\u5F55]
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><p>8.\u6D4B\u8BD5</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u8FDB\u5165bin\u76EE\u5F55
./redis-cli
set name &#39;Emma&#39;
get name
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div><p>9.\u5173\u95ED</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u8FDB\u5165bin\u76EE\u5F55
./redis-cli shutdown
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><h2 id="centos\u79BB\u7EBF\u5B89\u88C5gcc" tabindex="-1"><a class="header-anchor" href="#centos\u79BB\u7EBF\u5B89\u88C5gcc" aria-hidden="true">#</a> Centos\u79BB\u7EBF\u5B89\u88C5gcc</h2><p>1.\u4E0B\u8F7Dgcc\u5B89\u88C5\u5305(\u534E\u4E3A\u4E91\u76D8\u5B58\u5728)</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>gcc\u4E0B\u8F7D\u5730\u5740\uFF1Ahttps://mirrors.tuna.tsinghua.edu.cn/gnu/gcc/
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>2.\u5B89\u88C5rpm\u5305</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>(\u4E0A\u4F20\u7B2C\u4E00\u6B65\u4E0B\u8F7D\u7684\u5305\u4E0A\u4F20\u5230\u6307\u5B9A\u76EE\u5F55\uFF0C\u8FDB\u5165\u8BE5\u76EE\u5F55)
rpm -Uvh *.rpm --nodeps --force
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><p>3.\u9A8C\u8BC1</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>gcc -v
g++ -v
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><h2 id="centos\u79BB\u7EBF\u5B89\u88C5mysql" tabindex="-1"><a class="header-anchor" href="#centos\u79BB\u7EBF\u5B89\u88C5mysql" aria-hidden="true">#</a> Centos\u79BB\u7EBF\u5B89\u88C5mysql</h2><p>1.\u4E0B\u8F7D</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.7/
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>2.\u68C0\u67E5\u662F\u5426\u5DF2\u5B89\u88C5mariadb\uFF0C\u6709\u5219\u5148\u5220\u9664\uFF0C\u5426\u5219\u4F1A\u4F7F\u7528mariadb\u7684\u914D\u7F6E</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code> rpm -qa|grep mariadb
 rpm -e mariadb-libs
 rpm -e mariadb-libs postfix
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><p>3.\u521B\u5EFA\u7528\u6237</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>groupadd mysql
useradd  -g mysql mysql
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><p>4.\u89E3\u538B\u6587\u4EF6\uFF08\u653E\u5728/usr/local/mysql57\uFF09</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>tar -xvf mysql-5.7.18-linux-glibc2.5-x86_64.tar.gz
mv mysql-5.7.18-linux-glibc2.5-x86_64 mysql57
chown -R mysql:mysql mysql57
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><br><p>4.\u521B\u5EFA\u6570\u636E\u6587\u4EF6\u76EE\u5F55,\u65E5\u5FD7\u6587\u4EF6\u76EE\u5F55\uFF0Csocketfile\u76EE\u5F55(\u4F7F\u7528\u5B58\u5728\u7684\u76EE\u5F55\u4E0D\u7528\u521B\u5EFA,\u7B2C\u4E94\u6B65\u914D\u7F6E\u4F7F\u7528)\uFF0C\u5E76\u7ED9mysql\u7528\u6237\u5206\u914D\u6743\u9650</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>mkdir -p /usr/local/mysql57/data
chown -R mysql:mysql /usr/local/mysql57/data
mkdir -p /usr/local/mysql57/log
chown -R mysql:mysql /usr/local/mysql57/log
chmod -R 755 /usr/local/mysql57
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br></div></div><br><p>5.\u521B\u5EFA\u914D\u7F6E\u6587\u4EF6my.cnf(\u6307\u5B9A\u914D\u7F6E\u9700\u8981\u548C\u7B2C\u56DB\u6B65\u5BF9\u4E0A)\uFF0C\u4E00\u822C\u653E\u5728/etc/my.cnf,\u7528\u4E8E\u7B2C\u516D\u6B65\u521D\u59CB\u5316\u6570\u636E\u5E93</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>
[mysqld]
user                          = mysql
port                          = 3306
character_set_server          = utf8mb4
server_id                     = 10
socket                        = /usr/local/mysql57/mysql.sock
basedir                       = /usr/local/mysql57
datadir                       = /usr/local/mysql57/data
log-error                     = /usr/local/mysql57/log/error.log
pid-file                      = /usr/local/mysql57/mysql.pid
log-bin                       = /usr/local/mysql57/data/mysql-bin

[client]
default-character-set         = utf8mb4
port                          = 3306
socket                        = /usr/local/mysql57/mysql.sock


[mysqld_safe]
#log-error=/var/log/mariadb/mariadb.log
#pid-file=/var/run/mariadb/mariadb.pid

#
# include all files from the config directory
#
#!includedir /etc/my.cnf.d

</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br></div></div><p>6.\u521D\u59CB\u5316\u6570\u636E\u5E93\uFF08\u5F15\u7528\u7B2C\u4E94\u6B65\u7684\u914D\u7F6E\u6587\u4EF6\uFF09</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>/usr/local/mysql57/bin/mysqld --defaults-file=/etc/my.cnf --user=mysql --initialize

\u521D\u59CB\u5316\u540E\u5BC6\u7801\u653E\u5728\u914D\u7F6E\u7684\u65E5\u5FD7\u6587\u4EF6\uFF08/usr/local/mysql57/log/error.log\uFF09\uFF0C\u6216\u8005\u4F1A\u76F4\u63A5\u6253\u5370\u51FA\u6765\u3002
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><p>7.\u542F\u52A8mysql</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>\u7B2C\u4E00\u79CD\u65B9\u5F0F\uFF1Amysqld_safe
/usr/local/mysql57/bin/mysqld_safe --defaults-file=/etc/my.cnf

\u7B2C\u4E8C\u79CD\u65B9\u5F0F\uFF1A\u6DFB\u52A0\u5230\u670D\u52A1\u542F\u52A8\uFF08\u63A8\u8350\uFF09
cp /usr/local/mysql57/support-files/mysql.server /etc/init.d/mysql.server
service mysql.server start
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br></div></div><p>8.\u6DFB\u52A0\u73AF\u5883\u53D8\u91CF\uFF08/etc/profile\uFF09</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>export PATH=$PATH:/usr/local/mysql57/bin
source /etc/profile
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br></div></div><p>9.\u767B\u5F55mysql\u4FEE\u6539\u5BC6\u7801</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>mysql -uroot -p
mysql&gt; alter user &quot;root&quot;@&quot;localhost&quot; identified by &quot;Dtsw@sso220530EQ}^z~(r&quot;;
mysql&gt; flush privileges;
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br></div></div><h2 id="\u5B89\u88C5java\u73AF\u5883" tabindex="-1"><a class="header-anchor" href="#\u5B89\u88C5java\u73AF\u5883" aria-hidden="true">#</a> \u5B89\u88C5java\u73AF\u5883</h2><p>1.\u4E0B\u8F7D</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>2.\u89E3\u538B\u5230\u6307\u5B9A\u76EE\u5F55</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>tar -zxvf jdk-8u171-linux-x64.tar.gz -C /usr/local/java/
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>3.\u8BBE\u7F6E\u73AF\u5883\u53D8\u91CF</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>vim /etc/profile

# \u672B\u5C3E\u6DFB\u52A0
export JAVA_HOME=/usr/local/java/jdk1.8.0_311
export JRE_HOME=\${JAVA_HOME}/jre
export CLASSPATH=.:\${JAVA_HOME}/lib:\${JRE_HOME}/lib
export PATH=\${JAVA_HOME}/bin:$PATH

source /etc/profile
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br></div></div><p>4.\u6DFB\u52A0\u8F6F\u8FDE\u63A5</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>ln -s /usr/local/java/jdk1.8.0_311/bin/java /usr/bin/java
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div><p>5.\u68C0\u67E5\u662F\u5426\u6210\u529F</p><div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>java -version
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br></div></div>`,84);function _(k,w){const n=i("RouterLink");return t(),p(c,null,[m,e("nav",b,[e("ul",null,[e("li",null,[a(n,{to:"#centos\u5B89\u88C5php7"},{default:l(()=>[o]),_:1})]),e("li",null,[a(n,{to:"#centos\u5B89\u88C5nginx"},{default:l(()=>[g]),_:1})]),e("li",null,[a(n,{to:"#centos\u79BB\u7EBF\u5B89\u88C5nginx"},{default:l(()=>[v]),_:1})]),e("li",null,[a(n,{to:"#centos\u79BB\u7EBF\u5B89\u88C5reids"},{default:l(()=>[x]),_:1})]),e("li",null,[a(n,{to:"#centos\u79BB\u7EBF\u5B89\u88C5gcc"},{default:l(()=>[h]),_:1})]),e("li",null,[a(n,{to:"#centos\u79BB\u7EBF\u5B89\u88C5mysql"},{default:l(()=>[y]),_:1})]),e("li",null,[a(n,{to:"#\u5B89\u88C5java\u73AF\u5883"},{default:l(()=>[f]),_:1})])])]),q],64)}var j=r(u,[["render",_],["__file","envInstall.html.vue"]]);export{j as default};
