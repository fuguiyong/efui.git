import{_ as l,r as c,o as t,b as r,f as n,d as e,w as p,F as i,a as s,x as o}from"./app.7c4c2116.js";const u={},b=n("h1",{id:"nginx",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#nginx","aria-hidden":"true"},"#"),s(" Nginx")],-1),m={class:"table-of-contents"},k=s("\u8D1F\u8F7D\u5747\u8861"),d=s("location\u8BE6\u89E3"),v=s("nginx + php-fpm"),h=s("nginx\u624B\u52A8\u91CD\u542F"),x=s("nginx\u4E2Droot\u548Calias\u7684\u533A\u522B"),g=s("proxy_pass"),y=s("\u90E8\u7F72Vue\u9879\u76EE\u57FA\u672C\u914D\u7F6E"),_=o(`<h2 id="\u8D1F\u8F7D\u5747\u8861" tabindex="-1"><a class="header-anchor" href="#\u8D1F\u8F7D\u5747\u8861" aria-hidden="true">#</a> \u8D1F\u8F7D\u5747\u8861</h2><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code>\u57FA\u672C\u5199\u6CD5
<span class="token directive"><span class="token keyword">upstream</span> mysvr</span> <span class="token punctuation">{</span> 
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.121:3333</span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.122:3333</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token directive"><span class="token keyword">server</span></span> <span class="token punctuation">{</span>
    ....
    <span class="token directive"><span class="token keyword">location</span>  ~*^.+$</span> <span class="token punctuation">{</span>         
        <span class="token directive"><span class="token keyword">proxy_pass</span>  http://mysvr</span><span class="token punctuation">;</span>  <span class="token comment">#\u8BF7\u6C42\u8F6C\u5411mysvr \u5B9A\u4E49\u7684\u670D\u52A1\u5668\u5217\u8868         </span>
    <span class="token punctuation">}</span>
<span class="token punctuation">}</span>


<span class="token directive"><span class="token keyword">1\u3001\u70ED\u5907\uFF1A\u5982\u679C\u4F60\u67092\u53F0\u670D\u52A1\u5668\uFF0C\u5F53\u4E00\u53F0\u670D\u52A1\u5668\u53D1\u751F\u4E8B\u6545\u65F6\uFF0C\u624D\u542F\u7528\u7B2C\u4E8C\u53F0\u670D\u52A1\u5668\u7ED9\u63D0\u4F9B\u670D\u52A1\u3002\u670D\u52A1\u5668\u5904\u7406\u8BF7\u6C42\u7684\u987A\u5E8F\uFF1AAAAAAA\u7A81\u7136A\u6302\u5566\uFF0CBBBBBBBBBBBBBB.....</span>
upstream mysvr</span> <span class="token punctuation">{</span> 
    <span class="token directive"><span class="token keyword">server</span> 127.0.0.1:7878</span><span class="token punctuation">;</span> 
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.121:3333 backup</span><span class="token punctuation">;</span>  <span class="token comment">#\u70ED\u5907     </span>
<span class="token punctuation">}</span>

<span class="token directive"><span class="token keyword">2\u3001\u8F6E\u8BE2\uFF1Anginx\u9ED8\u8BA4\u5C31\u662F\u8F6E\u8BE2\u5176\u6743\u91CD\u90FD\u9ED8\u8BA4\u4E3A1\uFF0C\u670D\u52A1\u5668\u5904\u7406\u8BF7\u6C42\u7684\u987A\u5E8F\uFF1AABABABABAB....</span>
upstream mysvr</span> <span class="token punctuation">{</span> 
    <span class="token directive"><span class="token keyword">server</span> 127.0.0.1:7878</span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.121:3333</span><span class="token punctuation">;</span>       
<span class="token punctuation">}</span>

<span class="token directive"><span class="token keyword">3\u3001\u52A0\u6743\u8F6E\u8BE2\uFF1A\u8DDF\u636E\u914D\u7F6E\u7684\u6743\u91CD\u7684\u5927\u5C0F\u800C\u5206\u53D1\u7ED9\u4E0D\u540C\u670D\u52A1\u5668\u4E0D\u540C\u6570\u91CF\u7684\u8BF7\u6C42\u3002\u5982\u679C\u4E0D\u8BBE\u7F6E\uFF0C\u5219\u9ED8\u8BA4\u4E3A1\u3002\u4E0B\u9762\u670D\u52A1\u5668\u7684\u8BF7\u6C42\u987A\u5E8F\u4E3A\uFF1AABBABBABBABBABB....</span>
upstream mysvr</span> <span class="token punctuation">{</span> 
    <span class="token directive"><span class="token keyword">server</span> 127.0.0.1:7878 weight=1</span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.121:3333 weight=2</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token directive"><span class="token keyword">4\u3001ip_hash:nginx\u4F1A\u8BA9\u76F8\u540C\u7684\u5BA2\u6237\u7AEFip\u8BF7\u6C42\u76F8\u540C\u7684\u670D\u52A1\u5668\u3002</span>
upstream mysvr</span> <span class="token punctuation">{</span> 
    <span class="token directive"><span class="token keyword">server</span> 127.0.0.1:7878</span><span class="token punctuation">;</span> 
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.121:3333</span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">ip_hash</span></span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token directive"><span class="token keyword">5\u3001\u5173\u4E8Enginx\u8D1F\u8F7D\u5747\u8861\u914D\u7F6E\u7684\u51E0\u4E2A\u72B6\u6001\u53C2\u6570\u8BB2\u89E3\u3002</span>

down\uFF0C\u8868\u793A\u5F53\u524D\u7684server\u6682\u65F6\u4E0D\u53C2\u4E0E\u8D1F\u8F7D\u5747\u8861\u3002

backup\uFF0C\u9884\u7559\u7684\u5907\u4EFD\u673A\u5668\u3002\u5F53\u5176\u4ED6\u6240\u6709\u7684\u975Ebackup\u673A\u5668\u51FA\u73B0\u6545\u969C\u6216\u8005\u5FD9\u7684\u65F6\u5019\uFF0C\u624D\u4F1A\u8BF7\u6C42backup\u673A\u5668\uFF0C\u56E0\u6B64\u8FD9\u53F0\u673A\u5668\u7684\u538B\u529B\u6700\u8F7B\u3002

max_fails\uFF0C\u5141\u8BB8\u8BF7\u6C42\u5931\u8D25\u7684\u6B21\u6570\uFF0C\u9ED8\u8BA4\u4E3A1\u3002\u5F53\u8D85\u8FC7\u6700\u5927\u6B21\u6570\u65F6\uFF0C\u8FD4\u56DEproxy_next_upstream \u6A21\u5757\u5B9A\u4E49\u7684\u9519\u8BEF\u3002

fail_timeout\uFF0C\u5728\u7ECF\u5386\u4E86max_fails\u6B21\u5931\u8D25\u540E\uFF0C\u6682\u505C\u670D\u52A1\u7684\u65F6\u95F4\u3002max_fails\u53EF\u4EE5\u548Cfail_timeout\u4E00\u8D77\u4F7F\u7528\u3002

upstream mysvr</span> <span class="token punctuation">{</span> 
    <span class="token directive"><span class="token keyword">server</span> 127.0.0.1:7878 weight=2 max_fails=2 fail_timeout=2</span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">server</span> 192.168.10.121:3333 weight=1 max_fails=2 fail_timeout=1</span><span class="token punctuation">;</span>    
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br></div></div><h2 id="location\u8BE6\u89E3" tabindex="-1"><a class="header-anchor" href="#location\u8BE6\u89E3" aria-hidden="true">#</a> location\u8BE6\u89E3</h2><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code><span class="token directive"><span class="token keyword">1\u3001\u6CA1\u6709\u4FEE\u9970\u7B26</span> \u8868\u793A\uFF1A\u5FC5\u987B\u4EE5\u6307\u5B9A\u6A21\u5F0F\u5F00\u59CB\uFF0C\u5982\uFF1A

\u590D\u5236\u4EE3\u7801
server</span> <span class="token punctuation">{</span>
\u3000\u3000<span class="token directive"><span class="token keyword">server_name</span> baidu.com</span><span class="token punctuation">;</span>
\u3000\u3000<span class="token directive"><span class="token keyword">location</span> /abc</span> <span class="token punctuation">{</span>
\u3000\u3000\u3000\u3000\u2026\u2026
\u3000\u3000<span class="token punctuation">}</span>
<span class="token punctuation">}</span>

\u90A3\u4E48\uFF0C\u5982\u4E0B\u662F\u5BF9\u7684\uFF1A
<span class="token directive"><span class="token keyword">http://baidu.com/abc</span>
http://baidu.com/abc?p1
http://baidu.com/abc/
http://baidu.com/abcde

2\u3001=\u8868\u793A\uFF1A\u5FC5\u987B\u4E0E\u6307\u5B9A\u7684\u6A21\u5F0F\u7CBE\u786E\u5339\u914D

\u590D\u5236\u4EE3\u7801
server</span> <span class="token punctuation">{</span>
<span class="token directive"><span class="token keyword">server_name</span> sish
\u3000\u3000location = /abc</span> <span class="token punctuation">{</span>
\u3000\u3000\u3000\u3000\u2026\u2026
\u3000\u3000<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
\u90A3\u4E48\uFF0C\u5982\u4E0B\u662F\u5BF9\u7684\uFF1A
<span class="token directive"><span class="token keyword">http://baidu.com/abc</span>
http://baidu.com/abc?p1
\u5982\u4E0B\u662F\u9519\u7684\uFF1A
http://baidu.com/abc/
http://baidu.com/abcde

3\u3001~ \u8868\u793A\uFF1A\u6307\u5B9A\u7684\u6B63\u5219\u8868\u8FBE\u5F0F\u8981\u533A\u5206\u5927\u5C0F\u5199

\u590D\u5236\u4EE3\u7801
server</span> <span class="token punctuation">{</span>
<span class="token directive"><span class="token keyword">server_name</span> baidu.com</span><span class="token punctuation">;</span>
\u3000\u3000<span class="token directive"><span class="token keyword">location</span> ~ ^/abc$</span> <span class="token punctuation">{</span>
\u3000\u3000\u3000\u3000\u2026\u2026
\u3000\u3000<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
\u90A3\u4E48\uFF0C\u5982\u4E0B\u662F\u5BF9\u7684\uFF1A
<span class="token directive"><span class="token keyword">http://baidu.com/abc</span>
http://baidu.com/abc?p1=11&amp;p2=22
\u5982\u4E0B\u662F\u9519\u7684\uFF1A
http://baidu.com/ABC
http://baidu.com/abc/
http://baidu.com/abcde


4\u3001~* \u8868\u793A\uFF1A\u6307\u5B9A\u7684\u6B63\u5219\u8868\u8FBE\u5F0F\u4E0D\u533A\u5206\u5927\u5C0F\u5199

\u590D\u5236\u4EE3\u7801
server</span> <span class="token punctuation">{</span>
<span class="token directive"><span class="token keyword">server_name</span> baidu.com</span><span class="token punctuation">;</span>
<span class="token directive"><span class="token keyword">location</span> ~* ^/abc$</span> <span class="token punctuation">{</span>
\u3000\u3000\u3000\u3000\u2026\u2026
\u3000\u3000<span class="token punctuation">}</span>
<span class="token punctuation">}</span>
\u90A3\u4E48\uFF0C\u5982\u4E0B\u662F\u5BF9\u7684\uFF1A
http://baidu.com/abc
http://baidu..com/ABC
http://baidu..com/abc?p1=11&amp;p2=22
\u5982\u4E0B\u662F\u9519\u7684\uFF1A
http://baidu..com/abc/
http://baidu..com/abcde


5\u3001^~ \u7C7B\u4F3C\u4E8E\u65E0\u4FEE\u9970\u7B26\u7684\u884C\u4E3A\uFF0C\u4E5F\u662F\u4EE5\u6307\u5B9A\u6A21\u5F0F\u5F00\u59CB\uFF0C\u4E0D\u540C\u7684\u662F\uFF0C\u5982\u679C\u6A21\u5F0F\u5339\u914D\uFF0C
\u90A3\u4E48\u5C31\u505C\u6B62\u641C\u7D22\u5176\u4ED6\u6A21\u5F0F\u4E86\u3002

6\u3001@ \uFF1A\u5B9A\u4E49\u547D\u540Dlocation\u533A\u6BB5\uFF0C\u8FD9\u4E9B\u533A\u6BB5\u5BA2\u6237\u6BB5\u4E0D\u80FD\u8BBF\u95EE\uFF0C\u53EA\u53EF\u4EE5\u7531\u5185\u90E8\u4EA7\u751F\u7684\u8BF7
\u6C42\u6765\u8BBF\u95EE\uFF0C\u5982try_files\u6216error_page\u7B49
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br><span class="line-number">49</span><br><span class="line-number">50</span><br><span class="line-number">51</span><br><span class="line-number">52</span><br><span class="line-number">53</span><br><span class="line-number">54</span><br><span class="line-number">55</span><br><span class="line-number">56</span><br><span class="line-number">57</span><br><span class="line-number">58</span><br><span class="line-number">59</span><br><span class="line-number">60</span><br><span class="line-number">61</span><br><span class="line-number">62</span><br><span class="line-number">63</span><br><span class="line-number">64</span><br><span class="line-number">65</span><br><span class="line-number">66</span><br><span class="line-number">67</span><br><span class="line-number">68</span><br><span class="line-number">69</span><br><span class="line-number">70</span><br><span class="line-number">71</span><br><span class="line-number">72</span><br><span class="line-number">73</span><br></div></div><h2 id="nginx-php-fpm" tabindex="-1"><a class="header-anchor" href="#nginx-php-fpm" aria-hidden="true">#</a> nginx + php-fpm</h2><p>\u63D0\u524D\u5B89\u88C5\u597Dnginx\u4E0Ephp-fpm\uFF0Cnginx\u914D\u7F6E\u5982\u4E0B\uFF1A</p><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code><span class="token directive"><span class="token keyword">server</span></span><span class="token punctuation">{</span>
    <span class="token directive"><span class="token keyword">listen</span> <span class="token number">9099</span></span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">server_name</span> 127.0.0.1</span><span class="token punctuation">;</span>

    <span class="token directive"><span class="token keyword">location</span>  /</span> <span class="token punctuation">{</span>
        <span class="token directive"><span class="token keyword">root</span> /var/www/html/moodle</span><span class="token punctuation">;</span>
        <span class="token directive"><span class="token keyword">index</span>   index.php</span><span class="token punctuation">;</span>
     <span class="token punctuation">}</span> 

	<span class="token directive"><span class="token keyword">location</span> ~ ^(.+\\.php)(.*)$</span> <span class="token punctuation">{</span>
		<span class="token directive"><span class="token keyword">root</span> /var/www/html/moodle</span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">fastcgi_split_path_info</span>  ^(.+\\.php)(.*)$</span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">fastcgi_index</span>            index.php</span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">fastcgi_pass</span>             127.0.0.1:9000</span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">include</span> /etc/nginx/mime.types</span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">include</span>                  fastcgi_params</span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">fastcgi_param</span>   PATH_INFO       <span class="token variable">$fastcgi_path_info</span></span><span class="token punctuation">;</span>
		<span class="token directive"><span class="token keyword">fastcgi_param</span>   SCRIPT_FILENAME <span class="token variable">$document_root</span><span class="token variable">$fastcgi_script_name</span></span><span class="token punctuation">;</span>
	<span class="token punctuation">}</span>

<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br></div></div><h2 id="nginx\u624B\u52A8\u91CD\u542F" tabindex="-1"><a class="header-anchor" href="#nginx\u624B\u52A8\u91CD\u542F" aria-hidden="true">#</a> nginx\u624B\u52A8\u91CD\u542F</h2><ol><li>\u8FDB\u5165nginx\u5B89\u88C5\u76EE\u5F55sbin\u4E0B</li><li>./nginx -t \u68C0\u67E5\u914D\u7F6E\u662F\u5426\u6B63\u786E</li><li>./nginx -s reload \u91CD\u542F</li></ol><h2 id="nginx\u4E2Droot\u548Calias\u7684\u533A\u522B" tabindex="-1"><a class="header-anchor" href="#nginx\u4E2Droot\u548Calias\u7684\u533A\u522B" aria-hidden="true">#</a> nginx\u4E2Droot\u548Calias\u7684\u533A\u522B</h2><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code><span class="token directive"><span class="token keyword">1.</span> \u4F7F\u7528alias\u65F6\uFF0C\u76EE\u5F55\u540D\u540E\u9762\u4E00\u5B9A\u8981\u52A0<span class="token string">&quot;/&quot;</span>\u3002
2. alias\u76F8\u5F53\u4E8E\u7EDD\u5BF9\u8DEF\u5F84\uFF0Croot\u76F8\u5F53\u4E8E\u76F8\u5BF9\u8DEF\u5F84\u3002
3. alias\u5728\u4F7F\u7528\u6B63\u5219\u5339\u914D\u65F6\uFF0C\u5FC5\u987B\u6355\u6349\u8981\u5339\u914D\u7684\u5185\u5BB9\u5E76\u5728\u6307\u5B9A\u7684\u5185\u5BB9\u5904\u4F7F\u7528\u3002
4. alias\u53EA\u80FD\u4F4D\u4E8Elocation\u5757\u4E2D\u3002\uFF08root\u53EF\u4EE5\u4E0D\u653E\u5728location\u4E2D\uFF09

\u793A\u4F8B
alias

location /i/</span><span class="token punctuation">{</span>
    alias /usr/local/nginx/html/admin/\uFF1B
<span class="token punctuation">}</span>

<span class="token comment">#\u82E5\u6309\u7167\u4E0A\u8FF0\u914D\u7F6E\u7684\u8BDD\uFF0C\u5219\u8BBF\u95EE/i/\u76EE\u5F55\u91CC\u9762\u7684\u6587\u4EF6\u65F6\uFF0Cningx\u4F1A\u81EA\u52A8\u53BB/usr/local/nginx/html/admin\u76EE\u5F55\u627E\u6587\u4EF6\u3002</span>

<span class="token directive"><span class="token keyword">root</span>

location /i/</span> <span class="token punctuation">{</span>
    root /usr/local/nginx/html/admin\uFF1B
<span class="token punctuation">}</span>

<span class="token comment">#\u82E5\u6309\u7167\u8FD9\u79CD\u914D\u7F6E\u7684\u8BDD\uFF0C\u5219\u8BBF\u95EE/i/\u76EE\u5F55\u4E0B\u7684\u6587\u4EF6\u65F6\uFF0Cnginx\u4F1A\u53BB/usr/local/nginx/html/admin/i\u4E0B\u627E\u6587\u4EF6\u3002</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br></div></div><h2 id="proxy-pass" tabindex="-1"><a class="header-anchor" href="#proxy-pass" aria-hidden="true">#</a> proxy_pass</h2><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code>\u5047\u8BBE\u4E0B\u9762\u56DB\u79CD\u60C5\u51B5\u5206\u522B\u7528 <span class="token directive"><span class="token keyword">http://192.168.1.1/proxy/test.html</span> \u8FDB\u884C\u8BBF\u95EE\u3002

\u7B2C\u4E00\u79CD\uFF1A
location /proxy/</span> <span class="token punctuation">{</span>
<span class="token directive"><span class="token keyword">proxy_pass</span> http://127.0.0.1/</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
\u4EE3\u7406\u5230URL\uFF1Ahttp://127.0.0.1/test.html

\u7B2C\u4E8C\u79CD\uFF08\u76F8\u5BF9\u4E8E\u7B2C\u4E00\u79CD\uFF0C\u6700\u540E\u5C11\u4E00\u4E2A / \uFF09
<span class="token directive"><span class="token keyword">location</span> /proxy/</span> <span class="token punctuation">{</span>
<span class="token directive"><span class="token keyword">proxy_pass</span> http://127.0.0.1</span><span class="token punctuation">;</span>
<span class="token punctuation">}</span>
\u4EE3\u7406\u5230URL\uFF1Ahttp://127.0.0.1/proxy/test.html
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br></div></div><h2 id="\u90E8\u7F72vue\u9879\u76EE\u57FA\u672C\u914D\u7F6E" tabindex="-1"><a class="header-anchor" href="#\u90E8\u7F72vue\u9879\u76EE\u57FA\u672C\u914D\u7F6E" aria-hidden="true">#</a> \u90E8\u7F72Vue\u9879\u76EE\u57FA\u672C\u914D\u7F6E</h2><div class="language-nginx ext-nginx line-numbers-mode"><pre class="language-nginx"><code><span class="token directive"><span class="token keyword">server</span></span><span class="token punctuation">{</span>
    <span class="token directive"><span class="token keyword">listen</span> <span class="token number">8080</span></span><span class="token punctuation">;</span>
    <span class="token directive"><span class="token keyword">server_name</span> 127.0.0.1</span><span class="token punctuation">;</span>

    <span class="token directive"><span class="token keyword">location</span>  /</span> <span class="token punctuation">{</span>
        <span class="token directive"><span class="token keyword">alias</span> /var/www/hr/</span><span class="token punctuation">;</span>
        <span class="token directive"><span class="token keyword">index</span>  index.html index.htm</span><span class="token punctuation">;</span>
        <span class="token directive"><span class="token keyword">try_files</span> <span class="token variable">$uri</span> <span class="token variable">$uri</span>/ /index.html</span><span class="token punctuation">;</span>
     <span class="token punctuation">}</span> 

     <span class="token directive"><span class="token keyword">location</span> /api/</span> <span class="token punctuation">{</span>
         <span class="token directive"><span class="token keyword">client_max_body_size</span> <span class="token number">100m</span></span><span class="token punctuation">;</span>
         <span class="token directive"><span class="token keyword">proxy_pass</span> http://127.0.0.1:8083/</span><span class="token punctuation">;</span>        
	<span class="token punctuation">}</span>
        
   <span class="token comment"># \u9759\u6001\u8D44\u6E90\u6258\u7BA1\u914D\u7F6E \u6216\u8005\u4F7F\u7528try_files</span>
   <span class="token comment"># location ~* ^/test/files/.*\\.(doc|docx|txt|pdf|xls|xlsx|png|jpg|jpeg)$ {</span>
	<span class="token comment">#	expires 24h;</span>
	<span class="token comment">#	root /data;</span>
	<span class="token comment">#	}</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br></div></div>`,15);function w(f,B){const a=c("RouterLink");return t(),r(i,null,[b,n("nav",m,[n("ul",null,[n("li",null,[e(a,{to:"#\u8D1F\u8F7D\u5747\u8861"},{default:p(()=>[k]),_:1})]),n("li",null,[e(a,{to:"#location\u8BE6\u89E3"},{default:p(()=>[d]),_:1})]),n("li",null,[e(a,{to:"#nginx-php-fpm"},{default:p(()=>[v]),_:1})]),n("li",null,[e(a,{to:"#nginx\u624B\u52A8\u91CD\u542F"},{default:p(()=>[h]),_:1})]),n("li",null,[e(a,{to:"#nginx\u4E2Droot\u548Calias\u7684\u533A\u522B"},{default:p(()=>[x]),_:1})]),n("li",null,[e(a,{to:"#proxy-pass"},{default:p(()=>[g]),_:1})]),n("li",null,[e(a,{to:"#\u90E8\u7F72vue\u9879\u76EE\u57FA\u672C\u914D\u7F6E"},{default:p(()=>[y]),_:1})])])]),_],64)}var $=l(u,[["render",w],["__file","nginx.html.vue"]]);export{$ as default};
