import{_ as n,x as s}from"./app.7c4c2116.js";const a={},p=s(`<h1 id="fastjson\u53CD\u5E8F\u5217\u5316\u6CDB\u578B" tabindex="-1"><a class="header-anchor" href="#fastjson\u53CD\u5E8F\u5217\u5316\u6CDB\u578B" aria-hidden="true">#</a> fastjson\u53CD\u5E8F\u5217\u5316\u6CDB\u578B</h1><p>\u5BF9\u8FDB\u884C\u6CDB\u578B\u7684\u53CD\u5E8F\u5217\u5316\uFF0C\u4F7F\u7528TypeReference\u53EF\u4EE5\u660E\u786E\u7684\u6307\u5B9A\u53CD\u5E8F\u5217\u5316\u7684\u7C7B\u578B</p><div class="language-java ext-java line-numbers-mode"><pre class="language-java"><code><span class="token comment">// exec</span>
JSON<span class="token punctuation">.</span><span class="token function">parseObject</span><span class="token punctuation">(</span>responseString<span class="token punctuation">,</span> <span class="token keyword">new</span> <span class="token class-name">TypeReference</span><span class="token generics"><span class="token punctuation">&lt;</span><span class="token class-name">TestResponseObjDTO</span><span class="token punctuation">&lt;</span><span class="token class-name">TestResponseRowsDTO</span><span class="token punctuation">&lt;</span><span class="token class-name">TestItemVersionDTO</span><span class="token punctuation">&gt;</span><span class="token punctuation">&gt;</span><span class="token punctuation">&gt;</span></span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
        <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
        
<span class="token comment">// class</span>

<span class="token annotation punctuation">@Data</span>
<span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">TestResponseObjDTO</span><span class="token generics"><span class="token punctuation">&lt;</span><span class="token class-name">T</span><span class="token punctuation">&gt;</span></span> <span class="token punctuation">{</span>

    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span> <span class="token string">&quot;code&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token class-name">TestResponseCodeDTO</span> code<span class="token punctuation">;</span>

    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span> <span class="token string">&quot;bo&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token class-name">T</span> bo<span class="token punctuation">;</span>

    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span> <span class="token string">&quot;other&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token class-name">Object</span> other<span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token annotation punctuation">@Data</span>
<span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">TestResponseRowsDTO</span><span class="token generics"><span class="token punctuation">&lt;</span><span class="token class-name">T</span><span class="token punctuation">&gt;</span></span> <span class="token punctuation">{</span>

    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span><span class="token string">&quot;current&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token keyword">int</span> current<span class="token punctuation">;</span>

    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span><span class="token string">&quot;total&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token keyword">int</span> total<span class="token punctuation">;</span>

    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span> <span class="token string">&quot;rows&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token class-name">List</span><span class="token generics"><span class="token punctuation">&lt;</span><span class="token class-name">T</span><span class="token punctuation">&gt;</span></span> rows<span class="token punctuation">;</span>
<span class="token punctuation">}</span>

<span class="token annotation punctuation">@Data</span>
<span class="token keyword">public</span> <span class="token keyword">class</span> <span class="token class-name">TestItemVersionDTO</span> <span class="token punctuation">{</span>

    <span class="token doc-comment comment">/**
     * \u6D4B\u8BD5\u7F16\u7801
     */</span>
    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span> <span class="token string">&quot;testId&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token class-name">String</span> testId<span class="token punctuation">;</span>

    <span class="token doc-comment comment">/**
     * \u66F4\u65B0\u65F6\u95F4\u5F00\u59CB \u683C\u5F0F:YYYY-MM-DD HH:ss:mm
     */</span>
    <span class="token annotation punctuation">@JSONField</span><span class="token punctuation">(</span>name <span class="token operator">=</span> <span class="token string">&quot;lastupDateFrom&quot;</span><span class="token punctuation">)</span>
    <span class="token keyword">private</span> <span class="token class-name">String</span> lastupDateFrom<span class="token punctuation">;</span>

<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br><span class="line-number">19</span><br><span class="line-number">20</span><br><span class="line-number">21</span><br><span class="line-number">22</span><br><span class="line-number">23</span><br><span class="line-number">24</span><br><span class="line-number">25</span><br><span class="line-number">26</span><br><span class="line-number">27</span><br><span class="line-number">28</span><br><span class="line-number">29</span><br><span class="line-number">30</span><br><span class="line-number">31</span><br><span class="line-number">32</span><br><span class="line-number">33</span><br><span class="line-number">34</span><br><span class="line-number">35</span><br><span class="line-number">36</span><br><span class="line-number">37</span><br><span class="line-number">38</span><br><span class="line-number">39</span><br><span class="line-number">40</span><br><span class="line-number">41</span><br><span class="line-number">42</span><br><span class="line-number">43</span><br><span class="line-number">44</span><br><span class="line-number">45</span><br><span class="line-number">46</span><br><span class="line-number">47</span><br><span class="line-number">48</span><br></div></div>`,3);function e(t,o){return p}var l=n(a,[["render",e],["__file","genericDeserialization.html.vue"]]);export{l as default};
