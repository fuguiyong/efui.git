import{_ as e,r as l,o as t,b as r,f as n,d as o,F as p,a as s,x as c}from"./app.7c4c2116.js";var i="/imgs/nvm.png";const m={},b=n("h1",{id:"nvm",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#nvm","aria-hidden":"true"},"#"),s(" nvm")],-1),u=n("p",null,"nodejs\u7684\u7248\u672C\u7BA1\u7406\u5DE5\u5177\u3002\u4E3A\u4E86\u89E3\u51B3node.js\u5404\u79CD\u7248\u672C\u5B58\u5728\u4E0D\u517C\u5BB9\u73B0\u8C61\u53EF\u4EE5\u901A\u8FC7\u5B83\u53EF\u4EE5\u5B89\u88C5\u548C\u5207\u6362\u4E0D\u540C\u7248\u672C\u7684node.js\u3002",-1),d=n("blockquote",null,[n("p",null,"\u6CE8\u610F\uFF1A\u5148\u5378\u8F7D\u5DF2\u5B89\u88C5\u7684nodejs\uFF08\u73AF\u5883\u53D8\u91CF\u548C\u5B89\u88C5\u5305\u90FD\u9700\u5378\u8F7D\u5E72\u51C0\uFF09")],-1),_=s("\u4E0B\u8F7Dnvm-setup.zip "),v={href:"https://",target:"_blank",rel:"noopener noreferrer"},h=s("https://github.com/coreybutler/nvm-windows/releases"),k=n("li",null,"\u5B89\u88C5\u65F6 nvm\u5730\u5740\u5EFA\u8BAE:D:\\nvm \uFF0C symLink(node)\u5730\u5740\u5EFA\u8BAE:D:\\nodejs",-1),g=n("li",null,"\u5B89\u88C5\u5B8C\u6210\u540E\u914D\u7F6Envm settings.text",-1),f=c(`<div class="language-text ext-text line-numbers-mode"><pre class="language-text"><code>root: D:\\nvm
path: D:\\nodejs
node_mirror: https://npm.taobao.org/mirrors/node/ 
npm_mirror: https://npm.taobao.org/mirrors/npm/
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br></div></div><ol start="4"><li>\u68C0\u67E5NVM\u73AF\u5883\u53D8\u91CF(\u5B89\u88C5\u65F6\u4F1A\u81EA\u52A8\u751F\u4EA7)</li></ol><p><img src="`+i+`" alt="nvm"></p><ol start="5"><li>\u65B0\u5F00cmd\u7A97\u53E3\uFF0C\u8BD5\u8BD5nvm -v</li><li>nvm \u5E38\u7528\u547D\u4EE4</li></ol><div class="language-bash ext-sh line-numbers-mode"><pre class="language-bash"><code>
<span class="token comment"># \u67E5\u770B\u5168\u90E8\u53EF\u5B89\u88C5node\u7248\u672C</span>
nvm list available

<span class="token comment"># \u5B89\u88C5</span>
nvm <span class="token function">install</span> <span class="token number">14.18</span>.0 <span class="token punctuation">[</span><span class="token number">14.18</span><span class="token punctuation">]</span>

<span class="token comment"># \u67E5\u770B\u5B89\u88C5\u7684node\u7248\u672C</span>
nvm <span class="token function">ls</span> 

<span class="token comment"># \u5207\u6362\u7248\u672C</span>
nvm use  <span class="token number">14.18</span>.0

<span class="token comment"># \u663E\u793Anode\u662F\u8FD0\u884C\u572832\u4F4D\u8FD8\u662F64\u4F4D\u6A21\u5F0F</span>
nvm arch

<span class="token comment">#  \u5B89\u88C5\u6700\u65B0\u7A33\u5B9A\u7248\u672Clatest 64\u4F4D</span>
nvm <span class="token function">install</span> latest <span class="token number">64</span>
</code></pre><div class="line-numbers" aria-hidden="true"><span class="line-number">1</span><br><span class="line-number">2</span><br><span class="line-number">3</span><br><span class="line-number">4</span><br><span class="line-number">5</span><br><span class="line-number">6</span><br><span class="line-number">7</span><br><span class="line-number">8</span><br><span class="line-number">9</span><br><span class="line-number">10</span><br><span class="line-number">11</span><br><span class="line-number">12</span><br><span class="line-number">13</span><br><span class="line-number">14</span><br><span class="line-number">15</span><br><span class="line-number">16</span><br><span class="line-number">17</span><br><span class="line-number">18</span><br></div></div><ol start="7"><li>node -v \u68C0\u67E5\u662F\u5426\u6210\u529F</li></ol>`,6);function x(j,N){const a=l("ExternalLinkIcon");return t(),r(p,null,[b,u,d,n("ol",null,[n("li",null,[_,n("a",v,[h,o(a)])]),k,g]),f],64)}var D=e(m,[["render",x],["__file","nvm.html.vue"]]);export{D as default};
