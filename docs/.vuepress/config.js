const {
	blogsSidebars,
	componentsSidebars,
	guideSidebars
} = require('./config/sidebar.js');

const path = require('path');

// const {
// 	viteBundler
// } = require('@vuepress/bundler-vite')

module.exports = {
	// 站点配置
	lang: 'zh-CN',
	title: 'EFUI',
	description: ' A Vue3 Components Library',

	// 主题和它的配置
	theme: '@vuepress/theme-default',

	// markdown: {
	// 	extractHeaders: {
	// 		level: [2, 3]
	// 	}
	// },

	themeConfig: {
		darkMode: false,
		// Public 文件路径
		logo: '/imgs/head.jpg',
		editLink: false,
		repo: 'https://gitee.com/fuguiyong/efui.git',
		docsRepo: 'https://gitee.com/fuguiyong/efui.git',
		navbar: [
			// NavbarItem
			{
				text: '首页',
				link: '/',
			},
			{
				text: '指南',
				link: '/docs/guide/introduce/introduce',
			},
			{
				text: '组件',
				link: '/docs/components/basic/button',
			},
			{
				text: 'Ef-Vue3-Admin',
				link: '/docs/other/ef-vue3-admin',
			},
			{
				text: 'Blogs',
				link: '/docs/blogs/frontend/optimizeHome',
			},
		],
		sidebarDepth: 0,
		sidebar: { // SidebarItem
			'/docs/guide/': guideSidebars,
			'/docs/components/': componentsSidebars,
			'/docs/blogs/': blogsSidebars
		}
	},
	// 插件配置
	plugins: [
		// '@vuepress/back-to-top',
		[
			require('vuepress-plugin-demo-container-vue3'),
			{
				componentsDir: path.resolve(__dirname, './../examples')
			}
		]
	],
	// webpack 配置
	// configureWebpack: {
	// 	resolve: {
	// 		alias: {
	// 			'@emmafgy/efui': path.resolve(__dirname, '../../lib/EFUI.es.js')
	// 		}
	// 	}
	// }
	// alias: {
	// 	'@emmafgy/efui': path.resolve(__dirname, '../../lib/EFUI.es.js')
	// },
	// bundler: viteBundler({
	// 	viteOptions: {},
	// 	vuePluginOptions: {},
	// }),
}
