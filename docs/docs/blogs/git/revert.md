# git 回滚

## git reset

reset 命令的原理是根据 commitId 来恢复版本。因为每次提交都会生成一个 commitId，所以说 reset 可以帮你恢复到历史的任何一个版本

> 这里的版本和提交是一个意思，一个 commitId 就是一个版本

reset 命令格式如下：

```
git reset [option] [commitId]
用 git log 命令查看提交记录，可以看到 commitId 值
```

比如，要撤回到某一次提交

```
git reset --hard cc7b5be
```

option :

- --hard：撤销 commit，撤销 add，删除工作区改动代码
- --mixed：默认参数。撤销 commit，撤销 add，还原工作区改动代码
- --soft：撤销 commit，不撤销 add，还原工作区改动代码

> 注意 --hard，使用这个参数恢复会删除工作区代码。也就是说，如果你的项目中有未提交的代码，使用该参数会直接删除掉，不可恢复，慎重啊！

恢复到上一次提交的快捷方式：

```
 git reset --soft HEAD^
```

示例：

```
# 1. 回退到上次提交
$ git reset HEAD^
# 2. 修改代码...
...
# 3. 加入暂存
$ git add .
# 4. 重新提交
$ git commit -m 'fix: ***'
```

## git revert

revert 与 reset 的作用一样，都是恢复版本，但是它们两的实现方式不同。

简单来说，reset 直接恢复到上一个提交，工作区代码自然也是上一个提交的代码；而 revert 是新增一个提交，但是这个提交是使用上一个提交的代码。

因此，它们两恢复后的代码是一致的，区别是一个新增提交（revert），一个回退提交（reset）。

正因为 revert 永远是在新增提交，因此本地仓库版本永远不可能落后于远程仓库，可以直接推送到远程仓库，

示例:

```
git revert -n [commitId]
```