# git 分支

1. 查看本地分支：git branch
2. 查看远程分支：git branch -r
3. 查看所有分支：git branch -a
4. 新建本地分支：git branch 本地分支名
5. 新建本地分支并切换到该分支：git checkout -b 本地分支名
6. 切换分支：git checkout 分支名
7. 将本地新建的分支与远程分支相关联（在当前分支下输入以下命令）：git branch -u origin/分支名
8. 撤销本地分支与远程分支的关系： git branch --unset-upstream
9. 删除本地分支：git branch -d XXX)
10. 查看本地分支与远程分支关联关系： git branch -vv