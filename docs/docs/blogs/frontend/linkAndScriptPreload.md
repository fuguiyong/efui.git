# link script标签预加载
1. 没有 async/defer 属性的script标签 js 会阻塞 html 解析
2. async 属性的script标签 js 会异步加载并且执行
3. defer 属性的script标签 js 会异步预加载，等待 html 解析完成在执行
4. 对于浏览器没有预加载的资源可以通过设置link标签 rel='preload' 和 as='xxx' 或者 rel='prefetch'强制预加载，
```
   字体文件需要添加 crossorigin="anonymous"。注意 rel='preload'或者rel='prefetch'只是下载了文件，并没有执行。
   另外因为提前下载会消耗 TCP 连接数，请考虑同时连接数，不能所有资源文件全部提前下载，只考虑必要的资源。
   preload主要用于预加载当前页面需要的资源；（遇到link标签就开始预加载）
   而prefetch主要用于加载将来页面肯能需要的资源；（空闲时间加载）
```