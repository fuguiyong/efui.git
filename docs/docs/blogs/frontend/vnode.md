# VNode转换组件
场景：在template中使用组件直接渲染VNode<br/>
vue2示例:
```js
Vue.component("v-nodes", {
  functional: true,
  render: (h, ctx) => ctx.props.vnodes
});
// 使用实例
<v-nodes :vnodes="$scopedSlots.title()"></v-nodes>
```
vue3示例:
```js
import {
	h
} from 'vue';

export default {
	props: {
		vnodes: {
			type: Object,
			default: null
		}
	},
	setup(props, {
		attrs,
		slots,
		emit
	}) {
		return () => h(props.vnodes[0], {}, null);
	}
}
```