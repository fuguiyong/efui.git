# webpack 构建时增加自定义文件
场景：增加自定义文件到构建指定的目录 
<br/>
原理是使用copy-webpack-plugin插件实现，详见下方代码：
```js
const CopyPlugin = require('copy-webpack-plugin');
module.exports = {
	configureWebpack:{
		plugins:[
		 	new CopyPlugin([
				// 增加package.json到构建输出目录
		 	  { from: 'package.json', to: '.' },
		     ])
		]
	}
}
```
