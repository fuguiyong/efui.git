# nvm
nodejs的版本管理工具。为了解决node.js各种版本存在不兼容现象可以通过它可以安装和切换不同版本的node.js。

> 注意：先卸载已安装的nodejs（环境变量和安装包都需卸载干净）

1. 下载nvm-setup.zip [https://github.com/coreybutler/nvm-windows/releases](https://)
2. 安装时 nvm地址建议:D:\nvm ， symLink(node)地址建议:D:\nodejs
3. 安装完成后配置nvm settings.text

```text
root: D:\nvm
path: D:\nodejs
node_mirror: https://npm.taobao.org/mirrors/node/ 
npm_mirror: https://npm.taobao.org/mirrors/npm/
```

4. 检查NVM环境变量(安装时会自动生产)

![nvm](/imgs/nvm.png)


5. 新开cmd窗口，试试nvm -v
6. nvm 常用命令

```sh

# 查看全部可安装node版本
nvm list available

# 安装
nvm install 14.18.0 [14.18]

# 查看安装的node版本
nvm ls 

# 切换版本
nvm use  14.18.0

# 显示node是运行在32位还是64位模式
nvm arch

#  安装最新稳定版本latest 64位
nvm install latest 64
```

7. node -v 检查是否成功
