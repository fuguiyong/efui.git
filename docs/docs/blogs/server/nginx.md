# Nginx

[[toc]]

## 负载均衡

```nginx
基本写法
upstream mysvr { 
    server 192.168.10.121:3333;
    server 192.168.10.122:3333;
}
server {
    ....
    location  ~*^.+$ {         
        proxy_pass  http://mysvr;  #请求转向mysvr 定义的服务器列表         
    }
}


1、热备：如果你有2台服务器，当一台服务器发生事故时，才启用第二台服务器给提供服务。服务器处理请求的顺序：AAAAAA突然A挂啦，BBBBBBBBBBBBBB.....
upstream mysvr { 
    server 127.0.0.1:7878; 
    server 192.168.10.121:3333 backup;  #热备     
}

2、轮询：nginx默认就是轮询其权重都默认为1，服务器处理请求的顺序：ABABABABAB....
upstream mysvr { 
    server 127.0.0.1:7878;
    server 192.168.10.121:3333;       
}

3、加权轮询：跟据配置的权重的大小而分发给不同服务器不同数量的请求。如果不设置，则默认为1。下面服务器的请求顺序为：ABBABBABBABBABB....
upstream mysvr { 
    server 127.0.0.1:7878 weight=1;
    server 192.168.10.121:3333 weight=2;
}

4、ip_hash:nginx会让相同的客户端ip请求相同的服务器。
upstream mysvr { 
    server 127.0.0.1:7878; 
    server 192.168.10.121:3333;
    ip_hash;
}
5、关于nginx负载均衡配置的几个状态参数讲解。

down，表示当前的server暂时不参与负载均衡。

backup，预留的备份机器。当其他所有的非backup机器出现故障或者忙的时候，才会请求backup机器，因此这台机器的压力最轻。

max_fails，允许请求失败的次数，默认为1。当超过最大次数时，返回proxy_next_upstream 模块定义的错误。

fail_timeout，在经历了max_fails次失败后，暂停服务的时间。max_fails可以和fail_timeout一起使用。

upstream mysvr { 
    server 127.0.0.1:7878 weight=2 max_fails=2 fail_timeout=2;
    server 192.168.10.121:3333 weight=1 max_fails=2 fail_timeout=1;    
}
```


## location详解

```nginx
1、没有修饰符 表示：必须以指定模式开始，如：

复制代码
server {
　　server_name baidu.com;
　　location /abc {
　　　　……
　　}
}

那么，如下是对的：
http://baidu.com/abc
http://baidu.com/abc?p1
http://baidu.com/abc/
http://baidu.com/abcde

2、=表示：必须与指定的模式精确匹配

复制代码
server {
server_name sish
　　location = /abc {
　　　　……
　　}
}
那么，如下是对的：
http://baidu.com/abc
http://baidu.com/abc?p1
如下是错的：
http://baidu.com/abc/
http://baidu.com/abcde

3、~ 表示：指定的正则表达式要区分大小写

复制代码
server {
server_name baidu.com;
　　location ~ ^/abc$ {
　　　　……
　　}
}
那么，如下是对的：
http://baidu.com/abc
http://baidu.com/abc?p1=11&p2=22
如下是错的：
http://baidu.com/ABC
http://baidu.com/abc/
http://baidu.com/abcde


4、~* 表示：指定的正则表达式不区分大小写

复制代码
server {
server_name baidu.com;
location ~* ^/abc$ {
　　　　……
　　}
}
那么，如下是对的：
http://baidu.com/abc
http://baidu..com/ABC
http://baidu..com/abc?p1=11&p2=22
如下是错的：
http://baidu..com/abc/
http://baidu..com/abcde


5、^~ 类似于无修饰符的行为，也是以指定模式开始，不同的是，如果模式匹配，
那么就停止搜索其他模式了。

6、@ ：定义命名location区段，这些区段客户段不能访问，只可以由内部产生的请
求来访问，如try_files或error_page等
```

## nginx + php-fpm
提前安装好nginx与php-fpm，nginx配置如下：
```nginx
server{
    listen 9099;
    server_name 127.0.0.1;

    location  / {
        root /var/www/html/moodle;
        index   index.php;
     } 

	location ~ ^(.+\.php)(.*)$ {
		root /var/www/html/moodle;
		fastcgi_split_path_info  ^(.+\.php)(.*)$;
		fastcgi_index            index.php;
		fastcgi_pass             127.0.0.1:9000;
		include /etc/nginx/mime.types;
		include                  fastcgi_params;
		fastcgi_param   PATH_INFO       $fastcgi_path_info;
		fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
	}

}
```

## nginx手动重启
1. 进入nginx安装目录sbin下
2. ./nginx -t 检查配置是否正确
3. ./nginx -s reload 重启

## nginx中root和alias的区别
```nginx
1. 使用alias时，目录名后面一定要加"/"。
2. alias相当于绝对路径，root相当于相对路径。
3. alias在使用正则匹配时，必须捕捉要匹配的内容并在指定的内容处使用。
4. alias只能位于location块中。（root可以不放在location中）

示例
alias

location /i/{
    alias /usr/local/nginx/html/admin/；
}

#若按照上述配置的话，则访问/i/目录里面的文件时，ningx会自动去/usr/local/nginx/html/admin目录找文件。

root

location /i/ {
    root /usr/local/nginx/html/admin；
}

#若按照这种配置的话，则访问/i/目录下的文件时，nginx会去/usr/local/nginx/html/admin/i下找文件。
```

## proxy_pass
```nginx
假设下面四种情况分别用 http://192.168.1.1/proxy/test.html 进行访问。

第一种：
location /proxy/ {
proxy_pass http://127.0.0.1/;
}
代理到URL：http://127.0.0.1/test.html

第二种（相对于第一种，最后少一个 / ）
location /proxy/ {
proxy_pass http://127.0.0.1;
}
代理到URL：http://127.0.0.1/proxy/test.html
```

## 部署Vue项目基本配置
```nginx
server{
    listen 8080;
    server_name 127.0.0.1;

    location  / {
        alias /var/www/hr/;
        index  index.html index.htm;
        try_files $uri $uri/ /index.html;
     } 

     location /api/ {
         client_max_body_size 100m;
         proxy_pass http://127.0.0.1:8083/;        
	}
        
   # 静态资源托管配置 或者使用try_files
   # location ~* ^/test/files/.*\.(doc|docx|txt|pdf|xls|xlsx|png|jpg|jpeg)$ {
	#	expires 24h;
	#	root /data;
	#	}
}
```