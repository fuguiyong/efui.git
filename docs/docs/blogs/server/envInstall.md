# linux 环境/软件安装

[[toc]]

## Centos安装php7

1. 更换rpm源

```sh
#Centos 5.X：
rpm -Uvh http://mirror.webtatic.com/yum/el5/latest.rpm
#CentOs 6.x：
rpm -Uvh http://mirror.webtatic.com/yum/el6/latest.rpm
#CentOs 7.X：
rpm -Uvh https://mirror.webtatic.com/yum/el7/epel-release.rpm
rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

# 查看可用php版本
yum provides php
```

2. 删除已安装php版本

```sh
yum remove php*
```

3. 安装php7.2

```sh
yum install -y php72w php72w-opcache php72w-xml php72w-mcrypt php72w-gd php72w-devel php72w-mysql php72w-intl php72w-mbstring
```

4. 查看版本

```sh
php -v
```

5. 安装php-fpm

```sh
yum install php72w-fpm
systemctl start php-fpm.service 【启动】
systemctl enable php-fpm.service【开机自启动】
```

## Centos安装nginx

1.添加源

```
sudo rpm -Uvh http://nginx.org/packages/centos/7/noarch/RPMS/nginx-release-centos-7-0.el7.ngx.noarch.rpm
```

2.安装

```
yum install -y nginx
```

3.启动并且设置开机重启

```sh
sudo systemctl start nginx.service
sudo systemctl enable nginx.service
```

## Centos离线安装nginx

1.下载安装包

```
http://nginx.org/packages/centos/7/x86_64/RPMS/
```

2.上传&安装

```
sudo yum install -y nginx-1.16.1-1.el7.ngx.x86_64.rpm
```

3.启动

```
sudo service nginx start
#或者
sudo systemctl start nginx.service
sudo systemctl enable nginx.service
```

## Centos离线安装reids

1.检查环境是否存在gcc-c++

```
rpm -qa | grep gcc-c++ (未输入内容则无)
```

2.下载redis

```
https://download.redis.io/releases/
```

3.上传至服务器

4.解压文件

```
tar -zxvf
```

5.编译

```
进入解压目录
make MALLOC=libc
cd src && make install
```

6.配置密码及后台启动

```
打开redis.conf,将 daemonize 设置为 yes,requirepass “12345”
```

7.启动

```
进入bin（/usr/local/bin）目录
./redis-server [配置文件目录]
```

8.测试

```
进入bin目录
./redis-cli
set name 'Emma'
get name
```

9.关闭

```
进入bin目录
./redis-cli shutdown
```

## Centos离线安装gcc

1.下载gcc安装包(华为云盘存在)

```
gcc下载地址：https://mirrors.tuna.tsinghua.edu.cn/gnu/gcc/
```

2.安装rpm包

```
(上传第一步下载的包上传到指定目录，进入该目录)
rpm -Uvh *.rpm --nodeps --force
```

3.验证

```
gcc -v
g++ -v
```


## Centos离线安装mysql

1.下载

```
https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.7/
```

2.检查是否已安装mariadb，有则先删除，否则会使用mariadb的配置

```
 rpm -qa|grep mariadb
 rpm -e mariadb-libs
 rpm -e mariadb-libs postfix
```

3.创建用户

```
groupadd mysql
useradd  -g mysql mysql
```

4.解压文件（放在/usr/local/mysql57）

```
tar -xvf mysql-5.7.18-linux-glibc2.5-x86_64.tar.gz
mv mysql-5.7.18-linux-glibc2.5-x86_64 mysql57
chown -R mysql:mysql mysql57
```

<br/>

4.创建数据文件目录,日志文件目录，socketfile目录(使用存在的目录不用创建,第五步配置使用)，并给mysql用户分配权限

```
mkdir -p /usr/local/mysql57/data
chown -R mysql:mysql /usr/local/mysql57/data
mkdir -p /usr/local/mysql57/log
chown -R mysql:mysql /usr/local/mysql57/log
chmod -R 755 /usr/local/mysql57
```

<br/>

5.创建配置文件my.cnf(指定配置需要和第四步对上)，一般放在/etc/my.cnf,用于第六步初始化数据库

```

[mysqld]
user                          = mysql
port                          = 3306
character_set_server          = utf8mb4
server_id                     = 10
socket                        = /usr/local/mysql57/mysql.sock
basedir                       = /usr/local/mysql57
datadir                       = /usr/local/mysql57/data
log-error                     = /usr/local/mysql57/log/error.log
pid-file                      = /usr/local/mysql57/mysql.pid
log-bin                       = /usr/local/mysql57/data/mysql-bin

[client]
default-character-set         = utf8mb4
port                          = 3306
socket                        = /usr/local/mysql57/mysql.sock


[mysqld_safe]
#log-error=/var/log/mariadb/mariadb.log
#pid-file=/var/run/mariadb/mariadb.pid

#
# include all files from the config directory
#
#!includedir /etc/my.cnf.d

```

6.初始化数据库（引用第五步的配置文件）

```
/usr/local/mysql57/bin/mysqld --defaults-file=/etc/my.cnf --user=mysql --initialize

初始化后密码放在配置的日志文件（/usr/local/mysql57/log/error.log），或者会直接打印出来。
```

7.启动mysql

```
第一种方式：mysqld_safe
/usr/local/mysql57/bin/mysqld_safe --defaults-file=/etc/my.cnf

第二种方式：添加到服务启动（推荐）
cp /usr/local/mysql57/support-files/mysql.server /etc/init.d/mysql.server
service mysql.server start
```

8.添加环境变量（/etc/profile）

```
export PATH=$PATH:/usr/local/mysql57/bin
source /etc/profile
```

9.登录mysql修改密码

```
mysql -uroot -p
mysql> alter user "root"@"localhost" identified by "Dtsw@sso220530EQ}^z~(r";
mysql> flush privileges;
```



## 安装java环境

1.下载

```
http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
```

2.解压到指定目录

```
tar -zxvf jdk-8u171-linux-x64.tar.gz -C /usr/local/java/
```

3.设置环境变量

```
vim /etc/profile

# 末尾添加
export JAVA_HOME=/usr/local/java/jdk1.8.0_311
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib
export PATH=${JAVA_HOME}/bin:$PATH

source /etc/profile
```

4.添加软连接

```
ln -s /usr/local/java/jdk1.8.0_311/bin/java /usr/bin/java
```

5.检查是否成功

```
java -version
```