# Docker

## 镜像

1. 列出镜像列表

```sh
docker images
```

2. 获取镜像

```sh
docker pull imageName
```

3. 搜索镜像 (https://hub.docker.com/)

```sh
docker search imageName
```

4. 删除镜像

```sh
docker rmi hello-world
```

## 容器

1. 启动容器

```sh
docker run -it -p 8012:8012 keking/kkfileview
docker run --name kkFileV4.0-SNAPSHOT -dit -p 127.0.0.1:8012:8012 -v /etc/kkFile:/opt/kkFileView-4.0.0-SNAPSHOT/config keking/kkfileview:v4.0.0-SNAPSHOT
```

2. 查看运行中的容器

```sh
docker ps 
```

3. 查询所有容器

```sh
docker ps -a
```

4. 后台启动容器(-d)

```sh
docker run --name kkFile -dit -p 8012:8012 -v /etc/kkFile:/opt/kkFileView-4.1.0-SNAPSHOT/config keking/kkfileview:v4.0.0
docker run --name kkFileV4.0-SNAPSHOT -dit -p 127.0.0.1:8012:8012 -v /etc/kkFile:/opt/kkFileView-4.0.0-SNAPSHOT/config keking/kkfileview:v4.0.0-SNAPSHOT
```

5. 停止和重启容器

```sh
docker stop 容器Id
docker restart 容器Id
```

6. 进入容器

```sh
docker exec -it 容器Id /bin/bash
```

7. 导出和导入容器

```sh
 docker export 容器Id > ubuntu.tar
 docker import - 镜像名称
```

8. 删除容器

```sh
docker rm -f 容器Id
```

# [数据卷（容器与宿主机文件映射）](https://www.cnblogs.com/edisonchou/p/docker_volumes_introduction.html)

　在Docker中，要想实现数据的持久化（所谓Docker的数据持久化即数据不随着Container的结束而结束），需要将数据从宿主机挂载到容器中。

1. volumes：Docker管理宿主机文件系统的一部分，默认位于 /var/lib/docker/volumes 目录中

```sh
# docker volume create edc-nginx-vol // 创建一个自定义容器卷
# docker volume ls // 查看所有容器卷
# docker volume inspect edc-nginx-vol // 查看指定容器卷详情信息
# docker run -d -it --name=edc-nginx -p 8800:80 -v edc-nginx-vol:/usr/share/nginx/html nginx // 容器使用数据卷
# docker stop edc-nginx // 暂停容器实例
# docker rm edc-nginx // 移除容器实例
# docker volume rm edc-nginx-vol // 删除自定义数据卷
```

2. bind mounts：意为着可以存储在宿主机系统的任意位置

```sh
docker run -d -it --name=edc-nginx -v /app/wwwroot:/usr/share/nginx/html nginx
　这里指定了将宿主机上的 /app/wwwroot 目录（如果没有报错）挂载到 /usr/share/nginx/html （这个目录是yum安装nginx的默认网页目录）
```

## 查看日志

1. 查看指定时间后的日志，只显示最后100行：

```sh
$ docker logs -f -t --since="2018-02-08" --tail=100 CONTAINER_ID
```

2. 查看最近30分钟的日志:

```sh
$ docker logs --since 30m CONTAINER_ID
```

3. 查看某时间之后的日志：

```sh
$ docker logs -t --since="2018-02-08T13:23:37" CONTAINER_ID
```

4. 查看某时间段日志：

```sh
$ docker logs -t --since="2018-02-08T13:23:37" --until "2018-02-09T12:23:37" CONTAINER_ID
```
