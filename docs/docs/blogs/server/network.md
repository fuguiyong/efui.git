# 网卡配置

## 单网卡配置

1. nmcli dev 或者 nmcli con 或者 ifconfig 查询开启的网卡名称
2. 进入/etc/sysconfig/network-scripts/目录 找到对应的网卡配置文件（例：ifcfg-eth0），并参照如下示例编辑

```sh
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=no
IPV6_AUTOCONF=no
IPV6_DEFROUTE=no
IPV6_FAILURE_FATAL=no
NAME=ens1f1np1
UUID=02ec3c03-cc86-4435-8e70-e693602dd366
DEVICE=ens1f1np1
ONBOOT=yes
IPADDR=192.18.0.28
NETMASK=255.255.255.0
DNS1=100.0.100.10
GATEWAY=192.18.0.1
```

3. 重启网络

```sh
centos7: service network restart

centos8:
1. nmcli c reload
2. nmcli c up ethX

```

## 网络聚合配置

```sh

1- nmcli device status (nmcli dev)查看网口状态是否连接

1-1 nmcli device connect em1 : 打开需要开启的网口
 
2- nmcli connection add type bond ifname bond0 mode 0
  创建一个逻辑网口，聚合类型为bond，聚合口名称为bond0，模式为1。
（1为主备，我们实施时使用最多是模式0，0为负载分担，bond总共有7种模式

3- nmcli connection add type bond-slave ifname em1 master bond0，
  将真实网卡ens33和ens37作为成员口添加到聚合口bond0

4-修改聚合口配置文件：(主要配置好IP等信息) 
  文件路径：/etc/sysconfig/network-scripts/ifcfg-bond-bond0
  BONDING_OPTS=mode=balance-rr
  TYPE=Bond
  BONDING_MASTER=yes
  PROXY_METHOD=none
  BROWSER_ONLY=no
  BOOTPROTO=none
  DEFROUTE=yes
  IPV4_FAILURE_FATAL=no
  IPV6INIT=no
  IPV6_AUTOCONF=no
  IPV6_DEFROUTE=no
  IPV6_FAILURE_FATAL=no
  IPV6_ADDR_GEN_MODE=stable-privacy
  NAME=bond-bond0
  UUID=22e50880-cc3c-4886-b9a0-a7a929570f73
  DEVICE=bond0
  ONBOOT=yes
  IPADDR=192.18.0.27
  NETMASK=255.255.255.0
  DNS1=100.0.100.10
  GATEWAY=192.18.0.1

5-重启网络
  centos7: service network restart
  centos8:
1. nmcli c reload
2. nmcli c up ethX

其他命令说明
nmcli dev : 查看网路设备连接状态
nmcli con ：查看网络连接状态
ip a | grep bond :查看网络接口状态

```