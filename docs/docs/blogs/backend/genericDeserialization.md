# fastjson反序列化泛型
对进行泛型的反序列化，使用TypeReference可以明确的指定反序列化的类型
```java
// exec
JSON.parseObject(responseString, new TypeReference<TestResponseObjDTO<TestResponseRowsDTO<TestItemVersionDTO>>>() {
        });
        
// class

@Data
public class TestResponseObjDTO<T> {

    @JSONField(name = "code")
    private TestResponseCodeDTO code;

    @JSONField(name = "bo")
    private T bo;

    @JSONField(name = "other")
    private Object other;
}

@Data
public class TestResponseRowsDTO<T> {

    @JSONField(name ="current")
    private int current;

    @JSONField(name ="total")
    private int total;

    @JSONField(name = "rows")
    private List<T> rows;
}

@Data
public class TestItemVersionDTO {

    /**
     * 测试编码
     */
    @JSONField(name = "testId")
    private String testId;

    /**
     * 更新时间开始 格式:YYYY-MM-DD HH:ss:mm
     */
    @JSONField(name = "lastupDateFrom")
    private String lastupDateFrom;

}
```