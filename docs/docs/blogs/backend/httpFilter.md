# HttpFileter 拦截打印请求、响应体

## 场景：
把一个HTTP的请求，响应信息完整的纪录到日志。是一种常见有效的问题排查，BUG重现的手段。
但是流这种东西，有一个特点就是只能读取/写入一次，不能重复。下一次读写，就是一个空的流，为了实现流的重用，就很有必要，把读取和写入的数据缓存起来， 可以在某个地方，再一次的读取。

## 思路
HttpServletRequestWrapper
HttpServletResponseWrapper
这俩就是Request和Response的装饰模式实现。
通过装饰者设计模式，我们可以在Request读取请求body的时候，把读取到的数据复制一份缓存起来，记录日志时使用。同理，也可以把Response响应的数据，先缓存起来，用于记录日志，然后再响应给客户端。

## SpringBoot 示例
```java

/**
 * @author : fuguiyong
 * @date : 2022/6/17 15:46
 */
@Component
@WebFilter(filterName = "httpLogFilter", urlPatterns = "/*")
@Order(-9999)        // 保证最先执行
@Slf4j
public class HttpLogFilter extends HttpFilter {
    @Override
    protected void doFilter(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {

        ContentCachingRequestWrapper cachingRequestWrapper = new ContentCachingRequestWrapper(req);
        ContentCachingResponseWrapper cachingResponseWrapper = new ContentCachingResponseWrapper(res);

        try {
            // 执行请求链
            super.doFilter(cachingRequestWrapper, cachingResponseWrapper, chain);
        } finally {
            String requestUri = req.getRequestURI();        // 请求的
            String queryParam = req.getQueryString();        // 查询参数
            String method = req.getMethod();                // 请求方法
            int status = cachingResponseWrapper.getStatus();// 响应状态码
            // 请求体
            // 转换为字符串，在限制请求体大小的情况下，因为字节数据不完整，这里可能乱码，
            String requestBody = new String(cachingRequestWrapper.getContentAsByteArray(), StandardCharsets.UTF_8);
            // 响应体
            String responseBody = new String(cachingResponseWrapper.getContentAsByteArray(), StandardCharsets.UTF_8);

            log.info("{} {} {} {}", method, requestUri, queryParam, status);
            log.info("{}", requestBody);
            log.info("{}", responseBody);
            // 这一步很重要，把缓存的响应内容，输出到客户端
            cachingResponseWrapper.copyBodyToResponse();
        }


    }
}

```
