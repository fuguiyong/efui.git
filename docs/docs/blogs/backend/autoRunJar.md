# linux 添加jar包服务

1. 在/etc/init.d/新建sh文件

```sh
#!/bin/sh
# chkconfig: 2345 85 15
# description:auto_run

#JAR根位置
JAR_ROOT=/opt/test-server/

#JAR位置
JAR_PATH="$JAR_ROOT"test-sso-server.jar

#LOG位置
LOG_PATH="$JAR_ROOT"test-server-nohup.out

#配置文件
APP_CONFIG_PATH="$JAR_ROOT"application-prod.yml

#开始方法
start() {
    cd $JAR_ROOT
    nohup java -Dfile.encoding=utf-8 -jar $JAR_PATH --spring.config.location=$APP_CONFIG_PATH  >  $LOG_PATH 2>&1 &
   # nohup java -Dfile.encoding=utf-8 -jar -Xms128m -Xmx256m -XX:PermSize=32M -XX:MaxPermSize=64M $JAR_PATH >$LOG_PATH &
    echo "$JAR_PATH start success."
}

#结束方法
stop() {
    kill -9 `ps -ef|grep $JAR_PATH|grep -v grep|grep -v stop|awk '{print $2}'`
    echo "$JAR_PATH stop success."
}

case "$1" in
start)
    start
    ;;
stop)
    stop
    ;;
restart)
    stop
    start
    ;;
*)
    echo "Userage: $0 {start|stop|restart}"
    exit 1
esac
```

2. 给sh文件和jar可执行权限

```sh
chmod +x /etc/init.d/sh文件
chmod +x jar包位置
```

3. 设置开机启动

```sh
添加为系统服务
chkconfig --add sh文件

开机自启动
chkconfig sh文件 on

查看
chkconfig --list

启动
service sh文件 start

停用
service sh文件 stop

重启
service sh文件 restart
```