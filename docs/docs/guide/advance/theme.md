# 自定义主题
EFUI样式使用scss编写，目前也只支持scss自定义主题。

1. 创建自定义主题scss文件，[这里是所有scss变量](https://gitee.com/fuguiyong/efui/blob/master/theme/common/common.scss)

```scss
/* 改变主题色变量 */
$primary-color: #1890ff; // 主题色

// 引入EFUI样式
@import "@emmafgy/efui/theme/src/index.scss";
```

2. 在应用入口引入第一步创建的文件