# 介绍
 1. EFUI主要包含一些使用频率较高的Vue3组件，根据日常开发习惯优化了组件，使用简单。
 2. 组件只保留核心功能，整包体积小巧。
 3. 写EFUI主要目的是为了学习Vue3，组件功能与样式主要参照了[Element-Plus](https://element-plus.org/zh-CN/)。
 4. :tada: 持续开发中 :smirk: 