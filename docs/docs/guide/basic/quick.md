# 快速开始

## 完整引入
如果你对打包后的文件大小不是很在乎（目前也只有400多KB），那么使用完整导入会更方便。
```js
// 完整引入示例
import {createApp} from 'vue'
import App from './App.vue'
// EFUI
import EFUI from '@emmafgy/efui';
import "@emmafgy/efui/lib/theme/index.css";

const app = createApp(App)
app.use(EFUI)
app.mount('#app')
```
## 按需引入
EFUI提供了基于 ES Module 开箱即用的 [Tree Shaking](https://webpack.js.org/guides/tree-shaking/) 功能。<br/>
以下是Button单独使用示例,[这里是全部导出的组件](https://gitee.com/fuguiyong/efui/blob/master/package/index.js)
::: tip
按需引入组件后也必须引入对应的css，[这里是全部打包后的css](https://gitee.com/fuguiyong/efui/tree/master/lib/theme),
但是组件之间可能存在依赖关系（例如Table组件使用了Icon组件，那么单独使用Table组件时也需要引入Icon组件css），<br>
因为组件间依赖关系不好查询，建议直接在入口文件（main.js）引入全部css（import '@emmafgy/efui/lib/theme/index.css'），大约100多kb。
:::
```md

<template>
  <ef-button>Button</ef-button>
</template>
<script setup>
import {EfButton} from '@emmafgy/efui'
import '@emmafgy/efui/lib/theme/button.css'
</script>

```