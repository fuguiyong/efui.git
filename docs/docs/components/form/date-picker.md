# Date-Picker 日期选择器

## 选择某一天
1. 配置type=date（默认值）
2. 可配置width，height定义宽高
::: demo 
date-picker/basic
::: 

## 选择多个
添加multiple属性
::: demo 
date-picker/multiple
::: 

## 有禁用日期
配置disabled-date定义禁用日期，回调参数为当前日期，格式为YYYYMMDD
::: demo 
date-picker/date-disabled
::: 


## 其他时间单位
配置type定义其他时间单位
::: demo 
date-picker/type
::: 

## 选择一段时间
配置type=daterange ， rangeSeparator可配置分隔符，默认“-”
::: demo 
date-picker/date-range
::: 

## 选择月份范围
配置type=monthrange
::: demo 
date-picker/month-range
::: 

## 日期格式
配置labelFormatString格式化显示文字<br/>
配置valueFormatString格式化选中值<br/>
format语法详见[Dayjs Format](https://dayjs.gitee.io/docs/zh-CN/display/format)
::: demo 
date-picker/format
::: 

## 自定义内容
插槽date,month,year分别自定义类型为日期、月、年的内容
::: demo 
date-picker/date-slot
::: 


				
## Date-Picker 配置项 
| 参数				| 说明												| 类型			| 可选值																							| 默认值	|
|----------			|--------------										|----------		|--------------------------------																	|--------	|
| modelValue		| 绑定值，支持v-model								| String,Array	| -																									| -			|
| type				| 类型												| String		| 'year','month','date','datetime','daterange',<br/>'monthrange','datetimerange','time','timerange'	| date		|
| width				| 宽												| String,Number	| -																									| -			|
| height			| 高												| String,Number	| -																									| 40		|
| value-formatString| value 格式化字符串								| String		| 详见[Dayjs Format](https://dayjs.gitee.io/docs/zh-CN/display/format)								| YYYY-MM-DD|
| label-formatString| label 格式化字符串								| String		| 详见[Dayjs Format](https://dayjs.gitee.io/docs/zh-CN/display/format)								| YYYY-MM-DD|
| placeholder		| 占位符											| String		| -																									| -			|
| start-placeholder	| 范围选择时，开始占位符							| String		|																									| -			|
| end-placeholder	| 范围选择时，结束占位符							| String		| -																									| -			|
| disabled-date		| 禁用日期回调函数									| Function		| -																									| -			|
| range-separator	| 范围中间文字分隔符								| String		| -																									| -			|
| multiple			| 是否开启多选,仅支持type = 'year','month','date'	| Boolean		| -																									| false		|
| clearable			| 开启清除											| Boolean		| -																									| false		|

## Date-Picker 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| change	| 选择日期改变	|(val)		|


## Date-Picker 插槽
详见[自定义内容](./date-picker.md#自定义内容)
| 插槽		| 说明			|
|----------	|--------------	|
| date	| 日期插槽		|
| month	| 月份插槽		|
| year	| 年插槽		|

[[toc]]