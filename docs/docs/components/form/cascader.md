# Cascader 级联选择

## 基础用法
1. 配置modelValue，支持v-model
2. 配置options定义级联选择项
3. 配置props定义配置项，详见[props 配置项](./cascader.md#cascader-props-配置项)
::: demo 
cascader/basic
::: 

## 有禁用项
配置数据源（options）disabled=true定义禁用项，disabled属性可通过[props 配置项](./cascader.md#cascader-props-配置项)修改
::: demo 
cascader/disabled-item
::: 

## 禁用
添加disabled属性定义禁用级联选择
::: demo 
cascader/disabled
::: 

## 可清空
添加clearable属性定义可清空级联选择
::: demo 
cascader/clearable
::: 

## 不同触发方式
配置trigger属性定义不同触发方式级联选择，可选项click，hover，以下示例为hover
::: demo 
cascader/trigger
::: 

## 自定义节点内容
添加default插槽自定义节点内容
::: demo 
cascader/custom-option
::: 

## 动态加载
通过[props 配置项](./cascader.md#cascader-props-配置项)开启动态加载，并通过lazyload来设置加载数据源的方法。<br/>
lazyload方法有两个参数，第一个参数node为当前点击的节点，第二个resolve为数据加载完成的回调(必须调用)。<br/>
为了更准确的显示节点的状态，务必对节点数据添加是否为叶子节点的标志位 (默认字段为leaf，可通过props.leaf修改)。
::: demo 
cascader/lazy
::: 


## Cascader 配置项
| 参数		| 说明					| 类型			| 可选值							| 默认值	|
|----------	|--------------			|----------		|--------------------------------	|--------	|
| modelValue| 选中的值，支撑v-model	| Array			| -									| -			|
| options	| 数据源，动态加载不需要	| Array			| -									| -			|
| disabled	| 是否禁用				| Boolean		| -									| false		|
| props		| 配置选项				| String,VNode	| -									| -			|

## Cascader Props 配置项
| 参数			| 说明																					| 类型																					| 可选值							| 默认值	|
|----------		|--------------																			|----------																				|--------------------------------	|--------	|
| expandTrigger	| 次级菜单的展开方式																		| String																				| hover,click						| click		|
| leaf			| 指定选项的叶子节点的标志位为选项对象的某个属性值											| String																				| -									| -			|
| disabled		| 指定选项的禁用为选项对象的某个属性值													| String																				| -									| -			|
| children		| 指定选项的子选项为选项对象的某个属性值													| String																				| -									| -			|
| value			| 指定选项的值为选项对象的某个属性值														| String																				| -									| -			|
| label			| 指定选项标签为选项对象的某个属性值														| String																				| -									| -			|
| lazy			| 是否动态加载子节点，需与 lazyLoad 方法结合使用，详见[动态加载](./cascader.md#动态加载)	| Boolean																				| -									| false		|
| lazyLoad		| 加载动态数据的方法，仅在 lazy 为 true 时有效											| function(node, resolve)，node为当前点击的节点，resolve为数据加载完成的回调(必须调用)	| -									| -			|

## Cascader 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| change	| 选择内容改变	|(val)		|


## Cascader 插槽
| 插槽		| 说明															|
|----------	|--------------													|
| default	| 节点插槽，详见[自定义节点内容](./cascader.md#自定义节点内容)	|

[[toc]]