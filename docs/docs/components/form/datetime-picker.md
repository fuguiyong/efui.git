# Datetime-Picker 日期时间选择器
Datetime-Picker 与 Date-Picker 是同一组件，配置详见[Date-Picker 配置项 ](./date-picker.md#date-picker-配置项)

## 日期和时间
配置type = datetime
::: demo 
date-picker/datetime
::: 

## 日期和时间范围
配置type = datetimerange
::: demo 
date-picker/datetimerange
::: 

## Datetime-Picker 配置项
配置详见[Date-Picker 配置项 ](./date-picker.md#date-picker-配置项)

## Datetime-Picker 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| change	| 选择日期改变	|(val)		|

## Datetime-Picker 插槽
详见[Date-Picker自定义内容](./date-picker.md#自定义内容)
| 插槽		| 说明			|
|----------	|--------------	|
| date	| 日期插槽		|

[[toc]]