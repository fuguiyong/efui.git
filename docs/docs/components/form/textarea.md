# Textarea 文本域

## 基本用法
配置Input组件type=textarea定义基本文本域
::: demo 
input/textarea
::: 


## 自动调整大小
配置autosize对象定义自动调整大小得文本域，autosize.minRows定义最小行数，autosize.maxRows定义最大行数
::: demo 
input/textarea-autosize
::: 

## Textarea 配置项

详见[Input 配置项](./input.md#input-配置项) 

[[toc]]