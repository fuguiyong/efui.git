# Checkbox 多选框

## 基础用法
使用v-model控制选中状态
::: demo 
checkbox/basic
::: 

## 禁用的
配置disabled控制禁用状态
::: demo 
checkbox/basic-disabled
::: 

## 多选框组
使用ef-checkbox-group组件，配置options定义多选框组
::: demo 
checkbox/group
::: 

## 多选框组 - Option 插槽
使用ef-checkbox-group组件，配置default插槽自定义Option
::: demo 
checkbox/group-slot
::: 

## 禁用的多选框组
配置disabled控制多选框组禁用状态
::: demo 
checkbox/group-disabled
::: 

## CheckBox 配置项

| 参数		| 说明					| 类型		| 可选值							| 默认值	|
|----------	|--------------			|----------	|--------------------------------	|--------	|
| dataValue	| 绑定的value			| -			| -									| -			|
| modelValue| 选中状态，支持v-model	| Boolean	| -									| false		|
| disabled	| 禁用状态				| Boolean	| -									| false		|

## CheckBox 事件
| 事件名称	| 说明				| 参数				|
|----------	|--------------		|----				|
| change	| 选中状态变化时触发 |(checked,dataValue)|

## CheckBox Group 配置项

| 参数		| 说明						| 类型		| 可选值							| 默认值	|
|----------	|--------------				|----------	|--------------------------------	|--------	|
| options	| 多选项配置					| Array		| -									| -			|
| modelValue| 选中项的values,支持v-model| Array		| -									| -			|
| disabled	| 禁用状态					| Boolean	| -									| false		|

## CheckBox Group Options Item配置项

| 参数		| 说明			| 类型		| 可选值							| 默认值	|
|----------	|--------------	|----------	|--------------------------------	|--------	|
| value		| value			| -			| -									| -			|
| label		| label			| -			| -									| -			|


## CheckBox Group 事件
| 事件名称	| 说明				| 参数				|
|----------	|--------------		|----				|
| change	| 选中状态变化时触发 |(currentSelectedArray, currentChecked, currentValue)|


[[toc]]