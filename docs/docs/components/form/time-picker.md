# Time-Picker 时间选择器
Time-Picker 与 Date-Picker 是同一组件，配置详见[Date-Picker 配置项 ](./date-picker.md#date-picker-配置项)

## 时间
配置type = time
::: demo 
date-picker/time
::: 

## 时间范围
配置type = timerange
::: demo 
date-picker/timerange
::: 

## Time-Picker 配置项
配置详见[Date-Picker 配置项 ](./date-picker.md#date-picker-配置项)

## Time-Picker 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| change	| 选择时间改变	|(val)		|

[[toc]]