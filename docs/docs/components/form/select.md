# Select 下拉选择
Select组件基于Popper组件开发，[Popper配置项](../feedback/popper.md#popper-配置项) 均可以使用<br/>

## 基础用法
1. 配置v-model
2. 配置max-height，width，height（多选请使用single-row-height控制高度）
3. 添加Select Option
::: demo 
select/basic
::: 

## 有禁用选项
Select Option添加disabled属性
::: demo 
select/disabled-option
::: 

## 禁用
添加disabled属性
::: demo 
select/disabled
::: 

## 可清空
添加clearable属性,对应事件@clear
::: demo 
select/clearable
::: 

## 不同尺寸
配置size属性设置尺寸,可以值large，middle，small，mini
::: demo 
select/size
::: 

## 可过滤
1. 添加filterable属性
2. 配置filterMethod,默认根据label检索，以下代码是filterMethod默认配置
```js
	filterMethod: {
		type: Function,
		default: (inputVal, label) => {
			return label.indexOf(inputVal) > -1;
		}
	}
```
::: demo 
select/filterable
::: 

## 带图标的选项
Select Option配置icon-obj对象，[Icon 配置项](../basic/icon.md#icon-配置项)一致
::: demo 
select/icon
::: 

## 自定义选项
Select Option添加default插槽（label属性失效，过滤函数逻辑不变，也是根据Option label配置）
::: demo 
select/option-slot
::: 

## 选项分组
使用Select Group组件进行选项分组,v-model绑定的值必须为数组
::: demo 
select/group
::: 

## 多选
添加multiple属性定义多选Select
::: demo 
select/multiple
::: 

## 多选-可创建
添加allow-create,filterable两个属性定义可创建Select
::: demo 
select/allow-create
::: 



## Select 配置项 
| 参数				| 说明												| 类型							| 可选值							| 默认值	|
|----------			|--------------										|----------						|--------------------------------	|--------	|
| modelValue		| 选中值，支持v-model								| -								| -									| -			|
| disabled			| 是否禁用											| Boolean						| —								| false		|
| clearable			| 能否清除											| Boolean						| —								| false		|
| size				| 尺寸												| String						| mini,small,middle,large			| —		|
| filterable		| 是否可过滤											| Boolean						| —								| false		|
| filterMethod		| 过滤回调函数										| Function(inputVal,optionLabel)|									| -			|
| multiple			| 是否开启多选										| Boolean						|-									| false		|
| allow-create		| 开启多选时，能否新建Option(务必同时开启filterable)	| Boolean						| —								| false		|
| width				| 宽度												| String，Number				| —								| -			|
| height			| Select触发内容高度									| String，Number				| —								| -			|
| max-height		| Select选中内容最大高度，超出后出现滚动条			| String，Number				| —								| -			|
| single-row-height	| 多选才有效，单行高度								| String，Number				| —								| -			|
| itemHeight		| option高度										| String，Number				| —								| -			|
| placeholder		| 占位符												| String						| —								| -			|


## Select Option配置项 
| 参数		| 说明			| 类型		| 可选值							| 默认值	|
|----------	|--------------	|----------	|--------------------------------	|--------	|
| value		| value			| -			| -									| -			|
| label		| label			| -			| -									| -			|
| disabled	| 是否禁用		| Boolean	| -									| false		|
| icon-obj	| 前缀图标对象	| String	| 详见Icon组件						| —		|


## Select Option Group配置项 
| 参数		| 说明			| 类型		| 可选值							| 默认值	|
|----------	|--------------	|----------	|--------------------------------	|--------	|
| label		| label			| -			| -									| -			|

## Select 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| change	| 选择内容改变	|(val)		|
| clear		| 清除			|-			|

[[toc]]