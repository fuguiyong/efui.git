# Form 表单

## 基础用法
最基础的表单包括各种输入表单项，比如input、select、radio、checkbox等。<br/>
在每一个 form 组件中，你需要一个 form-item 字段作为输入项的容器，用于获取值与验证值。

> 注意form组件中事件需要使用@click.prevent阻止默认事件

::: demo 
form/basic
::: 

## 行内表单
通过设置 inline 属性为 true 可以让表单域变为行内的表单域。<br/>
建议配合item-width属性控制表单列数效果更佳，如下例：2列（item-width = 50%）

::: demo 
form/inline
::: 

## Label对齐方式
通过设置 label-position 属性可以改变表单域标签的位置，可选值为 top、left， 当设为 top 时标签会置于表单域的顶部

::: demo 
form/label-position
::: 

## 表单校验
1. 配置model属性，绑定表单数据对象
2. 配置rules属性传入约定的验证规则 (触发校验时机可通过rule对象trigger属性配置，支持change、blur)
3. 配置form-Item的prop为表单数据对象对应属性路径（嵌套对象prop类型使用数组描绘路径）
<br/>
更多高级用法可参考 [async-validator](https://github.com/yiminghe/async-validator)

::: demo 
form/validate
::: 

## 自定义表单校验
配置rule对象validator(rule, value, cb)属性<br/>
校验通过调用cb()即可，否则调用cb(new Error('错误信息')) <br/>
下例是自定义校验个人简介必须和姓名相等的示例。

::: demo 
form/validate-custom
::: 

## Form 配置项 
| 参数					| 说明					| 类型			| 可选值							| 默认值	|
|----------				|--------------			|----------		|--------------------------------	|--------	|
| model					| 表单数据对象			| Object		| -									| -			|
| rules					| 表单验证规则			| Object		| -									| -			|
| item-width			| FormItem宽度			| String,NUmber	| -									| -			|
| label-position		| label对齐方式			| String		| top,right.left					| right		|
| label-width			| label宽度				|  String,NUmber| -									| -			|
| label-suffix			| label文字后缀			| String		| -									| -			|
| inline				| 开启行内表单			| Boolean		| -									| false		|
| show-message			| 开启校验错误消息		| Boolean		| -									| true		|
| hide-required-asterisk| 隐藏必需红星标记		| Boolean		| -									| false		|
| scroll-to-error		| 校验错误滚动到首个错误| Boolean		| -									| false		|


## Form 方法
| 事件			| 说明			| 回调参数	|
|----------		|--------------	|----------	|
| clearValidate	| 清除校验		|()			|
| validateField	| 校验某个字段	|(prop)		|
| validate		| 校验全部字段	|()			|
| scrollToField	| 滚动到指定字段|(prop)		|


## Form 事件
| 事件		| 说明					| 回调参数				|
|----------	|--------------			|----------				|
| validate	| 任一表单项被校验后触发|(prop, valid, errMsg)	|


## Form 插槽
| 插槽		| 说明			|
|----------	|--------------	|
| default	| 默认插槽		|

## FormItem 配置项 
| 参数			| 说明																												| 类型			| 可选值							| 默认值	|
|----------		|--------------																										|----------		|--------------------------------	|--------	|
| label			| 标签文本																											| String		| -									| -			|
| item-width	| item宽度																											| String,Number	| -									| -			|
| label-width	| label宽度																											| String,Number	| -									| -			|
| prop			| model 的键名。 它可以是一个路径数组(例如 ['a', 'b', 0])。 在定义了 validate、resetFields 的方法时，该属性是必填的	| String,Array	| -									| -			|
| required		| 是否必须																											| Boolean		| -									| false		|
| show-message	| 是否展示错误信息																									| Boolean		| -									| true		|
| rules			| 表单验证规则（与Form rules合并）																					| Object		| -									| -			|

## FormItem 方法
| 事件			| 说明																| 回调参数	|
|----------		|--------------														|----------	|
| clearValidate	| 清除校验															|()			|
| validate		| 校验绑定字段，可传入触发器只校验对应触发时机，传空校验所有触发规则|(trigger)	|



## FormItem 插槽
| 插槽		| 说明					|作用域	|
|----------	|--------------			|---	|
| default	| 默认查询				|-		|
| error		| 验证错误信息的显示内容|{error}|
| label		| 标签位置显示的内容	|{label}|


[[toc]]