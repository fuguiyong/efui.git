# Radio 单选

## 基础用法
要使用 Radio 组件，只需要设置v-model绑定变量， 选中意味着变量的值为相应 Radio value属性的值， value可以是String、Number
::: demo 
radio/basic
::: 

## 禁用状态
添加disabled属性
::: demo 
radio/disabled
::: 

## 带有边框
添加border属性
::: demo 
radio/bordered
::: 

## 单选框组
使用ef-radio-group组件，配置options属性定义选项。
::: demo 
radio/group
::: 

## 单选框组-自定义内容
添加default插槽定义自定义内容
::: demo 
radio/group-slot
::: 

## 垂直方向的单选框组
配置direction=vertical来定义垂直方向的单选框组
::: demo 
radio/group-vertical
::: 


## Radio 配置项 
| 参数		| 说明					| 类型			| 可选值							| 默认值	|
|----------	|--------------			|----------		|--------------------------------	|--------	|
| modelValue| 选中的值，支持v-model	| String,Number	| -									| -			|
| value		| 绑定的值				| String,Number	| -									| -			|
| border	| 是否开启边框			| Boolean		| -									| false		|
| disabled	| 是否禁用				| Boolean		| -									| false		|

## Radio Group 配置项 
| 参数		| 说明																		| 类型			| 可选值							| 默认值	|
|----------	|--------------																|----------		|--------------------------------	|--------	|
| modelValue| 选中的值，支持v-model														| String,Number	| -									| -			|
| options	| 选项配置数组,数组元素为对象，对象value属性=单选值，对象label属性=显示文字	| Array			| -									| -			|
| border	| 是否开启边框																| Boolean		| -									| false		|
| disabled	| 是否禁用																	| Boolean		| -									| false		|
| direction	| 方向																		| String		| vertical , horizontal				| horizontal|

## Radio Group 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| change	| 选择改变		|(val)		|


## Radio Group 插槽
| 插槽		| 说明																	|
|----------	|--------------															|
| default	| 单选插槽，详见[单选框组-自定义内容](./radio.md#单选框组-自定义内容)	|


[[toc]]