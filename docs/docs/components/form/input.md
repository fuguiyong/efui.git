# Input 输入框

## 基础用法
配置v-model双向绑定,配置style属性设置width，height必须inputStyle配置设置，详见[设置原生input样式](./input.md#设置原生input样式) 
::: demo 
input/basic
::: 

## 禁用状态
配置disabled属性定义禁用状态Input
::: demo 
input/disabled
::: 

## 可清除
配置clearable属性定义可清除Input,默认清除后聚焦Input,可配置clearToFocus=false取消清除后聚焦
::: demo 
input/clearable
::: 

## 不同尺寸
配置size定义不同尺寸Input，可选值mini，small，middle，large
::: demo 
input/size
::: 

## 密码框
配置showPassword定义密码框
::: demo 
input/password
::: 

## 前后缀图标
配置suffixIcon定义后缀图标，配置prefixIcon定义前缀图标 <br/>
suffixIconColor设置后缀图标颜色，prefixIconColor设置前缀图标颜色
::: demo 
input/icon
::: 

## 前后缀图标插槽
配置prefix,suffix分别定义前后图标插槽，设置后prefixIcon，suffixIcon属性分别失效
::: demo 
input/icon-slot
::: 

## 前后缀内容插槽
配置append,prepend分别定义前后内容插槽，设置后前后缀图标和插槽都失效
::: demo 
input/append
::: 

## 设置原生input样式
配置inputStyle定义组件内部原生input样式，组件height需要使用该属性设置（因为该组件高度是由内部原生input撑开）
::: demo 
input/input-style
::: 

## 校验值
配置validate对象定义校验规则，详见示例代码
::: demo 
input/validate
::: 



## Input 配置项 
| 参数				| 说明																								| 类型		| 可选值							| 默认值	|
|----------			|--------------																						|----------	|--------------------------------	|--------	|
| type				| 类型																								| String	| text,textarea						| text		|
| disabled			| 是否禁用																							| Boolean	| —								| false		|
| clearable			| 能否清除																							| Boolean	| —								| false		|
| size				| 尺寸																								| String	| mini,small,middle,large			| —		|
| showPassword		| 是否密码框																						| Boolean	| —								| false		|
| suffixIcon		| 前缀图标																							| String	| 详见Icon组件						| -			|
| prefixIcon		| 后缀图标																							| String	|详见Icon组件						| -			|
| suffixIconColor	| 后缀图标颜色																						| String	| —								| -			|
| prefixIconColor	| 前缀图标颜色																						| String	| —								| -			|
| inputStyle		| 组件内部原生input样式																				| Object	| —								| -			|
| validate			| 校验规则对象，validate.validator：校验回调函数，validate.errmsg：校验错误提示						| Object	| —								| -			|
| autosize			| type=textarea生效，配置自动调整大小，autosize.minRows定义最小行数，autosize.maxRows定义最大行数	| Object	| —								| -			|

## Input 事件
| 事件		| 说明			| 回调参数	|
|----------	|--------------	|----------	|
| input		| 输入事件		|(val)		|
| change	| 输入内容改变	|(val)		|
| focus		| 聚焦			|($event)	|
| blur		| 失焦			|($event)	|
| clear		| 清除			|('')		|
| mouseleave| 鼠标离开		|($event)	|
| mouseenter| 鼠标移入		|($event)	|
| keydown	| 按键按下		|($event)	|


## Input 方法
| 方法		| 说明			|
|----------	|--------------	|
| blur		| 失焦			|
| select	| 选择输入框内容|
| focus		| 聚焦			|

## Input 插槽
| 插槽		| 说明			|
|----------	|--------------	|
| prefix	| 前缀图标		|
| suffix	| 后缀图标		|
| append	| 前缀内容		|
| prepend	| 后缀内容		|

[[toc]]