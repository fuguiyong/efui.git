# Table 表格

## 基础用法
1. 配置rowKey：每条数据的标识，支持函数详见配置说明<br/>
2. 配置columns：列配置<br/>
3. 配置dataSource：数据列表
4. 表格宽高可通过style标签直接配置，或者width、height属性配置，具体px、百分比都支持
5. 以下示例列配置均使用EFUI.EfUtil.genColumnsV2方法配置，详见[genColumnsV2](./table.md#efui-efutil-gencolumnsv2) 
6. 若固定头、列出现列未对齐情况，建议加一空白列撑开（或者留一列不设置宽度）
::: demo
table/basic
::: 

## 斑马纹表格
配置stripe=true定义斑马纹表格
::: demo
table/stripe
::: 

## 带边框表格
配置bordered=true带边框表格
::: demo
table/bordered
::: 

## 不同尺寸表格
配置size设置不同尺寸表格，可选值middle,small
::: demo
table/size
::: 

## 固定表头
配置fixedHeader=true设置固定表头
::: demo
table/fixedHeader
::: 

## 固定列
配置列属性fixed定义固定列
::: demo
table/fixedColumns
::: 

## 固定头列
参考固定头、固定列配置
::: demo
table/fixedHeaderAndColumns
::: 

## 多选
配置selectedConfig来设置表格多选，具体配置详见[selectedConfig配置项](./table.md#selectedconfig-配置项) 
::: demo
table/checkbox
::: 

## 单选
配置和多选一致，新增selectedConfig.radio = true 即可开启单选
::: demo
table/radio
::: 

## 列排序
配置列属性sort（asc=正序 desc=倒叙 unset=无），配合@sortChange事件使用，change事件不会对表格数据进行前端排序
::: demo
table/sort
::: 

## 自定义列（列内容插槽）
1. 配置列属性customRowRender来定义列内容的插槽名称
2. 然Table组件添加对应名称插槽
::: demo
table/columns-slot
::: 

## 自定义头部（列头部插槽）
1. 配置列属性customTitleRender来定义列头部的插槽名称
2. 然Table组件添加对应名称插槽
::: demo
table/header-slot
::: 

## 多级表头
配置列属性children定义多级表头，EFUI.EfUtil.genColumnsV2不支持，只能使用普通方式列配置
::: demo
table/multistage-header
::: 

## Loading
配置loading设置表格加载中状态，loading-text可以配置加载中文字
::: demo
table/loading
::: 

## Loading 插槽
配置name=loading插槽实现自定义Loading
::: demo
table/loading-slot
::: 

## nodata 插槽
配置name=nodata插槽实现自定义nodata
::: demo
table/nodata-slot
::: 

## EFUI.EfUtil.genColumnsV2
另外一种更友好得列配置方式（最终也是转换成普通配置方式,详见如下代码）
<br/>
genColumnsV2方法转换成普通配置方式步骤,假设传入参数名为columnsV2Map
1. 以columnsV2Map.titleMap为主数据遍历属性
2. 为第一步每一个属性对象，根据列配置项对应的map创建值（width->widthMap）
<br/>
tips: 只有titleMap是必传，其余列配置项map按需配置
```js

// 1. EFUI.EfUtil.genColumnsV2方式配置
const columnsV2Map = {
	titleMap: {
		employeeId: '工号'
	},
	customRowRenderMap: {
		employeeId: 'employeeIdContentSlot'
	},
	customTitleRenderMap: {
		employeeId: 'employeeIdHeaderSlot'
	},
	fixedMap: {
		employeeId: 'left'
	},
	widthMap: {
		employeeId: 120
	},
	sortMap:{
		employeeId:'asc'
	}
};
const columnsV2 = EFUI.EfUtil.genColumnsV2(columnsV2Map);

// 2. 普通方式配置
const columnsV1 = [{
	sort:"asc",
	customTitleRender:'employeeIdHeaderSlot',
	customRowRender:'employeeIdContentSlot',
	fixed:'left',
	width:120
	title:'工号'
}];

// 1 == 2
columnsV1 == columnsV2

```

## Table 配置项 
| 参数				| 说明								| 类型							| 可选值							| 默认值	|
|----------			|--------------						|----------						|--------------------------------	|--------	|
| dataSource		| 表格数据列表,必须					| Array							| —								| —		|
| columns			| 列配置，必须						| Array							| —								| —		|
| rowKey			| 数据项唯一标识，必须				| String						| —								| —		|
| selectedConfig	| 多选配置,详见SelectedConfig配置	| Object						| —								| —		|
| size				| 表格尺寸							| String						| small,middle					| —		|
| width				| 表格宽度,可以直接使用style属性控制	| String,Number					| —								| 100%		|
| height			| 表格高度,可以直接使用style属性控制	| String,Number					| —								| auto		|
| showHeader		| 是否展示头部						| Boolean						| —								| true		|
| fixedHeader		| 是否固定头部						| Boolean						| —								| false		|
| stripe			| 是否开启斑马纹						| Boolean						| —								| false		|
| bordered			| 是否开启边框						| Boolean						| —								| false		|
| loading			| 是否开启loading					| Boolean						| —								| false		|
| rowClassName		| 每行的class						| String，Function(row,index)	| —								| —		|
| headRowClassName	| 表头的class						| String						| —								| —		|

## SelectedConfig 配置项
| 参数			| 说明													| 类型		| 可选值							| 默认值	|
|----------		|--------------											|----------	|--------------------------------	|--------	|
| bindDataIndex	| checkbox 行唯一标识（一般同Table rowKey配置项）,必须	| String	| —								| false		|
| showSelectAll	| 是否开启全选											| Boolean	| —								| false		|
| width			| 多选列的宽度											| Number	| —								| —		|
| selectedValue	| 控制选中的值 ，支持双向绑定							| Array		| —								| —		|
| rowHighLighted| 是否行选中高亮										| Boolean	| —								| false		|
| rowClick		| 是否开启单击行选中									| Boolean	| —								| false		|

## Columns 配置项
| 参数				| 说明								| 类型		| 可选值							| 默认值	|
|----------			|--------------						|----------	|--------------------------------	|--------	|
| dataIndex			| 绑定数据项属性，必须	| String	| —								| —		|
| key				| 列唯一标识，必须			| String	| —								| —		|
| title				| 表头文字，必须					| String	| —								| —		|
| width				| 列宽				| String							|Number		| —								| —		|
| fixed				| 是否固定							| String	| left,right						| —		|
| customRowRender	| 列数据插槽名称					| String	| —								| —		|
| customTitleRender	| 列表头插槽名称						| String	| —								| —		|
| sort				| 列排序							| String	| unset,left,right					| —		|


## Table 事件
| 事件			| 说明			| 回调参数								|
|----------		|--------------	|----------								|
| rowDbclick	| 双击行事件	| (row)									|
| rowClick		| 单击行事件	| (row)									|
| sortChange	| 排序事件		| (sort,row)							|
| checkboxChange| 多选事件		| (selectedValue, checked, currentValue)|

## Table 方法
| 方法			| 说明															|
|----------		|--------------													|
| setupTableAttr| 重绘表格dom，防止固定列，表头有些场景出现行列未对齐	|

## Table 插槽
| 插槽		| 说明				|
|----------	|--------------		|
| loading	| 自定义loading样式	|
| nodata	| 自定义无数据样式	|

[[toc]]