# Dropdown 下拉菜单
Dropdown组件基于Popper组件开发，[Popper配置项](../feedback/popper.md#popper-配置项) 均可以使用<br/>

## 基本用法
1. 配置default插槽定义触发内容
2. 配置dropdown插槽定义下拉选项,ef-dropdown-item自定义每个选项（务必全部放在ef-dropdown-menu组件内）
::: demo 
dropdown/basic
::: 

## 可滚动
配置max-height属性，当选项高度超出后可滚动
::: demo 
dropdown/max-height
::: 

## 不同触发方式
配置trigger属性定义触发方式，hover：鼠标移入时，click：点击时，manual：手动
::: demo 
dropdown/trigger
::: 

## 带图标的下拉项
配置下拉项iconObj对象使用ef-icon,iconObj对象[Icon 配置项](./icon.md#icon-配置项)一致
::: demo 
dropdown/icon
::: 

## 禁用的下拉项
添加下拉项disabled属性
::: demo 
dropdown/disabled
::: 

## 带分割线的下拉项
添加下拉项divided属性
::: demo 
dropdown/divided
::: 

## 不同过渡动画
[请见Popper配置过渡动画](../feedback/popper.md#不同过渡动画)


## Dropdown 配置项 
以下只列出与多出Popper组件的配置，[Popper配置项](../feedback/popper.md#popper-配置项) 均可以使用
| 参数			| 说明				| 类型			| 可选值							| 默认值	|
|----------		|--------------		|----------		|--------------------------------	|--------	|
| item-height	| 下拉菜单项高度	| String，Number| -									| -			|
| max-height	| 下拉菜单最大高度	| String，Number| -									| -			|

## Dropdown Item 配置项 
| 参数		| 说明						| 类型					| 可选值							| 默认值	|
|----------	|--------------				|----------				|--------------------------------	|--------	|
| iconObj	| 图标配置（与ef-icon一致）	| Object				| -									| -			|
| command	| 下拉项命令				| Object, String, Number| -									| -			|
| disabled	| 是否禁用					| Boolean				| -									| false		|
| divided	| 分割线					| Boolean				| -									| false		|


## Dropdown 插槽
| 插槽		| 说明			|
|----------	|--------------	|
| dropdown	| 下拉内容		|
| default	| 触发内容		|

[[toc]]