# Button 按钮

## 基础用法
使用type=default来定义Button,或者不设置type
::: demo Basic Button
button/basic
::: 

## 不同类型的按钮
使用type来定义Button
::: demo Basic Button
button/type
::: 


## 带图标的按钮
使用icon属性添加图标，全部Icon请见[Icon组件](./icon.md)
::: demo Basic Button
button/icon
::: 

## 不同形状的按钮
使用shape属性定义不同形状按钮
::: demo Basic Button
button/shape
::: 

## 不同大小的按钮
使用size属性定义不同尺寸按钮,圆形按钮请手动设置按钮padding设置大小
::: demo Basic Button
button/size
::: 

## 加载中的按钮
使用loading属性定义加载中按钮
::: demo Basic Button
button/loading
::: 

## 禁用的按钮
使用disabled属性定义禁用按钮
::: demo Basic Button
button/disabled
::: 

## 自定义背景色的按钮
使用style属性设置背景色、边框、文字颜色
::: demo Basic Button
button/custom
::: 

## Button 配置项

| 参数			| 说明				| 类型			| 可选值							| 默认值	|
|----------		|--------------		|----------		|--------------------------------	|--------	|
| type			| 按钮类型			| String		| primary,danger					| default	|
| icon			| 按钮图标			| String		| 参见Icon图标组件					| -			|
| iconFontSize	| 按钮图标fontSize	| String,Number	| 对应Icon图标组件fontSize属性		| -			|
| shape			| 按钮形状			| String		| circle，round						| -			|
| loading		| loading			| Boolean		| -									| false		|
| size			| String			| middle，small	| -									| -			|

[[toc]]