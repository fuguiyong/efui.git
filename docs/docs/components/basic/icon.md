# Icon 图标
点击图标集合可复制图标类型

## 基础用法
设置type属性定义不同类型图标
::: demo Basic Button
icon/basic
:::
 
 ## Icon 配置项
| 参数		| 说明								| 类型			| 可选值							| 默认值	|
|----------	|--------------						|----------		|--------------------------------	|--------	|
| type		| 图标类型							| string		| —								| —		|
| rotate	| 旋转角度							| string，number| —								| —		|
| size		| 尺寸								| string		| middle,small,mini					| small		|
| color		| 颜色								| string		| —								| —		|
| fontSize	| icon fontSize,设置后size属性失效	| string，number| —								| —		|
| spin		| 旋转								| boolean		| —								| false		|

## 图标集合

<div class="view-container" style="	flex-wrap: wrap;padding:0">
	<div @click="onCopy(iconType)" style="width:25%;height:150px;border:1px solid #eee;box-sizing: border-box;" class="flex-column-center md-icon-wrapper" v-for="iconType of iconTypeList" :key="iconType">
	<div class="flex-column-center md-icon-item" style="width:100%">
		<div class="flex-column-center" style="width:100%">		
			<ef-icon class="md-icon" :type="iconType" fontSize="40"></ef-icon>
		</div>
		<div class="flex-column-center" style="width:100%">		
			<p class="" style="text-align:center;">{{iconType}}</p>
		</div>
	</div>
	</div>
</div>


[[toc]]



<script setup>
import {ref,onMounted} from "vue";
import { EfMessage as Message } from '@emmafgy/efui';

const iconTypeList = ref(['icon-prompt-fill', 'icon-manage-order-fill', 'icon-stop-fill', 'icon-multi-language-fill',
	  	'icon-column', 'icon-logistics-icon-fill', 'icon-add-account', 'icon-Newuserzone-fill', 'icon-column1',
	  	'icon-nightmode-fill', 'icon-add', 'icon-office-supplies-fill', 'icon-agriculture', 'icon-notice-fill',
	  	'icon-years', 'icon-mute', 'icon-add-cart', 'icon-order-fill', 'icon-arrow-right', 'icon-password1',
	  	'icon-arrow-left', 'icon-map1', 'icon-apparel', 'icon-paylater-fill', 'icon-all1', 'icon-phone-fill',
	  	'icon-arrow-up', 'icon-online-tracking-fill', 'icon-ascending', 'icon-play-fill1', 'icon-ashbin',
	  	'icon-pdf-fill', 'icon-atm', 'icon-phone1', 'icon-bad', 'icon-pin-fill', 'icon-attachent', 'icon-product-fill',
	  	'icon-browse', 'icon-rankinglist-fill', 'icon-beauty', 'icon-reduce-fill', 'icon-atm-away', 'icon-reeor-fill',
	  	'icon-assessed-badge', 'icon-pic-fill1', 'icon-auto1', 'icon-rankinglist', 'icon-bags', 'icon-product1',
	  	'icon-calendar', 'icon-prompt-fill1', 'icon-cart-full', 'icon-resonserate-fill', 'icon-calculator',
	  	'icon-remind-fill', 'icon-cameraswitching', 'icon-Rightbutton-fill', 'icon-cecurity-protection',
	  	'icon-RFQ-logo-fill', 'icon-category', 'icon-RFQ-word-fill', 'icon-close', 'icon-searchcart-fill',
	  	'icon-certified-supplier', 'icon-salescenter-fill', 'icon-cart-Empty', 'icon-save-fill', 'icon-code1',
	  	'icon-security-fill', 'icon-color', 'icon-Similarproducts-fill', 'icon-conditions', 'icon-signboard-fill',
	  	'icon-confirm', 'icon-service-fill', 'icon-company', 'icon-shuffling-banner-fill', 'icon-ali-clould',
	  	'icon-supplier-features-fill', 'icon-copy1', 'icon-store-fill', 'icon-credit-level', 'icon-smile-fill',
	  	'icon-coupons', 'icon-success-fill', 'icon-connections', 'icon-sound-filling-fill', 'icon-cry',
	  	'icon-sound-Mute1', 'icon-costoms-alearance', 'icon-suspended-fill', 'icon-clock', 'icon-tool-fill',
	  	'icon-CurrencyConverter', 'icon-task-management-fill', 'icon-cut', 'icon-unlock-fill', 'icon-data1',
	  	'icon-trust-fill', 'icon-Customermanagement', 'icon-vip-fill', 'icon-descending', 'icon-set1',
	  	'icon-double-arro-right', 'icon-Top-fill', 'icon-customization', 'icon-viewlarger1', 'icon-double-arrow-left',
	  	'icon-voice-fill', 'icon-discount', 'icon-warning-fill', 'icon-download', 'icon-warehouse-fill', 'icon-dollar1',
	  	'icon-zip-fill', 'icon-default-template', 'icon-trade-assurance-fill', 'icon-editor1', 'icon-vs-fill',
	  	'icon-eletrical', 'icon-video1', 'icon-electronics', 'icon-template-fill', 'icon-etrical-equipm',
	  	'icon-wallet1', 'icon-ellipsis', 'icon-training1', 'icon-email', 'icon-packing-labeling-fill', 'icon-falling',
	  	'icon-Exportservices-fill', 'icon-earth', 'icon-brand-fill', 'icon-filter', 'icon-collection', 'icon-furniture',
	  	'icon-consumption-fill', 'icon-folder', 'icon-collection-fill', 'icon-feeds', 'icon-brand', 'icon-history1',
	  	'icon-rejected-order-fill', 'icon-hardware', 'icon-homepage-ads-fill', 'icon-help', 'icon-homepage-ads',
	  	'icon-good', 'icon-scenes-fill', 'icon-Householdappliances', 'icon-scenes', 'icon-gift1',
	  	'icon-similar-product-fill', 'icon-form', 'icon-topraning-fill', 'icon-image-text', 'icon-consumption',
	  	'icon-hot', 'icon-topraning', 'icon-inspection', 'icon-gold-supplier', 'icon-leftbutton',
	  	'icon-messagecenter-fill', 'icon-jewelry', 'icon-quick', 'icon-ipad', 'icon-writing', 'icon-leftarrow',
	  	'icon-docjpge-fill', 'icon-integral1', 'icon-jpge-fill', 'icon-kitchen', 'icon-gifjpge-fill',
	  	'icon-inquiry-template', 'icon-bmpjpge-fill', 'icon-link', 'icon-tifjpge-fill', 'icon-libra',
	  	'icon-pngjpge-fill', 'icon-loading', 'icon-Hometextile', 'icon-listing-content', 'icon-home', 'icon-lights',
	  	'icon-sendinquiry-fill', 'icon-logistics-icon', 'icon-comments-fill', 'icon-messagecenter', 'icon-account-fill',
	  	'icon-mobile-phone', 'icon-feed-logo-fill', 'icon-manage-order', 'icon-feed-logo', 'icon-move',
	  	'icon-home-fill', 'icon-Moneymanagement', 'icon-add-select', 'icon-namecard', 'icon-sami-select', 'icon-map',
	  	'icon-camera', 'icon-Newuserzone', 'icon-arrow-down', 'icon-multi-language', 'icon-account', 'icon-office',
	  	'icon-comments', 'icon-notice', 'icon-cart-Empty1', 'icon-ontimeshipment', 'icon-favorites',
	  	'icon-office-supplies', 'icon-order', 'icon-password', 'icon-search', 'icon-Notvisible1',
	  	'icon-trade-assurance', 'icon-operation', 'icon-usercenter1', 'icon-packaging', 'icon-tradingdata',
	  	'icon-online-tracking', 'icon-microphone', 'icon-packing-labeling', 'icon-txt', 'icon-phone', 'icon-xlsx',
	  	'icon-pic1', 'icon-banzhengfuwu', 'icon-pin', 'icon-cangku', 'icon-play1', 'icon-daibancaishui',
	  	'icon-logistic-logo', 'icon-jizhuangxiang', 'icon-print', 'icon-jiaobiao', 'icon-product', 'icon-kehupandian',
	  	'icon-machinery', 'icon-dongtai', 'icon-process', 'icon-daikuan', 'icon-prompt', 'icon-shengyijing',
	  	'icon-QRcode1', 'icon-jiehui', 'icon-reeor', 'icon-fencengpeizhi', 'icon-reduce', 'icon-shenqingjilu',
	  	'icon-Non-staplefood', 'icon-shangchuanbeiandanzheng', 'icon-rejected-order', 'icon-shangchuan',
	  	'icon-resonserate', 'icon-kehuquanyi', 'icon-remind', 'icon-suoxiao', 'icon-responsetime', 'icon-quanyipeizhi',
	  	'icon-return', 'icon-shuangshen', 'icon-paylater', 'icon-tongguan', 'icon-rising1', 'icon-tuishui',
	  	'icon-Rightarrow', 'icon-tongguanshuju', 'icon-rmb1', 'icon-kuaidiwuliu', 'icon-RFQ-logo', 'icon-wuliuchanpin',
	  	'icon-save', 'icon-waihuishuju', 'icon-scanning', 'icon-xinxibar_shouji', 'icon-security',
	  	'icon-xinwaizongyewu', 'icon-salescenter', 'icon-wuliudingdan', 'icon-seleted', 'icon-zhongjianren',
	  	'icon-searchcart', 'icon-xinxibar_zhanghu', 'icon-raw', 'icon-yidatong', 'icon-service', 'icon-zhuanyequanwei',
	  	'icon-share', 'icon-zhanghucaozuo', 'icon-signboard', 'icon-xuanzhuandu', 'icon-shuffling-banner',
	  	'icon-tuishuirongzi', 'icon-Rightbutton', 'icon-AddProducts', 'icon-sorting', 'icon-ziyingyewu',
	  	'icon-sound-Mute', 'icon-addcell', 'icon-Similarproducts', 'icon-background-color', 'icon-sound-filling',
	  	'icon-cascades', 'icon-suggest', 'icon-beijing', 'icon-stop', 'icon-bold', 'icon-success', 'icon-zijin',
	  	'icon-supplier-features', 'icon-eraser', 'icon-switch', 'icon-centeralignment', 'icon-survey', 'icon-click',
	  	'icon-template', 'icon-aspjiesuan', 'icon-text', 'icon-flag', 'icon-suspended', 'icon-falg-fill',
	  	'icon-task-management', 'icon-Fee', 'icon-tool', 'icon-filling', 'icon-Top', 'icon-Foreigncurrency',
	  	'icon-smile', 'icon-guanliyuan', 'icon-textile-products', 'icon-language', 'icon-tradealert',
	  	'icon-leftalignment', 'icon-topsales', 'icon-extra-inquiries', 'icon-tradingvolume', 'icon-Italic',
	  	'icon-training', 'icon-pcm', 'icon-upload', 'icon-reducecell', 'icon-RFQ-word', 'icon-rightalignment',
	  	'icon-viewlarger', 'icon-pointerleft', 'icon-viewgallery', 'icon-subscript', 'icon-vehivles', 'icon-square',
	  	'icon-trust', 'icon-superscript', 'icon-warning', 'icon-tag-subscript', 'icon-warehouse', 'icon-danjuzhuanhuan',
	  	'icon-shoes', 'icon-Transfermoney', 'icon-video', 'icon-under-line', 'icon-viewlist', 'icon-xiakuangxian',
	  	'icon-set', 'icon-shouqi', 'icon-store', 'icon-zhankai', 'icon-tool-hardware', 'icon-tongxunlu', 'icon-vs',
	  	'icon-yiguanzhugongyingshang', 'icon-toy', 'icon-goumaipianhao', 'icon-sport', 'icon-Subscribe',
	  	'icon-creditcard', 'icon-becomeagoldsupplier', 'icon-contacts', 'icon-new', 'icon-checkstand', 'icon-free',
	  	'icon-aviation', 'icon-cad-fill', 'icon-Daytimemode', 'icon-robot', 'icon-infantmom', 'icon-inspection1',
	  	'icon-discounts', 'icon-block', 'icon-invoice', 'icon-shouhuoicon', 'icon-insurance', 'icon-nightmode',
	  	'icon-usercenter', 'icon-unlock', 'icon-vip', 'icon-wallet', 'icon-landtransportation', 'icon-voice',
	  	'icon-exchangerate', 'icon-contacts-fill', 'icon-add-account1', 'icon-years-fill', 'icon-add-cart-fill',
	  	'icon-add-fill', 'icon-all-fill1', 'icon-ashbin-fill', 'icon-calendar-fill', 'icon-bad-fill',
	  	'icon-bussiness-man-fill', 'icon-atm-fill', 'icon-cart-full-fill', 'icon-cart-Empty-fill',
	  	'icon-cameraswitching-fill', 'icon-atm-away-fill', 'icon-certified-supplier-fill', 'icon-calculator-fill',
	  	'icon-clock-fill', 'icon-ali-clould-fill', 'icon-color-fill', 'icon-coupons-fill',
	  	'icon-cecurity-protection-fill', 'icon-credit-level-fill', 'icon-auto', 'icon-default-template-fill',
	  	'icon-all', 'icon-CurrencyConverter-fill', 'icon-bussiness-man', 'icon-Customermanagement-fill',
	  	'icon-component', 'icon-discounts-fill', 'icon-code', 'icon-Daytimemode-fill', 'icon-copy', 'icon-exl-fill',
	  	'icon-dollar', 'icon-cry-fill', 'icon-history', 'icon-email-fill', 'icon-editor', 'icon-filter-fill',
	  	'icon-data', 'icon-folder-fill', 'icon-gift', 'icon-feeds-fill', 'icon-integral', 'icon-gold-supplie-fill',
	  	'icon-nav-list', 'icon-form-fill', 'icon-pic', 'icon-camera-fill', 'icon-Notvisible', 'icon-good-fill',
	  	'icon-play', 'icon-image-text-fill', 'icon-rising', 'icon-inspection-fill', 'icon-QRcode', 'icon-hot-fill',
	  	'icon-rmb', 'icon-company-fill', 'icon-similar-product', 'icon-discount-fill', 'icon-Exportservices',
	  	'icon-insurance-fill', 'icon-sendinquiry', 'icon-inquiry-template-fill', 'icon-all-fill',
	  	'icon-leftbutton-fill', 'icon-favorites-fill', 'icon-integral-fill1', 'icon-integral-fill', 'icon-help1',
	  	'icon-namecard-fill', 'icon-listing-content-fill', 'icon-pic-fill', 'icon-logistic-logo-fill', 'icon-play-fill',
	  	'icon-Moneymanagement-fill'
	  ]);
	  
	  // 复制到粘贴板
	 const copy = (text) => {
	  	var textareaEl = document.createElement('textarea');
	  	textareaEl.setAttribute('readonly', 'readonly'); // 防止手机上弹出软键盘
	  	textareaEl.value = text;
	  	document.body.appendChild(textareaEl);
	  	textareaEl.select();
	  	var res = document.execCommand('copy');
	  	document.body.removeChild(textareaEl);
	  	return res;
	  }
	  
	  const onCopy = (iconType)=>{
		copy(iconType);
		Message.success(`Icon复制成功：${iconType}`);
	}  
	
	onMounted(_=>{
		
			Message({
			  type: "success",
			  dangerouslyUseHTMLString: true,
			  message: '<strong>点击图标集合即可复制</strong>',
			  duration: 5000,
			  showClose: true
			});
		
	})


</script>

<style lang="scss">

.view-container{
	display: flex;
	align-items: center;
	background-color: #fafafa;
	border: 1px solid #eaeefb;
	padding: 24px;
}

.md-icon-wrapper{
	cursor: pointer;
	.md-icon-item{
		
		.md-icon{
			transition: .5s;
		}
		
		&:hover .md-icon{
			transform: scale(1.7);
		}
	}
}

</style>