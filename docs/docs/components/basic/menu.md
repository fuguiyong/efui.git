# Menu 菜单

## 基础用法
配置menuList（务必使用ref包裹成为响应式数据）定义基础菜单，配置项详情见文档最后 <br/>
Menu默认宽度100%，可直接使用style控制宽度
::: demo 
menu/basic
::: 

## 手风琴
配置collapse=true定义手风琴模式Menu
::: demo 
menu/accordion
::: 

## 可折叠
配置collapseVertical定义可折叠Menu
::: demo 
menu/collapse
::: 

## 使用方法选择菜单
通过组件暴露的selectMenu的方法来选择菜单项，入参为菜单项的key
::: demo 
menu/selectByApi
::: 


## Menu 配置项
| 参数					| 说明										| 类型			| 可选值							| 默认值	|
|----------				|--------------								|----------		|--------------------------------	|--------	|
| menuList				| 菜单列表，菜单项配置详见Menu Item 配置项	| Array			| -									| -			|
| collapse				| 开启手风琴								| Boolean		| true,false						| false		|
| collapseVertical		| 开启折叠菜单								| Boolean		| true,false						| false		|
| collapseTransitionName| 折叠动画名称								| String		| fade-in-linear，zoom-in			| zoom-in	|
| route					| 开启点击菜单项路由跳转					| Boolean		| true,false						| true		|
| liHeight				| 菜单项高度								| String，Number| -									| 50		|
| menuItemPaddingLeft	| 菜单项左边距								| String，Number| -									| 20		|

## Menu Item 配置项
| 参数				| 说明																| 类型		| 可选值							| 默认值	|
|----------			|--------------														|----------	|--------------------------------	|--------	|
| key				| 唯一标识															| String	| -									| -			|
| label				| 文字																| String	| -									| -			|
| icon				| 图标type															| String	| 参照Icon图标组件					| -			|
| customIconClass	| 自定义图标class,配置后icon配置失效								| String	| -									| -			|
| fullRoutePath		| 路由路径，点击触发Vue-Router跳转 （前置条件：Menu配置route=true）	| String	| -									| -			|
| children			| 子级，配置项同上													| Array		| -									| -			|


## Menu 事件
| 事件名称	| 说明			| 参数					|
|----------	|--------------	|----					|
| change	| 选择菜单项变化时触发	|(selectedKey,selectedMenuItem)|

## Menu 方法
| 方法名	| 说明			| 参数					|
|----------	|--------------	|----					|
| selectMenu| 选择菜单项	|(menuItemKey)|

[[toc]]