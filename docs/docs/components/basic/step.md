# Step 步骤条
这只是个非常简单的步骤条 :sweat_smile:
## 基础用法
配置steps来定义简单的步骤条
::: demo 
step/basic
::: 

## Step 配置项

| 参数			| 说明			| 类型		| 可选值							| 默认值	|
|----------		|--------------	|----------	|--------------------------------	|--------	|
| steps			| 步骤配置		| Array		| -									| -			|
| lineWidth		| 步骤线长度		| Number	| -									| 60		|
| circleWidth	| 圆形宽度		| Number	| -									| 40		|

## steps item  配置项
| 参数		| 说明			| 类型		| 可选值							| 默认值	|
|----------	|--------------	|----------	|--------------------------------	|--------	|
| title		| 步骤名称		| String	| -									| -			|


[[toc]]