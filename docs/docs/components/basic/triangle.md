# Triangle 三角形
这只是个由css绘制的非常简单的三角形 :sweat_smile:

## 基础用法
::: demo 
triangle/basic
::: 

## 不同宽度
::: demo 
triangle/width
::: 

## 不同颜色
::: demo 
triangle/color
::: 

## 可旋转
::: demo 
triangle/rotate
::: 

## Triangle 配置项

| 参数		| 说明			| 类型		| 可选值							| 默认值	|
|----------	|--------------	|----------	|--------------------------------	|--------	|
| width		| 宽度			| Number	| -									| -			|
| color		| 颜色			| String	| -									| -			|
| rotate	| 旋转角度		| Number	| -									| -			|

[[toc]]