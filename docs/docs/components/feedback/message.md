# Message 消息提示

## 基础用法
使用全局方法$message，默认消息类型是warn
::: demo 
message/basic
::: 

## 不同状态的消息
配置type属性来定义不同状态的消息，或者直接调用Message.[success,warn,error]
::: demo 
message/status
::: 

## 可关闭的消息
配置showClose=true属性定义可关闭的消息
::: demo 
message/closeable
::: 

## HTML作为消息体
配置dangerouslyUseHTMLString=true,message可为HTML
::: demo 
message/html
::: 

## VNode作为消息体
message配置项可为VNode，Tips：可使用Vue.resolveComponent根据组件名称获取全局注册的组件对象
::: demo 
message/vnode
::: 

## 全局引用
app.config.globalProperties 添加了全局方法 $message

## 单独引用
```js
import { EfMessage } from '@emmafgy/efui';
```
## 关闭所有消息
Message实例暴露了closeAll方法，可关闭当前所有消息实例
```js
import { EfMessage } from '@emmafgy/efui';
EfMessage.closeAll();
```

## Message 配置项
| 参数						| 说明					| 类型			| 可选值							| 默认值	|
|----------					|--------------			|----------		|--------------------------------	|--------	|
| type						| 消息类型				| string		| success,error,warn				| warn		|
| duration					| 消息关闭时间,单位毫秒	| Number		| -									| 3000		|
| showClose					| 是否开启关闭图标		| Boolean		| -									| false		|
| message					| 消息内容				| String,VNode	| -									| -			|
| dangerouslyUseHTMLString	| 是否开启HTML片段		| Boolean		| -									| false		|

## Message 方法
| 方法名      | 说明          | 
|---------- |-------------- |
| close | 关闭当前的 Message | 
| closeAll | 关闭当前所有的 Message |


[[toc]]