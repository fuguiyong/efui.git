# Tooltip 文字提示
Tooltip组件基于Popper组件开发，[Popper配置项](./popper.md#popper-配置项) 均可以使用<br/>
Tooltip组件只适合简单内容提示，过于复杂的场景建议使用[Popper组件](./popper.md)

## 基本用法
1. 配置default插槽定义触发内容
2. 配置content属性定义提示文字
3. 配置width属性定义宽度（可选）
::: demo 
tooltip/basic
::: 

## 不同主题
配置theme定义不同主题
::: demo 
tooltip/theme
::: 

## 内容插槽
添加content插槽自定义内容
::: demo 
tooltip/content-slot
::: 


## 不同方向
配置placement定义方向，[请见Popper配置方向](./popper.md#不同方向)
::: demo 
tooltip/placement
::: 

## 不同触发方式
[请见Popper配置触发方式](./popper.md#不同触发方式)

## 不同过渡动画
[请见Popper配置过渡动画](./popper.md#不同过渡动画)

## 自定义关闭
[请见Popper配置自定义关闭](./popper.md#自定义关闭)

## Tooltip 配置项 
以下只列出与多出Popper组件的配置，[Popper配置项](./popper.md#popper-配置项) 均可以使用
| 参数		| 说明			| 类型			| 可选值							| 默认值	|
|----------	|--------------	|----------		|--------------------------------	|--------	|
| content	| 内容文字		| String		| -									| -			|
| width		| 宽度			| String，Number| -									| -			|
| theme		| 主题			| String		| light,dark						| dark		|


## Tooltip 插槽
| 插槽		| 说明			|
|----------	|--------------	|
| content	| 提示内容		|

[[toc]]