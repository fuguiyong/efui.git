# Alert 消息提示
Alert只是原生alert方法的美化，过于复杂的提示建议使用[Modal 组件](./modal)

## 基础用法
使用全局方法$alert，或者使用EFUI.EfAlert方法，配置详见文档最后
::: demo 
alert/basic
::: 

## 不同状态
配置iconType定义不同状态Alert,可选值success,warn,error
::: demo 
alert/status
::: 

## 延迟关闭
确定按钮回调函数onConfim返回值为promise可延迟关闭,确认按钮默认是点击关闭，如不想点击关闭onConfirm请返回false
::: demo 
alert/promise
::: 



## Alert 配置项
| 参数				| 说明					| 类型		| 可选值							| 默认值	|
|----------			|--------------			|----------	|--------------------------------	|--------	|
| width				| 宽度,例：400px or 19%	| String	| -									| 400px		|
| showMark			| 是否展示蒙版			| Boolean	| -									| true		|
| showIcon			| 是否展示图标			| Boolean	| -									| true		|
| iconType			| 图标类型				| String	| success,warn,error				| error		|
| title				| 头部内容				| String	| -									| -			|
| ifBodyHtml		| body内容是否支持HTML	| String	| -									| false		|
| body				| 主要内容，支持HTML片段| String	| -									| -			|
| confirmText		| 确定按钮文字			| String	| -									| 确定		|
| cancelText		| 取消按钮文字			| String	| -									| 取消		|
| showCancelButton	| 是否显示取消按钮		| Boolean	| -									| false		|
| onCancel			| 确定按钮回调事件		| Funtion	| -									| -			|
| onConfirm			| 取消按钮回调事件		| Funtion	| -									| -			|

[[toc]]