# Popper 弹出
Popper组件是最基础弹出组件，date-picker、time-picker、select、tootip等需要弹出的组件都是基于Popper开发<br/>
所以Popper组件大部分配置适合基于它开发的组件

## 基础用法
1. 配置default插槽定义弹出内容
2. 配置trigger定义触发内容
::: demo 
popper/basic
::: 

## 不同触发方式
配置trigger定义不同触发方式,hover：鼠标移入时，click：点击时，manual：手动
::: demo 
popper/trigger
::: 

## 不同方向
配置placement定义不同方向，弹出内容在可视区域外，会自动计算合适方向，可选值如下：
			'top',
			'top-start',
			'top-end',
			'bottom',
			'bottom-start',
			'bottom-end',
			'left',
			'left-start',
			'left-end',
			'right',
			'right-start',
			'right-end'
::: demo 
popper/placement
::: 

## 不同主题
配置theme定义不同主题，light：白，dark：黑
::: demo 
popper/theme
::: 

## 不同过渡动画
配置transitionName定义不同过渡动画，可选值zoom-in，fade-in-linear
::: demo 
popper/transition
::: 

## 自定义关闭
控制v-model:visible实现自定义关闭
::: demo 
popper/custom-close
::: 

## Popper 配置项 
| 参数				| 说明							| 类型		| 可选值																															| 默认值		|
|----------			|--------------					|----------	|--------------------------------																									|--------		|
| visible			| 是否可见，支持v-model:visible	| Boolean	| -																																	| false			|
| trigger			| 触发方式						| String	| 'click', 'hover', 'manual'																										| hover			|
| placement			| 方向							| String	| 'top','top-start','top-end','bottom','bottom-start','bottom-end','left','left-start','left-end','right','right-start','right-end'	| top			|
| delayHideTimes	| 延迟关闭时间，单位：毫秒		| Number	| -																																	| 200			|
| transitionName	| 过渡动画						| String	| fade-in-linear，zoom-in																											| fade-in-linear|
| theme				| 主题							| String	| light,dark																														| light			|
| showArrow			| 是否展示箭头					| Boolean	|-																																	| true			|
| closeAllOnClick	| 是否关闭其他显示的Popper组件	| Boolean	| —																																| true			|
| interval			| 内容与trigger的间隔			| Number	| —																																| 5				|
| disabled			| 是否禁用						| Boolean	| —																																| false			|


## Popper 事件
| 事件			| 说明			| 回调参数	|
|----------		|--------------	|----------	|
| visibleChange	| visible change|(visible)	|



## Popper 插槽
| 插槽		| 说明			|
|----------	|--------------	|
| default	| 弹出内容		|
| trigger	| 触发内容		|


[[toc]]