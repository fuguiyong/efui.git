# Modal 模态框

## 基础用法
配置v-model:visible控制显示隐藏，配置title设置主题，默认插槽为内容<br/>
Tips:确认按钮回调事件(onConfirm)默认点击直接关闭，普通函数返回false取消关闭(Promise resolve(false))
::: demo 
modal/basic
::: 

## 自定义头部
配置header插槽自定义头部
::: demo 
modal/custom-header
::: 

## 自定义底部
配置footer插槽自定义头部
::: demo 
modal/custom-footer
::: 

## 延迟关闭
确认按钮回调事件(onConfirm)返回值类型为Promise，确定按钮进入Loading状态，直到Promise resolve 才会关闭模态框（resolve false 则不会关闭模态框）
::: demo 
modal/delay-close
::: 

## Modal 配置项

| 参数					| 说明														| 类型		| 可选值							| 默认值	|
|----------				|--------------												|----------	|--------------------------------	|--------	|
| visible				| 是否可见，支持v-model										| Boolean	| -									| -			|
| onCancel				| 取消按钮回调事件											| Funtion	| -									| -			|
| onConfirm				| 确认按钮回调事件（默认点击直接关闭，返回false取消关闭）		| Funtion	| -									| -			|
| width					| 宽度,例：520px or 33%										| String	| -									| 520px		|
| title					| 头部内容													| String	| -									| -			|
| confirmText			| 确定按钮文字												| String	| -									| 确定		|
| cancelText			| 取消按钮文字												| String	| -									| 取消		|
| showCancelButton		| 是否显示取消按钮											| Boolean	| -									| false		|
| showMark				| 是否展示蒙版												| Boolean	| -									| true		|
| showHeader			| 是否展示头部												| Boolean	| -									| true		|
| showFooter			| 是否展示底部												| Boolean	| -									| true		|
| showCloseIcon			| 是否展示关闭按钮											| Boolean	| -									| true		|
| ifShowConfirmLoading	| onConfirm返回值类型为Promise，确定按钮是否展示loading		| Boolean	| -									| true		|


## Modal 插槽

|插槽名	|说明				|
|---	|---				|
|header	|自定义头部插槽		|
|footer	|自定义底部插槽		|
|default|模态框主要内容插槽	|

[[toc]]