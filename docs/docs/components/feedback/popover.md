# Popover 弹出框
Popover组件基于Popper组件开发，[Popper配置项](./popper.md#popper-配置项) 均可以使用<br/>
Popover组件只适合展示简单主题和内容，过于复杂的场景建议使用[Popper组件](./popper.md)

## 基础用法
1. 配置trigger定义触发内容
2. 配置title、content属性定义主题和内容
3. 配置width设置宽度
::: demo 
popover/basic
::: 

## 不同主题
配置theme主题定义不同主题Popover
::: demo 
popover/theme
::: 

## 内容插槽
添加content插槽配置自定内容
::: demo 
popover/content-slot
::: 

## 不同触发方式
[请见Popper配置触发方式](./popper.md#不同触发方式)

## 不同方向
[请见Popper配置方向](./popper.md#不同方向)

## 不同过渡动画
[请见Popper配置过渡动画](./popper.md#不同过渡动画)

## 自定义关闭
[请见Popper配置自定义关闭](./popper.md#自定义关闭)

## Popper 配置项 
以下只列出与多出Popper组件的配置，[Popper配置项](./popper.md#popper-配置项) 均可以使用
| 参数		| 说明			| 类型			| 可选值							| 默认值	|
|----------	|--------------	|----------		|--------------------------------	|--------	|
| title		| 主题文字		| String		| -									| -			|
| content	| 内容文字		| String		| -									| -			|
| width		| 宽度			| String，Number| -									| -			|


## Popper 事件
| 事件			| 说明			| 回调参数	|
|----------		|--------------	|----------	|
| visibleChange	| visible change|(visible)	|


## Popper 插槽
| 插槽		| 说明			|
|----------	|--------------	|
| content	| 弹出内容		|
| trigger	| 触发内容		|

[[toc]]