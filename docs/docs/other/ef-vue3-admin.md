# Ef-Vue3-Admin
vue3中后台前端简单模板

## Gitee
[Vue-cli 版本](https://gitee.com/fuguiyong/vue3-admin-init) <br/>
[Vite 版本](https://gitee.com/fuguiyong/vite-vue3-admin.git)

## 预览地址
[Ef-Vue3-Admin](https://vue3-admin-init-1g65tkui043597e5-1253590140.ap-shanghai.app.tcloudbase.com)


## 主要dependencies说明
1. @emmafgy/efui:个人开发的vue3一些常用组件，[点我查看文档](https://efui-0g6cwnc29b464584-1253590140.ap-shanghai.app.tcloudbase.com/)
2. @emmafgy/util:个人收集的常用工具函数，[点我查看文档](https://emmafgy-static-web-1253590140.cos-website.ap-chengdu.myqcloud.com/)
3. element-plus:饿了吗，vue3组件库，[点我查看文档](https://element-plus.gitee.io/#/zh-CN/component/installation)

## src目录结构
```sh
├─api 			 # 接口
├─assets 		 # 资源
├─components	 # 组件
├─config 		 # 配置
├─directives 	 # 自定义指令
├─layouts 		 # 布局
├─lib 			 # UI库按需引入入口
├─model 		 # 模型
├─router 		 # 路由
├─store 		 # vuex
├─style 		 # 样式
├─utils  		 # 工具
└─views 		 # 视图
```

## 重要目录说明
1. src/lib: @emmafgy/efui 和 element-plus 两个主要依赖的入口，分别实现了按需引入和自定义主题
2. src/init.js: 为了保证src/main.js程序入口的整洁，应用初始化逻辑在这里
3. src/utils/index.js: 工具函数的入口，已经导入了@emmafgy/util依赖的所有工具函数，自定义工具函数直接加即可


## 重要功能说明
### 1. src/layouts/Home.vue的keep-alive组件缓存配置
demo的首页布局src/layouts/Home.vue使用keep-alive组件的include属性,配合vuex，vue-router实现了Home.vue子路由页面的动态缓存，具体配置说明如下：<br>
```js
1. 配置Home.vue子路由页面开启缓存，配置完毕进入该页面就会缓存该组件，销毁时机见步骤2
// src/router/home.js 新增子路由并设置meta.keepAlive = [{mutationName:"",componentName""}]即可实现缓存
// 以下是src/views/test/page2.vue缓存配置示例，具体文件：src/router/home.js
// src/views/test/page2.vue 页面的路由缓存配置
{
	path: 'page2',
	name: 'page2',
	component: () => import('../views/test/page2.vue'),
	meta: {
		title: "page2",
		keepAlive: [{
			mutationName: "addkeepAliveComponentsHome", // mutationName对应vuex的mutation，src/views/Home.vue 的 mutation已经配置好了，不需要改变
			componentName: "page2", // componentName对应需要缓存组件的name属性,不是路由配置的name属性
		}], // beforeEach钩子使用，对应kepp-alive组件的include属性
	}
}
2. 销毁缓存组件
	// 在缓存组件的BeforeRouteLeave设置取消缓存即可
	const store = useStore();
	onBeforeRouteLeave((to, from, next) => {
		const keepAliveFullPath = ["/home/page1"];
		if (!keepAliveFullPath.includes(to.fullPath)) {
			store.commit("removekeepAliveComponentsHome", "page2"); // 第二个参数 务必等于 该组件name
		}
		next();
	});
```

其他说明：<br>
```
如果想缓存的页面使用了router-view组件，并且设置了路由的redirect属性直接重定向到了子页面，
那么本来想缓存的页面就不会走router.beforeEach钩子函数了,因为直接被重定向到子页面了，
所以想要实现该页面的缓存就需要的配置子页面的meta.keepAlive里面新增该页面的缓存配置
```


## 其他说明
### 1. 侧边栏
侧边栏是由src/config/sidebar.js导出的sidebarMenuList对象配合ef-menu组件生成的
