---
home: true
navbar: true
heroImage: 
heroText: EFUI
tagline: A Vue3 Components Library
actions: 
  - text: 快速开始 →
    link: /docs/guide/introduce/introduce.md
    type: primary
features:
  - title: Vue3
    details: 一套Vue3 UI 组件库
  - title: Tree-Shaking
    details: 支持Tree-Shaking，组件可直接解构按需引入
  - title: Vite
    details: 使用vite搭建，极速的服务启动，轻量快速的热重载，优化的构建
footer:  联系方式 QQ：760720981 Email：760720981@qq.com
footerHtml: true
---

![imgs](/imgs/bg1.jpg)
