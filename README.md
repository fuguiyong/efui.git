# Vue3 UI Components
Vue3 UI Components 

## 说明文档地址
[点我查看](https://efui-0g6cwnc29b464584-1253590140.ap-shanghai.app.tcloudbase.com/)
使用vuepress2.x开发，/dcos目录下

## 组件开发说明
1. 组件开发主要目录在/package/components
2. 样式目录在/theme/src
3. 新增组件需要在/package/index 添加（单独导出组件,支持tree-shaking）
4. 新增组件需要在/package/default 添加（打包所有组件）
