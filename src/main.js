import {
	createApp
} from 'vue'
import App from './App.vue'
import '@/assets/iconfont/iconfont.css';
import '&/src/index.scss'
import {
	default as EFUI,
	EfPanelDatePick,
	EfPanelDateRangePick,
	EfPanelMonthRangePick
} from "#/index.js"

const app = createApp(App)

app.use(EFUI)
app.use(EfPanelDatePick)
app.use(EfPanelDateRangePick)
app.use(EfPanelMonthRangePick)
app.mount('#app')
