export const rules = {
	val1: {
		val1: [{
				required: true,
				message: '请输入姓名',
				trigger: 'blur'
			},
			{
				min: 3,
				max: 5,
				message: 'Length should be 3 to 5',
				trigger: 'blur'
			}
		]
	},
	val2: [{
			required: true,
			message: 'Please select Activity zone',
			trigger: 'change'
		},
		{
			min: 3,
			max: 5,
			message: 'Length should be 3 to 5',
			trigger: 'change'
		}
	],
	val3: [{
		required: true,
		message: 'Please select Activity zone',
		trigger: 'change'
	}],
	val4: [{
		required: true,
		message: 'Please select Activity zone',
		trigger: 'change'
	}],
	val5: [{
		required: true,
		message: 'Please select Activity zone',
		trigger: 'change'
	}],
	val6: [{
		required: true,
		message: 'Please select Activity zone',
		trigger: 'change'
	}],
	val7: [{
		required: true,
		message: '请选择数据源',
		trigger: 'change'
	}],
	val8: [{
		required: true,
		message: '请选择级联选择',
		trigger: 'change'
	}],
	val9: [{
		required: true,
		message: '请选择级联选择',
		trigger: 'change'
	}],
	val10: [{
		required: true,
		message: '请选择级联选择',
		trigger: 'change'
	}]
};

export const getRandomNumber = (min, max) => Math.round(Math.random() * (max - min) + min);

export const getData1 = num => {
	let dataSource = [];
	for (let i = 1; i <= num; i++) {
		dataSource.push({
			employeeId: 'sx00' + i,
			base2: getRandomNumber(100, 1000),
			base3: getRandomNumber(100, 1000),
			base4: getRandomNumber(100, 1000),
			base5: getRandomNumber(100, 1000),
			sxWage: getRandomNumber(100, 1000),
			wageTotal: getRandomNumber(100, 1000)
		});
	}
	return dataSource;
};

export const columnsV2Map = {
	titleMap: {
		order: '序号',
		employeeId: '工号',
		base2: '基本工资2',
		base3: '基本工资3',
		base4: '基本工资4',
		base5: '基本工资5',
		sxWage: '实习工资',
		wageTotal: '总工资',
		op: '操作',
		'': ''
	},
	customRowRenderMap: {
		order: 'order',
		employeeId: 'employeeId',
		op: 'op'
	},
	fixedMap: {
		order: 'left',
		employeeId: 'left'
	},
	widthMap: {
		order: 70,
		employeeId: 120,
		base2: 150,
		base3: 150,
		base4: 150,
		base5: 150,
		sxWage: 150,
		wageTotal: 150,
		op: 200
	}
};

export const columns = [{
		// dataIndex: 'employeeId',
		key: 'order',
		title: '序号',
		fixed: 'left',
		width: '150',
		customRowRender: 'order'
	},
	{
		dataIndex: 'employeeId',
		key: 'employeeId',
		title: '工号',
		width: '100',
		fixed: 'left',
		customRowRender: 'employeeId'
	},
	{
		dataIndex: 'base2',
		key: 'base2',
		width: '150',
		title: '基本工资2'
	},
	{
		dataIndex: 'base3',
		key: 'base3',
		width: '150',
		title: '基本工资3'
	},
	{
		dataIndex: 'base4',
		key: 'base4',
		width: '150',
		title: '基本工资4'
	},
	{
		dataIndex: 'base5',
		key: 'base5',
		width: '150',
		title: '基本工资5'
	},
	{
		dataIndex: 'sxWage',
		key: 'sxWage',
		width: '150',
		title: '实习工资'
	},
	{
		dataIndex: 'wageTotal',
		key: 'wageTotal',
		width: '150',
		title: '总工资'
	},
	{
		// dataIndex: 'employeeId',
		key: 'op',
		title: '操作',
		fixed: 'right',
		width: '250',
		customRowRender: 'op'
	}
];

export const menuList = [{
		key: '1',
		label: 'menu one',
		icon: 'icon-all-fill',
		fullRoutePath: '/',
		// poppervisible:false,
		children: [{
				key: '12',
				label: 'menu one'
				// icon: "icon-all-fill",
				// customIconClass:"iconfont icon-menu",
			},
			{
				key: '13',
				label: 'menu two'
				// icon: "icon-all-fill",
				// customIconClass:"iconfont icon-menu",
			}
		],
		customIconClass: "iconfont icon-a-aixinxihuanxiaishoucang",
	},
	{
		key: '2',
		label: 'menu two',
		icon: 'icon-scenes-fill'
		// customIconClass:"iconfont icon-menu",
	},
	{
		key: '4',
		label: 'menu three',
		icon: 'icon-account-fill',
		// showSubMenu: true,
		children: [{
				key: '5',
				label: 'menu one'
				// icon: "icon-all-fill",
				// customIconClass:"iconfont icon-menu",
			},
			{
				key: '99',
				label: 'menu nine 9',
				// icon: "icon-all-fill",
				children: [{
						key: '109',
						label: 'menu one 9',
						// icon: "icon-all-fill",
						// customIconClass:"iconfont icon-menu",
						children: [{
								key: '10911',
								label: 'menu one 911'
								// icon: "icon-all-fill",
								// customIconClass:"iconfont icon-menu",
							},
							{
								key: '11922',
								label: 'menu two 922'
								// icon: "icon-all-fill",
								// customIconClass:"iconfont icon-menu",
							}
						]
					},
					{
						key: '119',
						label: 'menu two 9'
						// icon: "icon-all-fill",
						// customIconClass:"iconfont icon-menu",
					}
				]
			},
			{
				key: '6',
				label: 'menu two'
				// icon: "icon-all-fill",
				// customIconClass:"iconfont icon-menu",
			},
			{
				key: '9',
				label: 'menu nine',
				// icon: "icon-all-fill",
				children: [{
						key: '10',
						label: 'menu one'
						// icon: "icon-all-fill",
						// customIconClass:"iconfont icon-menu",
					},
					{
						key: '11',
						label: 'menu two'
						// icon: "icon-all-fill",
						// customIconClass:"iconfont icon-menu",
					}
				]
			}
		]
	},
	{
		key: '8',
		label: 'menu twomenu twomenu',
		icon: 'icon-set1',
		children: [{
				key: '18',
				label: 'menu one'
				// icon: "icon-all-fill",
				// customIconClass:"iconfont icon-menu",
			},
			{
				key: '19',
				label: 'menu two'
				// icon: "icon-all-fill",
				// customIconClass:"iconfont icon-menu",
			}
		]
		// customIconClass:"iconfont icon-menu",
	},
	{
		key: '7',
		label: 'menu two',
		icon: 'icon-video1'
		// customIconClass:"iconfont icon-menu",
	}
];


export const stepsList = [{
		title: '数据定义'
	},
	{
		title: '基本信息定义'
	},
	{
		title: '请求信息定义'
	},
	{
		title: '返回信息定义'
	},
	{
		title: '预览发布'
	}
];

export const checkboxGroupOptionList = [{
		value: 'aaa',
		label: 'check-a'
	},
	{
		value: 'bbb',
		label: 'check-b'
	},
	{
		value: 'ccc',
		label: 'check-c'
	}
];

export const radioGroupOptionList = [{
		value: '1',
		label: 'OPTION1'
	},
	{
		value: '2',
		label: 'OPTION2'
	},
	{
		value: '3',
		label: 'OPTION3'
	}
];


export const cascaderOptions = [{
		value: 'Fu',
		label: 'Fu',
		lazyLoading: false
	},
	{
		value: 'guide',
		label: 'Guide',
		children: [{
				value: 'disciplines',
				label: 'Disciplines',
				children: [{
						value: 'consistency',
						label: 'Consistency',
					},
					{
						value: 'feedback',
						label: 'Feedback',
					},
					{
						value: 'efficiency',
						label: 'Efficiency',
					},
					{
						value: 'controllability',
						label: 'Controllability',
					},
				],
			},
			{
				value: 'navigation',
				label: 'Navigation',
				children: [{
						value: 'side nav',
						label: 'Side Navigation',
					},
					{
						value: 'top nav',
						label: 'Top Navigation',
					},
				],
			},
		],
	},
	{
		value: 'component',
		label: 'Component',
		children: [{
				value: 'basic',
				label: 'Basic',
				children: [{
						value: 'layout',
						label: 'Layout',
					},
					{
						value: 'color',
						label: 'Color',
					},
					{
						value: 'typography',
						label: 'Typography',
					},
					{
						value: 'icon',
						label: 'Icon',
					},
					{
						value: 'button',
						label: 'Button',
					},
				],
			},
			{
				value: 'form',
				label: 'Form',
				children: [{
						value: 'radio',
						label: 'Radio',
					},
					{
						value: 'checkbox',
						label: 'Checkbox',
					},
					{
						value: 'input',
						label: 'Input',
					},
					{
						value: 'input-number',
						label: 'InputNumber',
					},
					{
						value: 'select',
						label: 'Select',
					},
					{
						value: 'cascader',
						label: 'Cascader',
					},
					{
						value: 'switch',
						label: 'Switch',
					},
					{
						value: 'slider',
						label: 'Slider',
					},
					{
						value: 'time-picker',
						label: 'TimePicker',
					},
					{
						value: 'date-picker',
						label: 'DatePicker',
					},
					{
						value: 'datetime-picker',
						label: 'DateTimePicker',
					},
					{
						value: 'upload',
						label: 'Upload',
					},
					{
						value: 'rate',
						label: 'Rate',
					},
					{
						value: 'form',
						label: 'Form',
					},
				],
			},
			{
				value: 'data',
				label: 'Data',
				children: [{
						value: 'table',
						label: 'Table',
					},
					{
						value: 'tag',
						label: 'Tag',
					},
					{
						value: 'progress',
						label: 'Progress',
					},
					{
						value: 'tree',
						label: 'Tree',
					},
					{
						value: 'pagination',
						label: 'Pagination',
					},
					{
						value: 'badge',
						label: 'Badge',
					},
				],
			},
			{
				value: 'notice',
				label: 'Notice',
				children: [{
						value: 'alert',
						label: 'Alert',
					},
					{
						value: 'loading',
						label: 'Loading',
					},
					{
						value: 'message',
						label: 'Message',
					},
					{
						value: 'message-box',
						label: 'MessageBox',
					},
					{
						value: 'notification',
						label: 'Notification',
					},
				],
			},
			{
				value: 'navigation',
				label: 'Navigation',
				children: [{
						value: 'menu',
						label: 'Menu',
					},
					{
						value: 'tabs',
						label: 'Tabs',
					},
					{
						value: 'breadcrumb',
						label: 'Breadcrumb',
					},
					{
						value: 'dropdown',
						label: 'Dropdown',
					},
					{
						value: 'steps',
						label: 'Steps',
					},
				],
			},
			{
				value: 'others',
				label: 'Others',
				children: [{
						value: 'dialog',
						label: 'Dialog',
						disabled: true,
					},
					{
						value: 'tooltip',
						label: 'Tooltip',
					},
					{
						value: 'popover',
						label: 'Popover',
					},
					{
						value: 'card',
						label: 'Card',
					},
					{
						value: 'carousel',
						label: 'Carousel',
					},
					{
						value: 'collapse',
						label: 'Collapse',
					},
				],
			},
		],
	},
	{
		value: 'resource',
		label: 'Resource',
		disabled: true,
		children: [{
				value: 'axure',
				label: 'Axure Components',
			},
			{
				value: 'sketch',
				label: 'Sketch Templates',
			},
			{
				value: 'docs',
				label: 'Design Documentation',
			},
		],
	},
]
